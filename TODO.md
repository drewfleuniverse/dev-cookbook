# To Do

**Top**

- CSS layout - 3 ways horizontal centering
- CSS layout - 3 ways vertical centering
- CSS layout - 3 ways middle centering
- UI state CRUD API - React
- UI state CRUD API - React/Redux
- UI state CRUD API - React/Redux/Thunk
- UI state CRUD API - VanillaJS
- User login frontend - VanillaJS/Jest+Enzyme/Selenium/Security
- User login frontend - React/Jest+Enzyme/Selenium/Security
- User login backend - Express.js+Jest+JWT/Mongoose/Security/DockerCompose
- User login backend - Express.js+Jest+JWT/Sequelize/Security/DockerCompose
- User login - HTML5/Bootstrap4
- User login layout - HTML5/CSS3/BEM+CssGrip+Flex
- User login layout - HTML5/CSS3/BEM+Traditional
- User login layout - HTML5/SCSS/Webpack
- User login layout - HTML5/CSS-Next/Webpack
- User login accessibility
- User cards frontend - VanillaJS/Jest+Enzyme/Selenium/Security
- User cards frontend - React/Jest+Enzyme/Selenium/Security
- User cards backend - Express.js+Jest/Mongoose/Security/DockerCompose
- User cards backend - Express.js+Jest/Sequelize/Security/DockerCompose
- User cards - HTML5/Bootstrap4
- User cards layout - HTML5/CSS3/BEM+CssGrip+Flex
- User cards layout - HTML5/CSS3/BEM+Traditional
- User cards layout - HTML5/SCSS/Webpack
- User cards layout - HTML5/CSS-Next/Webpack
- User cards accessibility
- Node debugger
- Web perf - caching
- DFS/BFS code
- DFS/BFS DOM
- JS ES6 - Generator/Async/Await
- JS ES6 - inheritance
- JS ES5 - inheritance
- JS ES5 - valueOf vs ==
- JS event queue - Browser
- JS event queue - NodeJS
- Utils - `getProps(defaultProps, optionalProps)`

**Coding**

- `arr.map(Func)`, e.g. `arr.map(Number)` returns converted number
- js strict mode
- architecture of node js
- What is a JavaScript callback function?
- When it is a good time not use "use strict" in JavaScript!

**Databases**

**DSA**

**Testing**

**Web**

- css/sass
- Security: XSS, CSRF, and session storage safety fro cookies and web storage(local storage and session storage)
- CORS
- Given a DIV box of x width and y height in a browser how do you position four smaller images in each corner of the box?
- HTTP request: What happens when you click on the URL.
- DNS

**Coding**

**Databases**

**Problems - DSA**

- Coding test on hackerrank. given a matrix with 0 and 1. find paths of 1 from one corner of matrix to another
- DFS from any node == 1, mark them as any letter like "P", time complexity would be exponential
- If you go to amazon.com website, you'll see that there is a section which shows you the top selling items in amazon. Write an algorithm that mimics the same. For example, you have 10 items and each has number of sales increasing randomly per second. Use the right data structure and algorithm to host the top selling item.
- Write a function that spits out the Fibonacci sequence for any number of digits.
- Reverse the sequence of words in a string. Words are separated by spaces (any number) and we have to preserve the number of spaces, in order.
- Given two linked list having integer values at them, <9, I had to obtain the result of adding them.
- make a pyramid of an array of numbers using for loops.
- Given an array with only integers between 1 and 100, having only 99 elements, without any duplicates. Write a method that returns the missing number.
- Send a Tree across a Wire, i.e. an K-Ary Tree is to be transmitted across a network, and the same has to be built on the other side.
- Implement double linked list to stream data
- Write a search feature for a large dataset that required some cleanup
- Reverse linked list
- Implement the LRU
- The first question is given binary tree. populate the next node for each tree node. the node has {val, left, right, next} properties. similar to Populating Next Right Pointers in Each Node II in leetcode.
- Given a string which contains only lowercase letters, remove 3 or more alike letters in sequence so that even after removing there are no 3 or more repeating letters in sequence. Repeat this process as many times as possible.
- Write an algorithm on hackerrand to add two linked list
- Given an array containing non-negative integers, and each value of the array indicating a height of the container. Find the total volume of water that can be stored in this container
- Make a function to determine whether two numbers in an array add up to a target number
- substring anagram
  Given an array of integers that consists of two parts, one part descending and one part ascending, sort the array from least to greatest.
- Check if matrix is word square (check [i][j] === [j][i])
- How do you make a function that only calls input function f every 50 milliseconds?
- DFS on HTML nodes
- check if two linked lists intersect.

**Problems - Testing**

**Problems - Web**

- `problems-web/async-restful/fetch-github-api/`: fetch.js, XMLHttpRequest.js
- What's the difference between 400 and 500 error codes on an HTTP request?
- How does SASS work?
- What is REST?
- Design web application functionality for a table with checkboxes. Talk about how you would implement some features for it and show code.
- Build a file uploader.
- Implement Sum(3)(4)(5)=12 with javascript
- filter table using vanilla js
- Asynchronous & Synchronous web pages and functionalities onto web pages. Give examples.
- My algorithm questions were presented as DOM traversals. Using CS concepts here is the key. Casually refer to an array as a stack or queue depending on how you're using it, mention why you're using breadth first instead of depth, etc.
- How would you make this markup more accessible for a visually impaired person? Hearing impaired? How about a person on a slow network?
- Write code on the whiteboard that could take user input and determine if it is a palindrome.
- Take two arrays and compare them to find duplicates. Only list each duplicate once.
- What is the importance of CSS selectors? Where do you use them?
- Write a tool that will display a todo list that is pulled from a database. When an item is checked off the list, have some action happen to indicate that item is done.
- Make an accordion where when clicked, text expands from it and when another item is clicked, the first one collapses and the second one expands.
- How to get the nearest ancestor of one DOM element
- Optimize page loading
- JavaScript queue, heap, and stack usage
- React HOC patterns and state sharing
- Here is a JSON response and the desired output format. How would you render it in a DOM?
- Here is a purchase button. How would you structure the HTML and CSS? Now we are adding n variants of the button, how would you handle this?
- How would you build a sharable form validation component? Show the HTML and JS.
- How would you implement document.querySelectorAll manually using recursion?
- In one hour, implement a web service that can read a file greater than 10GB and store all the distinct words in it and the number of times they occur and do it in O(N) or less complexity. Then write all the code on the client (browser) side to allow anyone to search for all words beginning with what the user inputs, uses AJAX to pull the results into the page, and deals with a lot of results with pagination or infinite scrolling of some type.
-

## Interviewers

- [agenda/agendash](https://github.com/agenda/agendash)

## Usage

**Install and update node**

- `nvm install 8.9.0 --reinstall-packages-from=5.7.1`

**Kill process listening specific port**

- `sudo lsof -i tcp:<port_number>`
- `kill -9 <pid>`

```js
function killport() { lsof -i TCP:$1 | grep LISTEN | awk '{print $2}' | xargs kill -9 }
```
