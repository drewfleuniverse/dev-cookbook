/*
Terms
- Type parameter: interface Base<T> {...}
- Type argument: Base<String> varName = ...

Further reading:
- [Difference between <? super T> and <? extends T> in Java](http://stackoverflow.com/questions/4343202/difference-between-super-t-and-extends-t-in-java)
*/

interface Base<T> {
  void set(T t);
  T get();
}

class TryGeneric<T> implements Base<T> {
  private T myVar;
  public void set(T t) {
    myVar = t;
  }
  public T get() {
    return myVar;
  }
  @SuppressWarnings("unchecked")
  public T[] genericArray(int size) {
    return (T[])new Object[size];
  }
}

public class GenericDemo {
  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    /*
    Type parameters in generics have to be class type:
    */
    // when initializing generic classes, T has to be non-primitive types
    // TryGeneric<int> tryThis = new TryGeneric<>();
    Base<Integer> tryThis = new TryGeneric<>();
    Integer I = 100;
    int i = 200;
    tryThis.set(I);
    System.out.println(tryThis.get());
    // although `tryThis` only accepts Object/non-primitive types,
    // when assigning certain primitive types, autoboxing converts primitive
    // types to its object counterpart.
    tryThis.set(i);
    System.out.println(tryThis.get());

    /*
    Always use class types with generics, or convert primitive types to class
    types
    */
    Base<String> tryThat = new TryGeneric<>();
    String S = "this is a string";
    char c = 'c';
    tryThat.set(S);
    System.out.println(tryThat.get());
    // char cannot be autoboxed to String
    // tryThat.set(c);
    tryThat.set(Character.toString(c));
    System.out.println(tryThat.get());

    /*
    Generics eliminate the need of casting
    */
    Base rawType = new TryGeneric();
    rawType.set("hey");
    // error: incompatible types
    // String s = rawType.get();
    String s = (String)rawType.get();

    Base<String> hasType = new TryGeneric<>();
    hasType.set("hey");
    String s2 = hasType.get();

  }
}
