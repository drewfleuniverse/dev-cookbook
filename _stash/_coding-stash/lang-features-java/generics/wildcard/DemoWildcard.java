import java.util.List;
import java.util.ArrayList;

class A {
  private String value;
  public A(String s) { value = s; }
  public String getValue() { return value; }
}

class B extends A {
  public B(String s) { super(s); }
}

class C extends A {
  public C(String s) { super(s); }
}

public class DemoWildcard {
  public static void main(String[] args) {
    /**
     * Verifying inheritance properties
     */
    A a = new A("aA");
    B b = new B("bB");
    System.out.println(a.getValue()); // aA
    System.out.println(b.getValue()); // bA
    A _a = b;
    System.out.println(_a.getValue()); // bB

    /**
     * `listA = listB` is illegal:
     * - Can't guarantee listA may write element instances of type A or C to
     *   listB
     * - error: incompatible types: List<B> cannot be converted to List<A>
     *
     * `listB = listA` is illegal:
     * - Can't gurantee listB may read element instances of type A or C from
     *   listA:
     * - error: incompatible types: List<A> cannot be converted to List<B>
     */
    List<A> listA = new ArrayList<A>();
    List<B> listB = new ArrayList<B>();
    // listA = listB;
    // listB = listA;

    /**
     * `<?>` poses no restrictions on element type
     */
    List<?> listAnyA = new ArrayList<A>();
    List<?> listAnyB = new ArrayList<B>();
    listAnyA = listAnyB;
    listAnyB = listAnyA;

    /**
     * `listExtendsB = listA` is illegal:
     * - `<? extends B>` restricts to B and subclass types of B
     * - error: incompatible types: List<A> cannot be converted to
     *   List<? extends B>
     */
    List<? extends A> listExtendsA = new ArrayList<A>();
    List<? extends B> listExtendsB = new ArrayList<B>();
    listExtendsA = listB;
    // listExtendsB = listA;

    /**
     * `listSuperA = listB` is illegal:
     * - `<? super A>` restricts to A and superclass types of A
     * - error: incompatible types: List<B> cannot be converted to
     *   List<? super A>
     */
    List<? super A> listSuperA = new ArrayList<A>();
    List<? super B> listSuperB = new ArrayList<B>();
    //listSuperA = listB;
    listSuperB = listA;
  }
}
