/*
Interface
- Methods are implicitly abstract and public.
*/

interface Borough {
  void setName(String name);
  String getName();
}

interface NewBorough extends Borough {
  String doSomething();
}

class LondonBorough implements NewBorough, JustAMarker {
  private String name;

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public String doSomething() {
    return "Do this and do that";
  }
}

public class InterfaceDemo {
  public static void main(String[] args) {
    LondonBorough londonBorough = new LondonBorough();

    londonBorough.setName("Southwark");

    System.out.println(londonBorough.getName());
    System.out.println(londonBorough.doSomething());

    Borough var1 = londonBorough;
    System.out.println(var1.getName());
    // `Borough` doesn't have method `doSomething`
    // System.out.println(var1.doSomething());

    NewBorough var2 = londonBorough;
    System.out.println(var2.getName());
    System.out.println(var2.doSomething());

    if (londonBorough instanceof JustAMarker) {
      System.out.println("londonBorough has marker interface JustAMarker");
    }
  }
}
