import java.lang.Math;

class MathFuncDemo {
  public static void main(String[] args) {
    //System.out.println(Math);
    /*
    double abs(double)
    float abs(float)
    int abs(int)
    long abs(long)
    */
    System.out.println(Math.abs(-100));
    /*
    double cos(double)
    double sin(double)
    */
    System.out.println(Math.cos(0.3) * Math.cos(0.3)
      + Math.sin(0.3) * Math.sin(0.3));
    /*
    double min(double, double)
    float min(float, float)
    int min(int, int)
    long min(long, long)
    double max(double, double)
    float max(float, float)
    int max(int, int)
    long max(long, long)
    */
    System.out.println(Math.min(100, -100));
    System.out.println(Math.max(100, -100));
    /*
    double floor(double)
    double ceil(double)
    */
    System.out.println(Math.floor(1.5));
    System.out.println(Math.ceil(1.5));
    System.out.println(Math.floor(-1.5));
    System.out.println(Math.ceil(-1.5));
    /*
    long round(double)
    int round(float)
    */
    System.out.println(Math.round(1.5));
    System.out.println(Math.round(1.4));
    /*
    double pow(double, double)
    double exp(double)
    double log(double): base e
    double log10(double): base 10
    N.b.: Java doesn't have `log2`
    */
    System.out.println(Math.pow(2, 3));
    System.out.println(Math.exp(1));
    System.out.println(Math.log(2));
    System.out.println(Math.log10(2));
    /*
    `random` returns a double value between [0.0, 1.0)
    double random()
    */
    System.out.println(Math.random());
  }
}
