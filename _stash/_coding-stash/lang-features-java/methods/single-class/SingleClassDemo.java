class SingleClassDemo {
  void foo() {
    System.out.println("foo");
  }

  static void bar() {
    System.out.println("bar");
  }

  public static void main(String args[]) {
    /*
    When calling a non-static method within the class, javac yields error
    message:
      "non-static method foo() cannot be referenced from a static context"
    The reason is non-static methods cannot be called within a class itself,
    i.e. due to the class is uninitialized.
    */
    // foo();
    bar();
  }
}
