## Java Documentation

```
public class ArrayList<E>
extends AbstractList<E>
implements List<E>, RandomAccess, Cloneable, Serializable
```
In the above code snippet, `ArrayList` has all of the methods of `AbstractList`,
and it may or may not have access to every methods declared in `List` unless:
```
List<E> alist = ArrayList<>();
```
So `alist` is now accessible to List's methods. (not sure is all methods of
`List` needed to be implemented in `ArrayList`, but from the perspective of
memory alignment, the declared type should be `List`)


```
abstract class X implements Y {
  // implements all but one method of Y
}

class XX extends X {
  // implements the remaining method in Y
}
```

## Common Mistakes

### Remainder

```java
int i = 0%42; // `i` is `0`
int j = 42%0; // Outputs `java.lang.ArithmeticException: / by zero`
```

### Operators

```java
int i = 2 ^ 1; // `i` is 3.
// Java doesn't have a power operator. `^` is XOR operator.
// The entire operation is as below:
// 2 ^ 1
// 00000010 ^ 00000001, cast to binary
// 00000011, xor result
// 3, cast back to int
```

### java.io.PrintStream

Escape `%` in printf: `System.out.printf("%3d" + "%%", 100)`

`char c = '';`` yields error: empty character literal
