import java.util.Random;

public class RandomDemo {
  /*
  Returns a random integer, among 2^32 possible values, between [-(2^31), (2^31)-1],
  i.e. [-2147483648, 2147483647]
  */
  private static int getRandomDefaultInt() {
    Random r = new Random();
    return r.nextInt();
  }

  /*
  Returns a random float, among 2^24 possible values, between [0.0, 1.0)
  */
  private static float getRandomDefaultFloat() {
    Random r = new Random();
    return r.nextFloat();
  }

  /*
  Returns a random integer between [min, max)
  */
  private static int getRandomIntMaxExclusive(int min, int max) {
    Random r = new Random();
    // when `nextInt` invoked with an integer `n`, which yield a value between
    // [0, n)
    return r.nextInt(max - min) + min;
  }

  /*
  Returns a random integer between [min, max]
  */
  private static int getRandomIntMaxInclusive(int min, int max) {
    Random r = new Random();
    return r.nextInt(max - min + 1) + min;
  }

  /*
  Returns a random float between [min, max)
  */
  private static float getRandomFloatMaxExclusive(float min, float max) {
    Random r = new Random();
    return r.nextFloat() * (max - min) + min;
  }

  public static void main(String args[]) {
    System.out.println("getRandomDefaultInt:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomDefaultInt());
    }
    System.out.println();

    System.out.println("getRandomDefaultFloat:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomDefaultFloat());
    }
    System.out.println();

    System.out.println("getRandomIntMaxExclusive:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomIntMaxExclusive(-10, 10));
    }
    System.out.println();

    System.out.println("getRandomIntMaxInclusive:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomIntMaxInclusive(-10, 10));
    }
    System.out.println();

    System.out.println("getRandomFloatMaxExclusive:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomFloatMaxExclusive(-10.0f, 10.0f));
    }
    System.out.println();


  }
}
