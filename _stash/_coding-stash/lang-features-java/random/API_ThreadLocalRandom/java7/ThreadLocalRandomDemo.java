import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

/*
In Java7, `ThreadLocalRandom` inherited the following methods from Random:
nextBoolean(),
nextBytes(byte[] bytes),
nextDouble(),
nextFloat(),
nextGaussian(),
nextInt(),
nextInt(int n),
nextLong()

And `ThreadLocalRandom` has its own methods accessible from its static method
`current()`:
next(int bits)
nextDouble(double n)
nextDouble(double least, double bound)
nextInt(int least, int bound)
nextLong(long n)
nextLong(long least, long bound)
setSeed(long seed)
*/
public class ThreadLocalRandomDemo {
  /*
  Returns a random integer between [min, max)
  */
  private static int getRandomIntMaxExclusive(int min, int max) {
    return ThreadLocalRandom.current().nextInt(min, max);
  }
  /*
  Returns a random double between [min, max)
  */
  private static double getRandomDoubleMaxExclusive(double min, double max) {
    return ThreadLocalRandom.current().nextDouble(min, max);
  }

  public static void main(String args[]) {
    System.out.println("getRandomIntMaxExclusive:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomIntMaxExclusive(-10, 10));
    }
    System.out.println();

    System.out.println("getRandomDoubleMaxExclusive:");
    for (int i = 0; i < 10; i++) {
      System.out.println(getRandomDoubleMaxExclusive(-10.0, 10.0));
    }
    System.out.println();
  }
}
