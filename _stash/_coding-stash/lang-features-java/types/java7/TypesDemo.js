/*
- [How do I find the max and min values of primitive types?](http://www.avajava.com/tutorials/lessons/how-do-i-find-the-max-and-min-values-of-primitive-types.html)
- [Primitive Data Types](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)

##Primitive data type sizes

- byte, 8-bit: [-(2^7), (2^7)-1], [-128, 127]
  - n.b. A byte looks like 11111111. A bit is either 0 or 1. A byte contains 8
    bits. And it has size of 2^8, 256 kinds of values.
- int, 32-bit:
  - signed: [-(2^31), (2^31)-1], [-2,147,483,648, 2,147,483,647]
  - unsigned, java8 and later: [0, (2^32)-1], [0, 4,294,967,295]
- long, 64-bit:
  - signed: [-(2^63), (2^63)-1],
    [-9,223,372,036,854,775,808, 9,223,372,036,854,775,807]
  - unsigned, java8 and later: [0, (2^64)-1]
*/



public class TypesDemo {
  public static void main(String args[]) {
    int i = Integer.MIN_VALUE;
    int I = Integer.MAX_VALUE;
    System.out.println(i);
    System.out.println(I);
    float f = Integer.MIN_VALUE;
    float F = Integer.MAX_VALUE;
    System.out.println(f);
    System.out.println(F);
  }
}
