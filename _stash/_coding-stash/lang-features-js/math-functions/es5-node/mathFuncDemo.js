//console.log(Math);
console.log(Math.abs(-100));

console.log(Math.cos(0.3) * Math.cos(0.3) + Math.sin(0.3) * Math.sin(0.3));

console.log(Math.min(100, -100));
console.log(Math.max(100, -100));

console.log(Math.floor(1.5));
console.log(Math.ceil(1.5));
console.log(Math.floor(-1.5));
console.log(Math.ceil(-1.5));

console.log(Math.round(1.5));
console.log(Math.round(1.4));

console.log(Math.pow(2, 3));
console.log(Math.exp(1));
console.log(Math.log(2)); // base e
console.log(Math.log10(2)); // base 10
console.log(Math.log2(2)); // base 2

console.log(Math.random());
