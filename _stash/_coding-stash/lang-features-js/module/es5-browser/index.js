var bar = require("./foo.js");

// if (typeof window === 'undefined') {
if (typeof module !== 'undefined' && module.exports) {
    console.log(bar.foo());
} else {
    document.getElementById('app').innerHTML = bar.foo();
}

// (function(exports){
//
// }(typeof window === 'undefined' ? module.exports : window));
