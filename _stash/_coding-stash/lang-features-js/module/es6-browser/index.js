import theDefault, {foo} from "./foo";

if (typeof window === 'undefined') {
    console.log(foo());
} else {
    document.getElementById('app').innerHTML = theDefault();
    document.getElementById('app2').innerHTML = foo();
}
