/*
`Math.random()` returns a random float numebr between [0.0, 1.0)
*/
function getRandomDefaultFloat() {
  return Math.random();
}

/*
Returns a random integer number between [min, max)
*/
function getRandomIntMaxExclusive(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

/*
Returns a random integer number between [min, max]
*/
function getRandomIntMaxInclusive(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/*
Returns a random float number between [min, max)
*/
function getRandomFloatMaxExclusive(min, max) {
  return Math.random() * (max - min) + min;
}

console.log("getRandomDefaultFloat:");
for (var i = 0; i < 10; i++) {
  console.log(getRandomDefaultFloat());
}
console.log();

console.log("getRandomIntMaxExclusive:");
for (var i = 0; i < 10; i++) {
  console.log(getRandomIntMaxExclusive(-10, 10));
}
console.log();

console.log("getRandomIntMaxInclusive:");
for (var i = 0; i < 10; i++) {
  console.log(getRandomIntMaxInclusive(-10, 10));
}
console.log();

console.log("getRandomFloatMaxExclusive:");
for (var i = 0; i < 10; i++) {
  console.log(getRandomFloatMaxExclusive(-10, 10));
}
console.log();
