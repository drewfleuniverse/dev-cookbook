// @flow
// ...eslint-disable-next-line
import * as React from 'react';
import Form from './Form/Form';
import {
  Input,
  FormInputGroup,
} from './Controls/Controls';

export const userApi = Symbol.for('users');

type Props = {};
type State = {};

class App extends React.Component<Props, State> {
  shouldComponentUpdate(nextProps: Props, nextState: State) {
    const keys = this.state !== null
      ? Object.keys(this.state)
      : [];
    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      if (nextState[key] !== this.state[key]) {
        return true;
      }
    }
    return false;
  }
  handleChange = (event: SyntheticEvent<HTMLInputElement>) => {
    (event.currentTarget: HTMLInputElement); // eslint-disable-line
    this.setState({ [event.currentTarget.name]: event.currentTarget.value });
  }
  handleFileChange = (event: SyntheticEvent<HTMLInputElement>) => {
    (event.currentTarget: HTMLInputElement); // eslint-disable-line
    this.setState({
      [event.currentTarget.name]: event.currentTarget.files[0],
    });
  }
  handleSubmit = (event: SyntheticEvent<HTMLFormElement>) => {
    (event.currentTarget: HTMLFormElement); // eslint-disable-line
    event.preventDefault();
  }
  render() {
    return (
      <React.Fragment>
        <Form
          id="form"
        >
          <FormInputGroup
            id="form__username"
            type="text"
            text="Username"
            name="username"
            onChange={this.handleChange}
          />
          <FormInputGroup
            id="form__password"
            type="password"
            text="Password"
            name="password"
            onChange={this.handleChange}
          />
          <FormInputGroup
            id="form__picture"
            type="file"
            text="Picture"
            name="picture"
            onChange={this.handleFileChange}
          />
          <Input type="submit" />
        </Form>
        <div>{JSON.stringify(this.state)}</div>
      </React.Fragment>
    );
  }
}


export default App;
