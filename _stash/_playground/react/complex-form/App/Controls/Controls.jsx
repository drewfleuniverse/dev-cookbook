// @flow
// ...eslint-disable-next-line
import * as React from 'react';

const FormGroup = (props: {}) => (
  <div className="form-group" {...props} />
);

type LabelTypes = {
  htmlFor: string,
  text: string,
  props: {}
};

const Label = ({
  htmlFor, text, ...props
}: LabelTypes) => (
  // eslint-disable-next-line
  <label htmlFor={htmlFor} {...props}>{text}</label>
);


type InputTypes = {
  id?: string | void,
  type: string,
  name?: string | void,
  onChange?: Function | void,
};

const Input = ({
  id, type, name, onChange, ...props
}: InputTypes) => (
  <input
    id={id}
    type={type}
    name={name}
    onChange={onChange}
    {...props}
  />
);

Input.defaultProps = {
  id: undefined,
  name: undefined,
  onChange: undefined,
};


const FormInputGroup = ({
  id,
  text,
  type,
  name,
  onChange,
  labelProps,
  inputProps,
}: any) => (
  <FormGroup>
    <Label htmlFor={id} text={text} {...labelProps} />
    <Input
      id={id}
      type={type}
      name={name}
      onChange={onChange}
      {...inputProps}
    />
  </FormGroup>
);


export {
  Label,
  Input,
  FormGroup,
  FormInputGroup,
};
