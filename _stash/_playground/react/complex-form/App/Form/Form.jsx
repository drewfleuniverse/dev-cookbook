// @flow
import * as React from 'react';

type Props = {
  id: string,
  children?: any
};
type State = {};

class Form extends React.Component<Props, State> {
  static foo(): any {
    return 42;
  }
  render() {
    const {
      id,
      children,
    } = this.props;
    return (
      <form id={id}>
        {children}
      </form>
    );
  }
}

export default Form;
