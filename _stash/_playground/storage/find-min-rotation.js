/*
Find min array rotation
Perf:
Space:
 */




describe('findMin()', () => {
  it('should find min' => {
    const arr = [4, 5, 6, 1, 2, 3];
    it(findMin(arr) === 1);
  });
  it('should find min' => {
    const arr = [1, 2, 3, 4, 5, 6];
    it(findMin(arr) === 1);
  });
  it('should find min' => {
    const arr = [1, 1, 1];
    it(findMin(arr) === 1);
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
