/*
Binary search
Usion recursion
Perf - worst: O(log(n))
Space - worst: O(log(n))
Perf - best: O(1)
Space - best: O(1)
 */

const binarySearch = function (arr, val, left=0, right=arr.length-1) {
  if (left > right) {
    return -1;
  }
  const mid = Math.floor((left + right) / 2);
  if (arr[mid] > val) {
    binarySearch(arr, val, left, mid - 1);
  } else if (arr[mid] < val) {
    binarySearch(arr, val, mid + 1, right);
  } else {
    return mid;
  }
};


describe('binarySearch()', () => {
  it('should not find', () => {
    const arr = [];
    assert(binarySearch(arr, 42) === -1);
  });
  const arrOdd = [1, 2, 3, 4, 5];
  it('should find - 1', () => {
    assert(binarySearch(arrOdd, 3) === 2);
  });
  it('should find - 1', () => {
    assert(binarySearch(arrOdd, 1) === 0);
  });
  it('should find - 1', () => {
    assert(binarySearch(arrOdd, 5) === 4);
  });
  it('should find - 1', () => {
    assert(binarySearch(arrOdd, 42) === -1);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
