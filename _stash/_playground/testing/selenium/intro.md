# introduction to Selenium

## Setup

**macOS**

- Install: `brew install chromedriver`
- Use:
    - As system service: `brew services start chromedriver`
    - As front process: `chromedriver`




## References

- https://marmelab.com/blog/2016/04/19/e2e-testing-with-node-and-es6.html
