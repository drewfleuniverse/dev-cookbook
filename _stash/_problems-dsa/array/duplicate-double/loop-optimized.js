const cl = s => console.log(s)


const duplicate = function (arr) {
  const len = arr.length;
  const copy = new Array(len*2);

  for (let i = 0; i < len; i++) {
    const tmp = arr[i];
    copy[i] = tmp;
    copy[len+i] = tmp;
  }

  return copy
};


let a, r;


a = [1,2,3,4,5];
r = duplicate(a);
cl(r.toString() === `1,2,3,4,5,1,2,3,4,5`);

a = [1,2,3,4,5,6];
r = duplicate(a);
cl(r.toString() === `1,2,3,4,5,6,1,2,3,4,5,6`);
