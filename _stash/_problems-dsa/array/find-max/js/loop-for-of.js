const cl = s => console.log(s)


const getMax = function (arr) {
  let max = -Infinity

  for (const val of arr) {
    if (max < val) {
      max = val;
    }
  }

  return max
}


let a;


a = [1,2,42];
cl(getMax(a) === 42)
