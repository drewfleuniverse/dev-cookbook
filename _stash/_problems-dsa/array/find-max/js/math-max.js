const cl = s => console.log(s)


const getMax = arr => Math.max.apply(null, arr);


const getMaxES6 = arr => Math.max(...arr);


let a;


a = [1,2,42];
cl(getMax(a) === 42)
cl(getMaxES6(a) === 42)
