# Find Max


- Using `Math.max` caveats
  - Slower than using loop or reduce
  - Doesn't work with large input array
