const cl = s => console.log(s);
const js = o => JSON.stringify(o);


class HashTable {
  constructor(max) {
    this.max = max;
    this.size = 0;
    this.hashTable = new Array(max);
    for (let i = 0; i < max; i++) {
      this.hashTable[i] = [];
    }
  }

  put(key) {
    let i = this.hash(key);
    this.hashTable[i].push(key);
    this.size++;
  }

  has(key) {
    let i = this.hash(key);

    return this.hashTable[i].length !== 0;
  }

  delete(key) {
    let i = this.hash(key);

    if (this.hashTable[i].length === 0) {
      return undefined;
    }

    return this.hashTable[i].pop();
  }

  hash(key) {
    const strKey = key.toString();
    let hash = 0;

    for (const ch of strKey) {
      hash = ((hash * 31) + ch.charCodeAt())
        % this.max;
    }

    return hash;
  }

  next(index) {
    return (index + 1) % this.max;
  }
}

const findOppositNums = function (arr) {
  const minVal = Math.min.apply(null, arr);
  const maxVal = Math.max.apply(null, arr);
  const max = Math.abs(minVal) + maxVal + 0;
  const hashTable = new HashTable(max);

  for (let val of arr) {
    hashTable.put(val);
  }

  const r = [];

  for (const val of arr) {
    if (
      hashTable.has(val)
      && hashTable.has(-val)
    ) {
      const pos = hashTable.delete(val);
      const neg = hashTable.delete(-val);
      r.push([pos, neg]);
    }
  }

  return r;
}


let a, t;


a = [-3,9,1,-2,-5,3,-1,6,2];


r = findOppositNums(a);
cl(js(r) === '[[-3,3],[1,-1],[-2,2]]');
