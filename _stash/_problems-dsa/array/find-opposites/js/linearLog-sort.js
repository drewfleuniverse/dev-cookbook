const cl = s => console.log(s);
const js = o => JSON.stringify(o);


const findOppositNums = function (arr) {
  arr.sort((a, b) => {
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }

    return 0;
  });

  const res = [];
  let lo = 0;
  let hi = arr.length-1;
  while (lo < hi) {
    const l = arr[lo];
    const r = arr[hi];
    if (l < 0 && r > 0) {
      if (l + r === 0) {
        res.push([l, r]);
        lo = lo + 1;
        hi = hi - 1;
      } else if (Math.abs(l) > r) {
        lo = lo + 1;
      } else {
        hi = hi - 1;
      }
    }
  }

  return res;
}


let a, t;


a = [-3,9,1,-2,-5,3,-1,6,2];


r = findOppositNums(a);
cl(a.toString() === '-5,-3,-2,-1,1,2,3,6,9');
cl(js(r) === '[[-3,3],[-2,2],[-1,1]]');
