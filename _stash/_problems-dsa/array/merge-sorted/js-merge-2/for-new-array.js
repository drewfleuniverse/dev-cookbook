const cl = i => console.log(i)
const js = i => JSON.stringify(i)


// time: O(l+m); space O(l+m)
const mergeSorted = (a, b) => {
  let l = a.length, m = b.length
  if (l === 0) return b.slice()
  if (m === 0) return a.slice()

  const r = new Array(l+m)
  let i = 0, j = 0, k = 0
  while (i < l+m) {
    if (a[j] < b[k]) r[i++] = a[j++]
    else r[i++] = b[k++]
  }

  return r
}


let a, b, r


a = [1,2,2]
b = [3,3,5,6]
r = mergeSorted(a, b)
cl(js(r) === '[1,2,2,3,3,5,6]')


a = [1,2,3]
b = []
r = mergeSorted(a, b)
cl(js(r) === '[1,2,3]')
