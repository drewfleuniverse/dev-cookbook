'use strict'
const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const swap = (a, i, j) => {
  const t = a[i]
  a[i] = a[j]
  a[j] = t
}


class MinHeap {
  constructor(a=[]) {
    this.h = a
    if (a.length) this.heapify()
  }

  heapify() {
    const lastInternal = Math.floor(this.h.length/2)-1
    for (let i = lastInternal; i >= 0; i--)
      this.bubbleDown(i)
  }

  getMin() { return this.h[0] }

  replaceMin(v) {
    this.h[0] = v
    this.bubbleDown(0)
  }

  bubbleDown(p) {
    while (!this.isLeaf(p)) {
      let i = this.leftChild(p)

      if (i+1 < this.h.length
      && this.h[i] > this.h[i+1])
        i++
      if (this.h[p] <= this.h[i])
        return

      swap(this.h, p, i)
      p = i
    }
  }

  leftChild(p) { return p*2 + 1 }
  isLeaf(p) {
    return p >= Math.floor(this.h.length/2)
      && p < this.h.length
  }
  toString() {
    return this.h.toString()
  }
}


class Node {
  constructor(val, arrPos, nexPos) {
    this.val = val
    this.arrPos = arrPos
    this.nexPos = nexPos
  }
  valueOf() { return this.val }
}


const merge = a => {
  const r = [], t = []

  for (let i = 0; i < a.length; i++) {
    const val = a[i].length !== 0
      ? a[i][0]
      : Infinity

    t.push(new Node(val, i, 1))
  }

  const h = new MinHeap(t)

  while (h.getMin().val !== Infinity) {
    const {val, arrPos, nexPos} = h.getMin()

    r.push(val)

    let curVal
    if (nexPos < a[arrPos].length) {
      curVal = a[arrPos][nexPos]
    } else curVal = Infinity

    h.replaceMin(new Node(curVal, arrPos, nexPos+1))
  }

  return r
}


let q, r


q = [
  [7,8],
  [1,2,3,4],
  [5,6,9]
]
r = merge(q).toString()
cl(r === '1,2,3,4,5,6,7,8,9')

q = [
  [],
  [1,2,3,4],
  [5,6,9]
]
r = merge(q).toString()
cl(r === '1,2,3,4,5,6,9')
