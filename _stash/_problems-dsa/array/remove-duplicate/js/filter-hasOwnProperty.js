const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')
const js = i => JSON.stringify(i)


const rmDup = a => {
  const h = {}
  return a.filter(v => h.hasOwnProperty(v)
    ? false
    : h[v] = true
  )
}


const a = [3,2,3,2,3,3,1,2,1]
const r = rmDup(a)
cl(js(r) === '[3,2,1]')
