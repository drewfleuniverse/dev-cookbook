const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const rmDup = a => {
  return a.filter((v, i) => a.indexOf(v) === i)
}


const a = [3,2,3,2,3,3,1,2,1]
const r = rmDup(a)
cl(js(r) === '[3,2,1]')
