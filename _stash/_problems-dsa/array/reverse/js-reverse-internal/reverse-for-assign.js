const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const reverse = a => {
  const l = a.length
  const t = a.slice()
  for (let i = 0; i < l; i++)
    a[i] = t[l-i-1]

  return a
}


const a = [1, 2, 3, 4, 5]
const r = reverse(a)

cl(r === a)
cl(js(a) === '[5,4,3,2,1]')
