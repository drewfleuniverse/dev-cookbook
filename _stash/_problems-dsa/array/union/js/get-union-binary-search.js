/*
Using sort and binary search
Perf: O(m*log(m) + n*log(m)
Space: O(m)
 */


const binarySearch = (arr, val) => {
  let lo = 0;
  let hi = arr.length - 1;
  while (lo <= hi) {
    const mid = Math.floor((lo + hi) / 2);
    if (arr[mid] > val) {
      hi = mid - 1;
    } else if (arr[mid] < val) {
      lo = mid + 1;
    } else {
      return val;
    }
  }
  return null;
};


const getUnion = (arr1, arr2) => {
  if (arr1.length > arr2.length) {
    let tmp = arr1;
    arr1 = arr2;
    arr2 = arr1;
  }
  const result = [...arr1].sort((a, b) => (a-b));
  for (const el of arr2) {
    if (binarySearch(result, el) === null) {
      result.push(el);
    }
  }
  return result;
};


describe('getUnion()', () => {
  it('should return empty arrays', () => {
    assert(getUnion([], []).toString() === '');
  });
  it('should return intersction 1', () => {
    assert(getUnion([1, 2], []).toString() === '1,2');
    assert(getUnion([], [1, 2]).toString() === '1,2');
    const result = getUnion(
      [1, 2],
      [3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
  it('should return intersction 2', () => {
    const result = getUnion(
      [1, 2],
      [1, 2, 3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
