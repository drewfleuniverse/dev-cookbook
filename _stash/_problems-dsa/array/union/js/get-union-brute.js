// Brute force
// Perf: O(m*n); Space: O(1)
const getUnion = (arr1, arr2) => {
  const result = [...arr1];
  for (const el2 of arr2) {
    let hasEl = false;
    for (const el of result) {
      if (el2 === el) {
        hasEl = true;
      }
    }
    if (!hasEl) {
      result.push(el2);
    }
  }
  return result;
};


describe('getUnion()', () => {
  it('should return empty arrays', () => {
    assert(getUnion([], []).toString() === '');
  });
  it('should return intersction 1', () => {
    assert(getUnion([1, 2], []).toString() === '1,2');
    assert(getUnion([], [1, 2]).toString() === '1,2');
    const result = getUnion(
      [1, 2],
      [3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
  it('should return intersction 2', () => {
    const result = getUnion(
      [1, 2],
      [1, 2, 3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
});


/*
Utilsl
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
