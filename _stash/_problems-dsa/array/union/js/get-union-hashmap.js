/*
Using hashtable
Perf: O(m + n)
Space: O(m)
 */


const getUnion = (arr1, arr2) => {
  if (arr1.length > arr2.length) {
    let tmp = arr1;
    arr1 = arr2;
    arr2 = arr1;
  }
  const hash = new Set(arr1);
  let result = [...arr1];
  for (const el of arr2) {
    if (!hash.has(el)) {
      result.push(el);
    }
  }
  return result;
};


describe('getUnion()', () => {
  it('should return empty arrays', () => {
    assert(getUnion([], []).toString() === '');
  });
  it('should return intersction 1', () => {
    assert(getUnion([1, 2], []).toString() === '1,2');
    assert(getUnion([], [1, 2]).toString() === '1,2');
    const result = getUnion(
      [1, 2],
      [3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
  it('should return intersction 2', () => {
    const result = getUnion(
      [1, 2],
      [1, 2, 3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
