/*
Using hashmap
Perf: O(m + n + m + n)
Space: O(m + n)
 */
const getUnion = (arr1, arr2) => {
  const set = new Set([...arr1, ...arr2]);
  return [...set];
};


describe('getUnion()', () => {
  it('should return empty arrays', () => {
    assert(getUnion([], []).toString() === '');
  });
  it('should return intersction 1', () => {
    assert(getUnion([1, 2], []).toString() === '1,2');
    assert(getUnion([], [1, 2]).toString() === '1,2');
    const result = getUnion(
      [1, 2],
      [3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
  it('should return intersction 2', () => {
    const result = getUnion(
      [1, 2],
      [1, 2, 3, 4]
    );
    assert(result.toString() === '1,2,3,4');
  });
});


/*
Utilsl
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
