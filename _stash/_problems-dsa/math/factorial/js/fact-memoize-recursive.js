const lg = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const m = []

const fact = n => {
  if (m[n] !== undefined)
    return m[n]

  if (n <= 0)
    return 1

  const r = n * fact(n-1)
  m[n] = r

  return r
}

const r = []
for (let i = 0; i <= 18; i++)
  r.push(fact(i))

lg(jsf(r) === `[
  1,
  1,
  2,
  6,
  24,
  120,
  720,
  5040,
  40320,
  362880,
  3628800,
  39916800,
  479001600,
  6227020800,
  87178291200,
  1307674368000,
  20922789888000,
  355687428096000,
  6402373705728000
]`)

lg(jsf(m) === `[
  null,
  1,
  2,
  6,
  24,
  120,
  720,
  5040,
  40320,
  362880,
  3628800,
  39916800,
  479001600,
  6227020800,
  87178291200,
  1307674368000,
  20922789888000,
  355687428096000,
  6402373705728000
]`)
