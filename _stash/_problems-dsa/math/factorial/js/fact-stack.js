const lg = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const fact = n => {
  if (n <= 1)
    return 1
  
  const s = []
  while (n > 1)
    s.push(n--)
  
  let a = 1
  while (s.length)
    a *= s.pop()
  
  return a
}

const r = []
for (let i = 0; i <= 18; i++)
  r.push(fact(i))

lg(jsf(r) === `[
  1,
  1,
  2,
  6,
  24,
  120,
  720,
  5040,
  40320,
  362880,
  3628800,
  39916800,
  479001600,
  6227020800,
  87178291200,
  1307674368000,
  20922789888000,
  355687428096000,
  6402373705728000
]`)
