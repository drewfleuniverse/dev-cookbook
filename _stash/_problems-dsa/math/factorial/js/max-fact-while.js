const lg = i => console.log(i)
const js = i => JSON.stringify(i)


const maxFact = () => {
  const prev = {}
  let n = 1
  let a = 1
  while (true) {
    if (a > Number.MAX_SAFE_INTEGER)
      break
    prev.n = n-1
    prev.a = a
    a *= n
    n++
  }
  return prev
}

lg(js(maxFact()) ===
  '{"n":18,"a":6402373705728000}')
