/*
Using recursion
Perf: O(2^n)
- Each call to fib() results in calling fib() twice, i.e. fib(n) -> fib(n-1) + fib(n-2), and calling n times is 2^n.
Space: O(n)
- For fib(n), the maximum stack height is n.
*/

const fib = num => {
  if (num <= 1) {
    return 1;
  }
  return fib(num - 1) + fib(num - 2);
};


describe('fib()', () => {
  it('should return 1', () => {
    assert(fib(0) === 1);
    assert(fib(1) === 1);
  });
  it('should return 2', () => {
    assert(fib(2) === 2);
  });
  it('should return 3', () => {
    assert(fib(3) === 3);
  });
  it('should return 5', () => {
    assert(fib(4) === 5);
  });
  it('should return 55', () => {
    assert(fib(9) === 55);
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
