const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')

// params: a >= 0, b >= 0
// return: 0, no gcd found
const calcGcd = (a, b) => {
  if (a === b) return a
  if (a === 0) return b
  if (b === 0) return a

  let d = 2, g = 1

  while (d < a || d < b) {
    if (a%d === 0 && b%d === 0)
      g = d
    d++
  }

  return g
}


cl(calcGcd(0, 0) === 0)
cl(calcGcd(7, 0) === 7)
cl(calcGcd(0, 7) === 7)
cl(calcGcd(7, 7) === 7)
cl(calcGcd(12, 42) === 6)
cl(calcGcd(42, 12) === 6)
