const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


// params: a >= 0, b >= 0
// return: 0, no lcm found
const calcGcd = (a, b) => {
  if (b === 0) return a
  return calcGcd(b, a%b)
}

// params: a > 0, b > 0
const calcLcm = (a, b) => {
  return (a*b)/calcGcd(a,b)
}


cl(calcLcm(7, 7) === 7)
cl(calcLcm(12, 42) === 84)
cl(calcLcm(42, 12) === 84)
