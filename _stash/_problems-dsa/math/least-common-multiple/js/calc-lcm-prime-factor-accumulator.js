const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')
const js = i => JSON.stringify(i)


// params: a > 0, b > 0
const calcLcm = (a, b) => {
  let r = 1
  for (let d = 2; a > 2 || b > 2; d++) {
    if (a%d === 0 || b%d === 0) {
      if (a%d === 0) a /= d
      if (b%d === 0) b /= d
      r *= d
      d--
    }
  }

  return r
}


cl(calcLcm(7, 7) === 7)
cl(calcLcm(12, 42) === 84)
cl(calcLcm(42, 12) === 84)
