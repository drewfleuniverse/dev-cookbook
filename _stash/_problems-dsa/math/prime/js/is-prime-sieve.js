const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const isPrime = n => {
  if (n <= 1)
    return false

  const a = new Array(n+1).fill(true)
  const s = Math.floor(Math.sqrt(n))
  for (let i = 2; i <= s; i++) {
    if (a[i]) {
      for (let j = i*i; j <= n; j += i)
        a[j] = false
    }
  }

  return a[n]
}




const r = []
for (let i = 1; i <= 100; i++) {
  if (isPrime(i))
    r.push(i)
}
cl(jsf(r) === `[
  2,
  3,
  5,
  7,
  11,
  13,
  17,
  19,
  23,
  29,
  31,
  37,
  41,
  43,
  47,
  53,
  59,
  61,
  67,
  71,
  73,
  79,
  83,
  89,
  97
]`)
