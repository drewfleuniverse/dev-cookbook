/*
Attributes: n, start, goal, temp.
Solutions:
1. Recursion: To simply the problem, firstly, ignore the fact that poles has
order and, secondly, either thinking when n = 2, or when:
  n = u + r, u: upper elements, r: rest elements
and u is on top of r on pole start, to break all recursive executions into a
set of base steps:
  - move u from start to temp
  - move r from start to goal
  - move u from temp to goal
note that all three poles can in turn act as a temporary spare pole, as long as
no larger elements/rings on top of smaller ones. These steps can be transformed
into:
  TOH(n, start, goal, temp) {
    TOH(n-1, start, temp, goal)
    move(start, goal)
    TOH(n-1, temp, goal, start)
  }
*/

class RecursiveTOH {
  private long counter;

  RecursiveTOH() {
    counter = 1;
  }

  private void move(String start, String goal) {
    System.out.println(counter + ": " +  start + " -> " + goal);
    counter++;
  }

  public void toh(int n, String start, String goal, String temp) {
    if (n == 0) return;
    toh(n-1, start, temp, goal);
    move(start, goal);
    toh(n-1, temp, goal, start);
  }
}
