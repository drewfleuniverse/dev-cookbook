import java.util.*;

class StackTOH {
  private long counter;
  StackTOH() { counter = 1; }

  private enum operation { MOVE, TOH }

  private class TOHobj {
    public operation op;
    public int num;
    public String start, goal, temp;


    TOHobj(operation o, int n, String s, String g, String t) {
      op = o; num = n; start = s; goal = g; temp = t;
    }
    TOHobj(operation o, String s, String g) {
      op = o; start = s; goal = g;
    }
  }

  private void move(String start, String goal) {
    System.out.println(counter + ": " +  start + " -> " + goal);
    counter++;
  }

  public void toh(int n, String start, String goal, String temp) {
    Stack<TOHobj> S = new Stack<>();//2*n+1);
    S.push(new TOHobj(operation.TOH, n, start, goal, temp));
    while (S.size() > 0) {
      TOHobj it = S.pop();
      if (it.op == operation.MOVE) move(it.start, it.goal);
      else if (it.num > 0) {
        S.push(new TOHobj(operation.TOH, it.num-1, it.temp, it.goal, it.start));
        S.push(new TOHobj(operation.MOVE, it.start, it.goal));
        S.push(new TOHobj(operation.TOH, it.num-1, it.start, it.temp, it.goal));
      }
    }
  }
}
