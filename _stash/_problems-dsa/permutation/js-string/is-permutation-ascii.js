const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const isPermuteAscii = (a, b) => {
  if (a.length !== b.length)
    return false
  
  const c = new Array(128)
  c.fill(0)
  
  for (let i = 0; i < a.length; i++)
    c[a.charCodeAt(i)]++
  
  for (let i = b.length-1; i >= 0 ; i--) {
    if (--c[b.charCodeAt(i)] < 0)
      return false
  }
  return true
}


let a, b


a = 'abcdefg'
b = 'gcbdafe'
cl(isPermuteAscii(a, b) === true)

a = 'abcdefg'
b = 'abcdefg'
cl(isPermuteAscii(a, b) === true)

a = ''
b = ''
cl(isPermuteAscii(a, b) === true)

a = 'abcdefg'
b = 'abcdefgg'
cl(isPermuteAscii(a, b) === false)

a = 'abcdefg'
b = 'abcdefa'
cl(isPermuteAscii(a, b) === false)






