const RAD = 10;


const sortdigit = function (num) {
  const cnt = new Array(RAD).fill(0);

  for (let pl = 1; pl <= num; pl *= RAD) {
    const digit = Math.floor(num / pl) % RAD;
    cnt[digit]++;
  }

  let accumulator = 0;

  for (let pl = 1; pl <= num; pl *= RAD) {
    let digit = 0;

    while (cnt[digit] === 0) {
      digit++;
    }

    accumulator += digit * pl;
    cnt[digit]--;
  }

  return accumulator;
}

console.log(sortdigit(38756765))
