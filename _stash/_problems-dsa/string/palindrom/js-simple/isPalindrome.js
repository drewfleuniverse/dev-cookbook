const cl = i => console.log(i)


const isPalindrome = function (str) {
  if (str.length === 0) {
    return false;
  }

  let lo = 0;
  let hi = str.length-1;

  while (lo < hi) {
    if (str[lo] !== str[hi]) {
      return false;
    }

    lo++;
    hi--;
  }

  return true;
};

console.log(isPalindrome('abcd') === false);
console.log(isPalindrome('abc') === false);
console.log(isPalindrome('') === false);
console.log(isPalindrome('abba') === true);
console.log(isPalindrome('aba') === true);
console.log(isPalindrome('a') === true);
