/*  Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases. */

const isPalindrome= (inputStr) => {
  const codeA = 'a'.charCodeAt();
  const codeZ = 'z'.charCodeAt();
  const codeZero = '0'.charCodeAt();
  const codeNine = '9'.charCodeAt();

  const formatedStr = [...inputStr.toLowerCase()].filter(ch =>
    (ch.charCodeAt() >= codeA && ch.charCodeAt() <= codeZ) ||
    (ch.charCodeAt() >= codeZero && ch.charCodeAt() <= codeNine)
  );

  let left = 0;
  let right = formatedStr.length - 1;

  while (left < right) {
    if (formatedStr[left] !== formatedStr[right]) {
      return false;
    }

    left++;
    right--;
  }

  return true;
}



 console.log(isPalindrome('dollop'));  // >false
 console.log(isPalindrome('level'));  // >true
 console.log(isPalindrome('Ana'));  // >true
 console.log(isPalindrome('A car, a man, a maraca.'));  // >true
 console.log(isPalindrome('12a-=*1')); //false
 console.log(isPalindrome('')); //True
