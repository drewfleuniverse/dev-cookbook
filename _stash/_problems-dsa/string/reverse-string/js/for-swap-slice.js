const cl = i => console.log(i)


const reverse = s => {
  const l = s.length
  const m = Math.floor(l/2)
  let j
  for (let i = 0; i < m; i++) {
    j = l-i-1
    s = s.slice(0, i) +
      s[j] +
      s.slice(i+1, j) +
      s[i] +
      s.slice(j+1, l)
  }

  return s
}


let s


s = 'raboof'
cl(reverse(s) === 'foobar')

s = 'rab'
cl(reverse(s) === 'bar')

s = ''
cl(reverse(s) === '')