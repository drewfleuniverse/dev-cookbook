const cl = i => console.log(i)


const reverse = s => s
  .split('')
  .reverse()
  .join('')


let s


s = 'raboof'
cl(reverse(s) === 'foobar')

s = 'rab'
cl(reverse(s) === 'bar')

s = ''
cl(reverse(s) === '')
