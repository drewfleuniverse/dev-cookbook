const cl = i => console.log(i)


let zip;


const isZip = function (zip) {
  // Also works
  // if (
  //   zip.length !== 10
  //   && zip.length !== 5
  // ) {
  //   return false;
  // }
  if (!(
    zip.length === 10
    || zip.length === 5
  )) {
    return false;
  }

  for (let i = 0; i < zip.length; i++) {
    if (i !== 5) {
      if (zip[i] < 0 || zip[i] > 9) {
        return false;
      }
    } else {
      if (zip[i] !== '-') {
        return false;
      }
    }
  }

  return true;
};


cl(isZip('12345-6789') === true);
cl(isZip('12345') === true);

cl(isZip('12345-678') === false);
cl(isZip('1234-678') === false);
cl(isZip('12345-') === false);
cl(isZip('1234') === false);
