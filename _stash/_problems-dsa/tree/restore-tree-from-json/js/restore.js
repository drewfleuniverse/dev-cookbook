const restoreTree = function (input) {
  let KVPairs = input.map(el => ([
    el._id,
    el.children
  ]));

  const map = new Map(KVPairs);

  KVPairs = null;

  let root = null;

  for (const node of input) {

    const { parentId } = node;

    if (parentId === null) {
      root = node;
    } else {
      map.get(parentId).push(node);
    }

  }

  return root;
};


let input, output;


input = [
  {
    "_id": "1",
    "parentId": "3",
    "children": []
  },
  {
    "_id": "2",
    "parentId": "5",
    "children": []
  },
  {
    "_id": "3",
    "parentId": "2",
    "children": []
  },
  {
    "_id": "4",
    "parentId": "5",
    "children": []
  },
  {
    "_id": "5",
    "parentId": null,
    "children": []
  },
  {
    "_id": "6",
    "parentId": "2",
    "children": []
  },
  {
    "_id": "7",
    "parentId": "3",
    "children": []
  },
  {
    "_id": "8",
    "parentId": "9",
    "children": []
  },
  {
    "_id": "9",
    "parentId": "4",
    "children": []
  },
  {
    "_id": "10",
    "parentId": "4",
    "children": []
  }
];

output = restoreTree(input);

console.log(JSON.stringify(output) === '{"_id":"5","parentId":null,"children":[{"_id":"2","parentId":"5","children":[{"_id":"3","parentId":"2","children":[{"_id":"1","parentId":"3","children":[]},{"_id":"7","parentId":"3","children":[]}]},{"_id":"6","parentId":"2","children":[]}]},{"_id":"4","parentId":"5","children":[{"_id":"9","parentId":"4","children":[{"_id":"8","parentId":"9","children":[]}]},{"_id":"10","parentId":"4","children":[]}]}]}');


input = [];

output = restoreTree(input);

console.log(output === null);
