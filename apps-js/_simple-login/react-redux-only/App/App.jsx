// @flow
import * as React from 'react';
import { createStore } from 'redux';
import reducer from './reducer';
import {
  updateFormData,
  updateFormErrors,
  setIsFormValid,
  setErrorResponse,
  resetState,
} from './actions';


const API_USERS = 'http://localhost:9000/users';
export const store = createStore(reducer);


type FormErrorProps = { message: string };
const FormError = ({ message }: FormErrorProps) => {
  if (message === '') {
    return null;
  }
  return (
    <div className="form-error">
      <div className="padding" />
      <div className="error">{message}</div>
    </div>
  );
};


class App extends React.Component<{}> {
  static validateformData(name: string) {
    const { formData } = store.getState();
    let errorMessage: string = '';
    if (!formData[name]) {
      errorMessage = `${name} cannot be empty`;
    } else {
      errorMessage = !formData[name].match(/^[a-zA-Z0-9]+$/)
        ? `${name} is not alphanumeric`
        : '';
    }
    store.dispatch(updateFormErrors(name, errorMessage));
    App.isFormValid();
  }
  static isFormValid() {
    const { formData, formErrors } = store.getState();
    const isFormValid: boolean = (
      Object.values(formData).every(val => val !== '') &&
      Object.values(formErrors).every(val => val === '')
    );
    store.dispatch(setIsFormValid(isFormValid));
  }
  constructor(props: {}) {
    super(props);
    store.dispatch(resetState());
  }
  handleChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    store.dispatch(updateFormData(name, value));
    App.validateformData(name);
    this.forceUpdate();
  };
  handleSubmit = async (event: SyntheticEvent<HTMLFormElement>) => {
    const { formData } = store.getState();
    event.preventDefault();
    try {
      const response = await fetch(API_USERS, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
      });
      if (!response.ok) {
        throw new Error(`${response.status}: ${response.statusText}`);
      }
      store.dispatch(resetState());
      this.forceUpdate();
    } catch (error) {
      store.dispatch(setErrorResponse(error.message));
      this.forceUpdate();
    }
  };
  render() {
    const {
      formData,
      formErrors,
      isFormValid,
    } = store.getState();
    return (
      <div className="container">
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              id="username"
              type="text"
              name="username"
              value={formData.username}
              onChange={this.handleChange}
            />
            <FormError message={formErrors.username} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              type="password"
              name="password"
              value={formData.password}
              onChange={this.handleChange}
            />
            <FormError message={formErrors.password} />
          </div>
          <div className="submit-wrapper">
            <input type="submit" value="Submit" disabled={!isFormValid} />
          </div>
        </form>
      </div>
    );
  }
}


export default App;
