// @flow

type UpdateFormDataAction = {
  type: 'UPDATE_FORM_DATA',
  payload: { username?: string, password?: string },
};
type UpdateFormErrorsAction = {
  type: 'UPDATE_FORM_ERRORS',
  payload: { username?: string, password?: string },
};
type SetIsFormValidAction = {
  type: 'SET_IS_FORM_VALID',
  payload: { isFormValid: boolean },
};
type SetErrorResponseAction = {
  type: 'SET_ERROR_RESPONSE',
  payload: { errorResponse: string },
};
type ResetStateAction = { type: 'RESET_STATE' };
export type Action =
  | UpdateFormDataAction
  | UpdateFormErrorsAction
  | SetIsFormValidAction
  | SetErrorResponseAction
  | ResetStateAction;

const updateFormData = (
  name: string,
  value: string,
): UpdateFormDataAction => ({
  type: 'UPDATE_FORM_DATA',
  payload: { [name]: value },
});

const updateFormErrors = (
  name: string,
  value: string,
): UpdateFormErrorsAction => ({
  type: 'UPDATE_FORM_ERRORS',
  payload: { [name]: value },
});

const setIsFormValid = (isFormValid: boolean): SetIsFormValidAction => ({
  type: 'SET_IS_FORM_VALID',
  payload: { isFormValid },
});

const setErrorResponse = (errorResponse: string): SetErrorResponseAction => ({
  type: 'SET_ERROR_RESPONSE',
  payload: { errorResponse },
});

const resetState = (): ResetStateAction => ({
  type: 'RESET_STATE',
});


export {
  updateFormData,
  updateFormErrors,
  setIsFormValid,
  setErrorResponse,
  resetState,
};
