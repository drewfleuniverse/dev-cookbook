// @flow
import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducer';
import {
  updateFormData as updateFormDataAction,
  updateFormErrors as updateFormErrorsAction,
  setIsFormValid as setIsFormValidAction,
  setErrorResponse as setErrorResponseAction,
  resetState as resetStateAction,
} from './actions';


const API_USERS = 'http://localhost:9000/users';
export const store = createStore(reducer);


type FormErrorProps = { message: string };
const FormError = ({ message }: FormErrorProps) => {
  if (message === '') {
    return null;
  }
  return (
    <div className="form-error">
      <div className="padding" />
      <div className="error">{message}</div>
    </div>
  );
};

type Props = {
  formData: any,
  formErrors: any,
  isFormValid: boolean,
  updateFormData: Function,
  setErrorResponse: Function,
  resetState: Function,
  checkIsFormValid: Function,
};
class App extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    props.resetState();
  }
  shouldComponentUpdate(nextProps: Props) {
    if (
      nextProps.formData.username === this.props.formData.username &&
      nextProps.formData.password === this.props.formData.password &&
      nextProps.formErrors.username === this.props.formErrors.username &&
      nextProps.formErrors.password === this.props.formErrors.password &&
      nextProps.isFormValid === this.props.isFormValid
    ) {
      return false;
    }
    return true;
  }
  componentDidUpdate() {
    this.props.checkIsFormValid();
  }
  handleChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    const { updateFormData } = this.props;
    updateFormData(name, value);
  };
  handleSubmit = async (event: SyntheticEvent<HTMLFormElement>) => {
    const {
      formData,
      setErrorResponse,
      resetState,
    } = this.props;
    event.preventDefault();
    try {
      const response = await fetch(API_USERS, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
      });
      if (!response.ok) {
        throw new Error(`${response.status}: ${response.statusText}`);
      }
      resetState();
    } catch (error) {
      setErrorResponse(error.message);
    }
  };
  render() {
    const {
      formData,
      formErrors,
      isFormValid,
    } = this.props;
    return (
      <div className="container">
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              id="username"
              type="text"
              name="username"
              value={formData.username}
              onChange={this.handleChange}
            />
            <FormError message={formErrors.username} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              type="password"
              name="password"
              value={formData.password}
              onChange={this.handleChange}
            />
            <FormError message={formErrors.password} />
          </div>
          <div className="submit-wrapper">
            <input type="submit" value="Submit" disabled={!isFormValid} />
          </div>
        </form>
      </div>
    );
  }
}


function validateformData(dispatch: Function, name: string, value: string) {
  let errorMessage: string = '';
  if (!value) {
    errorMessage = `${name} cannot be empty`;
  } else {
    errorMessage = !value.match(/^[a-zA-Z0-9]+$/)
      ? `${name} is not alphanumeric`
      : '';
  }
  dispatch(updateFormErrorsAction(name, errorMessage));
}


const mapStateToProps = state => ({
  formData: state.formData,
  formErrors: state.formErrors,
  isFormValid: state.isFormValid,
});
const mapDispatchToProps = dispatch => ({
  updateFormData(name, value) {
    dispatch(updateFormDataAction(name, value));
    validateformData(dispatch, name, value);
  },
  updateFormErrors(name, errorMessage) {
    dispatch(updateFormErrorsAction(name, errorMessage));
  },
  setErrorResponse(msg) {
    dispatch(setErrorResponseAction(msg));
  },
  setIsFormValid(isFormValid) {
    dispatch(setIsFormValidAction(isFormValid));
  },
  resetState() {
    dispatch(resetStateAction());
  },
});
const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  checkIsFormValid() {
    const isFormValid: boolean = (
      Object.values(stateProps.formData).every(val => val !== '') &&
      Object.values(stateProps.formErrors).every(val => val === '')
    );
    dispatchProps.setIsFormValid(isFormValid);
  },
});

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(App);


const AppProvider = () => (
  <Provider store={store}>
    <AppContainer />
  </Provider>
);

export default AppProvider;
