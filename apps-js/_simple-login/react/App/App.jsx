// @flow
import * as React from 'react';


const API_USERS = 'http://localhost:9000/users';


type FormErrorProps = { message: string };

const FormError = ({ message }: FormErrorProps) => {
  if (message === '') {
    return null;
  }
  return (
    <div className="form-error">
      <div className="padding" />
      <div className="error">{message}</div>
    </div>
  );
};


type Props = {};
type State = {
  formData: {
    username: string,
    password: string,
  },
  formErrors: {
    username: string,
    password: string,
  },
  isFormValid: boolean,
  errorResponse: string,
};

class App extends React.Component<Props, State> {
  static getDefaultState = () => ({
    formData: {
      username: '',
      password: '',
    },
    formErrors: {
      username: '',
      password: '',
    },
    isFormValid: false,
    errorResponse: '',
  });
  state = App.getDefaultState();
  shouldComponentUpdate(nextProps: Props, nextState: State) {
    if (
      nextState.formData.username === this.state.formData.username &&
      nextState.formData.password === this.state.formData.password &&
      nextState.formErrors.username === this.state.formErrors.username &&
      nextState.formErrors.password === this.state.formErrors.password &&
      nextState.isFormValid === this.state.isFormValid &&
      nextState.errorResponse === this.state.errorResponse
    ) {
      return false;
    }
    return true;
  }
  handleChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [name]: value,
      },
    }), () => this.validateformData(name));
  };
  handleSubmit = async (event: SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      const response = await fetch(API_USERS, {
        method: 'POST',
        body: JSON.stringify(this.state.formData),
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
      });
      if (!response.ok) {
        throw new Error(`${response.status}: ${response.statusText}`);
      }
      this.resetState();
    } catch (error) {
      this.setState({ errorResponse: error.message });
    }
  };
  validateformData(name: string) {
    const { formData } = this.state;
    let errorMessage: string = '';
    if (!formData[name]) {
      errorMessage = `${name} cannot be empty`;
    } else {
      errorMessage = !formData[name].match(/^[a-zA-Z0-9]+$/)
        ? `${name} is not alphanumeric`
        : '';
    }
    this.setState(prevState => ({
      formErrors: {
        ...prevState.formErrors,
        [name]: errorMessage,
      },
    }), () => this.isFormValid());
  }
  isFormValid() {
    this.setState({
      isFormValid: (
        Object.values(this.state.formData).every(val => val !== '') &&
        Object.values(this.state.formErrors).every(val => val === '')
      ),
    });
  }
  resetState() {
    this.setState(App.getDefaultState());
  }
  render() {
    const { formData, formErrors, isFormValid } = this.state;
    return (
      <div className="container">
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              id="username"
              type="text"
              name="username"
              value={formData.username}
              onChange={this.handleChange}
            />
            <FormError message={formErrors.username} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              type="password"
              name="password"
              value={formData.password}
              onChange={this.handleChange}
            />
            <FormError message={formErrors.password} />
          </div>
          <div className="submit-wrapper">
            <input type="submit" value="Submit" disabled={!isFormValid} />
          </div>
        </form>
      </div>
    );
  }
}


export default App;
