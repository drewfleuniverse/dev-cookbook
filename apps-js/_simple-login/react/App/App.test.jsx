import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import App from './App';


describe('App component', () => {
  describe('Rendering', () => {
    it('renders without error', () => {
      shallow(<App />);
    });
    it('renders correctly', () => {
      const app = renderer.create(<App />).toJSON();
      expect(app).toMatchSnapshot();
    });
  });
  describe('Updating input fields', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<App />);
    });
    const createInputEvent = (name, value) => ({
      currentTarget: {
        name,
        value,
      },
    });
    it('should have the following components', () => {
      const username = wrapper.find('#username');
      const password = wrapper.find('#password');
      const submit = wrapper.find('[type="submit"]');
      expect(username).toHaveLength(1);
      expect(password).toHaveLength(1);
      expect(submit).toHaveLength(1);
    });
    it('should not display error on valid fields', () => {
      const username = wrapper.find('#username');
      const password = wrapper.find('#password');
      username.simulate('change', createInputEvent('username', 'valid'));
      password.simulate('change', createInputEvent('password', 'valid'));
      const usernameError = wrapper.find('#username + FormError').prop('message');
      const passwordError = wrapper.find('#password + FormError').prop('message');
      expect(!!usernameError).toBeFalsy();
      expect(!!passwordError).toBeFalsy();
    });
    it('should display error on invalid fields', () => {
      const username = wrapper.find('#username');
      const password = wrapper.find('#password');
      username.simulate('change', createInputEvent('username', '!nvalid'));
      password.simulate('change', createInputEvent('password', '!nvalid'));
      const usernameError = wrapper.find('#username + FormError').prop('message');
      const passwordError = wrapper.find('#password + FormError').prop('message');
      expect(!!usernameError).toBeTruthy();
      expect(!!passwordError).toBeTruthy();
    });
    it('should clear error when an field is valid', () => {
      const username = wrapper.find('#username');
      const password = wrapper.find('#password');
      username.simulate('change', createInputEvent('username', '!nvalid'));
      password.simulate('change', createInputEvent('password', '!nvalid'));
      username.simulate('change', createInputEvent('username', 'valid'));
      password.simulate('change', createInputEvent('password', 'valid'));
      const errors = wrapper.find('.form-error');
      expect(errors).toHaveLength(0);
    });
    it('should enable Submit when all fields are valid', () => {
      let submit;
      const username = wrapper.find('#username');
      const password = wrapper.find('#password');
      submit = wrapper.find('[type="submit"]');
      expect(submit.prop('disabled')).toBeTruthy();
      username.simulate('change', createInputEvent('username', 'valid'));
      password.simulate('change', createInputEvent('password', 'valid'));
      submit = wrapper.find('[type="submit"]');
      expect(submit.prop('disabled')).toBeFalsy();
    });
    it('should disable Submit when any one of the fields is invalid', () => {
      let submit;
      const username = wrapper.find('#username');
      const password = wrapper.find('#password');
      username.simulate('change', createInputEvent('username', '!nvalid'));
      submit = wrapper.find('[type="submit"]');
      expect(submit.prop('disable')).toBeUndefined();
      password.simulate('change', createInputEvent('password', 'valid'));
      submit = wrapper.find('[type="submit"]');
      expect(submit.prop('disable')).toBeUndefined();
    });
  });
  describe('Submitting form data', () => {
    const fakeEvent = { preventDefault() {} };
    const successResponseInit = {
      status: 204,
      statusText: 'No Content',
    };
    const failedResponseInit = {
      status: 400,
      statusText: 'Bad Request',
    };
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<App />);
      wrapper.setState({
        formData: {
          username: 'foo',
          password: 'bar',
        },
        isFormValid: true,
      });
      fetch.resetMocks();
    });
    it('should submit form data', () => {
      fetch.mockResponse(JSON.stringify({}), successResponseInit);
      wrapper.find('form').simulate('submit', fakeEvent);
      expect(fetch).toBeCalled();
      expect(fetch.mock.calls[0][1].body).toBe(JSON.stringify({
        username: 'foo',
        password: 'bar',
      }));
    });
    it('should reset state on successful submission', () => {
      fetch.mockResponse(JSON.stringify({}), successResponseInit);
      wrapper.instance().handleSubmit(fakeEvent).then(() => {
        expect(wrapper.state()).toEqual(App.getDefaultState());
      });
    });
    it('should update state.errorResponse on failed submission', () => {
      fetch.mockResponse(JSON.stringify({}), failedResponseInit);
      wrapper.instance().handleSubmit(fakeEvent).then(() => {
        expect(wrapper.state('errorResponse')).toBe('400: Bad Request');
      });
    });
  });
});
