const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser'); // eslint-disable-line
const logger = require('morgan');


/*
Set up
 */

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(logger('dev'));

/*
User router
 */

const usersRouter = express.Router();
usersRouter.post('/',
(req, res, next) => {
  if (
    !req.body.username ||
    !req.body.password
  ) {
    const error = new Error('No empty field allowed');
    error.status = 400;
    next(error);
  }
  next();
},
(req, res) => {
  res.sendStatus(204);
});
app.use('/users', usersRouter);


/*
Error handling
 */

app.use((error, req, res, next) => { // eslint-disable-line
  res.status(error.status || 500);
  res.send(error.message);
});


/*
Finish up
 */

app.get('/', (req, res) => res.json({
  message: 'It works'
}));
app.listen('9000');
