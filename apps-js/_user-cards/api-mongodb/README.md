## Development

### Quick start

```sh
docker-compose up -d
npm i -g eslint-plugin-jest
npm i
npm run seed
npm start
```

### Docker swarm stack

**Launch database**

```sh
docker-compose up -d
```

**Access database admin UI**

`http://localhost:8081`

**Stop or remove the database**

```sh
docker-compose stop
docker-compose down
```

## API Documentation

```sh
npm run doc
serve doc
```
