const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser'); // eslint-disable-line
const morgan = require('morgan');
const mongoose = require('mongoose');


/*
Databases
 */

(async () => {
  try {
    const { url, db } = require('./config').mongo;
    await mongoose.connect(`${url}/${db}`);
    mongoose.set('debug', true);
    console.log(`Connected to database...`);
  } catch (err) {
    console.error(err);
  }
})();
require('./models');


/*
Init
 */

const app = express();


/*
Set up
 */

app.use(cors());
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(express.static('public'));


/*
Routes
 */

app.use('/', require('./routes'));


/*
Error handling
 */

app.use((error, req, res, next) => { // eslint-disable-line
  res.status(error.status || 500);
  res.send(error.message);
});


/*
Serve
 */
app.listen('8080');
