const mongoose = require('mongoose');


const FieldSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true,
    unique: true,
  },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now }
});


module.exports = mongoose.model('Field', FieldSchema);
