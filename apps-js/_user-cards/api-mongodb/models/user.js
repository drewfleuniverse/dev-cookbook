const mongoose = require('mongoose');
const { Schema } = mongoose;


const UserSchema = Schema({
  username: {
    type: String,
    required: true,
    index: true,
    unique: true,
  },
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  picture: { type: String },
  description: { type: String },
  fields: [{ type: Schema.Types.ObjectId, ref: 'Field' }],
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now }
});


module.exports = mongoose.model('User', UserSchema);
