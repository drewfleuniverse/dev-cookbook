const express = require('express');
const router = express.Router();
const User = require('../models/user');


router.get('/',
async (req, res, next) => {
  try {
    const users = await User.find({
      firstname: RegExp(req.query.firstname, 'i'),
      lastname: RegExp(req.query.lastname, 'i')
    }).populate({
      path: 'fields',
      select: 'name -_id'
    });
    return res.json(users);
  } catch (err) {
    return next(err);
  }
});


module.exports = router;
