const ids = require('./ids');


const fields = [{
  _id: ids.field.id1,
  name: 'painting',
}, {
  _id: ids.field.id2,
  name: 'print',
}, {
  _id: ids.field.id3,
  name: 'drawing',
}, {
  _id: ids.field.id4,
  name: 'sculpture',
}, {
  _id: ids.field.id5,
  name: 'installation',
}, {
  _id: ids.field.id6,
  name: 'video',
}];


module.exports = fields;
