const { ObjectId } = require('mongodb');


const generateIds = function (num) {
  const obj = {};
  for (let i = 1; i <= num; i++) {
    obj[`id${i}`] = new ObjectId();
  }
  return obj;
};


const field = generateIds(6);


module.exports = {
  field,
};
