const ids = require('./ids');


const users = [{
  username: 'quasi',
  firstname: 'Tiger',
  lastname: 'Balm1',
  picture: 'http://localhost:8080/images/tiger.jpg',
  description: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
  fields: [
    ids.field.id1,
    ids.field.id2,
  ]
}, {
  username: 'architecto',
  firstname: 'Tiger',
  lastname: 'Balm1',
  picture: 'http://localhost:8080/images/tiger.jpg',
  description: 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
  fields: [
    ids.field.id3,
  ]
}, {
  username: 'beatae',
  firstname: 'Tiger',
  lastname: 'Balm1',
  picture: 'http://localhost:8080/images/tiger.jpg',
  description: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  fields: [
    ids.field.id3,
    ids.field.id6,
  ]
}, {
  username: 'vitae',
  firstname: 'Tiger',
  lastname: 'Balm1',
  picture: 'http://localhost:8080/images/tiger.jpg',
  description: 'Et harum quidem rerum facilis est et expedita distinctio.',
  fields: [
    ids.field.id2,
    ids.field.id4,
  ]
}, {
  username: 'dicta',
  firstname: 'Tiger',
  lastname: 'Balm1',
  picture: 'http://localhost:8080/images/tiger.jpg',
  description: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  fields: [
    ids.field.id5,
  ]
}];


module.exports = users;
