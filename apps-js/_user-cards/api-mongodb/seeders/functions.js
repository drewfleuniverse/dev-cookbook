const MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const { mongo } = require('../config');


const countSeeds = function (results, ...names) {
  const outputs = []
  results.forEach((result, i) => {
    outputs.push(`Collection ${
      names[i]
    }: ${
      result.length
    }`);
  });
  console.log(outputs.join('\n')); // eslint-disable-line
  return outputs;
};


const resetDb = async function (driver=MongoClient) {
  try {
    const client = await driver.connect(mongo.url);
    const db = client.db('appDev');
    const collections = await db.collections();
    if (collections.length) {
      await db.dropDatabase();
    }
    client.close();
  } catch (err) {
    console.error(err); // eslint-disable-line
  }
};


const seedDb = async function (driver=mongoose) {
  try {
    await driver.connect(`${mongo.url}/${mongo.db}`);
    const results = await Promise.all([
      require('../models/user').insertMany(require('./data/users')),
      require('../models/field').insertMany(require('./data/fields')),
    ]);
    countSeeds(
      results,
      'users',
      'fields'
    );
    driver.connection.close();
    return results;
  } catch (err) {
    console.error(err); // eslint-disable-line
    return err;
  }
};


module.exports = {
  countSeeds,
  resetDb,
  seedDb,
};
