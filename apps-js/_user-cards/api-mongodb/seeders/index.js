const {
  resetDb,
  seedDb,
} = require('./functions');

(async function () {
  await resetDb();
  await seedDb();
})();
