const {
  countSeeds,
  resetDb,
  seedDb,
} = require('../../seeders/functions');


describe('countSeeds()', () => {
  test('should return correct outputs', () => {

    const result = countSeeds(
      [[{}], [{}, {}]],
      'foo',
      'bar'
    );
    expect(result.join()).toBe('Collection foo: 1,Collection bar: 2');
  });
});


test('resetDb()', () => {
  expect(0).toBe(1);
});


test('seedDb()', () => {
  expect(0).toBe(1);
});
