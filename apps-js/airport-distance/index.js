if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}


const form = document.getElementById('form');
const from = document.getElementById('from');
const to = document.getElementById('to');
const fromError = document.getElementById('from-error');
const toError = document.getElementById('to-error');
const fromDatalist = document.getElementById('from-datalist');
const toDatalist = document.getElementById('to-datalist');
const result = document.getElementById('distance');
const airportMap = new Map();

let mapCanvas;
let flightPath;


/*
Populate datalist
- Store airports to a hash table due to limited event access in datalists
- Create options and append to datalists
 */

getAirports().then(json => {
  const fragment = document.createDocumentFragment();

  for (airport of json) {
    const {latitude, longitude} = airport
    const name = formatAirportString(airport);
    const option = document.createElement('option');

    airportMap.set(name, [latitude, longitude]);

    option.value = name;
    fragment.appendChild(option);
  }

  fromDatalist.appendChild(fragment.cloneNode(true));
  toDatalist.appendChild(fragment);
});


/*
- Reset distance result
- Clear path on google map
- Clear error messages
 */

form.addEventListener('input', function (e) {
  if (
    e.target === from ||
    e.target === to
  ) {
    if (result.textContent !== '--') {
      result.textContent = '--';
    }

    if (flightPath !== undefined) {
      flightPath.setMap(null);
    }
  }

  if (e.target === from) {
    resetInputErrorMessage(fromError);
  }

  if (e.target === to) {
    resetInputErrorMessage(toError);
  }
});


/*
- Input validation
- Calculate distance
- Plot path on google map
 */

form.addEventListener('submit', function (e) {
  e.preventDefault();

  if (!airportMap.has(from.value)) {
    addInputErrorMessage(fromError);
    return;
  }

  if (!airportMap.has(to.value)) {
    addInputErrorMessage(toError);
    return;
  }

  result.textContent = calcDistance(
    ...airportMap.get(from.value),
    ...airportMap.get(to.value)
  );

  plot(
    ...airportMap.get(from.value),
    ...airportMap.get(to.value)
  );

  return false;
});


/*
Async call to an API endpoint
 */

async function getAirports() {
  const response = await fetch('./airports.json');
  const json = await response.json();

  return json;
};


/*
Calculate distance in nautical miles
 */

function calcDistance(lat1, lon1, lat2, lon2) {
  const nauticalMileRadius = 3440.064795;
  const lat1Rad = lat1.toRad();
  const lat2Rad = lat2.toRad();
  const diffLatRad = (lat2-lat1).toRad();
  const diffLonRad = (lon2-lon1).toRad();
  const a = Math.sin(diffLatRad/2) * Math.sin(diffLatRad/2) +
        Math.cos(lat1Rad) * Math.cos(lat2Rad) *
        Math.sin(diffLonRad/2) * Math.sin(diffLonRad/2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  const distance = nauticalMileRadius * c;

  return distance.toFixed(2);
};


/*
Initialize google map
 */

function initMap() {
  mapCanvas = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: {lat: 40.730610, lng: -73.935242},
    mapTypeId: 'terrain',
    draggable: false,
    disableDefaultUI: true,
  });
}


/*
- Plot path on google map
- Fix weird behavior when bound locations are equal
- Fix bound order to prevent plotted path splitted into two
 */

function plot(lat1, lon1, lat2, lon2) {
  const flightPlanCoordinates = [
    {lat: lat2, lng: lon2},
    {lat: lat1, lng: lon1},
  ];

  flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  flightPath.setMap(mapCanvas);

  if (
    lat2-lat1 === 0 &&
    lon2-lon1 === 0
  ) {
    mapCanvas.setCenter({lat: lat1, lng: lon1});
  } else {
    let bounds;

    if (lon1 < lon2) {
      bounds = new google.maps.LatLngBounds(
        {lat: lat1, lng: lon1},
        {lat: lat2, lng: lon2},
      );
    } else {
      bounds = new google.maps.LatLngBounds(
        {lat: lat2, lng: lon2},
        {lat: lat1, lng: lon1},
      );
    }

    mapCanvas.fitBounds(bounds);
  }
}


/*
Concat airport name and codes for easier searching
 */

function formatAirportString(airport) {
  const code = `${
    airport.iata
      ? airport.iata
      : ''
    }/${
      airport.iaco
      ? airport.iaco
      : ''
    }`;

  return `${airport.city}, ${airport.name} (${code})`;
}


/*
Reset error massage span
 */

function resetInputErrorMessage(error) {
  if (error.classList.contains('active')) {
    error.classList.remove('active');
    error.textContent = '';
  }
}


/*
Add error massage to span
 */

function addInputErrorMessage(error) {
  error.classList.add('active');
  error.textContent = '*airport does not exist*';
}
