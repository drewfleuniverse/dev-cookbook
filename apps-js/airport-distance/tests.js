/*
Tests
 */

describe('getAirports()', () => {
  it('returns airports', () => {
    getAirports().then(json => {
      assert(Object.keys(json[0]).toString() === 'airportId,name,city,iata,iaco,latitude,longitude');
    });
  });
});

describe('calcDistance()', () => {
  const jfk = [40.639751, -73.778925];
  const lax = [33.942536, -118.408075];

  it('calculates correct distance', () => {
    assert(calcDistance(...jfk, ...lax) === '2145.90');
    assert(calcDistance(...jfk, ...jfk) === '0.00');
  });
});

describe('Manage error messages', () => {
  const createSpan = (className='', textContent='') => {
    const span = document.createElement('span');
    span.className = `error ${className}`;
    span.textContent = textContent;

    return span;
  };

  describe('resetInputErrorMessage()', () => {
    it('resets error span', () => {
      const span = createSpan('active', 'some errors');
      resetInputErrorMessage(span);
      assert(span.className === 'error');
      assert(span.textContent === '');
    });
  });

  describe('addInputErrorMessage()', () => {
    it('adds active class and message to error span', () => {
      const span = createSpan();
      addInputErrorMessage(span);
      assert(span.className === 'error active');
      assert(span.textContent !== '');
    });
  });
});


/*
Utils
 */

function describe(c,e){console.log('%c'+c,'font-weight: bold;'),e()};
function it(c,e){console.log('%c'+c,'color:grey;'),e()};
function assert(c,e=!1,f=''){if(e&&console.log(`Assertion log: ${c}`),!c)throw new Error(f)};
