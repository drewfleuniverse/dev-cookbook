const config = {
  env: {
    dev: {
      mongo: {
        url: 'mongodb://root:password@localhost',
        db: 'appDev',
        options: 'authSource=admin'
      }
    },
    test: {
      mongo: {
        url: 'mongodb://root:password@localhost:27017',
        db: 'appTest',
        options: 'authSource=admin'
      }
    }
  }
};

module.exports = config;
