const countSeeds = require('../countSeeds');

describe(__filename, () => {
  const results = [{ length: 7 }, { length: 42 }];
  const names = ['users', 'posts'];
  it('should count seeds', () => {
    const outputs = countSeeds(results, names);
    expect(outputs.toString()).toBe('Collection users: 7,Collection posts: 42');
  });
});
