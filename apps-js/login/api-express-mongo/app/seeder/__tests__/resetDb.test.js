const { MongoClient } = require('mongodb');
const resetDb = require('../resetDb');

describe(__filename, () => {
  const tempDb = 'temp';
  const mongoConfig = {
    url: global.__MONGO_URI__.substring(0, global.__MONGO_URI__.length - 5),
    db: tempDb,
    options: 'authSource=admin'
  };
  const findTempDb = el => el.name === tempDb;
  let connection;
  let db;
  let users;

  beforeAll(async () => {
    connection = await MongoClient.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true }
    );
    db = await connection.db(tempDb);
    users = db.collection('users');
  });

  afterAll(async () => {
    await db.dropDatabase(tempDb);
    await db.close();
    await connection.close();
  });

  beforeEach(async () => {
    const fakeUser = { _id: 'fakeId', name: 'fakeName' };
    await users.insertOne(fakeUser);
  });

  afterEach(async () => {
    await users.deleteOne({ _id: 'fakeId' });
  });

  it('should insert a doc into collection', async () => {
    const adminDb = db.admin();
    const { databases: beforeReset } = await adminDb.listDatabases();
    expect(beforeReset.some(findTempDb)).toBeTruthy();

    await resetDb(mongoConfig);
    const { databases: afterReset } = await adminDb.listDatabases();
    expect(afterReset.some(findTempDb)).toBeFalsy();
  });

  it('should handle error', async () => {
    try {
      await resetDb({ url: '', db: '' });
      expect(true).toBe(false);
    } catch (e) {
      expect(e.message).toBe('Invalid connection string');
    }
  });
});
