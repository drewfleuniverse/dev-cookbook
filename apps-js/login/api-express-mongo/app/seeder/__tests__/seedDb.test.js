const { MongoClient } = require('mongodb');
const mongoose = require('mongoose');
const seedDb = require('../seedDb');
const UserModel = require('../../models/user');

describe(__filename, () => {
  const tempDb = 'temp';
  const mongoConfig = {
    url: global.__MONGO_URI__.substring(0, global.__MONGO_URI__.length - 5), // move into util
    db: tempDb,
    options: 'authSource=admin'
  };
  const fakeUser = {
    username: 'fakeuser',
    firstname: 'Fake',
    lastname: 'User',
    password: 'password'
  };
  const seeders = [
    {
      name: 'users',
      model: UserModel,
      data: [fakeUser]
    }
  ];

  beforeAll(async () => {
    await mongoose.connect(
      `${mongoConfig.url}/${mongoConfig.db}`,
      { useNewUrlParser: true }
    );
  });

  afterAll(async () => {
    await UserModel.remove();
    await mongoose.disconnect();
  });

  it('should seed data', async () => {
    await seedDb(seeders, mongoConfig);
    expect(await UserModel.findOne()).toMatchObject(fakeUser);
  });

  it('should handle error', async () => {
    // try {
    //   const invalidSeeder = {};
    //   await seedDb(invalidSeeder);
    //   expect(true).toBe(false);
    // } catch (e) {
    //   expect(e.message).toBe('Invalid connection string');
    // }
  });
});
