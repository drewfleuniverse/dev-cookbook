const countSeeds = function(results, names) {
  const outputs = [];
  results.forEach((result, i) => {
    outputs.push(`Collection ${names[i]}: ${result.length}`);
  });
  console.log(outputs.join('\n')); // eslint-disable-line
  return outputs;
};

module.exports = countSeeds;
