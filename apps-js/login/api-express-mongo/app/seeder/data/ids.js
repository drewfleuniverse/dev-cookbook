const { ObjectId } = require('mongodb');


const generateIds = num => {
  const obj = {};
  for (let i = 1; i <= num; i++) {
    obj[`id${i}`] = new ObjectId();
  }
  return obj;
};


const userIds = generateIds(6);


module.exports = {
  userIds,
};
