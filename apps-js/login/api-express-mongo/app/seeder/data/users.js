const { userIds } = require('./ids');

const users = [
  {
    _id: userIds.id1,
    username: 'user1',
    firstname: 'User',
    lastname: 'One',
    password: 'password'
  },
  {
    _id: userIds.id2,
    username: 'user2',
    firstname: 'User',
    lastname: 'Two',
    password: 'password'
  },
  {
    _id: userIds.id3,
    username: 'user3',
    firstname: 'User',
    lastname: 'Three',
    password: 'password'
  }
];

module.exports = users;
