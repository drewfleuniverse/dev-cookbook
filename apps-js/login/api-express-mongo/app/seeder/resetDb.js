const { MongoClient } = require('mongodb');

/**
 * my resetDb
 * @param {object} confs
 */
const resetDb = async ({ url, db }) => {
  try {
    const client = await MongoClient.connect(
      `${url}/${db}`,
      { useNewUrlParser: true }
    );
    const database = client.db(db);
    const collections = await database.collections();
    /* istanbul ignore else */
    if (collections.length) {
      await database.dropDatabase();
    }
    client.close();
    console.log(`Successfully reset ${url}/${db}`); // eslint-disable-line
  } catch (err) {
    console.error(err); // eslint-disable-line
    throw err;
  }
};

module.exports = resetDb;
