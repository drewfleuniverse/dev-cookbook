const mongoose = require('mongoose');
const { mongo } = require('../config/env');

const count = (results, ...names) => {
  const outputs = [];
  results.forEach((result, i) => {
    outputs.push(`Collection ${names[i]}: ${result.length}`);
  });
  console.log(outputs.join('\n')); // eslint-disable-line
  return outputs;
};

const seed = async (seeders, driver = mongoose) => {
  try {
    await driver.connect(`${mongo.url}/${mongo.db}`);
    const results = await Promise.all(
      seeders.map(seeder => require(seeder.model).insertMany(require(seeder.data)))
      // [
      // require('../models/user').insertMany(require('./data/users')),
      // require('../models/field').insertMany(require('./data/fields'))
      // ]
    );
    count(results, 'users', 'fields');
    driver.connection.close();
    return results;
  } catch (err) {
    console.error(err); // eslint-disable-line
    return err;
  }
};

module.exports = {
  count,
  seed
};
