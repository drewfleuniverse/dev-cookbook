const countSeeds = require('./countSeeds');

const seedDb = async function(seeders) {
  try {
    // await mongoose.connect(
    //   `${url}/${db}`,
    //   { useNewUrlParser: true }
    // );
    const results = await Promise.all(seeders.map(seeder => seeder.model.insertMany(seeder.data)));
    countSeeds(results, seeders.map(seeder => seeder.name));
    // mongoose.connection.close();
  } catch (err) {
    console.error(err); // eslint-disable-line
    throw err;
  }
};

module.exports = seedDb;
