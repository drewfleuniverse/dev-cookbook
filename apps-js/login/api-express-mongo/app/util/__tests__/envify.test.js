const envify = require('../envify');

describe('envify', () => {
  let curEnv;

  beforeAll(() => {
    curEnv = process.env.NODE_ENV;
  });

  afterAll(() => {
    process.env.NODE_ENV = curEnv;
  });

  const env = {
    prod: { url: 'foo.com/prod' },
    dev: { url: 'foo.com/dev' },
    test: { url: 'foo.com/test' }
  };

  it(`should return production env`, () => {
    process.env.NODE_ENV = 'production';
    expect(envify({ env })).toEqual(env.prod);
  });

  it(`should return development env`, () => {
    process.env.NODE_ENV = 'development';
    expect(envify({ env })).toEqual(env.dev);
  });

  it(`should return test env`, () => {
    process.env.NODE_ENV = 'test';
    expect(envify({ env })).toEqual(env.test);
  });

  it(`should return development env if NODE_ENV has no match`, () => {
    process.env.NODE_ENV = undefined;
    expect(envify({ env })).toEqual(env.dev);
  });
});
