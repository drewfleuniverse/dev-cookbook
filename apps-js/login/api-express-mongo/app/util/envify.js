const envify = ({ env }) => {
  const { NODE_ENV } = process.env;
  let result;
  switch (NODE_ENV) {
    case 'production':
      result = env.prod;
      break;
    case 'development':
      result = env.dev;
      break;
    case 'test':
      result = env.test;
      break;
    default:
      result = env.dev;
  }
  return result;
};

module.exports = envify;
