# Browser Compatibility


## HTML5 Elements

**Use in older browsers**

```css
header, section, footer, aside, nav, main, article, figure {
    display: block;
}
```

**Use in older IE**

```js
document.createElement('main');
// ...
```


## Validation

- [W3C HTML validator Nu](https://validator.w3.org/nu/)


## Feature lookup and detection

- [Can I use...](https://caniuse.com)
- [MDN Web Docs](https://developer.mozilla.org)
- [compat-table](https://kangax.github.io/compat-table/es6/)
- [modernizr](https://github.com/Modernizr/Modernizr)
    - HTML5 and CSS3 feature detection
    - Conditionally load third-party shims and polyfills


## Build systems

**Webpack**

- [postcss-loader](https://github.com/postcss/postcss-loader)
- [babel-loader](https://github.com/babel/babel-loader)
- [ts-loader](https://github.com/TypeStrong/ts-loader)
- [flowtype-loader](https://github.com/torifat/flowtype-loader)

**Gulp**

- [gulp-postcss](https://github.com/postcss/gulp-postcss)


## Comprehensive Tools

**JavaScript**

- [Babel/Env preset](https://babeljs.io/docs/plugins/preset-env/)
    - Allows specifying browser support using `browserlist`

**CSS**

- [PostCSS](http://postcss.org/)
    - [autoprefixer](https://github.com/postcss/autoprefixer)
    - [postcss-cssnext](https://github.com/MoOx/postcss-cssnext)
    - [css-modules](https://github.com/css-modules/css-modules)
    - [stylelint](https://stylelint.io/)
    - [lost](https://github.com/peterramsing/lost)


## Minify

**CSS**

- [cssnano](https://github.com/ben-eb/cssnano)


## Shims and polyfills

**HTML**

- [html5shiv](https://github.com/afarkas/html5shiv)
    - HTML5: IE 6-9, iPhone 3, Safari 4, and Firefox 3

**CSS**

- [normalize](https://github.com/necolas/normalize.css/)
    - IE 10, Safari 9
- [css3pie](https://github.com/lojjic/PIE)
    - IE 6-9
