# DOM CRUD and Traversal


**Create**

```ts
Document.createElement(tag: string): Element // DOM?
Document.createTextNode(text: string): Text // DOM2
Document.createDocumentFragment(): DocumentFragment // DOM?
```


**Read**

```ts
Document.getElementById(): Element | null // DOM1

Document.getElementsByClassName(): HTMLCollection // DOM?
Element.getElementsByClassName(): HTMLCollection // DOM?

Document.getElementsByTagName(): HTMLCollection // DOM2
Element.getElementsByTagName(): HTMLCollection // DOM2

Document.querySelector(): Element | null // DOM?
Element.querySelector(): Element | null // DOM?

DocumentFragment.querySelector(): Element | null // DOM?

Document.querySelectorAll(): NodeList // DOM4
Element.querySelectorAll(): NodeList // DOM4
DocumentFragment.querySelectorAll(): NodeList | null // DOM4
ParentNode.querySelectorAll(): NodeList // DOM4
```


**Update**

```ts
Node.nodeValue: string | null // DOM1, Writable
Node.textContent: string // DOM?, Writable
Node.innerHTML: string // DOM?, Writable

Node.appendChild(new: Node): Node // DOM1
Node.insertBefore(new: Node, prevSibling: Node): Node // DOM1
Node.replaceChild(new: Node, old: Node): Node // DOM1
```


**Remove**

```ts
Node.removeChild(child: Node): Node // DOM1
ChildNode.remove(): undefined // DOM4
```


**Traversal**

```ts
Node.parentNode: Node // DOM2
Node.childNodes: NodeList // DOM1, live collection
Node.firstChild: Node | null  // DOM1
Node.lastChild: Node | null  // DOM1
Node.previousSibling: Node | null // DOM1
Node.nextSibling: Node | null // DOM1

Node.parentElement: Element | null // DOM4
ParentNode.children: HTMLCollection // DOM4, live collection
ParentNode.firstElementChild: Element | null // DOM4
ParentNode.lastElementChild: Element | null // DOM4
ParentNode.previousElementSibling: Element | null // DOM4
ParentNode.nextElementSibling: Element | null // DOM4
```


**Node types**

```ts
Node.ELEMENT_NODE // 1
Node.TEXT_NODE // 3
Node.DOCUMENT_TYPE_NODE // 10
Node.DOCUMENT_NODE // 9
Node.DOCUMENT_FRAGMENT_NODE // 11
```


**Node attributes**

```ts
Node.nodeType: Number
Element.attributes: NamedNodeMap
Element.id: string | '' // RW
Element.getAttribute(name: string): string | '' | null
Element.setAttribute(name: string, value: string): undefined
Element.hasAttribute(name: string): boolean
Element.removeAttribute(name: String): undefined
Element.className: string // RW
Element.classList: DOMTokenList // R
HTMLElement.style: CSS2Properties | CSSStyleDeclaration // R
Window.getComputedStyle(elem [, pseudoElem]): CSSStyleDeclaration
```

`Element.classList: DOMTokenList`
```ts
DOMTokenList.add( String [, String [, ...]] )
DOMTokenList.remove( String [, String [, ...]] )
DOMTokenList.item( Number )
DOMTokenList.toggle( String [, force] )
DOMTokenList.contains( String )
DOMTokenList.replace( oldClass, newClass )
```

`HTMLElement.style: CSS2Properties | CSSStyleDeclaration`
```ts
```
