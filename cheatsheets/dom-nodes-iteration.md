```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" href="">
  <title>Document</title>
  <script>
  const describe = function (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
  const it = function (s,f) {console.log('%c'+s,'color:grey;');f();};
  const assert = function (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
  </script>
</head>
<body>
<script>
const filledUl = function () {
  const ul = document.createElement('ul');
  ul.innerHTML = '<li>A</li>X<li>B</li>Y<li>C</li>Z';

  return ul;
};


describe('Iterate through NodeList', () => {
  it('for of, ES6', () => {
    const ul = filledUl();
    const r = [];

    for (const node of ul.childNodes) {
      if (node.nodeType === 1) {
        r.push(node.textContent);
      } else if (node.nodeType === 3) {
        r.push(node.data);
      }
    }

    assert(r.toString() === 'A,X,B,Y,C,Z');
  });

  it('forEach, Array.from, ES6', () => {
    const ul = filledUl();
    const r = [];

    const children = Array.from(ul.childNodes);
    children.forEach(chi => {
      if (chi.nodeType === 1) {
        r.push(chi.textContent);
      } else if (chi.nodeType === 3) {
        r.push(chi.data);
      }
    });

    assert(r.toString() === 'A,X,B,Y,C,Z');
  });

  it('for, ES5', () => {
    const ul = filledUl();
    const r = [];

    const children = ul.childNodes;
    for (let i = 0; i < children.length; i++) {
      const chi = children[i];

      if (chi.nodeType === 1) {
        r.push(chi.textContent);
      } else if (chi.nodeType === 3) {
        r.push(chi.data);
      }
    }

    assert(r.toString() === 'A,X,B,Y,C,Z');
  });

  it('for, nextSibling, ES5', () => {
    const ul = filledUl();
    const r = [];

    for (let chi = ul.firstChild; chi !== null; chi = chi.nextSibling) {
      if (chi.nodeType === 1) {
        r.push(chi.textContent);
      } else if (chi.nodeType === 3) {
        r.push(chi.data);
      }
    }

    assert(r.toString() === 'A,X,B,Y,C,Z');
  });

  it('forEach, slice, ES5', () => {
    const ul = filledUl();
    const r = [];

    const children = Array.prototype.slice.call(ul.childNodes);
    children.forEach(chi => {
      if (chi.nodeType === 1) {
        r.push(chi.textContent);
      } else if (chi.nodeType === 3) {
        r.push(chi.data);
      }
    });

    assert(r.toString() === 'A,X,B,Y,C,Z');
  });
});

describe('Iterate through HTMLCollection', () => {
  it('for of, ES6', () => {
    const ul = filledUl();
    const r = [];

    for (const node of ul.children) {
      r.push(node.textContent);
    }

    assert(r.toString() === 'A,B,C');
  });

  it('for, ES5', () => {
    const ul = filledUl();
    const r = [];

    for (let i = 0; i < ul.children.length; i++) {
      r.push(ul.children[i].textContent);
    }

    assert(r.toString() === 'A,B,C');
  });

  it('forEach, ES6', () => {
    const ul = filledUl();
    const r = [];

    const children = Array.from(ul.children);
    children.forEach(chi => {
      r.push(chi.textContent);
    });

    assert(r.toString() === 'A,B,C');
  });
});

</script>
</body>
</html>
```
