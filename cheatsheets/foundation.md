# Foundation


## Punctuation

- `()`: parentheses, parens *pehRen*
    - `(`: open paren
    - `)`: close paren
- `[]`: square brackets, brackets
    - `[`: open bracket
    - `]`: close bracket
- `{}`: curly brackets
    - `{`: open curly
    - `}`: close curly
- `<>`: angle brackets
    - `<`: open angle, less than
    - `>`: close angle, greater than
- `|`: pipe
- `"`: double quote
- `'`: single quote
- `:`: colon
- `;`: semicolon, sem
- `!`: exclamation mark, bang, not
- `^`: caret, hat
- `°`: degree, degrees, degree sign
- `#`: pound / number / sharp / hash sign
- \`: back tick
- ´ : tick
- `_`: underline, underscore
- `.`: dot
- `,`: comma
- `&`: ampersand, and
- `$`: dollar sign
- `\`: back slash
- `/`: slash, division operator
- `+`: plus
- `++`: two plus signs
- `-`: hyphen, minus
- `--`: two hyphens
- `*`: asterisk, multiplication operator
- `%`: percent sign, remainder / modulo / mod operator
- `=`: equals sign
- `~`: twiddle, tilde
