# HTML Forms


**A proper form**

```html
<form id="form">
  <div class="form-group">
    <label for="form__text">Username</label>
    <input id="form__text" type="text" name="username">
  </div>

  <div class="form-group">
    <label for="form__password">Password</label>
    <input id="form__password" type="password" name="password">
  </div>

  <div class="form-group">
    <label for="form__file">Picture</label>
    <input id="form__file" type="file" name="picture">
  </div>

  <div class="form-group">
    <label for="form__datalist">City</label>
    <input id="from__datalist" list="datalist-city" name="city">
    <datalist id="datalist-city">
      <option value="New York">
      <option value="Seattle">
      <option value="San Francisco">
    </datalist>
  </div>

  <div class="form-group">
    <label for="form__select">Country</label>
    <select id="form__select" type="select" name="country">
      <option value="us" selected>US</option>
      <option value="uk">UK</option>
      <option value="canada" >Canada</option>
    </select>
  </div>

  <!-- Checkboxes allow multiple choices -->
  <p>Interests:</p>
  <div class="form-group">
    <input id="form__checkbox1" type="checkbox" name="interests" value="politics">
    <label for="form__checkbox1">Politics</label>

    <input id="form__checkbox2" type="checkbox" name="interests" value="technology">
    <label for="form__checkbox2">Technology</label>

    <input id="form__checkbox3" type="checkbox" name="interests" value="culture">
    <label for="form__checkbox3">Culture</label>
  </div>

  <!-- Radio buttons only allow a single choice -->
  <p>Preferred contact method:</p>
  <div class="form-group">
    <input id="form__radio1" type="radio" name="contact" value="phone">
    <label for="form__radio1">Phone</label>

    <input id="form__radio2" type="radio" name="contact" value="email">
    <label for="form__radio2">Email</label>
  </div>

  <p>Notes:</p>
  <div class="form-group">
    <textarea id="form__textarea" name="textarea" placeholder="Notes..."></textarea>
  </div>

  <p>Reset:</p>
  <div class="form-group">
    <input id="form__button" type="button" value="Reset">
  </div>

  <div class="form-group">
    <input type="submit" value="Submit">
  </div>
</form>
```

```js
const form = document.getElementById('form');
const resetButton = document.getElementById('form__button');
form.addEventListener('click', function (e) {
  if (e.target === resetButton) {
    for (const el of form.elements) {
      if (el.name === 'country') {
        el[0].selected = true;
      }
      if (
        el.type === 'text' ||
        el.type === 'password' ||
        el.type === 'file' ||
        el.type === 'select' ||
        el.type === 'textarea' ||
        !!el.list
      ) {
        el.value = '';
      } else if (
        el.type === 'checkbox' ||
        el.type === 'radio'
      ) {
        el.checked = false;
      }
    }
  }
});



form.addEventListener('submit', function (e) {
  e.preventDefault();
  const formData = new FormData(form);

  // if (formData.has('picture')) {
  //   console.log(formData.get('picture').size === 0);
  // }

  fakeFetch('https://foo.com', {
    method: 'POST',
    body: formData
  }).then(response => {
    console.log([...response.keys()].toString())
    // username,password,picture,city,country,textarea
    console.log([...response.values()].toString())
  });
});
```
