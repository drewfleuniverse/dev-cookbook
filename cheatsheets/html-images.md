# HTML Images


**`<img>`**

```html
<img
  src="https://foo.com/img.png"
  alt="text"
>
```

**`<figure>`, `<figcaption>`**

```html
<figure>
  <img
    src="https://foo.com/img.png"
    alt="text"
  >
  <figcaption>Caption</figcaption>
</figure>
```
