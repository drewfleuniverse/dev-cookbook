# HTML Tables


**A minimal table**

```html
<table>
  <tr>
    <td>Data</td>
  </tr>
</table>
```

**A proper table**

```css
table{
  /* Equally distribute col width within table width; default: auto */
  table-layout: fixed;
  /* Collapse borders; default: separate  */
  border-collapse: collapse;
  width: 100%;
}
table, th, td {
  border: 1px solid black;
}
```

```html
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>BOD</th>
      <th>Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Kate</th>
      <td>01/01/2001</td>
      <td>Some notes</td>
    </tr>
    <tr>
      <th>Primary data 2</th>
      <td>02/02/2002</td>
      <td>Some notes</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th>Person Count</td>
      <td colspan="2">2</td>
    </tr>
  </tfoot>
</table>
```
