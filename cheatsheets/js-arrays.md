# JavaScript Arrays


## Usage and operators

```js
[] // new array
['x', 'y'][0] // access element at index
new Array(42) // new array with length 42
new Array(1, 2) // [1,2]
[...arr] // clone
[...arr1, ...arr2] // concat
```
```ts
new Array(element0, element1[, ...[, elementN]])
new Array(arrayLength)
```


## Common properties and methods

```ts
array.length

Array.of(elem1[, ...[, elemN]]): newArray
Array.from(arrayLikeObject[, mapFn[, thisArg]]): newArray
Array.isArray(anyType): boolean

// Accessor
arrar.slice([beginIndexNumber=0[, endIndexNumber]]): newArray
array.concat(array1 | anyOtherType1[, ...[, arrayN | anyOtherTypeN]]): newArray

arrar.join([separatorString=',']): string
array.toString(): string

array.includes(elem[, startIndex]): boolean
array.indexOf(elem[, startIndex]): index | -1
array.lastIndexOf(elem[, startIndex]): index | -1

// Iteration
array.forEach(function callback(elem[, index[, array]]) {
  // do something
}[, thisArg]): void

array.map(function callback(elem[, index[, array]]) {
  // return processed element
}[, thisArg]): newArray

array.reduce(function callback(accumulator, elem[, index]) {
  // return processed accumulator
}[, initialValue]): accumulatedValue

array.find(callback[, thisArg]): elem | undefined
array.findIndex(callback[, thisArg]): index | -1
array.filter(callback(currentValue[, index[, array]])[, thisArg]): newArray
array.every(callback[, thisArg]): boolean
array.some(callback[, thisArg]): boolean

array.entries(): iterator
array.values(): iterator
array.keys(): iterator

// Mutators
array.sort([compareFn]): modifiedArray // T: O(n^2), S: O(n)
array.reverse(): modifiedArray // T: O(n), S: O(1)

array.fill(value[, startIndex[, endIndex]]): modifiedArray // T: O(n), S: O(1)
array.splice(startIndex[, deleteCount[, elem1[, ...[, elemN]]]]) // T: O(n), S: O(1)

array.pop(): removedElem | undefined // T: O(1), S: O(1)
array.push(elem1[, ...[, elemN]]): modifiedArrayLength // T: O(1), S: O(1)

arr.shift(): removedElem | undefined // T: O(n), S: O(1)
arr.unshift(elem1[, ...[, elemN]]): modifiedArrayLength // T: O(n), S: O(1)
```


## Notes

**Get indices of a sparse array**

```js
const array = [1,,3,,5];
assert(Object.keys(array).toString() === '0,2,4');
```

**Convert an array-like object to an array**

```js
const arrayLike = {
  0: 'a',
  1: 'b',
  length: 2,
};

assert(Array.from(arrayLike).toString() === 'a,b');
assert(Array.prototype.map.call(arrayLike, el => el).toString() === 'a,b');
```

**Fill pre-allocated array with reference values**

```js
// fill() only copies reference
// forEach(), map(), for...of are merely accessors
it('use standard for loop instead', () => {
  const array = new Array(3);
  for (let i = 0; i < array.length; i++) {
    array[i] = [];
  }
  array[0].push('x');
  assert(array.toString() === 'x,,');
});
```
