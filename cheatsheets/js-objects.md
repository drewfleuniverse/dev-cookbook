# JavaScript Objects


## Usage and operators

```js
{}; // new object
let a = 1, b = 2; { a, b }; // new object using shorthand
{ [new Date()]: 3 } // new object using computed prop

({ a: 1, b: 2 }).a; // access value at key
({ a: 1, b: 2 })['a']; // access value at key
{ ...obj }; // clone
{ ...obj1, ...obj2 }; // concat

'prop' in obj // check if a prop or symbol is in obj prototype chain
obj instanceof CtorFunc // check if CtorFunc is one of the constructor functions of obj in its prototype chain

delete obj.a // delete a prop
delete obj['b'] // delete a prop
```


## Common properties and methods

```ts
Object.is(value1, value2): boolean // an enhanced ===; returns true for +/-0 and NaN

Object.assign(target, ...sources): target // only enumerable props and symbols; getters are reduced to props
Object.create(prototype | null[, descriptors]): newObject // preferred
Object.getPrototypeOf(object): prototype
Object.setPrototypeOf(object, prototype): self // slower than create()

Object.getOwnPropertyNames(object): array
Object.getOwnPropertySymbols(object): array

Object.keys(object): array // no symbol
Object.values(object): array // no symbol
Object.entries(object): array // no symbol

Object.defineProperty(object, propString | propSymbol, descriptor): self
Object.defineProperties(object, descriptors): self
Object.getOwnPropertyDescriptor(object, propString | propSymbol): descriptor | undefined
Object.getOwnPropertyDescriptors(object): descriptor

Object.preventExtensions(object): self
Object.isExtensible(object): boolean

Object.seal(object): self // props not configurable
Object.isSealed(object): boolean
Object.freeze(object): self // props not configurable and not writable
Object.isFrozen(object): boolean

object.constructor: CtorFunc | Function | Object
object.__proto__: CtorFunc.prototype | Function.prototype | Object.prototype | null

object.hasOwnProperty(propString | propSymbol): boolean
object.propertyIsEnumerable(propString | propSymbol): boolean

parentObject.isPrototypeOf(childObject): boolean
Parent.prototype.isPrototypeOf(childObject): boolean

object.toString(): string
object.valueOf(): primitiveValue : self
```


## Notes

**Object.create()**

Object prototype chain with very private props:

```js
const parent = { /* has methods */ };
const child = Object.create(
  parent,
  { /* prop descriptors */ }
);
```

Classic inheritance:

```js
Child.prototype = Object.create(Parent.prototype);
Child.prototype.constructor = Child;

// The above is equivalent to any one of the lines below:
class Child extends Parent { /*...*/ }
Object.setPrototypeOf(Child.prototype, Parent.prototype); // slow
Child.prototype.__proto__ = Parent.prototype; // slow
```

**Default values of a property descriptor**

```js
// For a prop
{
  enumerable: false,
  configurable: false,
  writable: false,
  value: undefined
}

// For a getter and a setter
{
  enumerable: false,
  configurable: false,
  get: undefined, // to use: get() { /*...*/ }
  set: undefined // to use: set() { /*...*/ }
}
```

**Declaring getter and setters**

```js
const sKey = Symbol('sKey');

const obj = {
  [sKey]: 9,
  get key() { return this[sKey]; },
  set key(key) { this[sKey] = key; }
};

const Ctor = function (key) {
  this[sKey] = key;
};
Ctor.prototype = Object.create({
  get key() { return this[sKey]; },
  set key(key) { this[sKey] = key; },
});
Ctor.prototype.constructor = Ctor;
// alternatively, Object.defineProperties() would work but slower than create()
```

**In-extensible vs sealed vs frozen**

- An in-extensible object:
    - No new props are allowed to be added to object
    - Existing normal props:
        - Enumerable
        - Writable
        - Configurable
            - Thus these props are deletable
- A sealed object:
    - Object: Not extensible
    - Normal props:
        - Enumerable
        - Writable
        - Not configurable
- A frozen object:
    - Object: Not extensible
    - Normal props:
        - Enumerable
        - Not writable
        - Not configurable

**valueOf() vs toString()**

```js
const obj = { a: 1 };
assert(obj.valueOf() === obj);
assert(obj.toString() === '[object Object]');
```
