# JavaScript OOP


**Some synonyms used in this document**

- Constructor functions: classes
- Objects:
    - Instances
    - Objects created by constructor functions
    - Objects created by classes
- Custom objects: objects created by user-defined constructor functions


## Prerequisites

- JavaScript utilizes prototype-based programming to achieve OOP
- A function is both of a function and an object, this holds true for both of functions and arrow functions
- All objects have a `[[Prototype]]` that:
    - Can be assessed using getter and setter method `__proto__`
    - Points to either a constructor function or `null`
        - E.g. `assert({}.__proto__ === Object.prototype)`
        - E.g. `assert(Object.prototype.__proto__ === null`


## Plain objects vs custom objects

**Normal uses**

- Plain objects are useful as containers for data
- Custom objects are useful as containers for both of data and custom behavior

**Other uses: plain objects**

```js
const factory = function (state=0) {
  const getState = function () {
    return this.state;
  };

  return {
    state,
    getState,
  };
};

const moduleFactory = (function () {
  let state = 0;
  const getState = () => state;

  return function() {
    return {
      getState,
    };
  };
})();
```


## Constructor functions and classes

**Constructor functions and classes are equivalent**

Classes are wrappers for constructor functions. The following snippets are equivalent:

```js
function Custom() {
  this.objectState;
}
Custom.prototype.prototypeState;
Custom.prototype.prototypeMethod = function () {};
```

```js
class Custom {
  constructor() {
    this.objectState;
  }
  prototypeMethod() {}
}
Custom.prototype.prototypeState;
```

**On `prototype`**

- A constructor function can only have one `prototype`
- `prototype` is an object that:
    - Will become the immediate `[[Prototype]]` of its instances
    - Should have a `constructor` property points to its constructor function
    - Has a getter and a setter method named `__proto__`


## Prototype-based inheritance

**Common prototype chain patterns**

- Default constructor functions:
    - `Function.prototype`
    - `Object.prototype`
    - `null`
- Objects created by custom constructor functions:
    - `CtorFunc1.prototype`
    - `CtorFunc2.prototype`
    - `...`
    - `Object.prototype`
    - `null`
- Plain objects:
    - `Object.prototype`
    - `null`
- N.b. The last `[[Prototype]]` must points to `null`

```js
const Custom = function () {};
const custom = new Custom();
const plain = {};

assert(Object.prototype.__proto__ === null);

assert(Object.__proto__ === Function.prototype);
assert(Object.__proto__.__proto__ === Object.prototype);
assert(Function.__proto__ === Function.prototype);
assert(Function.__proto__.__proto__ === Object.prototype);
assert(Custom.__proto__ === Function.prototype);
assert(Custom.__proto__.__proto__ === Object.prototype);

assert(custom.__proto__ === Custom.prototype);
assert(custom.__proto__.__proto__ === Object.prototype);
assert(plain.__proto__ === Object.prototype);
```

**Accessing `[[Prototype]]`**

```js
// Get [[Prototype]]:
const plain = {};
assert(plain.__proto__ === Object.prototype);
assert(Object.getPrototypeOf(plain) === Object.prototype);

// Set [[Prototype]]:
const proto1 = {};
plain.__proto__ = proto1;
assert(plain.__proto__ === proto1);

const proto2 = {};
Object.setPrototypeOf(plain, proto2);
assert(plain.__proto__ === proto2);
```

**Inheritance**

The following snippets are equivalent:

```js
const Parent = function () {};
const Child = function () {
  Parent.call(this);
}
Child.prototype.__proto__ = Parent.prototype;
```

```js
class Parent {}
class Child extends Parent {  
  constructor() {
    super();
  }
}
```
