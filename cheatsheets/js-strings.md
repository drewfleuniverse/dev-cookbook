# JavaScript Strings


## Operators

```js
'' // new string
'a' + 'b' // concat and return a new string
'ab'[0] // access char at index
```


## Common properties and methods

```ts
string.length

string.slice(beginIndexNumber[, endIndexNumber]): string
string.split([separatorString | separatorRegExp[, limitNumber]]): array
string.substr(startIndexNumber[, lengthNumber]): string
string.substring(startIndexNumber[, endIndexNumber]): string // slower than substr

string.includes(subString[, startIndexNumber]): boolean
string.indexOf(subString[, startIndexNumber]): indexNumber | -1
string.lastIndexOf(subString[, startIndexNumber]): indexNumber | -1

string.match(regExp): array | null
string.search(regExp): indexNumber | -1
string.replace(regExp | subString, newSubString | Function): string

String.fromCharCode(codeNumber1[, ...[, codeNumberN]]): string
string.charCodeAt(indexNumber=0): codeNumber

string.trim(): string
string.toUpperCase(): string
string.toLowerCase(): string

string.charAt(indexNumber=0): charString | emptyString
string.concat(string2[, string3, ..., stringN]): string // slower than +
```


## Notes

**`a-z` range**

```js
const start = 0;
const end = 'z'.charCodeAt() - 'a'.charCodeAt();
```

**`0-9`, 'A-Z', `a-z`**

```js
const ascii = new Array(128);

assert('0'.charCodeAt() === 48);
assert('9'.charCodeAt() === 57);

assert('A'.charCodeAt() === 65);
assert('Z'.charCodeAt() === 90);

assert('a'.charCodeAt() === 97);
assert('z'.charCodeAt() === 122);
```

**Common char tables**

```
int[26] for Letters 'a' - 'z' or 'A' - 'Z'
int[128] for ASCII
int[256] for Extended ASCII
```
