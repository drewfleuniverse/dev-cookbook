# React Lifecycle


## Cycles

**Instantiation**

```ts
constructor(props): void
+ static getDerivedStateFromProps(nextProps, prevState): stateChange | null
- UNSAFE_componentWillMount(): void
render(): React.Node
componentDidMount(): void // async, timers, invisible re-render
```

**Update state**

```ts
shouldComponentUpdate(nextProps, nextState, nextContext): boolean
+ static getDerivedStateFromProps(nextProps, prevState): stateChange | null
- UNSAFE_componentWillUpdate(nextProps, nextState): void // prep re-render
+ getSnapshotBeforeUpdate(prevProps, prevState): snapshot | null // prep re-render
render(): React.Node
componentDidUpdate(prevProps, prevState, prevContext | snapshot): void
```

**Update props**

```ts
shouldComponentUpdate(nextProps, nextState, nextContext): boolean
+ static getDerivedStateFromProps(nextProps, prevState): stateChange | null
- UNSAFE_componentWillReceiveProps(nextProps): void
[+ getSnapshotBeforeUpdate(prevProps, prevState): snapshot | null]
//^: prep re-render
- UNSAFE_componentWillUpdate(nextProps, nextState): void // prep re-render
render(): React.Node
componentDidUpdate(prevProps, prevState, prevContext | snapshot): void
```

**Unmount**

```ts
componentWillUnmount(): void // Cancel async and timers
```


## APIs

```ts
constructor(props): void
static getDerivedStateFromProps(nextProps, prevState): stateChange | null
render(): React.node
componentDidMount(): void
shouldComponentUpdate(nextProps, nextState, nextContext): boolean
getSnapshotBeforeUpdate(prevProps, prevState): snapshot | null
componentDidUpdate(prevProps, prevState, prevContext): void
componentDidCatch(error, info): void
componentWillUnmount(): void
render(): React.node
UNSAFE_componentWillMount(): void
UNSAFE_componentWillReceiveProps(nextProps): void
UNSAFE_componentWillUpdate(nextProps, nextState): void
setState(updater[, callback])
forceUpdate(callback)
```

**Notes**

- `setState()` updater signature:
    - Function: `(prevState, props) => ({...})`:
    - Object: `{...}`:
