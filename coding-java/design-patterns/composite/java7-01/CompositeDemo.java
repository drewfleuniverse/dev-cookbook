import java.util.List;
import java.util.ArrayList;

interface Station {
  void print();
}


class CompositeStation implements Station {
  private List<Station> nearbyStations = new ArrayList<Station>();

  @Override
  public void print() {
    for (Station station : nearbyStations) {
      station.print();
    }
  }

  public void add(Station station) {
    nearbyStations.add(station);
  }
}

class LeafStation implements Station {
  private String name;
  private String line;

  public LeafStation(String name, String line) {
    this.name = name;
    this.line = line;
  }

  @Override
  public void print() {
    System.out.println("Name: " + name + ", " + "Line: " + line);
  }
}


public class CompositeDemo {
  public static void main(String[] args) {
    CompositeStation homeStation = new CompositeStation();
    CompositeStation station1 = new CompositeStation();
    CompositeStation station2 = new CompositeStation();
    CompositeStation station3 = new CompositeStation();

    LeafStation southwark = new LeafStation("Southwark", "Jublee");
    LeafStation londonBridge = new LeafStation("London Bridge", "Jublee/Northern");
    LeafStation waterloo = new LeafStation("Waterloo", "Jublee");
    LeafStation elephantAndCastle = new LeafStation("Elephant & Castle", "Northern/Bakerloo");
    LeafStation charingCross = new LeafStation("Charing Cross", "Northern/Bakerloo");

    homeStation.add(southwark);
    homeStation.add(station1);
    homeStation.add(station2);

    station1.add(waterloo);
    station1.add(station3);

    station3.add(charingCross);
    station3.add(elephantAndCastle);

    station2.add(londonBridge);

    homeStation.print();
  }
}
