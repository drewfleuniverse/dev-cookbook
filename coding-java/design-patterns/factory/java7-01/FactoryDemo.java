/*
`CityFactory` only creates cities without `new` keyword.
*/

interface City {
  void makeNoise();
}

class London implements City {
  @Override
  public void makeNoise() {
    System.out.println("dondondondondondon");
  }
}

class NewYork implements City {
  @Override
  public void makeNoise() {
    System.out.println("yoryoryoryoryoryor");
  }
}

class CityFactory {
  public City getCity(String cityName) {
    if (cityName == null) {
      return null;
    }

    if(cityName.equals("LONDON")) {
      return new London();
    } else if(cityName.equals("NEW_YORK")) {
      return new NewYork();
    }

    return null;
  }
}

public class FactoryDemo {
  public static void main(String[] args) {
    CityFactory cityFactory = new CityFactory();

    City london = cityFactory.getCity("LONDON");
    london.makeNoise();

    City newYork = cityFactory.getCity("NEW_YORK");
    newYork.makeNoise();
  }
}
