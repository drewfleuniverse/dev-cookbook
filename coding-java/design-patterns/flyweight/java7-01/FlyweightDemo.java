import java.util.HashMap;

class City {
  private String name;
  private String noise;

  public City(String name) {
    this.name = name;
  }

  public void setNoise(String noise) {
    this.noise = noise;
  }

  public void makeNoise() {
    System.out.println(name + " is shouting " + noise);
  }
}

class Factory {
  private static final HashMap<String, City> cityMap = new HashMap<>();

  public static City getCity(String name) {
    City city = (City)cityMap.get(name);

    if (city == null) {
      city = new City(name);
      cityMap.put(name, city);
      System.out.println("This is city: " + name);
    }

    return city;
  }
}


public class FlyweightDemo {
  private static final String[] cities = {"NewYork", "London"};

  public static void main(String[] args) {
    City newYork1 = (City)Factory.getCity(cities[0]);
    newYork1.setNoise("nonononono");
    newYork1.makeNoise();

    City newYork2 = (City)Factory.getCity(cities[0]);
    newYork2.setNoise("yeahyeahyeahyeah");
    newYork2.makeNoise();

    City london1 = (City)Factory.getCity(cities[1]);
    london1.setNoise("lonlonlonlon");
    london1.makeNoise();

    City london2 = (City)Factory.getCity(cities[1]);
    london2.setNoise("dondondondon");
    london2.makeNoise();
  }
}
