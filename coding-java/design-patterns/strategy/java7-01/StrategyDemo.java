import java.util.List;
import java.util.ArrayList;

interface CommuteStrategy {
  String getRoute(String name);
}

class TubeCommuteStrategy implements CommuteStrategy {
  @Override
  public String getRoute(String name) {
    return ("Gonna take tube " + name);
  }
}

class BusCommuteStrategy implements CommuteStrategy {
  @Override
  public String getRoute(String name) {
    return ("Gonna take bus " + name);
  }
}

class Londoner {
  private CommuteStrategy strategy;
  private List<String> routes;

  public Londoner(CommuteStrategy strategy) {
    this.strategy = strategy;
    this.routes = new ArrayList<String>();
  }

  public void add(String name) {
    routes.add(strategy.getRoute(name));
  }

  public void printRoutes() {
    for (String route : routes) {
      System.out.println(route);
    }
  }

  public void setStrategy(CommuteStrategy strategy) {
    this.strategy = strategy;
  }
}

public class StrategyDemo {
  public static void main(String[] args) {
    Londoner drewfle = new Londoner(new TubeCommuteStrategy());
    drewfle.add("Waterloo");
    drewfle.add("charing Cross");
    drewfle.setStrategy(new BusCommuteStrategy());
    drewfle.add("Tottenham Court road");
    drewfle.add("Brick Lane");
    drewfle.printRoutes();
  }
}
