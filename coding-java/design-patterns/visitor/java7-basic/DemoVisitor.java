/*
Visitor pattern separates duplicated operations from a set of classes. For example, both of class NodeTypeA and NodeTypeB require the same set of operations such as counting nodes and print all nodes, this set of operations can be separated and forms as visitor classes. By doing so, each of Node classes can add an accept method to use the visitor classes that present different operations.
*/
interface AlgorithmVisitor {
  void visit(OneAlgorithm one);
  void visit(AnotherAlgorithm another);
}

interface Algorithm {
  void accept(AlgorithmVisitor visitor);
  String lotsOfWork();
}

class TheAlgorithmVisitor implements AlgorithmVisitor {
  public void visit(OneAlgorithm one) {
    System.out.println("Visitor consumes an algorithm: " + one.lotsOfWork());
  }
  public void visit(AnotherAlgorithm another) {
    System.out.println("Visitor consumes an algorithm: " +
      another.lotsOfWork());
  }
}

class OneAlgorithm implements Algorithm {
  public void accept(AlgorithmVisitor visitor) { visitor.visit(this); }
  public String lotsOfWork() { return "One did some computations"; }
}

class AnotherAlgorithm implements Algorithm {
  public void accept(AlgorithmVisitor visitor) { visitor.visit(this); }
  public String lotsOfWork() { return "Another did some computations"; }
}


public class DemoVisitor {
  public static void main(String[] args) {
    TheAlgorithmVisitor visitor = new TheAlgorithmVisitor();
    Algorithm one = new OneAlgorithm();
    Algorithm another = new AnotherAlgorithm();
    one.accept(visitor);
    another.accept(visitor);
  }
}
