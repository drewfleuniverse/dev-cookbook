interface CityBoroughVisitor {
  void visit(Southwark southwark);
  void visit(Islington islington);
  void visit(London london);
}

interface CityBorough {
  void accept(CityBoroughVisitor visitor);
}

class Southwark implements CityBorough {
  public void accept(CityBoroughVisitor visitor) {
    visitor.visit(this);
  }
}

class Islington implements CityBorough {
  public void accept(CityBoroughVisitor visitor) {
    visitor.visit(this);
  }
}

class London implements CityBorough {
  CityBorough[] boroughs;

  public London() {
    this.boroughs = new CityBorough[] {new Southwark(), new Islington()};
  }

  public void accept(CityBoroughVisitor visitor) {
    for (CityBorough borough : boroughs) {
      borough.accept(visitor);
    }
    visitor.visit(this);
  }
}

class CityBoroughAnnounceVisitor implements CityBoroughVisitor {
  public void visit(Southwark southwark) {
    System.out.println("Visiting Southwark");
  }
  public void visit(Islington islington) {
    System.out.println("Visiting Islington");
  }
  public void visit(London london) {
    System.out.println("Visiting London");
  }
}

public class DemoVisitor {
  public static void main(String[] args) {
    CityBorough london = new London();
    london.accept(new CityBoroughAnnounceVisitor());
  }
}
