class Demo {
  String bar() { return "bar() is not static"; }
  static String foo() { return "foo() is static"; }

  public static void main(String[] args) {
    /**
     * Access static method
     */
    System.out.println(foo());

    /**
     * Access non-static method
     *
     * Note: The following yields `error: non-static method bar() cannot be
     * referenced from a static context
     */
    // System.out.println(bar());

    /**
     * Access non-static method through an object instance
     *
     * Note: non-static methods are available through object instances during
     * runtime
     */
    Demo demo = new Demo();
    System.out.println(demo.bar());
  }
}
