class Demo {
  /**
   * Output:
   * ```
   * do{}: 1
   * foo(): 1
   * do{}: 2
   * foo(): 2
   * ```
   */
  public static void main(String[] args) {
    int cnt = 0;
    do {
      cnt++;
      System.out.println("do{}: " + cnt);
    } while (foo(cnt));
  }

  static boolean foo(int n) {
    System.out.println("foo(): " + n);
    return n == 2 ? false : true;
  }
}
