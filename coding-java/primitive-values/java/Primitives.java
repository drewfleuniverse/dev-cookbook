class Primitives {
  public static void main(String[] args) {
    assert Byte.MAX_VALUE == Math.pow(2, 7) -1;
    assert Byte.MIN_VALUE == -Math.pow(2, 7);

    assert Short.MAX_VALUE == Math.pow(2, 15) -1;
    assert Short.MIN_VALUE == -Math.pow(2, 15);

    assert Integer.MAX_VALUE == Math.pow(2, 31) - 1;
    assert Integer.MIN_VALUE == -Math.pow(2, 31);

    assert Long.MAX_VALUE == Math.pow(2, 63) - 1;
    assert Long.MIN_VALUE == -Math.pow(2, 63);

    /**
     * Study IEEE float definitions, e.g. NaN, infinity
     */

    assert Character.MAX_VALUE == '\uFFFF';
    assert Character.MIN_VALUE == '\u0000';

    // *.SIZE yields bits, divide by 8 yields bytes
    assert Byte.SIZE/8 == 1;
    assert Short.SIZE/8 == 2;
    assert Integer.SIZE/8 == 4;
    assert Long.SIZE/8 == 8;
    assert Character.SIZE/8 == 2;
    assert Float.SIZE/8 == 4;
    assert Double.SIZE/8 == 8;
  }
}
