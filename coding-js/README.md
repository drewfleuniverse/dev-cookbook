# Language Cookbook

## Run Examples

### Java

```sh
javac Demo.java && java Demo
```

### JavaScript

```sh
node index.js
```

### HTML

- Install with `npm i -g serve`.
- Use browser to navigate to the file

```sh
serve .
```
