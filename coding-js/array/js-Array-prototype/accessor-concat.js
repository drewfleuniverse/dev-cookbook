/*
array.concat(anyType1[, ...[, anyTypeN]]): newArray
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [1, 2]
let r

r = a.concat(3, 4)
cl(js(r) === '[1,2,3,4]')

r = a.concat([3, 4])
cl(js(r) === '[1,2,3,4]')

const _a = [[3]]

r = a.concat(_a)
cl(js(r) === '[1,2,[3]]')

_a[0][0] = 5
cl(js(r) === '[1,2,[5]]')
