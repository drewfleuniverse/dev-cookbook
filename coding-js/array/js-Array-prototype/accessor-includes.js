/*
array.includes(elemAnyType[, startIndexNumber]): boolean
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [1, 2, 3]


cl(a.includes(1) === true)
cl(a.includes(2) === true)
cl(a.includes(3) === true)

cl(a.includes(42) === false)

cl(a.includes(2, 1) === true)
cl(a.includes(2, 2) === false)
cl(a.includes(2, -2) === true)
