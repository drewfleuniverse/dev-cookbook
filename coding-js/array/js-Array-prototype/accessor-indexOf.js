/*
array.indexOf(elemAnyType[, startIndexNumber]): indexNumber | -1
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [0, 1, 2, 0, 1, 2]

cl(a.indexOf(0) === 0)
cl(a.indexOf(1) === 1)
cl(a.indexOf(2) === 2)

cl(a.indexOf(42) === -1)

cl(a.indexOf(1, 0) === 1)
cl(a.indexOf(1, 1) === 1)
cl(a.indexOf(1, 2) === 4)

cl(a.indexOf(1, -1) === -1)
cl(a.indexOf(1, -2) === 4)
