/*
array.lastIndexOf(elemAnyType[, startIndexNumber]): indexNumber | -1
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [0, 1, 2, 0, 1, 2]

cl(a.lastIndexOf(1) === 4)
cl(a.lastIndexOf(42) === -1)

cl(a.lastIndexOf(1, 5) === 4)
cl(a.lastIndexOf(1, 4) === 4)
cl(a.lastIndexOf(1, 3) === 1)
// ^: startIndex is where to
// start search backwards

cl(a.lastIndexOf(1, -1) === 4)
cl(a.lastIndexOf(1, -2) === 4)
cl(a.lastIndexOf(1, -3) === 1)
