/*
arrar.slice([beginIndexNumber=0[, endIndexNumber]]): newArray
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [1, 2, 3]
let r

r = a.slice()
cl(js(r) === '[1,2,3]')

r = a.slice(1)
cl(js(r) === '[2,3]')

r = a.slice(1, 2)
cl(js(r) === '[2]')
