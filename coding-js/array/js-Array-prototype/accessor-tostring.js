/*
array.toString(): string
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [1, 2, 3]
let r

r = a.toString()
cl(r === '1,2,3')
