const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = ['foo', 'bar']
const it = a.entries()

cl(js(it.next())
  === '{"value":[0,"foo"],"done":false}')
cl(js(it.next())
  === '{"value":[1,"bar"],"done":false}')
cl(js(it.next())
  === '{"done":true}')
