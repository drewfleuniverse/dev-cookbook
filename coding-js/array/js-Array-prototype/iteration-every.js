const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const a = [1, 2, 3]
let r


r = a.every(v => v < 42)
cl(r === true)

r = a.every(v => v > 42)
cl(r === false)

r = []
a.every((v, i, _a) => {
  r.push(`${v}|${i}|${_a === a}`)
  return v < 42
})
cl(jsf(r) === `[
  "1|0|true",
  "2|1|true",
  "3|2|true"
]`)

r = []
a.every((v, i, _a) => {
  r.push(`${v}|${i}|${_a === a}`)
  return v > 42
})
cl(jsf(r) === `[
  "1|0|true"
]`)
