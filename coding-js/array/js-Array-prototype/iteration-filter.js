const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const a = [1, 2, 3]
let r


r = a.filter(v => v > 1)
cl(js(r) === '[2,3]')

r = a.filter(v => v > 3)
cl(js(r) === '[]')

r = []
a.filter((v, i, _a) => {
  r.push(`${v}|${i}|${_a === a}`)
})
cl(jsf(r) === `[
  "1|0|true",
  "2|1|true",
  "3|2|true"
]`)
