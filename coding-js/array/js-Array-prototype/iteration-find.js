const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


let a, r


a = [1, 2, 3]

r = a.find(v => v === 2)
cl(r === 2)

r = a.find(v => v === 42)
cl(r === undefined)


r = []
a.find((v, i, _a) => {
  r.push(`${v}|${i}|${_a === a}`)
})
cl(jsf(r) === `[
  "1|0|true",
  "2|1|true",
  "3|2|true"
]`)


a = [,, 42]
r = []
a.find((v, i, _a) => {
  r.push(`${v}|${i}`)
})
cl(jsf(r) === `[
  "undefined|0",
  "undefined|1",
  "42|2"
]`)
