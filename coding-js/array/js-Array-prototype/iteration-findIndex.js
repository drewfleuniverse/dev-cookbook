const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


let a, r

a = [1, 2, 3]

r = a.findIndex(v => v === 2)
cl(r === 1)

r = a.findIndex(v => v === 42)
cl(r === -1)


a = [1, , 3]

r = []
a.findIndex((v, i, _a) =>
  r.push(`${v}|${i}|${_a === a}`))
cl(jsf(r) === `[
  "1|0|true",
  "undefined|1|true",
  "3|2|true"
]`)
