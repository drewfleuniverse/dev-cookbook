/*
array.forEach(function callback(elem[, index[, array]]) {
  //...
}[, thisArg]): void
 */
const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


let a, r, _r


a = [1, 2, 3]

r = []
_r = a.forEach(v => r.push(v))
cl(js(r) === '[1,2,3]')
cl(_r === undefined)


a = new Array(3)
r = []
a.forEach(e => r.push(42))
cl(js(r) === '[]')


a = [1,2,3]
a.forEach(e => e = 42)
cl(js(a) === '[1,2,3]')


r = a
a.forEach((v, i, _a) =>
  r.push(`${v}|${i}|${_a === a}`))
cl(jsf(r) === `[
  "1|0|true",
  "2|1|true",
  "3|2|true"
]`)


a = [,, 42]
r = []
a.forEach((v, i, _a) =>
  r.push(`${v}|${i}`))
cl(jsf(r) === `[
  "42|2"
]`)
