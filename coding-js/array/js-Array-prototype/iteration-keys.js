const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const a = ['foo', 'bar']
const it = a.keys()

cl(js(it.next())
  === '{"value":0,"done":false}')
cl(js(it.next())
  === '{"value":1,"done":false}')
cl(js(it.next())
  === '{"done":true}')
