const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const a = [{x: 1}, {x: 2}, {x: 3}]
const r = a
  .map(n => n.x)
  .reduce((a, c) => (a + c))
cl(r === 6)
