/*
array.map(function callback(elem[, index[, array]]) {
  // ...
  return processedElem;
}[, thisArg]): array
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const a = [1, 2, 3]
let r

r = a.map(el => el*el)
cl(js(r) === '[1,4,9]')

r = a.map((el, i, _a) =>
  `${el}|${i}|${_a === a}`
)
cl(jsf(r) === `[
  "1|0|true",
  "2|1|true",
  "3|2|true"
]`)


const o = {}

r = [42].map(function (el) {
  return `${el}|${this === o}`
}, o)
cl(js(r) === '["42|true"]')

r = [42].map(el =>
  `${el}|${this === o}`
, o)
cl(js(r) === '["42|false"]')
