const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const a = [1, 3, 5]
let s
let r


s = []
r = a.reduceRight((a, c) => {
  s.push(`${a}|${c}`)
  return a + c
})

cl(js(s) === '["5|3","8|1"]')
cl(r === 9)


const init = 1
s = []
r = [42].reduceRight((a, c)=> {
  s.push(`${a}|${c}`)
  return a + c
}, init)

cl(js(s) === '["1|42"]')
cl(r === 43)
