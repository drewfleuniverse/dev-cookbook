const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


/*
No support on major browsers yet
 */
const a = ['foo', 'bar']

/*
const it = a.values()

cl(js(it.next())
  === '{"value":"foo","done":false}')
cl(js(it.next())
  === '{"value":"bar","done":false}')
cl(js(it.next())
  === '{"done":true}')
*/
