const cl = i => console.log(i)
const js = i => JSON.stringify(i)

const _a = [0, 1, 2, 3, 4]
let a, r


a = _a.slice()

r = a.copyWithin(2)
cl(js(r) === '[0,1,0,1,2]')
cl(r === a)


a = _a.slice() 

r = a.copyWithin(2, 0, a.length)
cl(js(r) === '[0,1,0,1,2]')


a = _a.slice() 

r = a.copyWithin(2, 1, 3)
cl(js(r) === '[0,1,1,2,4]')