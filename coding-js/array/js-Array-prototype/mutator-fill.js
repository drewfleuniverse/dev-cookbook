const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const _a = [0, 1, 2, 3, 4]
let a, r


a = _a.slice()

r = a.fill(7)
cl(js(r) === '[7,7,7,7,7]')
cl(r === a)


a = _a.slice()

r = a.fill(7, 2)
cl(a === r)
cl(js(a) === '[0,1,7,7,7]')


a = _a.slice()

a.fill(7, 2, 3)
cl(js(a) === '[0,1,7,3,4]')


a = new Array(3);
a.fill([])
cl(js(a) === '[[],[],[]]')
a[0][0] = 42
cl(js(a) === '[[42],[42],[42]]')
