const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [1]
let r

r = a.pop()
cl(js(a) === '[]')
cl(r === 1)

r = a.pop()
cl(r === undefined)
