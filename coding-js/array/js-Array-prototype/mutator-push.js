const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = []
let r

r = a.push(1)
cl(js(a) === '[1]')
cl(r === 1)

r = a.push(2,3)
cl(js(a) === '[1,2,3]')
cl(r === 3)

r = a.push([4,5])
cl(js(a) === '[1,2,3,[4,5]]')
cl(r === 4)

Array.prototype.push.apply(a, [6,7])
cl(js(a) === '[1,2,3,[4,5],6,7]')
