const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const _a = [0, 1, 2, 3, 4]
let a, r


a = _a.slice()

r = a.reverse()
cl(js(r) === '[4,3,2,1,0]')
cl(r === a)
