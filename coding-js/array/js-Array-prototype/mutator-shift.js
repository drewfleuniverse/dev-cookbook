const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = ['a', 'b']
let r

r = a.shift()
cl(r === 'a')
cl(js(a) === '["b"]')

r = a.shift()
cl(r === 'b')
cl(js(a) === '[]')

r = a.shift()
cl(r === undefined)
cl(js(a) === '[]')
