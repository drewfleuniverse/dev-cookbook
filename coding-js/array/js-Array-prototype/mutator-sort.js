const cl = s => console.log(s);


/*
V8 implements sort with quicksort optimized with insertion sort
 */


let a, r;


/*
`sort` converts numeric values into unicode point
 */

a = [80,9,1,0,-1];
r = a.sort().toString();
cl(r === '-1,0,1,80,9');

a = ['80','9','1','0','-1'];
r = a.sort().toString();
cl(r === '-1,0,1,80,9');


/*
`sort` sorts strings
 */

 a = ['abc', 'abb', 'aba'];
 r = a.sort().toString();
 cl(r === 'aba,abb,abc');


/*
`sort` may need a compare function
 */

 a = [80,9,1,0,-1];
 r = a.sort((a, b) => {
   if (a < b) {
     return -1;
   }
   if (a > b) {
     return 1;
   }

   return 0;
 }).toString();
 cl(r === '-1,0,1,9,80');


 a = ['80','9','1','0','-1'];
 r = a.sort((a_, b_) => {
   const a = Number(a_);
   const b = Number(b_);

   if (a < b) {
     return -1;
   }
   if (a > b) {
     return 1;
   }

   return 0;
 }).toString();
 cl(r === '-1,0,1,9,80');
