const cl = i => console.log(i)
const js = i => JSON.stringify(i)

/*
array.splice(start[, deleteCount[, item1[, item2[, ...]]]])
 */

const a = [0, 1, 2, 3]
let r

r = a.splice()
cl(js(r) === '[]')
cl(js(a) === '[0,1,2,3]')

r = a.splice(2)
cl(js(r) === '[2,3]')
cl(js(a) === '[0,1]')

r = a.splice(1, 1, 3)
cl(js(r) === '[1]')
cl(js(a) === '[0,3]')

r = a.splice(1, 0, 5)
cl(js(r) === '[]')
cl(js(a) === '[0,5,3]')
