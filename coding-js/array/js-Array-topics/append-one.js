const cl = i => console.log(i)
const js = i => JSON.stringify(i)

const A = ['foo', 'bar']
let a, _a

a = A.slice()
a.push(42)
cl(js(a) === '["foo","bar",42]')


a = A.slice()
_a = a.concat(42)
cl(js(_a) === '["foo","bar",42]')
