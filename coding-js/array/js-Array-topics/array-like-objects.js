
describe('Converting an array-like object to an array', () => {
  const arrayLike = {
    0: 'a',
    1: 'b',
    2: 'not parsed',
    'x': 'not parsed',
    length: 2,
  };

  it('should work', () => {
    assert(Array.from(arrayLike).toString() === 'a,b');
    assert(Array.prototype.map.call(arrayLike, el => el).toString() === 'a,b');
  });

  it('should not work', () => {
    try {
      [...arrayLikeObject];
    } catch (e) {
      assert(e.name === 'ReferenceError');
    }
  });
});


/*
Utils
 */

function describe (s,f) {console.log(`# ${s} #`);f();};
function it (s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert (c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
