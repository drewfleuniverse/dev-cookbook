const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const ok = i => Object.keys(i)

let a1, a2


a1 = [undefined, null]
cl(js(a1) === '[null,null]')
cl(a1[0] === undefined)
cl(a1[1] === null)
cl(a1.toString() === ',')

a2 = new Array(undefined, null)
cl(js(a2) === '[null,null]')
cl(a2[0] === undefined)
cl(a2[1] === null)
cl(a2.toString() === ',')


const a3 = []
a3[0] = 'foo'
a3[1] = 'bar'
a3[-1] = 42
cl(js(a3) === '["foo","bar"]')
cl(a3.length === 2)
cl(js(ok(a3)) === '["0","1","-1"]')
cl(a3[-1] === 42)
