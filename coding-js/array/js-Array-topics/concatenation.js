const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = [1, 2, 3]
const b = [4, 5, 6]
let r1, r2


r1 = a.concat(b)
cl(js(r1) === '[1,2,3,4,5,6]')


r2 = a.slice(a)
Array.prototype.push.apply(r2, b)
cl(js(r2) === '[1,2,3,4,5,6]')
