const cl = s => console.log(s);
const js = o => JSON.stringify(o);


let a, f, r;


/*
Failed
 */
a = new Array(3);
r = a.fill([]);
cl(a === r)
cl(js(a) === '[[],[],[]]');
a[0][0] = 42;
cl(js(a) === '[[42],[42],[42]]');


/*
Failed. `forEach` doesn't loop over *uninitialized* values
 */
a = new Array(3);
a.forEach(e => []);
cl(js(a) === '[null,null,null]')


/*
Failed. `forEach` does NOT change source array values
 */
a = a = new Array(3).fill(undefined);
a.forEach(e => []);
cl(js(a) === '[null,null,null]')


/*
Failed. `for...of` does loop over uninitialized values but does NOT do assignment
 */
a = new Array(3);
for (let e of a) {
  e = [];
}

cl(js(a) === '[null,null,null]')


/*
Failed. `map` doesn't loop over *uninitialized* values
 */
a = new Array(3).map(e => []);
cl(js(a) === '[null,null,null]')
cl(a[0] === undefined);


/*
Success with additional array allocation
 */
a = new Array(3).fill(undefined).map(e => []);
cl(js(a) === '[[],[],[]]');
a[0][0] = 42;
cl(js(a) === '[[42],[],[]]');

/*
Success with additional array allocation
 */
a = new Array(3);
r = Array.from(new Array(3), e => []);
cl(a !== r)
cl(a.toString() === ',,');
cl(js(r) === '[[],[],[]]');
r[0][0] = 42;
cl(js(r) === '[[42],[],[]]');


/*
Success WITHOUT additional array allocation
 */
f = function* (l) {
 let i = 0;
 while (i < l){
   yield [];
   i++;
 }
};

a = [...f(3)];
cl(js(a) === '[[],[],[]]');
r[0][0] = 42;
cl(js(r) === '[[42],[],[]]');

/*
Success with good old for loop
 */
a = [];
for (let i = 0; i < 3; i++) {
  a.push([])
}
cl(js(a) === '[[],[],[]]');
r[0][0] = 42;
cl(js(r) === '[[42],[],[]]');
