const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let a, _a


a = [1, 2, 3]
a.unshift(0)
cl(js(a) === '[0,1,2,3]')

a = [1, 2, 3]
a = [0].concat(a)
cl(js(a) === '[0,1,2,3]')
