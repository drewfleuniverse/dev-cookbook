const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')

const c = [42]
const a = ['foo', 'bar', c]

const x = a.slice()
const y = Array.from(a)
const z = Object.assign([], a)


cl(js(x) === '["foo","bar",[42]]')
cl(x !== a)
cl(x[2] === c)

cl(js(y) === '["foo","bar",[42]]')
cl(y !== a)
cl(y[2] === c)

cl(js(y) === '["foo","bar",[42]]')
cl(y !== a)
cl(y[2] === c)
