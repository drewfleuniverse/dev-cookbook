const cl = s => console.log(s);
const js = o => JSON.stringify(o);


let a, r;



/*
`forEach` does NOT iterate over uninitialized values
 */
a = new Array(3);
r = [];
a.forEach(e => r.push(e));
cl(r.length === 0);

a = new Array(3).fill(undefined);
r = [];
a.forEach(e => r.push(e));
cl(r.length === 3);

a = new Array(3).fill(null);
r = [];
a.forEach(e => r.push(e));
cl(r.length === 3);


/*
`map` does NOT iterate over uninitialized values
 */
a = new Array(3);
r = [];
a.map(e => r.push(e));
cl(r.length === 0);

a = new Array(3).fill(undefined);
r = [];
a.map(e => r.push(e));
cl(r.length === 3);


/*
`reduce` throws error on *empty* array
 */
a = new Array(3);
try {
  a.reduce((a,b) => {;})
} catch (e) {
  cl(e.name === 'TypeError');
}

a = new Array(3).fill(undefined);
r = [];
r.push(a[0])
a.reduce((a,b) => r.push(b));
cl(r.length === 3);


/*
`for...of` works
 */
a = new Array(3);
r = [];
for (const e of a) {
  r.push(e);
}
cl(r.length === 3);


/*
`for...in` does NOT iterate over uninitialized values
 */
a = new Array(3);
r = [];
for (const e in a) {
  r.push(e);
}
cl(r.length === 0);

a = new Array(3).fill(undefined);
r = [];
for (const e in a) {
  r.push(e);
}
cl(r.length === 3);


/*
`find` and `findIndex` can catch uninitialized values
 */
a = [42,,undefined];
r = a.find(e => (e === undefined));
a = [42,,undefined];
r = a.findIndex(e => (e === undefined));
cl(r === 1)
