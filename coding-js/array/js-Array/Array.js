const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const ok = i => Object.keys(i)

let a1, a2


a1 = []
a2 = new Array()

cl(typeof a1 === 'object')
cl(typeof a2 === 'object')
cl(a1 instanceof Array)
cl(a2 instanceof Array)


a1 = [3]
a2 = new Array(3)
cl(js(a1) === '[3]')
cl(js(a2) === '[null,null,null]')
cl(a2.toString() === ',,')


a1 = [1,2,3]
a2 = new Array(1, 2, 3)
cl(js(a1) === '[1,2,3]')
cl(js(a2) === '[1,2,3]')


a1 = [[1,2,3]]
a2 = new Array([1, 2, 3])
cl(js(a1) === '[[1,2,3]]')
cl(js(a2) === '[[1,2,3]]')
