/*
Array.from(arrayLikeObject[, mapFn[, thisArg]]): array
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const ok = o => Object.keys(o)


let a


const s = 'foo'
a = Array.from(s)
cl(js(ok(s)) === '["0","1","2"]')
cl(js(a) === '["f","o","o"]')


const o = {
  '0': 'foo',
  '1': 'bar',
  length: 1
}

a = Array.from(o)
cl(js(ok(o)) === '["0","1","length"]')
cl(js(a) === '["foo"]')


a = Array.from(
  ['foo', 'bar'],
  (v, i) => `${i}: ${v}`
)
cl(js(a) === '["0: foo","1: bar"]')


a = Array.from(
  {length: 3},
  el => []
)
cl(js(a) === '[[],[],[]]')
