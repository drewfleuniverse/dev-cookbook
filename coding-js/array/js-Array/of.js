const cl = i => console.log(i)
const js = i => JSON.stringify(i)

let a


a = Array.of(3)
cl(js(a) === '[3]')


a = Array.of(1,2,3)
cl(js(a) === '[1,2,3]')


a = Array.of([1,2,3])
cl(js(a) === '[[1,2,3]]')


a = Array.of(undefined)
cl(js(a) === '[null]')
cl(a[0] === undefined)


a = Array.of(null)
cl(js(a) === '[null]')
cl(a[0] === null)


a = Array.of(-1)
cl(js(a) === '[-1]')
