## Array shallow clone - `Object.assign` vs `Array.from` vs `slice`

- Preference: `slice`
- Perf:
  - Chrome: `slice` > `Array.from (-85%)` > `Object.assign (-93%)`
  - Firefox: `slice` > `Array.from (-62%)` > `Object.assign (-83%)`


## Array concatenation - `concat` vs `push`

- Preference: `concat`
- Perf:
  - Chrome: `concat` > `push (-17%)`
  - Firefox: `concat` > `push (-26%)`


## Array append one - `concat` vs `push`

- Preference: `push`
- Perf:
  - Chrome: `push` > `concat (-94%)`
  - Firefox: `push` > `concat (-91%)`


## Array prepend one - `unshift` vs `concat`

- Preference: `unshift` for mutable prepend, `concat` for immutable prepend
- Perf:
  - Chrome: `unshift` > `concat (-35%)` > > `unshift cloned array(-44%)`
  - Firefox: `unshift` > `concat (-15%)` > > `unshift cloned array(-50%)`
