const cl = i => console.log(i)
const js = o => JSON.stringify(o)
const jsa = o => JSON.stringify(Array.from(o))


let m, f, o, r


m = new Map()

r = m.set('a', 'b')
cl(r === m)
r = m.get('a')
cl(r === 'b')

m.set('a', 'c')
cl(m.get('a') === 'c')

f = () => ''
o = {}

m = new Map()

m.set(42, 'num')
m.set('foo', 'str')
m.set(f, 'fun')
m.set(o, 'obj')

cl(m.get(42) === 'num')
cl(m.get('foo') === 'str')
cl(m.get(f) === 'fun')
cl(m.get(o) === 'obj')


m = new Map()

m.set('x')
cl(m.get('x') === undefined)

m.set(undefined, 1)
cl(m.get(undefined) === 1)
m.set(null, 2)
cl(m.get(null) === 2)
m.set(NaN, 3)
cl(m.get(NaN) === 3)

m = new Map([[NaN, 1], [NaN, 2]])
cl(m.get(NaN) === 2)
cl(m.size === 1)


m = new Map([['a', 1],['b', 2]])

r = m.delete('a')
cl(r === true)
cl(m.size === 1)

r = m.delete('c')
cl(r === false)
cl(m.size === 1)

r = m.clear()
cl(r === undefined)
cl(m.size === 0)
