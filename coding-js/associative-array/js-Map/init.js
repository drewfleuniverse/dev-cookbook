const cl = i => console.log(i)
const js = o => JSON.stringify(o)
const jsa = o => JSON.stringify(Array.from(o))


let m


m = new Map()
cl(jsa(m) === '[]')

m = new Map([['a', 1], ['b', 2]])
cl(jsa(m) === '[["a",1],["b",2]]')


m = new Map(new Map())
cl(jsa(m) === '[]')

m = new Map(new Set())
cl(jsa(m) === '[]')


try {
  m = new Map([1,2,3])
} catch (e) {
  cl(e.name === 'TypeError')
}

try {
  m = new Map(new Set([1,2,3]))
} catch (e) {
  cl(e.name === 'TypeError')
}
