const cl = i => console.log(i)
const js = o => JSON.stringify(o)
const jsa = o => JSON.stringify(Array.from(o))


let m, r


m = new Map()

m.set('a', 1)
m.set('b', 2)
m.set('c', 3)

cl(m.has('a') === true)
cl(m.has('d') === false)


r = []
m.forEach((v, k, _m) => {
  r.push(`${v}|${k}|${m === _m}`)
})
cl(js(r) ===
  '["1|a|true","2|b|true","3|c|true"]')

// Map is iterable
r = []
for (const [k,v] of m) {
  r.push(`${k}|${v}`)
}
cl(js(r) === '["a|1","b|2","c|3"]')

// Map is not enumerable
r = []
for (const e in m) r.push(e)
cl(js(r) === '[]')


m = new Map()
m.set(7, 6)

r = m.entries()
cl(js(r.next()) ===
  '{"value":[7,6],"done":false}')
cl(js(r.next()) ===
  '{"done":true}')

r = m.keys()
cl(js(r.next()) ===
 '{"value":7,"done":false}')
cl(js(r.next()) ===
  '{"done":true}')

r = m.values()
cl(js(r.next()) ===
 '{"value":6,"done":false}')
cl(js(r.next()) ===
  '{"done":true}')
