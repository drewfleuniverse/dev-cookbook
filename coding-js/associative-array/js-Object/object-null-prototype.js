const cl = i => console.log(i)
const ogpo = o => Object.getPrototypeOf(o)


let o, d


o = {}
cl(ogpo(o) === Object.prototype)
cl('toString' in o === true)

d = Object.setPrototypeOf({}, null);
cl(ogpo(d) === null)
cl('toString' in d === false)

d = Object.create(null)
cl(ogpo(d) === null)
cl('toString' in d === false)
