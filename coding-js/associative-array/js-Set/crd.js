const cl = i => console.log(i)
const js = o => JSON.stringify(o)
const jsa = o => JSON.stringify(Array.from(o))


let s, f, o, r


s = new Set()

r = s.add(1)
cl(r === s)

s.add(2).add(3)

cl(s.has(1) === true)
cl(s.has(42) === false)


f = () => ''
o = {}

s = new Set()

s.add(42)
s.add('foo')
s.add(f)
s.add(o)

cl(s.has(42) === true)
cl(s.has('foo') === true)
cl(s.has(f) === true)
cl(s.has(o) === true)


s = new Set()

s.add(undefined)
cl(s.has(undefined) === true)
s.add(null)
cl(s.has(null) === true)
s.add(NaN)
cl(s.has(NaN) === true)

s = new Set([NaN, NaN])
cl(s.has(NaN) === true)
cl(s.size === 1)


s = new Set([0,1])

r = s.delete(1)
cl(r === true)
cl(s.size === 1)

r = s.clear()
cl(r === undefined)
cl(s.size === 0)
