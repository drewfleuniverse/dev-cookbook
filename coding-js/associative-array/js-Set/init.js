const cl = i => console.log(i)
const js = o => JSON.stringify(o)
const jsa = o => JSON.stringify(Array.from(o))


let s, v


s = new Set()
cl(jsa(s) === '[]')

s = new Set('1123')
cl(jsa(s) === '["1","2","3"]')

s = new Set([1,1,2,3])
cl(jsa(s) === '[1,2,3]')

v = new Set([0,1])
s = new Set(v)
cl(jsa(s) === '[0,1]')

v = new Map([['a',0]])
s = new Set(v)
cl(jsa(s) === '[["a",0]]')
