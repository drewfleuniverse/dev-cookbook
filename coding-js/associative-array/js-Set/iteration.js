const cl = i => console.log(i)
const js = o => JSON.stringify(o)
const jsa = o => JSON.stringify(Array.from(o))


let s, r


s = new Set()

s.add(1)
s.add(1)
s.add(2)

cl(s.has(1) === true)
cl(s.has(3) === false)


r = []
s.forEach((v, _v, _s) => {
  r.push(`${v}|${_v}|${s === _s}`)
})
cl(js(r) ===
  '["1|1|true","2|2|true"]')

// Set is iterable
r = []
for (const v of s) r.push(v)
cl(js(r) === '[1,2]')

// Set is not enumerable
r = []
for (const e in s) r.push(e)
cl(js(r) === '[]')


s = new Set()
s.add(1)

r = s.entries()
cl(js(r.next()) ===
  '{"value":[1,1],"done":false}')
cl(js(r.next()) ===
  '{"done":true}')

r = s.keys()
cl(js(r.next()) ===
 '{"value":1,"done":false}')
cl(js(r.next()) ===
  '{"done":true}')

r = s.values()
cl(js(r.next()) ===
 '{"value":1,"done":false}')
cl(js(r.next()) ===
  '{"done":true}')
