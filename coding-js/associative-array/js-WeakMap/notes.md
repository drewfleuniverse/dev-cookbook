- WeakMap can only have object keys and arbitrary value
- WeakMap maintains a weak reference to its value
- Meaning if no other reference points to the value, then the object key is destroyed
- Otherwise use delete to explicitly remove the key