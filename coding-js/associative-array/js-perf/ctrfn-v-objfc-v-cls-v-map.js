const suite = new Benchmark.Suite;


/*
Chrome
- Fastest is ctor-func,class,object-factory
- object-factory x 729,643,893 ops/sec
- object-factory-null x 3,229,970 ops/sec
- ctor-func x 731,827,270 ops/sec
- class x 730,001,355 ops/sec
- map x 1,919,713 ops/sec

Firefox
- Fastest is class
- object-factory x 1,231,180,794 ops/sec
- object-factory-null x 36,937,149 ops/sec
- ctor-func x 1,539,534,085 ops/sec
- class x 1,573,918,415 ops/sec
- map x 1,070,392 ops/sec
 */


const prep = {
  'setup': () => {
    const f = (a, b, c) => ({
      x: a, y: b, z: c
    })

    const g = (a, b, c) => {
      const o = Object.create(null)
      o.x = a
      o.y = b
      o.z = c

      return o
    }
    const H = function (a, b, c) {
      this.x = a
      this.y = b
      this.z = c
    }
    class I {
      constructor(a, b, c) {
        this.x = a
        this.y = b
        this.z = c
      }
    }
    const j = (a, b, c) => new Map([
      ['x', a], ['y', b], ['z', c]
    ])
  },
  'teardown': () => {

  }
}

suite
.add('object-factory', () => {
  f(1,2,3)
}, prep)
.add('object-factory-null', () => {
  g(1,2,3)
}, prep)
.add('ctor-func', () => {
  new H(1,2,3)
}, prep)
.add('class', () => {
  new I(1,2,3)
}, prep)
.add('map', () => {
  j(1,2,3)
}, prep)
.on('cycle', event => {
  console.log(String(event.target));
})
.on('complete', function () {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run({ 'async': true });
