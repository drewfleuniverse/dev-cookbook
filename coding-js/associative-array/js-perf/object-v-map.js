const suite = new Benchmark.Suite;


/*
Chrome
- Fastest is object
- object x 3,633,721 ops/sec
- map x 1,788,975 ops/sec

FireFox
- Fastest is object
- object x 6,149,311 ops/sec
- map-loop x 928,663 ops/sec
 */


const prep = {
  'setup': () => {
    let a = [0,1,2,3,4,5,6,7,8,9]
  },
  'teardown': () => {
  }
}

suite
.add('object', () => {
  const o = Object.create(null)
  for (let i = 0; i < a.length; i++)
    o[i] = a[i]
}, prep)
.add('map', () => {
  const m = new Map()
  for (let i = 0; i < a.length; i++)
    m.set(i, a[i])
}, prep)
.on('cycle', event => {
  console.log(String(event.target));
})
.on('complete', function () {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run({ 'async': true });
