## Terms

In data structure, an associative array is equivalent to a map, set, dictionary, or symbol table. N.b. map is a higher-order function in functional programming.
