const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


function O() {
  let x = 42
  this.getX = function () {
    return x
  }
}
O.prototype._getX = function () {
  return x
}

const o = new O()

cl('x' in o === false)
cl(o.getX() === 42)

try {
  o._getX()
} catch (e) {
  cl(e.message === "Can't find variable: x")
}


