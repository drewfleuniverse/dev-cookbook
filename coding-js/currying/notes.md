# Currying

Currying

- `f(n, m) --> f'(n)(m)`
- Always return a function takes single argument, i.e. unary
- Always return the same function, i.e. pure
- Remembers arguments applied, i.e. closure


Un-currying

- `f(n)(m) ---> f'(n, m)`


## Currying vs Partial Application

Currying

```
g = curried(f)
g(a)(b)(c)(d)(e) -> r

# equivalent to:
g(a) -> g
g(b) -> g
g(c) -> g
g(d) -> g
g(e) -> r
```

Partial application

```
g = partial(f)
g(a, b, c)(d, e) -> r

# equivalent to:
g(a, b, c) -> g
g(d, e) -> r
```
