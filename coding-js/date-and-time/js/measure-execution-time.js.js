const isBrowser = new Function('return typeof window === "object"')
const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


/*
1 millisecond =
1,000 microseconds =
1,000,000 nanoseconds
 */


const dateStart = Date.now()
setTimeout(() => {
  const dateEnd = Date.now()
  cl(`${dateEnd - dateStart}ms`)
  // ^: 1236ms
}, 1234)


if (isBrowser()) {
  const perfStart = performance.now()
  setTimeout(() => {
    const perfEnd = performance.now()
    cl(`${perfEnd - perfStart}ms`)
    // ^: 1237.44ms
  }, 1234)
} else {
  const hrStart = process.hrtime();
  setTimeout(() => {
    const hrEnd = process.hrtime(hrStart);
    cl(`${hrEnd[0]*1000 + hrEnd[1]/1000000}ms`)
    // ^: 1239.548539ms
  }, 1234)
}
