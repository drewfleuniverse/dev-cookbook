const isBrowser = new Function('return typeof window === "object"')
const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


let d, e


d = Date()
cl(typeof d === 'string')

d = new Date()
cl(d instanceof Date)

d = Date.now()
cl(typeof d === 'number')
// ^: in milliseconds
d = new Date().getTime()
cl(typeof d === 'number')
// ^: in milliseconds


d = new Date(1999, 9)

cl(d.getFullYear() === 1999)
cl(d.getMonth() === 9) // 0 indexed
cl(d.getDate() === 1)
cl(d.getDay() === 5) // weekday, 0-6

cl(d.getTime() === 938750400000)
cl(d.valueOf() === 938750400000)
cl(d.toString() === 'Fri Oct 01 1999 00:00:00 GMT-0400 (EDT)')
cl(d.toUTCString() === 'Fri, 01 Oct 1999 04:00:00 GMT')
cl(d.toDateString() === 'Fri Oct 01 1999')
cl(d.toISOString() === '1999-10-01T04:00:00.000Z')


e = new Date(d)
cl(d !== e)
cl(d.getTime() === e.getTime())
cl(d.valueOf() === e.valueOf())


d = new Date('1999/12/26')
d.setFullYear(d.getFullYear()-1)
cl(d.toISOString() === '1998-12-26T05:00:00.000Z')
d.setMonth(d.getMonth()-11)
cl(d.toISOString() === '1998-01-26T05:00:00.000Z')
d.setDate(d.getDate()-25)
cl(d.toISOString() === '1998-01-01T05:00:00.000Z')
