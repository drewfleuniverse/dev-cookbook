const cl = i => console.log(i)


cl(42 === 42)
cl(42 == 42)

cl(NaN !== NaN)
cl(NaN != NaN)

cl(+0 === 0)
cl(+0 == 0)
cl(-0 === 0)
cl(-0 == 0)
cl(+0 === -0)
cl(+0 == -0)


cl('' === '')
cl('' == '')

cl('1' !== 1)
cl('1' == 1)

cl('1' !== true)
cl('1' == true)
cl('0' !== false)
cl('0' == false)


cl(undefined === undefined)
cl(undefined == undefined)
cl(null === null)
cl(null == null)
cl(undefined !== null)
cl(undefined == null)
