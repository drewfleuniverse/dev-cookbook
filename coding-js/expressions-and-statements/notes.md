Expressions

- Contain indentifiers, literals, and operators
- Are evaluated to values
- Do not have side effects


Statements

- Contain expressions
- Are executed
- Have side effects


[Expressions vs. statements](https://fsharpforfunandprofit.com/posts/expressions-vs-statements/)

> In programming language terminology, an “expression” is a combination of values and functions that are combined and interpreted by the compiler to create a new value, as opposed to a “statement” which is just a standalone unit of execution and doesn’t return anything. One way to think of this is that the purpose of an expression is to create a value (with some possible side-effects), while the sole purpose of a statement is to have side-effects.
