# Heap and Stack


## General Concepts

### Stack

- Static memory allocation
    - Fixed size
    - Allocation or exact size is defined in compile time
- LIFO
- Thread specific: In a multi-threaded process, each thread has its own stack
- Uses:
    - Use when input size is known
    - Stores local variables; variables have fixed sizes based on type

### Heap

- Dynamic memory allocation
    - Dynamic size
    - Allocation happens in run time
- Elements stored have no dependencies
- Application specific: In a multi-threaded process, each thread shares the same heap
- Uses:
    - Use when input size is unknown
    - Stores reference types, e.g. objects, strings, ...etc

## References

- [Differences between Stack and Heap](http://net-informations.com/faq/net/stack-heap.htm)
