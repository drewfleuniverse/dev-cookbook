# Programming Paradigms


## Imperative Programming

- Imperative Programming:
    - Built from procedures/ subroutines/ functions

## Functional Programming

- treats computation as the evaluation of mathematical functions and avoids changing-state and mutable data

## Declarative Programming

- Declarative Programming:
    - Express the logic of a computation without describing its control flow
    - Describe results rather than specifying steps of solution

- The trait of of declarative programming can be seen in the following  programming paradigms:
    - Functional programming languages
    - Logical programming languages
    - Domain-Specific Languages (DSL)
        - Examples:
            - Regular expressions
            - SQL
            - Markup languages
