// https://spin.atomicobject.com/2011/04/10/javascript-don-t-reassign-your-function-arguments/

var makePerson = function(favoriteColor, name, age) {
  if (arguments.length < 3) {
    favoriteColor = "green";
    name = arguments[0];
    age = arguments[1];
  }

  return {
    name: name,
    age: age,
    favoriteColor: favoriteColor
  };
};

var person = makePerson("Joe", 18);
console.log(JSON.stringify(person));
// // => {"name":"green","age":"green","favoriteColor":"green"}
// Strangely, all of the values in the result object are set to "green". I was expecting to see

// // => {"name":"Joe","age":18,"favoriteColor":"green"}
// But when I set favoriteColor to "green" I was also changing arguments[0] to be "green". The situation only got worse when I set name = arguments[0] effectively changing arguments[1] to be "green" as well.
