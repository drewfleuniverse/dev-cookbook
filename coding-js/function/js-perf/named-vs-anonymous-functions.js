const suite = new Benchmark.Suite;


/*
Chrome:
arrow function x 46,382,920 ops/sec ±1.01% (63 runs sampled)
function declaration x 44,460,591 ops/sec ±5.56% (62 runs sampled)
function expression x 43,990,052 ops/sec ±5.49% (60 runs sampled)
anonymous x 40,693,527 ops/sec ±5.07% (59 runs sampled)
Fastest is arrow function
 */


const prep = {
  'setup': () => {
  },
  'teardown': () => {
  }
}

const func1 = () => {
  const array = [];
  array.push('foo');
};

const func2 = function() {
  const array = [];
  array.push('foo');
};

function func3() {
  const array = [];
  array.push('foo');
}

suite
.add('anonymous', () => {
  (function() {
    const array = [];
    array.push('foo');
  })();
}, prep)
.add('arrow function', () => {
  func1();
}, prep)
.add('function expression', () => {
  func2();
}, prep)
.add('function declaration', () => {
  func3();
}, prep)
.on('cycle', event => {
  console.log(String(event.target));
})
.on('complete', function () {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run({ 'async': true });
