const isBrowser = new Function('return typeof window === "object"')
const cl = i => console.log(i)


let o


o = Object.create({
  f () {
    setTimeout(function () {
      this.a = 42
    }, 1)
  },
  g () {
    setTimeout((function () {
      this.b = 42
    }).bind(this), 1)
  },
  h () {
    setTimeout((function () {
      'use strict';
      cl(this === null)
    }).bind(null), 1)
  },
  i () {
    setTimeout(() => (
      this.d = 42
    ), 1)
  },
  k () {
    (setTimeout.bind(null, () => (
      this.e = 42
    ), 1))();
  },
  l () {
    'use strict';
    (setTimeout.bind(null, function () {
      console.log(this);
      this.x = 42
    }, 1))();
  }
})


o.f()
o.g()
o.h()
o.i()
o.k()
o.l()


setTimeout(() => {
  if (isBrowser()) {
    cl(this.a === 42)
  } else {
    cl(this.a === undefined)
  }

  cl(o.b === 42)

  cl(o.d === 42)

  cl(o.e === 42)


  cl(o.x === undefined);
  if (isBrowser()) {
    cl(this.x === 42)
  } else {
    cl(this.x === undefined)
  }

}, 2)
