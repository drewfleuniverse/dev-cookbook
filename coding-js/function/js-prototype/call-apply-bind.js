const cl = i => console.log(i)


let f, g, o


f = function (r, a, b) {
  this[r] = a + b
}


o = {}


f.call(o, 'a', 1, 2)

f.apply(o, ['b', 1, 2])

g = f.bind(o)
g('c', 1, 2)


cl(o.a === 3)
cl(o.b === 3)
cl(o.c === 3)
