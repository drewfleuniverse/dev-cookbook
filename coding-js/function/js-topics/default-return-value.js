const cl = i => console.log(i)


function f1() {}
const r1 = f1()

function f2() { return }
const r2 = f2()

cl(r1 === undefined)
cl(r2 === undefined)
