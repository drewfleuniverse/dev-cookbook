const cl = s => console.log(s)
const js = o => JSON.stringify(o)
const isBrowser = new Function('return typeof window === "object"')



;(function () {
  cl(Array.isArray(arguments) === false)
  cl(typeof arguments === 'object')
  cl(arguments.constructor === Object)
})()

if (isBrowser()) {
  ;(() => {
    try {
      arguments
    } catch (e) {
      cl(e.name === 'ReferenceError')
    }
  })()
} else {
  ;(() => {
    cl(typeof arguments === 'object')
    cl(arguments.constructor === Object)
  })()
}


;(function () {
  cl(arguments.length === 2)
  cl(arguments[0] === 'foo')
  cl(arguments[1] === 'bar')
  arguments[2] = 42
  cl(arguments[2] === 42)
})('foo', 'bar')


;(function (a) {
  a = 'bar'
  cl(arguments[0] === 'bar')
})('foo')

;(function (a) {
'use strict'
  a = 'bar'
  cl(arguments[0] === 'foo')
})('foo')






