const f1 = new Function('a', 'b', 'return a + b')

console.assert(f1(1, 2) === 3)
