console.assert(typeof f1 === 'function')

try {
  f1()
} catch (e) {
  console.assert(!"doesn't evaluate");
}

function f1() {}


(() => {
  'use strict'
  console.assert(typeof f1 === 'function')

  try {
    f1()
  } catch (e) {
    console.assert(!"doesn't evaluate");
  }

  function f1() {}
})()
