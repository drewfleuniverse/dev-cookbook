console.assert(typeof f1 === 'undefined')

try {
  f1()
} catch (e) {
  console.assert(e instanceof TypeError);
  console.assert(e.message === 'f1 is not a function');
}

var f1 = function () {}
