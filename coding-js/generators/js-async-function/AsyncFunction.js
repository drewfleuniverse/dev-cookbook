const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)

try {
	cl(AyncFunction)
	// ^: AsyncFunction is not globally accessible
} catch (e) {
	cl(e instanceof ReferenceError)
}


const AF = gp(async function(){}).constructor
cl(AF.name === 'AsyncFunction')
// ^: A work around to obtain AsyncFunction

const p = (success, x) =>
new Promise((resolve, reject) => {
  window.setTimeout(() => {
    success
      ? resolve(x)
      : reject(new Error('wrong'))
  }, 1)
})

const af = new AF(
  'x',
  'return await p(true, x*2)'
)

const it = af(21).then(r => cl(r === 42))
