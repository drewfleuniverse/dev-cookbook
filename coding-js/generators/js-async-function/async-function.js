const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


async function af() {
  return 42
}

cl(af instanceof
  gp(async function(){}).constructor)

const a = af()

cl(a instanceof Promise)

a.then(r => cl(r === 42))
