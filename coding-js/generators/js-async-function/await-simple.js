const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const pro = i => Promise.resolve(i)


let f, p;


f = async function () {
  let s = ''
  s += await pro('foo');
  s += pro('bar');
  s += '42';

  return s;
}

p = f();
p.then(r => {
  cl(r === 'foo[object Promise]42')
})
