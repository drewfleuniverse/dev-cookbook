const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const pro = i => Promise.resolve(i)

async function af1() {
  let r = []
  r.push(pro('foo'))
  r.push(pro('bar'))
  r.push(42)

  return r
}

af1().then(r =>
  cl(js(r) === '[{},{},42]')
)


async function af2() {
  let r = []
  r.push(await pro('foo'))
  r.push(await pro('bar'))
  r.push(42)

  return r
}

af2().then(r =>
  cl(js(r) === '["foo","bar",42]')
)
