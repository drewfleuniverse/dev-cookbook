const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


const p = (success, x) =>
new Promise((resolve, reject) => {
 window.setTimeout(() => {
   success
     ? resolve(x)
     : reject(Error(x))
 }, 1)
})


async function af1() {
  let r = []
  r.push(await p(true, 'foo'))
  r.push(await p(true, 'bar'))

  return r
}


af1().then(r =>
 cl(js(r) === '["foo","bar"]')
)

async function af2() {
 try {
   let r = []
   r.push(await p(true, 'foo'))
   r.push(await p(true, 'bar'))
   cl(js(r) === '["foo","bar"]')

   await p(false, 'failed')

   return r
   // ^: will not return
 } catch(e) {
   cl(e.message === 'failed')
   throw new Error(`af2 ${e.message}`)
 }
}

af2().then(r => {
  /*will do nothing*/
}).catch(e =>
  cl(e.message === 'af2 failed')
)
