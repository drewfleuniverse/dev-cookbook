const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const prs = i => Promise.resolve(i)

async function af() {
  const r = await Promise.all([
    prs('foo'),
    prs('bar')
  ])

  return r
}

af().then(r =>
  cl(js(r) === ["foo","bar"])
)
