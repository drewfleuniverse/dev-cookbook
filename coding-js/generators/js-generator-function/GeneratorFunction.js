const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)

try {
	cl(GeneratorFunction)
	// ^: GeneratorFunction is not globally accessible
} catch (e) {
	cl(e instanceof ReferenceError)
}


const GF = gp(function*(){}).constructor
cl(GF.name === 'GeneratorFunction')
// ^: A work around to obtain GeneratorFunction

const gf = new GF('x', 'yield x*2');

const it = gf(21);
cl(js(it.next()) === '{"done":false,"value":42}')
cl(js(it.next()) === '{"done":true}')
