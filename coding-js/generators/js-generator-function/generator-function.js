const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


let gf, it, r


gf = function* () {}
// Generator function returns a generator object
// here the generator object is called `it`, short
// for iterator
it = gf()

cl(gp(gf).constructor.name === 'GeneratorFunction')
// `it` doesn't have a constructor and name property
cl(it.toString() === '[object Generator]')


gf = function* (x) {
  yield x*=2
  yield x*=3
}

// `it` implements iterator protocol, i.e. `it` can
// only be iterated once

it = gf(7)
r = []
for (i of it) r.push(i)
cl(js(r) === '[14,42]')
r = []
for (i of it) r.push(i)
cl(js(r) === '[]')

it = gf(7)
cl(it.next().value === 14)
cl(it.next().value === 42)
cl(it.next().value === undefined)
cl(it.next().value === undefined)
