const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


function* gf() {
  yield 'foo'
  return 'bar'
  yield 'useless'
}

const it = gf()

const r = []
r.push(it.next().value)
r.push(it.next().value)
r.push(it.next().value)

cl(js(r) === '["foo","bar",null]')
