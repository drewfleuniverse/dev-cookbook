const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


function* gf1(i) {
  yield i*2
}

function* gf2() {
  yield 1
  yield* gf1(21)
  yield 2
}

const it = gf2()

const r = []
r.push(it.next().value)
r.push(it.next().value)
r.push(it.next().value)
r.push(it.next().value)

cl(js(r) === '[1,42,2,null]')
