const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


let gf, it, r

gf = function* () {
  yield 1
  yield 2
}


it = gf()
r = []
r.push(it.next().value)
r.push(it.next().value)
r.push(it.next().value)

cl(js(r) === '[1,2,null]')
// ^: end of iterator may be undefined


gf = function* (a) {
  a.push(yield 1)
  a.push(yield 2)
}

const a = []
it = gf(a)

r = []
r.push(it.next('useless').value)
r.push(it.next('foo').value)
r.push(it.next('bar').value)

cl(js(r) === '[1,2,null]')
cl(js(a) === '["foo","bar"]')
