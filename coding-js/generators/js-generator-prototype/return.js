const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


function* gf() {
  yield 'foo'
  yield 'bar'
  yield 'useless'
}

const it = gf()

const r = []
r.push(it.next().value)
r.push(it.return(42).value)
r.push(it.next().value)

cl(js(r) === '["foo",42,null]')
