const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


function* gf() {
  while(true) {
    try {
      yield 42
    } catch(e) {
      yield `Ugh!${e.message}`
    }
  }
}

var it = gf();

const r1 = []
r1.push(it.next().value)
r1.push(it.throw(
  new Error('Wrong')
).value)
r1.push(it.next().value)

cl(js(r1) === '[42,"Ugh!Wrong",42]')
