const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


const p = (success, x) =>
new Promise((resolve, reject) => {
  window.setTimeout(() => {
    success
      ? resolve(x)
      : reject(new Error('wrong'))
  }, 1)
})


const async = (gf) => {
  const it = gf()
  const doIt = n => {
    if (n.done)
      return
    n.value
      .then(r => doIt(it.next(r)))
      // ^: promise result is returned
      // via next
      .catch(e => it.throw(e))
  }

  try {
    doIt(it.next())
  } catch(e) {
    it.throw(e)
  }
}



const r1 = []
const e1 = []
async(function* () {
  try{
    r1.push(yield p(true, 'foo'))
  } catch (e) {
    e1.push(e.message)
  }
  cl(js(r1) === '["foo"]')
  cl(js(e1) === '[]')
})


const r2 = []
const e2 = []
async(function* () {
  try{
    r2.push(yield p(true, 'foo'))
    r2.push(yield p(true, 'bar'))
    r2.push(yield p(false, 'useless'))
  } catch (e) {
    e2.push(e.message)
  }
  cl(js(r2) === '["foo","bar"]')
  cl(js(e2) === '["wrong"]')
})
