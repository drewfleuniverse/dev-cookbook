const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


const g = a => {
  let i = 0
  return {
    next () {
      return i < a.length
        ? { value: a[i++], done: false }
        : {done: true}
    }
  }
}


const r = []
const it = g([1, 2, 3])

while(1) {
  const i = it.next()
  if (!i.done) r.push(i.value)
  else break
}

cl(js(r) === '[1,2,3]')
