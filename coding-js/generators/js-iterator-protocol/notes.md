Iterable objects

- Array, Map and Set
- NodeList (in browser context and in newer browsers)


Iteration

- Array, Map, Set and NodeList: `for...of`, `forEach`, spread, destructuring
- Array: `for` with index
- NodeList: `for` with index
- N.b. Use `for...in` with Array iterates numeric keys, not element value


Notes on String

- String is not an iterable object, but it has a iterator method to use with `for...of`
- To retrieve string iterator, use `str[Symbol.iterator]`


Notes on generators

- Generators are iterable, but can only iterate once
