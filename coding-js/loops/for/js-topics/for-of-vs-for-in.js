let o, r;


o = {};

Object.defineProperties(o, {
  a: { enumerable: true },
  b: { enumerable: false },
})

Object.setPrototypeOf(o, {
  c: undefined
})

// for...in loop iterates over enumerable properties
r = [];
for (const prop in o) {
  r.push(prop);
}

cl(r.toString() === 'a,c')


r = [];
o[Symbol.iterator] = function* () {
  yield 'a';
  yield 'b';
}
// for..of iterates over generator functions and
// iterable objects including String, Array,
// arguments, NodeList, TypedArray, Map, and Set
for (const val of o) {
  r.push(val);
}

cl(r.toString() === 'a,b')
