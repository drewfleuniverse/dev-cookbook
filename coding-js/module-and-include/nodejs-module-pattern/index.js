const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


const m1 = (() => {
  let x = 0
  return {
    incX: () => ++x
  }
})()

(m => {
  let y = 100
  m.incY = () => ++y
})(m1)

cl('x' in m1 === false)
cl('y' in m1 === false)

cl(m1.incX() === 1)
cl(m1.incY() === 101)

