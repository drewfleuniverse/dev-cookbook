# Nested function, closure, and callback


## Languages

**Nested function**

- Languages:
    - JavaScript, Scala, Common Lisp, Scheme, Ruby, Python, PHP, Perl, GCC, ...etc
    - Notes:
        - GCC: as a language extension
        - Ruby, Python, PHP, Perl: not full support
- Languages without direct support:
    - C++: using nested class or c++11 lambda
    - Java: using nested class, Java8 lambda, or Java8 anonymous class
    
## References

**Nested function**

- [Nested function](https://en.wikipedia.org/wiki/Nested_function)

**Closure**

- [Closure (computer programming)](https://en.wikipedia.org/wiki/Closure_(computer_programming))

**Callback**

- [Callback (computer programming)](https://en.wikipedia.org/wiki/Callback_(computer_programming))

**Discussions**

- [What is a difference between closure and nested function javascript?](https://www.quora.com/What-is-a-difference-between-closure-and-nested-function-javascript)
