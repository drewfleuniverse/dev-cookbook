const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogpo = o => Object.getPrototypeOf(o)
const ogopns = o => Object.getOwnPropertyNames(o)
const pie = (o, p) => Object.prototype
  .propertyIsEnumerable.call(o, p)


function C() {
  this.a = 'c.a'
  this.f = () => this.a
  this.g = () => this
}
C.prototype.h = function () {
  return this.a
}
C.prototype.i = function () {
  return this
}


const c = new C()


cl(c instanceof C)
cl(c instanceof Object)


cl(ogpo(c) === C.prototype)
cl(ogpo(ogpo(c)) === Object.prototype)

cl(js(ogopns(c)) === '["a","f","g"]')
cl(js(ogopns(C.prototype)) === '["constructor","h","i"]')

cl(C.prototype.constructor === C)
cl(Object.prototype.constructor === Object)
cl(pie(C.prototype, 'constructor') === false)


cl(c.a === 'c.a')

cl(c.f() === 'c.a')
cl(c.g() === c)

cl(c.h() === 'c.a')
cl(c.i() === c)

cl(C.prototype.h() === undefined)
cl(C.prototype.i() === C.prototype)
