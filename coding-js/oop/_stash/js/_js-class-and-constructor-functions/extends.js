const cl = i => console.log(i)
const js = i => JSON.stringify(i)


class P {
  constructor(v) {
    this.v = v
  }

  f() { return `p${this.v}` }
}

class C extends P {
  // implicitly called constructor of P
  f() { return `c${this.v}` }
}

class D extends P {
  constructor(v, w) {
    // equivalent to P.call(this)
    super(v)
    this.w = w
  }

  f() { return `d${this.v}${this.w}` }
}


const c = new C(4), d = new D(4,2)

cl(c.f() === 'c4')
cl(d.f() === 'd42')
