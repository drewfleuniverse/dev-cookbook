const cl = s => console.log(s)
const js = o => JSON.stringify(o)


class C {
  *f () {
    yield 'foo';
    yield 'bar';
  }
}


const o = new C();
const r = [...o.f()];
cl(r.toString() === 'foo,bar')
