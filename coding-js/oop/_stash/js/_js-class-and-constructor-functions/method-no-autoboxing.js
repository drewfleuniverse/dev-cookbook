const cl = s => console.log(s)
const js = o => JSON.stringify(o)


/*
ES6 classes are executed in strict mode by design, thus methods do not return global object when used alone as in non-strict mode
*/
class C {
  a() { return this }
  static b() { return this }
}


const c = new C()


cl(c.a() === c)

const a = c.a
cl(a() === undefined) // no autoboxing


cl(C.b() === C)

const b = C.b
cl(b() === undefined) // no autoboxing
