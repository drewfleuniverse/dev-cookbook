const cl = s => console.log(s)
const js = o => JSON.stringify(o)


const _v = Symbol('_v')
class C {
  constructor(v) {
    this.constructor[_v] = v
  }
  
  get v() {
    return this.constructor[_v]
  }
}
C[_v] = 'foo'


const D = function (v) {
  D[_v] = v
}
D.prototype = {
  get v () {
    return D[_v]
  }
}
D[_v] = 'foo'


let a


cl(C.v === undefined)
cl(C[_v] === 'foo')

a = new C('bar')
cl(a.v === 'bar')
cl(C[_v] === 'bar')

a.v = 42
cl(a.v === 'bar')
cl(C[_v] === 'bar')


cl(D.v === undefined)
cl(D[_v] === 'foo')

a = new D('bar')
cl(a.v === 'bar')
cl(D[_v] === 'bar')

a.v = 42
cl(a.v === 'bar')
cl(D[_v] === 'bar')
