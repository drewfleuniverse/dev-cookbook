const cl = s => console.log(s)
const js = o => JSON.stringify(o)


/*
Static function in JavaScript under the hood is:
- a method attached to constructor function that does not get initialized via new operator
- a method not accessible by its instances due to not in prototype chain
 */


class C {
  constructor(v) {
    this.v = v
  }
  static sum (...c) {
    return c.reduce((acc, val) => (acc.v + val.v))
  }
}


let a, b


a = new C(21)
b = new C(21)

cl(C.sum(a, b) === 42)

cl(a.sum === undefined)
try { a.sum() } catch (e) {
  cl(e.name === 'TypeError')
}


const D = function (v) {
  this.v = v
}
D.sum = function (...d) {
  return d.reduce((acc, val) => (acc.v + val.v))
}

a = new D(21)
b = new D(21)

cl(D.sum(a, b) === 42)

cl(a.sum === undefined)
try { a.sum() } catch (e) {
  cl(e.name === 'TypeError')
}
