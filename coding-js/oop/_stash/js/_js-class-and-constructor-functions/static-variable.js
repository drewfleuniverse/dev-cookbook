const cl = s => console.log(s)
const js = o => JSON.stringify(o)


class C {
  constructor(v) {
    this.constructor.v = v
  }

  get v_() {
    return this.constructor.v
  }
  
  set v_(v) {
    this.constructor.v = v
  }
}
C.v = 'foo'


const D = function (v) {
  D.v = v
}
D.prototype = {
  get v_ () {
    return D.v
  },
  set v_ (v) {
    D.v = v
  }
}
D.v = 'foo'


let a


cl(C.v === 'foo')

a = new C('bar')
cl(a.v === undefined)
cl(a.v_ === 'bar')
cl(C.v === 'bar')

a.v_ = 42
cl(a.v_ === 42)
cl(C.v === 42)


cl(D.v === 'foo')

a = new D('bar')
cl(a.v === undefined)
cl(a.v_ === 'bar')
cl(D.v === 'bar')

a.v_ = 42
cl(a.v_ === 42)
cl(D.v === 42)
