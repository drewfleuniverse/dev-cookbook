const cl = i => console.log(i)


class P {
  static f () { return 'foo' }
}

class C extends P{
  static g () { return super.f() + 'bar' }
}


cl(C.g() === 'foobar')
