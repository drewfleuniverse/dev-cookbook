const cl = i => console.log(i)
const js = i => JSON.stringify(i)


class P {
  constructor() {
    // p: is non-writable
    Object.defineProperty(this, 'p', {
      value: 1
    })
  }
}

class C extends P {
  f() { super.p = 2 }
}


const c = new C()


try { c.f() } catch (e) {
  cl(e.name === 'TypeError')
}
