const cl = i => console.log(i)


/*
Do not try this buggy super keyword ...
mdn mistake...
 */


const p = {
  f() { return 'foo' }
}


const c = {
  g() { return `${super.f()}bar` }
}
Object.setPrototypeOf(c, p)

cl(c.g())



/*
const d = Object.create(p)
d.h = () => super.f()
^: results in uncatchable error: SyntaxError: use of super property accesses only valid within methods or eval code within methods
*/
