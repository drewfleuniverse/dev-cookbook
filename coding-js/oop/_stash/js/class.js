const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogpo = o => Object.getPrototypeOf(o)
const ogopns = o => Object.getOwnPropertyNames(o)
const pie = (o, p) => Object.prototype
  .propertyIsEnumerable.call(o, p)

class P {
  constructor() {
    this.a = 'p.a'
    this.f = () => this.a
    this.g = () => this
  }
  h() { return this.a }
  i() { return this }
}

class C extends P {
  constructor() {
    super()
    this.a = 'c.a'
  }
}


const c = new C()


cl(c instanceof C)
cl(c instanceof P)
cl(c instanceof Object)


cl(ogpo(c) === C.prototype)
cl(ogpo(ogpo(c)) === P.prototype)
cl(ogpo(ogpo(ogpo(c))) === Object.prototype)

cl(js(ogopns(c)) === '["a","f","g"]')
cl(js(ogopns(C.prototype)) === '["constructor"]')
cl(js(ogopns(P.prototype)) === '["constructor","h","i"]')

cl(C.prototype.constructor === C)
cl(P.prototype.constructor === P)
cl(Object.prototype.constructor === Object)
cl(pie(C.prototype, 'constructor') === false)
cl(pie(P.prototype, 'constructor') === false)


cl(c.a === 'c.a')

cl(c.f() === 'c.a')
cl(c.g() === c)

cl(c.h() === 'c.a')
cl(c.i() === c)

cl(P.prototype.h() === undefined)
cl(P.prototype.i() === P.prototype)
