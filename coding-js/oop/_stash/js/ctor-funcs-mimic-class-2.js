const cl = i => console.log(i);
const ogpo = o => Object.getPrototypeOf(o);

const P = function () {
  this.x = 'x';
};

const C = function () {
  P.call(this);
  this.y = 'y'
};

Object.setPrototypeOf(C.prototype, P.prototype);
// C.prototype = Object.create(P.prototype)
// can it be done manually

const c = new C();
cl(c)

cl(c instanceof C)
cl(c instanceof P)
cl(c instanceof Object)

cl(ogpo(c) === C.prototype)
cl(ogpo(ogpo(c)) === P.prototype)
cl(ogpo(ogpo(ogpo(c))) === Object.prototype)
