const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogpo = o => Object.getPrototypeOf(o)
const ogopns = o => Object.getOwnPropertyNames(o)
const pie = (o, p) => Object.prototype
  .propertyIsEnumerable.call(o, p)

/*
Not an ideal way to do inheritance
 */
function P() {
  this.a = 'p.a'
  this.f = () => this.a
  this.g = () => this
  this._f = function () { this.a }
  this._g = function () { this }
}
P.prototype.h = function () {
  return this.a
}
P.prototype.i = function () {
  return this
}


function C() {
  this.a = 'c.a'
}
C.prototype = new P()
Object.defineProperty(C.prototype, 'constructor', {
  value: C,
  enumerable: false,
  writable:  true,
  configurable: true
})


const c = new C()


cl(c instanceof C)
cl(c instanceof P)
cl(c instanceof Object)

cl(ogpo(c) === C.prototype)
cl(ogpo(ogpo(c)) === P.prototype)
cl(ogpo(ogpo(ogpo(c))) === Object.prototype)

cl(js(ogopns(c)) === '["a"]')
cl(js(ogopns(C.prototype)) === '["a","f","g","_f","_g","constructor"]')
cl(js(ogopns(P.prototype)) === '["constructor","h","i"]')

cl(C.prototype.constructor === C)
cl(P.prototype.constructor === P)
cl(Object.prototype.constructor === Object)
cl(pie(C.prototype, 'constructor') === false)
cl(pie(P.prototype, 'constructor') === false)


cl(c.a === 'c.a')

cl(c.f() === 'p.a')
cl(c.g() === C.prototype)
cl(c._f() === undefined)
cl(c._g() === undefined)

cl(c.h() === 'c.a')
cl(c.i() === c)

cl(P.prototype.h() === undefined)
cl(P.prototype.i() === P.prototype)
