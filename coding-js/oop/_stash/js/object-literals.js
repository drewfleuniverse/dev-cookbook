const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogpo = o => Object.getPrototypeOf(o)
const ogopns = o => Object.getOwnPropertyNames(o)
const pie = (o, p) => Object.prototype
  .propertyIsEnumerable.call(o, p)


const p = {
  a: 'p.a',
  f () { return this.a },
  g () { return this },
  _f: () => this.a,
  _g: () => this
}

const c = Object.create(p)
c.a = 'c.a'


cl(c instanceof Object)

cl(ogpo(c) === p)
cl(ogpo(ogpo(c)) === Object.prototype)

cl(js(ogopns(c)) === '["a"]')
cl(js(ogopns(p)) === '["a","f","g","_f","_g"]')

cl(c.constructor === Object)
cl(pie(c, 'constructor') === false)


cl(c.a === 'c.a')

cl(c.f() === 'c.a')
cl(c.g() === c)

cl(c._f() === undefined)
cl(c._g() === this)
