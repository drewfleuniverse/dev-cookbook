const cl = i => console.log(i)


const op = {
  a: undefined
}

const oc = Object.create(op)
oc.b = undefined


cl('a' in oc === true)
cl('b' in oc === true)
cl(oc.hasOwnProperty('a') === false)
cl(oc.hasOwnProperty('b') === true)
