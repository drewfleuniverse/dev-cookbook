const cl = i => console.log(i);
const ogpo = o => Object.getPrototypeOf(o);
const ohop = (o, p) => Object.hasOwnProperty.call(o, p);
const js = i => JSON.stringify(i);


let o;


function O() {}

o = new O()



cl(o instanceof O === true)
cl(o instanceof Object === true)
cl(o.constructor === O)

cl(ohop(o, 'constructor') === false);
cl(ohop(ogpo(o), 'constructor') === true);

cl(ogpo(o).constructor === O)
cl(ogpo(ogpo(o)).constructor === Object)


o = {}

cl(o instanceof Object === true)
cl(o.constructor === Object)
