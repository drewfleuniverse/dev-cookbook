const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogpo = o => Object.getPrototypeOf(o)
const ogopd = (o, p) => Object.getOwnPropertyDescriptor(o, p)

let x

x = {}
cl(x.constructor === Object)
x = () => 42
cl(x.constructor === Function)
x = []
cl(x.constructor === Array)
x = 42
cl(x.constructor === Number)
x = ''
cl(x.constructor === String)
x = true
cl(x.constructor === Boolean)
x = Symbol()
cl(x.constructor === Symbol)


x = undefined
try {
  x.constructor
} catch (e) {
  cl(e.name === 'TypeError')
}

x = null
try {
  x.constructor
} catch (e) {
  cl(e.name === 'TypeError')
}


function C1 () {}
cl(C1.constructor === Function)
// is ctor own proprety?
cl(C1.prototype.constructor === C1)

const c1 = new C1()
cl(c1.constructor === C1)


class C2 {}
cl(C2.constructor === Function)
cl(C2.prototype.constructor === C2)

const c2 = new C2()
cl(c2.constructor === C2)
