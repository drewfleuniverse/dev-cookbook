const cl = i => console.log(i)
const ogpo = o => Object.getPrototypeOf(o)


let o

const b = Symbol('b')
o = {}
o.a = 1
o[b] = 2
Object.defineProperty(o, 'c', {
  value: 3
})


cl(o.hasOwnProperty('a') === true)
cl(o.hasOwnProperty(b) === true)
cl(o.hasOwnProperty('c') === true)

cl(o.hasOwnProperty('toString')
  === false)
cl(ogpo(o).hasOwnProperty('toString')
  === true)
