const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)

let o
function C() {}
o = new C()



cl(C.prototype.isPrototypeOf(o) ===
  true)
cl(Object.prototype.isPrototypeOf(o) ===
  true)


p = {}
o = Object.create(p)

cl(p.isPrototypeOf(o) === true)
cl(Object.prototype.isPrototypeOf(p) ===
  true)
