const cl = i => console.log(i)
const js = i => JSON.stringify(i)


// try symbol

let o = Object.defineProperties({}, {
  a: { enumerable: true },
  b: {}
})

cl(o.propertyIsEnumerable('a') === true)
cl(o.propertyIsEnumerable('b') === false)
