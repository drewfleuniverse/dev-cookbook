const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


let o, r


o = { a: 42 }
cl(o.toString() === '[object Object]')
// Implicitly calls `toString` due to `valueOf` returns
// object reference, i.e. non-primitive value, by default
cl(o + 42 === '[object Object]42')
// Implicitly calls `toString`
cl(o + '42' === '[object Object]42')


// N.b. `String` without `new` returns a primitive,
// string; with `new`, return a String object
o.toString = function () { return String(this.a) }
cl(o.toString() === '42')
// Implicitly calls `toString` due to `valueOf` returns
// object
cl(o + 42 === '4242')
// Implicitly calls `toString`
cl(o + '42' === '4242')


/*
DO NOT return non-string value in `toString`, otherwise
`valueOf` will be called
 */
o = {}
const x = { x: undefined }
o.toString = () => x
o.valueOf = () => 41

cl(o + 1 === 42)
cl(o + '1' === '411')

o.toString = () => 99
cl(o + 1 === 42)
cl(o + '1' === '411')


const ts = o => Object.prototype
  .toString.call(o)

cl(ts(42) === '[object Number]')
cl(ts('') === '[object String]')
cl(ts([]) === '[object Array]')
cl(ts({}) === '[object Object]')

// Since JavaScript 1.8.5
cl(ts(undefined) === '[object Undefined]')
cl(ts(null) === '[object Null]')

// Otehr objects
cl(ts(new Date) === '[object Date]')
cl(ts(Math) === '[object Math]')
