const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const a = {
  x: 7,
  valueOf () { return this.x; }
};
const b = {
  x: 7,
  valueOf () { return this.x; }
};

cl(a !== b);
cl(a != b);
cl(a + 2 === 9);
cl(b * 3 === 21);


const c = {}

// default returns self
cl(c.valueOf() === c);

c.valueOf = () => '21';
// valueOf returns string
// plus sign works concat operator
cl(c + 21 === '2121');
// the first plus convert number-formated
// string into number, ths second plus
// works as addition operator
cl(+c + 21 === 42);


/*
`valueOf` returns the object itself by default
 */
let o


o = { a: 41 }

// `console.log` uses object reference for
// standalone object
cl(o === o)
// `JSON.stringify` traverses through enumerable
// own properties of an object
cl(js(o) === '{"a":41}')
// By default, `valueOf` returns the object
// itself, thus produce same string as the above
cl(js(o.valueOf()) === '{"a":41}')
// `valueOf` is implicitly called
cl(isNaN(o + 1))
// `toString` is implicitly called
cl(o + '1' === '[object Object]1')


o.valueOf = function () { return this.a }

cl(o.valueOf() === 41)
// `valueOf` is implicitly called due to
// 1: custom `valueOf` is defined
// 2: the custom `valueOf` returns primitive value
cl(o + 1 === 42)
cl(o + '1' === '411')



/*
DO NOT return non-primitive value in `valueOf`,
otherwise unpredictable behavior may happen as
below
 */
o = {}
const x = { x: undefined }
o.valueOf = () => x

cl(o.valueOf() === x)
// `toString` is implicitly called due to custom
// `valueOf` returns non-primitive value
cl(o + 1  === '[object Object]1')
cl(o + '1'  === '[object Object]1')
