const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let o, r;


o = {};

Object.defineProperties(o, {
  a: {configurable: true},
  b: {configurable: false},
});

delete o.a;
delete o.b;

r = Object.getOwnPropertyNames(o);
cl(r.toString() === 'b');
