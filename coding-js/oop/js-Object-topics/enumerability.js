const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ok = i => Object.keys(i)
const ogopd = (o, p) => Object.getOwnPropertyDescriptor(o, p)


let o, r


const sl = Symbol('local')
const sg = Symbol.for('global')

o = {}

o.a = 1
Object.defineProperty(o, 'b', {
  value: 2
})
o[sl] = 3
o[sg] = 4


cl(o.propertyIsEnumerable('a') === true)
cl(o.propertyIsEnumerable('b') === false)
cl(o.propertyIsEnumerable(sl) === true)
cl(o.propertyIsEnumerable(sg) === true)


cl(o.hasOwnProperty('a') === true)
cl(o.hasOwnProperty('b') === true)
cl(o.hasOwnProperty(sl) === true)
cl(o.hasOwnProperty(sg) === true)


cl(js(Object.getOwnPropertyNames(o))
  === '["a","b"]')
cl(Object.getOwnPropertySymbols(o)[0]
  === sl)
cl(Object.getOwnPropertySymbols(o)[1]
  === sg)


cl(js(Object.entries(o)) === '[["a",1]]')
cl(js(Object.keys(o)) === '["a"]')
cl(js(Object.values(o)) === '[1]')


cl(JSON.stringify(o) === '{"a":1}')


r = []
for (p in o)
  r.push(p)
cl(js(r) === '["a"]')


cl('a' in o === true)
cl('b' in o === true)
cl(sl in o === true)
cl(sg in o === true)
cl('toString' in o)