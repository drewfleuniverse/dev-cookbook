describe('extensible vs sealed vs frozen', () => {
  const objFactory = () => ({ a: 1 });

  it('default to extensible, writable, and configurable', () => {
    const obj = objFactory();
    assert(Object.isExtensible(obj) === true);
    assert(Object.isSealed(obj) === false);
    assert(Object.isFrozen(obj) === false);
    assert(
      JSON.stringify(Object.getOwnPropertyDescriptors(obj)) ===
      '{"a":{"value":1,"writable":true,"enumerable":true,"configurable":true}}'
    );
  });

  const defaultDescriptors = '{"a":{"value":1,"writable":true,"enumerable":true,"configurable":true}}';

  it('preventExtensions(): not extensible, but writable, and configurable', () => {
    const obj = objFactory();
    assert(Object.isExtensible(obj) === true);

    Object.preventExtensions(obj);
    assert(Object.isExtensible(obj) === false);
    assert(Object.isSealed(obj) === false);
    assert(Object.isFrozen(obj) === false);
    assert(JSON.stringify(Object.getOwnPropertyDescriptors(obj)) === defaultDescriptors);
  });

  it('seal(): not extensible and configurable, but writable', () => {
    const obj = objFactory();
    assert(Object.isExtensible(obj) === true);

    Object.seal(obj);
    assert(Object.isExtensible(obj) === false);
    assert(Object.isSealed(obj) === true);
    assert(Object.isFrozen(obj) === false);
    assert(
      JSON.stringify(Object.getOwnPropertyDescriptors(obj)) ===
      '{"a":{"value":1,"writable":true,"enumerable":true,"configurable":false}}'
    );
  });

  it('freeze(): not extensible, configurable, and writable', () => {
    const obj = objFactory();
    assert(Object.isExtensible(obj) === true);

    Object.freeze(obj);
    assert(Object.isExtensible(obj) === false);
    assert(Object.isSealed(obj) === true);
    assert(Object.isFrozen(obj) === true);
    assert(
      JSON.stringify(Object.getOwnPropertyDescriptors(obj)) ===
      '{"a":{"value":1,"writable":false,"enumerable":true,"configurable":false}}'
    );
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
