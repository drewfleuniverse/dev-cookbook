const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogopds = o => Object.getOwnPropertyDescriptors(o)

let o, r


const a = ['foo']
cl(jsf(ogopds(a)) === `{
  "0": {
    "value": "foo",
    "writable": true,
    "enumerable": true,
    "configurable": true
  },
  "length": {
    "value": 1,
    "writable": true,
    "enumerable": false,
    "configurable": false
  }
}`)


Object.freeze(a)
cl(jsf(ogopds(a)) === `{
  "0": {
    "value": "foo",
    "writable": false,
    "enumerable": true,
    "configurable": false
  },
  "length": {
    "value": 1,
    "writable": false,
    "enumerable": false,
    "configurable": false
  }
}`)
cl(Object.isExtensible(a) === false)


;(() => {
  a[0] = 'bar'
  a[1] = 42
  cl(js(a) == '["foo"]')
})


;(() => {
  'use strict'

  try {
    o[0] = 'bar'
  } catch (e) {
    cl(e.name === 'TypeError')
  }

  try {
    o[1] = 42
  } catch (e) {
    cl(e.name === 'TypeError')
  }

  cl(js(a) === '["foo"]')
})()
