describe('getters and setters', () => {
  it('CAUTION: prop name and getter/setter name cannot be the same', () => {
    const obj = {
      key: 9,
      get key() { return this.key; }
    };

    try {
      obj.key;
    } catch (e) {
      assert(e.name === 'RangeError');
      // exceeded maximum call stack!
    }
  });

  it('in a plain object', () => {
    const sKey = Symbol('sKey');
    const obj = {
      [sKey]: 9,
      get key() { return this[sKey]; },
      set key(key) { this[sKey] = key; }
    };

    assert(obj.key === 9);

    obj.key = -9;
    assert(obj.key === -9);
  });

  it('in a ctor function', () => {
    const sKey = Symbol('sKey');
    const Ctor = function (key) {
      this[sKey] = key;
    };
    Ctor.prototype = Object.create({
      get key() { return this[sKey]; },
      set key(key) { this[sKey] = key; },
    });
    Ctor.prototype.constructor = Ctor;

    const obj = new Ctor(9);
    assert(obj.key === 9);

    obj.key = -9;
    assert(obj.key === -9);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};


const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let o


o = {
  x: 'foo',
  get f () { return this.x },
  set f (v) { this.x = v }
}

cl(o.f === 'foo')

o.f = 'bfr'
cl(o.f === 'bfr')


delete o.f
cl('f' in o === false)


o = {
  x: 'foo',
  get ['f'+1] () { return this.x },
  set ['f'+1] (v) { this.x = v }
}

cl(o.f1 === 'foo')

o.f1 = 'bar'
cl(o.f1 === 'bar')


o = { x: 'foo' }
Object.defineProperty(o, 'f', {
  get: function () { return this.x },
  set: function (v) { this.x = v }
})

cl(o.f === 'foo')

o.f = 'bar'
cl(o.f === 'bar')
