const cl = i => console.log(i);


const someHeavyComputation = () => 42;

let o = {
  get a () {
    delete this.a;
    this.a = someHeavyComputation();

    return this.a;
  }
};

cl(o.a === 42);
