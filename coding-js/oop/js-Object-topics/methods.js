const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let o


o = {
  x: 42,
  a () { return this.x },
  b: function () { return this.x },
  c: () => this.x
}

cl(o.a() === 42)
cl(o.b() === 42)
cl(o.c() === undefined)


o = {
  * a () { yield 42 },
  b: function * () { yield 42 }
}

cl(o.a().next().value === 42)
cl(o.b().next().value === 42)



o = {
  async a () {
    return await Promise.resolve(42)
  },
  b: async function () {
    return await Promise.resolve(42)
  }
}

o.a().then(r => cl(r === 42))
o.b().then(r => cl(r === 42))


o = {
  ['f'+1] () { return 42 },
  *['f'+2] () { yield 42 },
  async ['f'+3] () {
    return await Promise.resolve(42)
  }
}

cl(o.f1() === 42)
cl(o.f2().next().value === 42)
o.f3().then(r => cl(r === 42))


o = {
  x: 'foo',
  get a () { return this.x },
  set a (x) { this.x = x}
}

cl(o.a === 'foo')
o.a = 'bar'
cl(o.a === 'bar')
