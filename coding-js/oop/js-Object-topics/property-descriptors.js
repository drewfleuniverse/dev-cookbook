const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogopds = o => Object.getOwnPropertyDescriptors(o)
const oie = o => Object.isExtensible(o)


/*
N.b. configurable defines whether
a prop can be deleted
 */
let o


o = { a: 0 }
cl(oie(o) === true)
cl(jsf(ogopds(o)) === `{
  "a": {
    "value": 0,
    "writable": true,
    "enumerable": true,
    "configurable": true
  }
}`)


o = { a: 0 }
Object.defineProperty(o, 'a', {
  value: 42
})
cl(o.a === 42)


o = { a: 0 }
Object.defineProperty(o, 'a', {
  writable: false
})
o.a = 42
cl(o.a === 0)
delete o.a
cl(js(o) === '{}')


o = { a: 0 }
Object.defineProperty(o, 'a', {
  enumerable: false
})
cl(js(o) === '{}')


o = { a: 0 }
Object.defineProperty(o, 'a', {
  configurable: false
})
o.a = 42
delete o.a
cl(o.a === 42)
