const sourcePrototype = {
  prop3: 'c'
};
const source = Object.create(sourcePrototype);
source.prop1 = 'a';
Object.defineProperty(source, 'prop2', {
  enumerable: false,
  configurable: false,
  writable: false,
  value: 'b'
});

it('Verify source', () => {
  assert(source.hasOwnProperty('prop1'));
  assert(source.hasOwnProperty('prop2'));
  assert(Object.getPrototypeOf(source) === sourcePrototype);
});

describe('Shallow clone objects without unenumerable properties and prototype chain', () => {
  it('using spread operator', () => {
    const clone = {...source};
    assert(clone.hasOwnProperty('prop1'));
    assert(clone.hasOwnProperty('prop2') === false);
    assert(Object.getPrototypeOf(clone) !== sourcePrototype);
  });

  it('using Object.assign()', () => {
    const clone = Object.assign({}, source);
    assert(clone.hasOwnProperty('prop1'));
    assert(clone.hasOwnProperty('prop2') === false);
    assert(Object.getPrototypeOf(clone) !== sourcePrototype);
  });
});


describe('Shallow clone objects with unenumerable properties but without prototype chain', () => {
  it('using Object.defineProperties() and Object.getOwnPropertyDescriptors()', () => {
    const clone = Object.defineProperties(
      {},
      Object.getOwnPropertyDescriptors(source)
    );
    assert(clone.hasOwnProperty('prop1'));
    assert(clone.hasOwnProperty('prop2'));
    assert(Object.getPrototypeOf(clone) !== sourcePrototype);
  });
});


describe('Shallow clone objects with unenumerable properties and prototype chain', () => {
  it('using Object.create(), Object.getPrototypeOf() and Object.getOwnPropertyDescriptors()', () => {
    const clone = Object.create(
      Object.getPrototypeOf(source),
      Object.getOwnPropertyDescriptors(source)
    );
    assert(clone.hasOwnProperty('prop1'));
    assert(clone.hasOwnProperty('prop2'));
    assert(Object.getPrototypeOf(clone) === sourcePrototype);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
