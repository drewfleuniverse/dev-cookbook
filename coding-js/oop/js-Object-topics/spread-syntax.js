
describe('spread syntax', () => {
  it('works with iterable objcts', () => {
    assert(Array.prototype.hasOwnProperty(Symbol.iterator));
    assert(String.prototype.hasOwnProperty(Symbol.iterator));
    assert(Map.prototype.hasOwnProperty(Symbol.iterator));
    assert(Set.prototype.hasOwnProperty(Symbol.iterator));

    (function () {
      assert(arguments.hasOwnProperty(Symbol.iterator));
    })();
  })

  const array = [1, 2];
  const string = '12';
  const iterableObject = {
    *[Symbol.iterator] () {
      yield 1;
      yield 2;
    }
  };
  it('can be used in function arguments', () => {
    const add = (a, b) => (Number(a) + Number(b));
    assert(add(...array) === 3);
    assert(add(...string) === 3);
    assert(add(...iterableObject) === 3);
  });

  it('can be used to clone arrays', () => {
    assert([...array].toString() === '1,2');
  });

  it('can be used to convert iterable objects into arrays', () => {
    assert([...string].toString() === '1,2');
    assert([...iterableObject].toString() === '1,2');
  });

  it('can be used to concat arrays', () => {
    assert([...array, ...array].toString() === '1,2,1,2');
  });

  it('cannot be used as a stand alone expression', () => {
    // ...array
    // Outputs: Uncaught SyntaxError: Unexpected token ...
  });
});


/*
Utils
 */

function describe (s,f) {console.log(`# ${s} #`);f();};
function it (s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert (c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
