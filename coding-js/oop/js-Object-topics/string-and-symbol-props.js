describe('Objects', () => {
  it('can have computable props', () => {
    const date = new Date();
    const obj = { [date]: 'x' };

    assert(obj[date] === 'x');
    assert(obj[date.toString()] === 'x');
  });
});


describe('Symbols', () => {
  const sym = Symbol('sym');
  const obj = { [sym]: 'x' };

  it('are enumerable', () => {
    const objDescriptors = Object.getOwnPropertyDescriptors(obj);
    assert(objDescriptors.hasOwnProperty(sym));
    assert(objDescriptors[sym].enumerable);
  });

  it('can be cpoied', () => {
    assert(({...obj})[sym] === 'x');
    assert(Object.assign({}, obj)[sym] === 'x');
  });

  it('are accessible with in, hasOwnProperty(), getOwnPropertySymbols()', () => {
    assert(sym in obj);
    assert(obj.hasOwnProperty(sym));
    assert(Object.getOwnPropertySymbols(obj)[0] === sym);
  });

  it('are not accessible wiht getOwnPropertyNames() and stringify()', () => {
    assert(Object.getOwnPropertyNames(obj).toString() === '');
    assert(JSON.stringify(obj) === '{}');
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
