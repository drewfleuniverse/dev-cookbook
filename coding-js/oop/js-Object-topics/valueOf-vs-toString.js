describe('valueOf() vs toString()', () => {
  const obj = { a: 1 };

  it('valueOf(): by default, a plain object returns itself', () => {
    assert(obj.valueOf() === obj);
  });

  it('toString(): by default, a plain object returns [object Object]', () => {
    assert(obj.toString() === '[object Object]');
  });

  const number = new Number(9);
  const string = new String('9');
  const node = Object.create({
    valueOf () { return this.key; },
    toString () { return `${this.key}` }
  }, {
    key: {
      value: 9,
      writable: true
    }
  });

  it('valueOf() should return primitive values while toString() should return strings', () => {
    assert(number.valueOf() !== number.toString());
    assert(number.valueOf() === 9);
    assert(number.toString() === '9');
    assert(number + number === 18);

    assert(string.valueOf() === string.toString());
    assert(string.valueOf() === '9');
    assert(string.toString() === '9');
    assert(string + string === '99');

    assert(node.valueOf() !== node.toString());
    assert(node.valueOf() === 9);
    assert(node.toString() === '9');
    assert(node + node === 18);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
