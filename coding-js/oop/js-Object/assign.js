it('Object.assign() does not copy getters', () => {
  const source = { get prop() { return 42; } };
  const target = {};

  Object.assign(target, source);

  const sourceDescriptor = Object.getOwnPropertyDescriptor(source, 'prop');
  const targetDescriptor = Object.getOwnPropertyDescriptor(target, 'prop');

  assert(typeof sourceDescriptor.get === 'function');
  assert(sourceDescriptor.value === undefined);
  assert(typeof targetDescriptor.get === 'undefined');
  assert(targetDescriptor.value === 42);
});





const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)

let o, o1, o2, r


o = { a: 1, b: 2 }

r = Object.assign({}, o)
cl(js(r) === '{"a":1,"b":2}')
cl(r !== o)


o1 = { a: 0 }
o2 = { a: 42 }

r = Object.assign({}, o1, o2)
cl(js(r) === '{"a":42}')


o = { a: { b: 0 } }

r = Object.assign({}, o)
cl(js(r) === '{"a":{"b":0}}')
o.a.b = 42
cl(js(o) === '{"a":{"b":42}}')
cl(js(r) === '{"a":{"b":42}}')


const p = Symbol('p')
o = { [p]: 42 }

r = Object.assign({}, o)
cl(o[p] === r[p])

o[p] = 0
cl(o[p] === 0)
cl(r[p] === 42)


// b is not enumerable
o = Object.create({ a: 0 }, {
  b: { value: 0 }
})

r = Object.assign({}, o)
cl(js(r) === '{}')


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
