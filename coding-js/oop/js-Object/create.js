const cl = i => console.log(i)
const ogpo = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


let o, r


o = { f () { return 42 }  }
r = Object.create(o)

cl(js(r) === '{}')
cl(ogpo(r) === o)
cl(ogpo(o) === Object.prototype)


r = Object.create(null)
cl(js(r) === '{}')
cl(ogpo(r) === null)


r = Object.create(Object.prototype)
// ^: equivalent to `r = {}`
cl(js(r) === '{}')
cl(ogpo(r) === Object.prototype)


r = Object.create(Object.prototype, {
  d: {
    value: 42,
    enumerable: true
  }
})
cl(js(r) === '{"d":42}')
cl(ogpo(r) === Object.prototype)
