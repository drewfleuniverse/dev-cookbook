const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogopd = (o, p) => Object.getOwnPropertyDescriptor(o, p)


let o

o = { d1: undefined }
Object.defineProperty(o, 'd2', {})
Object.defineProperty(o, 'd3', { value: 42})

cl(o.d1 === undefined)
cl(jsf(ogopd(o, 'd1')) === `{
  "writable": true,
  "enumerable": true,
  "configurable": true
}`)

cl(o.d2 === undefined)
cl(jsf(ogopd(o, 'd2')) === `{
  "writable": false,
  "enumerable": false,
  "configurable": false
}`)

cl(o.d3 === 42)
cl(jsf(ogopd(o, 'd3')) === `{
  "value": 42,
  "writable": false,
  "enumerable": false,
  "configurable": false
}`)


o = {
  get f1 () { return undefined },
  set f1 (v) {}
}
Object.defineProperty(o, 'f2', {
  get: function () { return undefined },
  set: function (v) {}
})

cl(jsf(ogopd(o, 'f1')) === `{
  "enumerable": true,
  "configurable": true
}`)


cl(jsf(ogopd(o, 'f2')) === `{
  "enumerable": false,
  "configurable": false
}`)
