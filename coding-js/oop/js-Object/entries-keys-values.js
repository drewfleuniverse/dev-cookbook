const cl = i => console.log(i)
const js = i => JSON.stringify(i)


/*
Unlike iterable objects, `entries` on an object only returns an array of kv pairs. `entries` on an iterable object returns an itrerator.
 */


let o, p, r


o = {}
o.a = 1
o[Symbol('b')] = 2
Object.defineProperty(o, 'c', { value: 3 })


r = Object.entries(o)
cl(js(r) === '[["a",1]]')

r = Object.keys(o)
cl(js(r) === '["a"]')

r = Object.values(o)
cl(js(r) === '[1]')
