const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogopd = (o, p) => Object.getOwnPropertyDescriptor(o, p)


/*
Frozen objects:
- Not extensible
- Sealed
- All props are not writable
 */
let o, r


o = { x: 'foo' }

r = Object.freeze(o)

cl(r === o)

cl(Object.isExtensible(o) === false)
cl(Object.isSealed(o) === true)
cl(Object.isFrozen(o) === true)

cl(jsf(ogopd(o, 'x')) === `{
  "value": "foo",
  "writable": false,
  "enumerable": true,
  "configurable": false
}`)


;(() => {
  o.x = 'bar'
  cl(o.x === 'foo')
  delete o.x
  cl(o.x === 'foo')
  o.y = 42
  cl(o.y === undefined)
})()


;(() => {
  'use strict'
  try {
    o.x = 'bar'
  } catch (e) {
    cl(e.name === 'TypeError')
  }
  try {
    delete o.x
  } catch (e) {
    cl(e.name === 'TypeError')
  }
  try {
    o.y = 42
  } catch (e) {
    cl(e.name === 'TypeError')
  }
})()


try {
  Object.defineProperty(o, 'y', {})
} catch (e) {
  cl(e.name === 'TypeError')
}


try {
  Object.setPrototypeOf(o, { y: undefined })
} catch (e) {
  cl(e.name === 'TypeError')
}
