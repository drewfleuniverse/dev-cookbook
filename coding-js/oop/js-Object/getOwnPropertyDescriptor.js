const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


let o, r


o = { x: 'foo' }
r = Object.getOwnPropertyDescriptor(o, 'x')
cl(jsf(r) === `{
  "value": "foo",
  "writable": true,
  "enumerable": true,
  "configurable": true
}`)
