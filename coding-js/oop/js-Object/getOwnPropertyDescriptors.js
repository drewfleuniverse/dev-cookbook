const cl = i => console.log(i);
const jsf = i => JSON.stringify(i, null, '  ');


let o, s, r;

s = Symbol('s');
o = {
  x: 'foo',
  [s]: 'bar'
};

r = Object.getOwnPropertyDescriptors(o);
cl(jsf(r) === `{
  "x": {
    "value": "foo",
    "writable": true,
    "enumerable": true,
    "configurable": true
  }
}`);

cl(o[s] === 'bar');
