const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let o, r

o = Object.create({ a: 'a' })
o.b = 'b'
o[Symbol('c')] = 'c'
Object.defineProperty(o, 'd', {})


r = Object.getOwnPropertyNames(o)
cl(js(r) === '["b","d"]')
