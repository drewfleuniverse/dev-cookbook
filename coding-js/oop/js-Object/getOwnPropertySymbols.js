const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let o, r

const a = Symbol('a')
const b = Symbol.for('b')
o = {
  [a]: 'local',
  [b]: 'global'
}

r = Object.getOwnPropertySymbols(o)
cl(r[0] === a)
cl(r[1] === b)
