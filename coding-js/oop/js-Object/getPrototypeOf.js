const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


const o1 = {}
const o2 = Object.create(o1)

cl(Object.getPrototypeOf(o2) === o1)
cl(Object.getPrototypeOf(o1)
  === Object.prototype)
cl(Object.getPrototypeOf(
  Object.prototype) === null)
