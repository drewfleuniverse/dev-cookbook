const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


cl(Object.is('', '') === true)
cl(Object.is(0, 0) === true)
cl(Object.is(this, this) === true)


cl(Object.is('', false) === false)
cl('' !== false)
cl('' == false)


cl(Object.is(0, +0) === true)
cl(Object.is(0, -0) === false)
cl(Object.is(+0, -0) === false)
cl(0 === +0)
cl(0 === -0)
cl(+0 === -0)
cl(0 == +0)
cl(0 == -0)
cl(+0 == -0)


cl(Object.is(NaN, NaN) === true)
cl(NaN !== NaN)
cl(NaN != NaN)