const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)

let o, r


o = { x: 0 }

cl(Object.isSealed(o) === false)


o = Object.seal({ x: 0 })

cl(Object.isExtensible(o) === false)
cl(Object.isFrozen(o) === false)
cl(Object.isSealed(o) === true)


o = Object.seal({})

cl(Object.isExtensible(o) === false)
cl(Object.isFrozen(o) === true)
cl(Object.isSealed(o) === true)
