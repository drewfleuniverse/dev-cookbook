const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


/*
A non-extensible object means no
new prop can be added
 */
let o, r


o = { x: 'foo' }

r = Object.preventExtensions(o)

cl(r === o)

cl(Object.isExtensible(o) === false)
cl(Object.isSealed(o) === false)
cl(Object.isFrozen(o) === false)

cl(jsf(Object.getOwnPropertyDescriptor(o, 'x')) === `{
  "value": "foo",
  "writable": true,
  "enumerable": true,
  "configurable": true
}`)


;(() => {
  o.y = 42
  cl(o.y === undefined)
})()


;(() => {
  'use strict'

  try {
    o.y = 42
  } catch (e) {
    cl(e.name === 'TypeError')
  }
})()


try {
  Object.defineProperty(o, 'y', {})
} catch (e) {
  cl(e.name === 'TypeError')
}

try {
  Object.setPrototypeOf(o, { y: undefined })
} catch (e) {
  cl(e.name === 'TypeError')
}
