const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const ogopd = (o, p) => Object.getOwnPropertyDescriptor(o, p)


/*
Sealed objects:
- Not extensible
- All props are not deletable
 */
let o, r


o = { x: 'foo' }

r = Object.seal(o)

cl(r === o)

cl(Object.isExtensible(o) === false)
cl(Object.isSealed(o) === true)
cl(Object.isFrozen(o) === false)

cl(jsf(ogopd(o, 'x')) === `{
  "value": "foo",
  "writable": true,
  "enumerable": true,
  "configurable": false
}`)


;(() => {
  delete o.x
  cl(o.x === 'foo')
})()


;(() => {
  'use strict'
  try {
    delete o.x
  } catch (e) {
    cl(e.name === 'TypeError')
  }
})()


try {
  Object.defineProperty(o, 'y', {})
} catch (e) {
  cl(e.name === 'TypeError')
}


try {
  Object.setPrototypeOf(o, { y: undefined })
} catch (e) {
  cl(e.name === 'TypeError')
}
