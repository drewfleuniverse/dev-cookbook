const cl = i => console.log(i)
const ogpo = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


let c, p, r


p = { f() { return 42 }  }
c = {}

cl(ogpo(c) === Object.prototype)

r = Object.setPrototypeOf(c, p)
cl(r === c)
cl(ogpo(c) === p)
cl(ogpo(p) === Object.prototype)
