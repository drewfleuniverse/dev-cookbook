const cl = s => console.log(s)
const js = o => JSON.stringify(o)


class C {
  constructor(...args) {
    let i = 0;
    for (const a of args) {
      this[`p${i}`] = a;
      i++;
    }
  }
  *[Symbol.iterator] () {
    let i = 0;
    while (this.hasOwnProperty(`p${i}`)) {
      yield this[`p${i}`];
      i++;
    }
  }
}


const c = new C('foo', 'bar');
const r = [...c];
cl(r.toString() === 'foo,bar')
