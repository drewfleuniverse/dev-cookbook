const cl = i => console.log(i)
const js = i => JSON.stringify(i)


class A extends Array {
  static get [Symbol.species]() {
    return Array
  }
}

const a = new A(1, 2, 3)

cl('map' in a)
cl('includes' in a)
cl('push' in a)

a[3] = 42
cl(js(a) === '[1,2,3,42]')

cl(a instanceof A === true) // false in mdn
cl(a instanceof Array === true)
