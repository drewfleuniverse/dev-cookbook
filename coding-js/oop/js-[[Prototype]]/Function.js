/*
`Function`:

- Has type `function`
- Is an instance of both `Function` and `Object`
- Has two [[prototype]] point to:
    - `Function.prototype`
    - `Object.prototype`

`Function.prototype`:

- Has a `constructor` points to itself, i.e. `Function`
- Has type `function`
- Is an instance of `Object`
- Has [[Prorotype]] points to `Object.prototype`
 */

describe('Function', () => {
  it('has type function', () => {
    assert(typeof Function === 'function');
  });

  it('has own properties', () => {
    assert(Object.getOwnPropertyNames(Function).join(', '));
    // Chrome: prototype, length, name
    assert(Object.getOwnPropertySymbols(Function).length === 0);
    // Chrome: []
  });

  it('is an instance of both Function and Object', () => {
    assert(Function instanceof Function);
    assert(Function instanceof Object);
    assert(Function.__proto__.constructor === Function);
    assert(Function.__proto__.__proto__.constructor === Object);
  });

  it(`has two [[Prototype]] point to Function.prototype and Object.prototype`, () => {
    assert(Function.__proto__ === Function.prototype);
    assert(Function.__proto__.__proto__ === Object.prototype);
    assert(Function.__proto__.__proto__.__proto__ === null);
  });
});


describe('Function.prototype', () => {
  it('has only one prototype', () => {
    assert(Function.prototype);
    assert(Function.prototype.prototype === undefined);
  });

  it('has own properties', () => {
    assert(Object.getOwnPropertyNames(Function.prototype).join(', '));
    // Chrome: length, name, arguments, caller, constructor, apply, bind, call, toString
    assert(Object.getOwnPropertySymbols(Function.prototype));
    // Chrome: [Symbol(Symbol.hasInstance)]
  });

  it('has constructor points to itself', () => {
    assert(Function.prototype.constructor === Function);
  });

  it('has type function', () => {
    assert(typeof Function.prototype === 'function');
  });

  it('is an instance of Object', () => {
    assert(Function.prototype instanceof Object);
    assert(Function.prototype.__proto__.constructor === Object);
  });

  it('has [[Prorotype]] points to Object.prototype', () => {
    assert(Function.prototype instanceof Object);
    assert(Function.prototype.__proto__.constructor === Object);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
