/*
`Object`:

- Has type `function`
- Is an instance of both `Function` and `Object`
- Has two [[prototype]] point to:
    - `Function.prototype`
    - `Object.prototype`

`Object.prototype`:

- Has a `constructor` points to itself, i.e. `Object`
- Has type `object`
- Has no [[Prototype]]
    - I.e. Not an instance of `Object`
- Is the last [[Prototype]] of:
    - An Object instance
    - A Function instance
    - Most primitive variables
 */

describe('Object', () => {
  it('has type function', () => {
    assert(typeof Object === 'function');
  });

  it('has own properties as static properties and methods', () => {
    assert(Object.getOwnPropertyNames(Object).join(', '));
    // Chrome: 'length, name, prototype, assign, getOwnPropertyDescriptor, getOwnPropertyDescriptors, getOwnPropertyNames, getOwnPropertySymbols, is, preventExtensions, seal, create, defineProperties, defineProperty, freeze, getPrototypeOf, setPrototypeOf, isExtensible, isFrozen, isSealed, keys, entries, values'
  });

  it('is an instance of both Function and Object', () => {
    assert(Object instanceof Function);
    assert(Object instanceof Object);
    assert(Object.__proto__.constructor === Function);
    assert(Object.__proto__.__proto__.constructor === Object);
  });

  it(`has two [[Prototype]] point to Function.prototype and Object.prototype`, () => {
    assert(Object.__proto__ === Function.prototype);
    assert(Object.__proto__.__proto__ === Object.prototype);
    assert(Object.__proto__.__proto__.__proto__ === null);
  });
});


describe('Object.prototype', () => {
  it('has only one prototype', () => {
    assert(Object.prototype);
    assert(Object.prototype.prototype === undefined);
  });

  it('has own properties for Object instances to inherit', () => {
    assert(Object.getOwnPropertyNames(Object.prototype).join(', '));
    // Chrome: constructor, __defineGetter__, __defineSetter__, hasOwnProperty, __lookupGetter__, __lookupSetter__, isPrototypeOf, propertyIsEnumerable, toString, valueOf, __proto__, toLocaleString
  });

  it('has constructor points to itself', () => {
    assert(Object.prototype.constructor === Object);
  });

  it('has type object', () => {
    assert(typeof Object.prototype === 'object');
  });

  it('has no [[Prototype]]', () => {
    assert(Object.prototype.__proto__ === null);
  });

  it(`is the [[Prototype]] of a plain object`, () => {
    assert({}.__proto__ === Object.prototype);
  });

  it(`is the last [[Prototype]] of a Function instance`, () => {
    const func = function () {};
    assert(func.__proto__.__proto__ === Object.prototype);
  });

  it(`is the last [[Prototype]] of most primitive valuables`, () => {
    const boolean = true;
    const number = 0;
    const string = '';
    const symbol = Symbol();

    assert(boolean.__proto__.__proto__ === Object.prototype);
    assert(number.__proto__.__proto__ === Object.prototype);
    assert(string.__proto__.__proto__ === Object.prototype);
    assert(symbol.__proto__.__proto__ === Object.prototype);
  });

  it(`n.b. null and undefined do not have a [[Prototype]]`, () => {
    const null_ = null;
    const undefined_ = undefined;

    try {
      null_.__proto__;
    } catch (e) {
      assert(e.name === 'TypeError');
    };

    try {
      undefined_.__proto__;
    } catch (e) {
      assert(e.name === 'TypeError');
    };
  });

  it(`n.b. an Object instance is capabale of not having a [[Prototype]]`, () => {
    const obj = {};
    obj.__proto__ = null;
    assert(obj.__proto__ === undefined);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
