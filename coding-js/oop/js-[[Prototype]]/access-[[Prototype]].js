describe('Prototype chain for ctor funcitons', () => {
  const Parent = function () {};

  it('create() is preferred due to direct modify [[Prototype]] can be slow', () => {
    const Child = function () { Parent.call(this); };
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
  });

  it('direct access [[Prototype]] with __proto__', () => {
    const Child = function () { Parent.call(this); };
    Child.prototype.__proto__ = Parent.prototype;
  });

  it('direct access [[Prototype]] with wrapper method setPrototypeOf()', () => {
    const Child = function () { Parent.call(this); };
    Object.setPrototypeOf(Child.prototype, Parent.prototype)
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
