/*
Given a Function instance `func`:

`func`:

- Has type `function`
- Is an instance of both `Function` and `Object`
- Has two [[prototype]] point to:
    - `Function.prototype`
    - `Object.prototype`

`func.prototype`:

- Is an instance of `Object`
- Has a `constructor` property points to `func`
 */


describe('Two ways of creating an empty function instance', () => {
  it('uses function statement', () => {
    const func = function () {};
    assert(func.__proto__.constructor === Function);
  });

  it('uses constructor function Function()', () => {
    const func = new Function();
    assert(func.__proto__.constructor === Function);
  });
});


const func = function () {};


describe('func', () => {
  it('is a keyword, do NOT try to use it', () => {
    // function;
    // Outputs: Uncaught SyntaxError: Unexpected string
    // Not catchable in try...catch block
  });

  it('is the only object that has type function', () => {
    assert(typeof func === 'function');
  });

  it('has own properties', () => {
    assert(Object.getOwnPropertyNames(func).toString());
    // Chrome: length, name, arguments, caller, prototype
    assert(Object.getOwnPropertySymbols(func).length === 0);
  });

  it('is an instance of both Function and Object', () => {
    assert(func instanceof Function);
    assert(func instanceof Object);
    assert(func.__proto__.constructor === Function);
    assert(func.__proto__.__proto__.constructor === Object);
  });

  it(`has two [[Prototype]] point to Function.prototype and Object.prototype`, () => {
    assert(func.__proto__ === Function.prototype);
    assert(func.__proto__.__proto__ === Object.prototype);
    assert(func.__proto__.__proto__.__proto__ === null);
  });
});


describe('func.prototype', () => {
  it('has only one prototype', () => {
    assert(func.prototype);
    assert(func.prototype.prototype === undefined);
  });

  it('has own properties', () => {
    assert(Object.getOwnPropertyNames(func.prototype).toString() === 'constructor');
    assert(Object.getOwnPropertySymbols(func.prototype).length === 0);
  });

  it('has constructor points to itself', () => {
    assert(func.prototype.constructor === func);
  });

  it(`has [[Prototype]] points to Object.prototype`, () => {
    assert(func.prototype.__proto__ === Object.prototype);
  });

  it('is an instance of Object', () => {
    assert(func.prototype instanceof Object);
    assert(func.prototype.__proto__.constructor === Object);
  });
});

/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
