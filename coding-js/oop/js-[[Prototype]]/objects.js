/*
Objects:

- Are type `object`
- Have no `prorotype`

A plain object:

- Is an instance of Object
- Has one [[Prototype]] points to `Object.prototype`

An object created by a custom constructor function `Ctor`:

- Is an instance of `Ctor` and `Object`
- Has two [[Prototype]] point to `Ctor.prorotype` and `Object.prorotype`

An Object instance without [[Prototype]]:

- Has no [[Prototype]]
- Is not an instance of `Object`
 */


describe('Three ways of creating a plain object:', () => {
  it('literal notation', () => {
    const obj = {};
    assert(obj.__proto__.constructor ===  Object);
  });

  it('constructor function Object()', () => {
    const obj = new Object();
    assert(obj.__proto__.constructor ===  Object);
  });

  it('Object.create()', () => {
    const obj = Object.create(Object.prototype);
    assert(obj.__proto__.constructor ===  Object);
  });
});


describe('Object instances', () => {
  function Ctor() {}
  const obj1 = {};
  const obj2 = Object.create(null);
  const obj3 = new Ctor();

  it('are type object', () => {
    assert(typeof obj1 === 'object');
    assert(typeof obj2 === 'object');
    assert(typeof obj3 === 'object');
  });

  it('have no prototype', () => {
    assert(obj1.prototype === undefined);
    assert(obj2.prototype === undefined);
    assert(obj3.prototype === undefined);
  });
});


describe('A plain object', () => {
  it('is an instance of Object', () => {
    assert({} instanceof Object);
    assert({}.__proto__.constructor === Object);
  });

  it('has one [[Prototype]] points to Object.prototype', () => {
    assert({}.__proto__ === Object.prototype);
    assert({}.__proto__.__proto__ === null);
  });
});


describe('An object created by a custom constructor function Ctor', () => {
  const Ctor = function () {};
  const obj = new Ctor();

  it('is an instance of Ctor and Object', () => {
    assert(obj instanceof Ctor);
    assert(obj instanceof Object);
    assert(obj.__proto__.constructor === Ctor);
    assert(obj.__proto__.__proto__.constructor === Object);
  });

  it('has two [[Prototype]] points to Ctor.prototype and Object.prototype', () => {
    assert(obj.__proto__ === Ctor.prototype);
    assert(obj.__proto__.__proto__ === Object.prototype);
    assert(obj.__proto__.__proto__.__proto__ === null);
  });
});


describe('An Object instance without [[Prototype]]', () => {
  const obj1 = {};
  obj1.__proto__ = null;

  const obj2 = Object.create(null);

  it('has no [[Prototype]]', () => {
    assert(obj1.__proto__ === undefined);
    assert(obj2.__proto__ === undefined);
  });

  it('Is not an instance of Object', () => {
    assert(obj1 instanceof Object === false);
    assert(obj2 instanceof Object === false);
  })
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
