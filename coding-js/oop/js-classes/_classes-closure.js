const factoryModule = (function () {
  let moduleState = 0;

  const getModuleState = () => moduleState;
  const incrementModuleState = () => {
    moduleState += 1;
  };

  const getState = function () {
    return this.state;
  };
  const increment = function () {
    this.state += 10;
  };

  return function (state=0) {
    return {
      state,
      getState,
      increment,
      getModuleState,
      incrementModuleState,
    };
  };
})();

const object1 = factoryModule(100);
const object2 = factoryModule(200);

assert(object1.getModuleState() === 0);
assert(object2.getModuleState() === 0);
assert(object1.getState() === 100);
assert(object2.getState() === 200);

object1.incrementModuleState();
object2.incrementModuleState();
object1.increment();
object2.increment();
assert(object1.getModuleState() === 2);
assert(object2.getModuleState() === 2);
assert(object1.getState() === 110);
assert(object2.getState() === 210);

/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
