const Custom = function (state=0) {
  this.state = state;
};

Custom.prototype.increment = function () {
  this.state += 10;
}


describe('A custom constructor function', () => {
  it('creates custom objects with own state and methods', () => {
    const custom = new Custom(32);
    assert(custom.state === 32);

    custom.increment();
    assert(custom.state === 42);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
