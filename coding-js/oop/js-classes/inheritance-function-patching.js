const Parent = function () {
  this.parentMsg = 'parent';
  this.count = 0;
};
Parent.prototype = {
  constructor: Parent,
  increment () {
    this.count += 20;
  }
};

const Child = function () {
  Parent.call(this);
  this.childMsg = 'child'
};
Child.prototype = {
  __proto__: Parent.prototype,
  constructor: Child,
  decrement () {
    this.count -= 10;
  }
};


describe('Child instance', () => {
  const child = new Child();

  it('has type object', () => {
    assert(typeof child === 'object');
  });

  it('has instance properties of both Child and Parent', () => {
    assert(child.hasOwnProperty('parentMsg'));
    assert(child.hasOwnProperty('count'));
    assert(child.hasOwnProperty('childMsg'));

    assert(child.parentMsg === 'parent');
    assert(child.count === 0);
    assert(child.childMsg === 'child');
  });

  it('is an instance of Child, Parent, and Object', () => {
    assert(child instanceof Child);
    assert(child instanceof Parent);
    assert(child instanceof Object);

    assert(child.__proto__.constructor === Child);
    assert(child.__proto__.__proto__.constructor === Parent);
    assert(child.__proto__.__proto__.__proto__.constructor === Object);
  });

  it('has three [[Prototype]] point to Child.prototype, Parent.prototype, and Object.prototype', () => {
    assert(child.__proto__ === Child.prototype);
    assert(child.__proto__.__proto__ === Parent.prototype);
    assert(child.__proto__.__proto__.__proto__ === Object.prototype);

    child.increment();
    assert(child.count === 20);
    child.decrement();
    assert(child.count === 10);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
