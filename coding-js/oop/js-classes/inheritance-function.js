const Parent = function (state=0) {
  this.state = state;
};
Parent.prototype.increment = function () {
  this.state += 10;
};

const Child = function (state=0) {
  Parent.call(this, state);
}
Child.prototype.__proto__ = Parent.prototype;
Child.prototype.decrement = function () {
  this.state -= 10;
};


describe('A subclass constructor function', () => {
  it(`creates objects with own state and methods and parent's methods`, () => {
    const child = new Child(42);
    assert(child.state === 42);

    child.increment();
    assert(child.state === 52);

    child.decrement();
    assert(child.state === 42);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
