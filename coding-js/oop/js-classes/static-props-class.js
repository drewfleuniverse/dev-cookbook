class Custom {
  constructor() {
    Custom.count++;
  }

  static getCount() {
    return Custom.count;
  }
  static getCountThis() {
    return this.count;
  }

  getCount() {
    return Custom.count;
  }
  getCountThis() {
    return this.constructor.count;
  }
}

Custom.count = 0;


describe('Static variables', () => {
  it('are common to all custom objects', () => {
    assert(Custom.count === 0);

    const custom1 = new Custom();
    const custom2 = new Custom();

    assert(Custom.count === 2);
    assert(custom1.getCount() === Custom.count);
    assert(custom2.getCount() === Custom.count);
  });
});


describe('Static methods', () => {
  assert(Custom.count === 2);

  it('are accessible within static methods', () => {
    assert(Custom.getCount() === Custom.count);
    assert(Custom.getCountThis() === Custom.count);
  });

  it('are accessible within object methods', () => {
    const custom = new Custom();

    assert(custom.getCount() === Custom.count);
    assert(custom.getCountThis() === Custom.count);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
