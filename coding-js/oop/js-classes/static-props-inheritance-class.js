class Parent {
  constructor() {
    Parent.count++;
  }
};
Parent.count = 0;

class Child extends Parent {
  constructor() {
    super();
    Child.count++;
  }
  static getParentCountSuper() {
    return super.count;
  };
  static getParentCount() {
    return Parent.count;
  };
  static getParentCountThis() {
    return this.prototype.__proto__.constructor.count;
  };
}
Child.count = 0;



describe('Accessing static properties of a parent object', () => {
  new Parent();
  new Child();
  new Child();

  it('checks current counts', () => {
    assert(Parent.count === 3);
    assert(Child.count === 2);
  });

  it('should equal', () => {
    assert(Child.getParentCountSuper() === Parent.count);
    assert(Child.getParentCount() === Parent.count);
    assert(Child.getParentCountThis() === Parent.count);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
