const Parent = function () {
  Parent.count++;
};
Parent.count = 0;


const Child = function () {
  Parent.call(this);
  Child.count++;
}
Child.prototype.__proto__ = Parent.prototype;
Child.count = 0;

Child.getParentCount = function () {
  return Parent.count;
};
Child.getParentCountThis = function () {
  return this.prototype.__proto__.constructor.count;
};


describe('Accessing static properties of a parent object', () => {
  new Parent();
  new Child();
  new Child();

  it('checks current counts', () => {
    assert(Parent.count === 3);
    assert(Child.count === 2);
  });

  it('should equal', () => {
    assert(Child.getParentCount() === Parent.count);
    assert(Child.getParentCountThis() === Parent.count);
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
