Note: the name of a constructor function is equivalent to the class name

**Accessing static properties**

- Use the constructor function name

**Accessing static properties in static methods**

- Method 1: Use the constructor function name
- Method 2: Use `this`
    - `this` points to the constructor function

**Accessing static properties in class methods**

- Method 1: Use the constructor function name
- Mtdhod 2: Use `this.constructor`
    - `this` points to the current object
    - `this.constructor` points to the constructor function
