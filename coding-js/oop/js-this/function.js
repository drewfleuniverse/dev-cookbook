const cl = s => console.log(s)
const isBrowser = new Function('return typeof window === "object"')


let THIS, THAT, f


if (isBrowser()) {
  THIS = THAT = window
} else {
  THAT = module.exports
  THIS = global
}


f = function () {
  cl(this === THIS)
}

f()
f = function () {
  'use strict'
  cl(this === undefined)
}
f()

;(function () {
  cl(this === THIS)
})()

;(function () {
  'use strict'
  cl(this === undefined)
})()

;(() => {
  cl(this === THAT)
})()

;(() => {
  'use strict'
  cl(this === THAT)
})()
