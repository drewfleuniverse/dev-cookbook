const isBrowser = new Function('return typeof window === "object"')
const cl = i => console.log(i)

var v = 'foo'
let l = 'bar'
const c = 42


cl(this.l === undefined)
cl(this.c === undefined)


if (isBrowser()) {
  cl(this === window);
  cl(this.v === 'foo')
} else {
  cl(this === module.exports)
  cl(this.v === undefined)
  cl(this.hasOwnProperty('v') === false)
}
