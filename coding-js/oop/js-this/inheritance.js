// 'use strict'
// ^: same results
const cl = i => console.log(i)


const func = function() {
  return this
}


const p1 = {
  f1: func,
  f2: () => this,
  f3: func.bind(this),
}

const c1 = Object.create(p1)
c1.f4 = func
c1.f5 = () => this
c1.f6 = func.bind(this)

cl(c1.f1() === c1);
cl(c1.f2() === this);
cl(c1.f3() === this);
cl(c1.f4() === c1);
cl(c1.f5() === this);
cl(c1.f6() === this);


function P2() {
  this.f1 = func
  this.f2 = () => this
  this.f3 = func.bind(this)
}
P2.prototype.f4 = func
P2.prototype.f5 = () => this
P2.prototype.f6 = func.bind(this)

function C2() {
  P2.call(this)
  this.f7 = func
  this.f8 = () => this
  this.f9 = func.bind(this)
}

C2.prototype = Object.create(P2.prototype)
C2.prototype.constructor = C2
const c2 = new C2();


cl(c2.f1() === c2);
cl(c2.f2() === c2);
cl(c2.f3() === c2);
cl(c2.f4() === c2);
cl(c2.f5() === this);
cl(c2.f6() === this);
cl(c2.f7() === c2);
cl(c2.f8() === c2);
cl(c2.f9() === c2);
