// 'use strict'
// ^: same results
const cl = i => console.log(i)
const isBrowser = new Function('return typeof window === "object"')

let THIS, THAT, f


if (isBrowser()) {
  THIS = THAT = window
} else {
  THAT = module.exports
  THIS = global
}


const func = function() {
  return this
}

const o1 = {
  f1: func,
  f2: () => this,
  f3: func.bind(this),
  x: {
    f1: func,
    f2: () => this,
    f3: func.bind(this)
  }
}


cl(o1.f1() === o1);
cl(o1.f2() === THAT);
cl(o1.f3() === THAT);
cl(o1.x.f1() === o1.x);
cl(o1.x.f2() === THAT);
cl(o1.x.f3() === THAT);


const O2 = function() {
  this.f1 = func
  this.f2 = () => this
  this.f3 = func.bind(this)

  const X = function() {
    this.f1 = func
    this.f2 = () => this
    this.f3 = func.bind(this)
  }
  this.x = new X()
}
O2.prototype.f4 = func
O2.prototype.f5 = () => this
O2.prototype.f6 = func.bind(this)

const o2 = new O2()

cl(o2.f1() === o2);
cl(o2.f2() === o2);
cl(o2.f3() === o2);

cl(o2.f4() === o2);
cl(o2.f5() === THAT);
cl(o2.f6() === THAT);

cl(o2.x.f1() === o2.x);
cl(o2.x.f2() === o2.x);
cl(o2.x.f3() === o2.x);
