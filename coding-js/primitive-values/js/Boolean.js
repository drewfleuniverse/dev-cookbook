const cl = i => console.log(i)


let t, f, r


cl(true instanceof Boolean === false)
cl(typeof true === 'boolean')
cl(new Boolean(true) instanceof Boolean === true)
cl(typeof new Boolean(true) === 'object')

t = new Boolean(true)
f = new Boolean(false)


cl(t == true)
cl(f == false)


// t and f are objects, not primitive
// boolean values
cl(t !== true)
cl(f !== false)

if(t) r = 42
cl(r === 42)
if(f) r = 42
cl(r === 42)


cl(t.valueOf() === true)
cl(f.valueOf() === false)
