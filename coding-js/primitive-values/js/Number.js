const cl = i => console.log(i)


let a, b


cl(42 instanceof Number === false)
cl(new Number(42) instanceof Number === true)


a = Number.MAX_SAFE_INTEGER
b = Number.MIN_SAFE_INTEGER
cl(a === Math.pow(2, 53) - 1)
cl(b === -(Math.pow(2, 53) - 1))


cl(Number.NaN !== NaN)
cl(NaN !== NaN)

cl(Number.isNaN(NaN) === true);
cl(isNaN(NaN))


a = Number.POSITIVE_INFINITY
b = Number.NEGATIVE_INFINITY
cl(a === Infinity)
cl(b === -Infinity)
