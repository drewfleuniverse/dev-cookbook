describe('Local symbol', () => {
  it('', () => {
    const localKeyAccessor = Symbol('local');
    const obj = {
      prop: 1,
      [localKeyAccessor]: 2
    };

    assert(typeof localKeyAccessor === 'symbol');
    assert(localKeyAccessor in obj);

    let arr = [];
    for (const prop in obj) {
      arr.push(obj[prop]);
    }
    assert(arr.toString() === '1');
  });
});

describe('Global symbol', () => {
  let globalKeyAccessor;

  it('Symbol.for(key) adds key in the global symbol registry', () => {
    globalKeyAccessor = Symbol.for('global.key');
    assert(typeof globalKeyAccessor === 'symbol');

    assert(globalKeyAccessor === Symbol.for('global.key'))
    assert(Symbol.for('global.key') === Symbol.for('global.key'));
  });

  it('Symbol.keyFor(key) gets key in the gobal symbol registry', () => {
    assert(Symbol.keyFor(globalKeyAccessor) === 'global.key');
    assert(Symbol.keyFor(Symbol.for('global.key')) === 'global.key');
  });

  it('Use case: (pretty useless)', () => {
    // Prep
    const fakeEnv = 'prod';
    const fakeFetch = url => url;

    // fileA.js
    const DEV_URL = Symbol.for('localhost');
    const PROD_URL = Symbol.for('foo.com');

    // fileB.js
    const result = fakeFetch(
      fakeEnv === 'prod'
        ? Symbol.keyFor(PROD_URL)
        : Symbol.keyFor(DEV_URL)
    );

    assert(result === 'foo.com');
  });
});



/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
