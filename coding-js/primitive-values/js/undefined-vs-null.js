const cl = i => console.log(i)
const ts = o => Object.prototype.toString.call(o)


let a, b


a = undefined
b = null

cl(a === undefined)
cl(typeof a === 'undefined')
cl(ts(a) === '[object Undefined]')
cl(b === null)
cl(typeof b === 'object')
cl(ts(b) === '[object Null]')

cl(undefined !== null)
cl(undefined == null)


let x


cl(x === undefined)

x = function () {}
cl(x() === undefined)
x = function () { return }
cl(x() === undefined)
x = function (a) { return a }
cl(x() === undefined)

x = {}
cl(x.y === undefined)


cl(1 + null === 1)
cl(isNaN(1 + undefined))
cl('' + null === 'null')
cl('' + undefined === 'undefined')
