const cl = s => console.log(s)


const curried = function (f, l) {
  const args = []

  const helper = a => {
    args.push(a)

    if (--l > 0) return helper
    else return f.apply(null, args)
  }

  return helper
}


const f = (a,b,c) => a+b+c
const c = curried(f, 3)


let r


r = c('foo')('bar')(42)
cl(r)
