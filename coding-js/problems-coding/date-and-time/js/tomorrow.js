const cl = i => console.log(i)


Date.prototype.tomorrow = function () {
  const date = this.getDate()
  return new Date(this.setDate(date-1))
}


let d, e


d = new Date('1998/12/15')
cl(d.toDateString() === 'Tue Dec 15 1998')
e = d.tomorrow()
cl(e.toDateString() === 'Mon Dec 14 1998')
