const cl = s => console.log(s)
const js = o => JSON.stringify(o)


;(function () {
  let a
  
  a = [...arguments]
  cl(Array.isArray(a))
  cl(js(a) === '["foo","bar"]')
  
  a = Array.prototype.slice
    .call(arguments)
  cl(Array.isArray(a))
  cl(js(a) === '["foo","bar"]')
  
  a = Object.assign([], arguments)
  cl(Array.isArray(a))
  cl(js(a) === '["foo","bar"]')
})('foo', 'bar')





