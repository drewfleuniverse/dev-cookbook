const cl = i => console.log(i)


const isFooPassed = function() {
  return [...arguments].includes('foo')
}


let r


r = isFooPassed('foo', 'bar', 42)
cl(r === true)

r = isFooPassed(42, 'bar', 'foo')
cl(r === true)

r = isFooPassed('bar', 42)
cl(r === false)
