const cl = i => console.log(i)


Function.prototype.bind =
Function.prototype.bind || function (context) {
  const self = this
  return function () {
    return self.apply(context, arguments)
  }
}


Function.prototype.goodBind = function (context) {
  const self = this
  return function () {
    return self.apply(context, arguments)
  }
}

Function.prototype.badBind = function (context) {
  return function () {
    return this.apply(context, arguments)
  }
}

const f = function (k, v) {
  this[k] = v
}


let o, r


o = {}
r = f.goodBind(o)
r('foo', 'bar')
cl(o.foo === 'bar')

o = {}
r = f.badBind(o)
try {
  r('foo', 'bar')
} catch (e) {
  cl(e.name === 'TypeError')
  cl(e.message === 'this.apply is not a function')
}
