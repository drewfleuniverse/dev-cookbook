const cl = i => console.log(i)


const foo = {
  n: 'foo',
  f () { this.n = `${this.n}42` }
}

const bar = { n: 'bar' }


foo.f()
cl(foo.n === 'foo42')

const f = foo.f.bind(bar)
f()
cl(bar.n === 'bar42')
