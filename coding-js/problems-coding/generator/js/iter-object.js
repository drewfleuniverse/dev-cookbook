const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let o, r, a


o = Object.create({
  x: 'x'
})
o.a = 'a'
Object.defineProperty(o, 'b', {
  value: 'b'
})
o[Symbol('c')] = 'c'


r = []
for (const k in o) {
  r.push(k)
}
cl(js(r) === '["a","x"]')


r = []
for (k in o) {
  if (o.hasOwnProperty(k))
    r.push(k)
}
cl(js(r) === '["a"]')


r = []
a = Object.keys(o)
for (const k of a) {
  r.push(o[k])
}
cl(js(r) === '["a"]')


r = []
a = Object.getOwnPropertyNames(o)
for (const k of a) {
  r.push(o[k])
}
cl(js(r) === '["a","b"]')


r = []
a = Object.getOwnPropertySymbols(o)
for (const k of a) {
  r.push(o[k])
}
cl(js(r) === '["c"]')


o[Symbol.iterator] = function () {
  const self = this

  return {
    i: 0,
    next() {
      const a = Object.keys(self)
      if (this.i < a.length) {
        return {
          value: self[a[this.i++]],
          done: false
        }
      }
      return {value: undefined, done:true}
    }
  }
}

r = []
for (const k of o) {
  r.push(k)
}
cl(js(r) === '["a"]')



o[Symbol.iterator] = function* () {
  const a = Object.keys(this);
  while (a.length !== 0) {
    yield a.pop();
  }
};

r = []
for (const k of o) {
  r.push(k)
}
cl(js(r) ) === '["a"]')