const cl = i => console.log(i)


Function.prototype.newObject =
function () {
  const ctor = this;
  const obj = Object.create(ctor.prototype);
  // obj is the context to be applied
  ctor.apply(obj, arguments);

  return obj;
};

Function.prototype.badNewObject =
function () {
  const ctor = this;
  const obj = Object.create(ctor.prototype);
  ctor.apply(null, arguments);

  return obj;
};


const Ctor = function () {
  [...arguments].forEach((arg, i) => (
    this[`p${i}`] = arg
  ));
}


let o;


o = Ctor.newObject('foo', 'bar');
cl(o.constructor === Ctor);
cl(o.p0 === 'foo');
cl(o.p1 === 'bar');

o = Ctor.badNewObject('foo', 'bar');
cl(o.constructor === Ctor);
cl(o.p0 === undefined);
cl(o.p1 === undefined);

o = new Ctor('foo', 'bar');
cl(o.constructor === Ctor);
cl(o.p0 === 'foo');
cl(o.p1 === 'bar');
