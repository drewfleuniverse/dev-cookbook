const cl = i => console.log(i)


Function.prototype.ctorFunc =
function () {
  const o = Object.create(this.prototype)
  this.call(o, ...arguments)

  return o
}


const C = function () {
  [...arguments].forEach((arg, i) =>
    this[`p${i}`] = arg)
}


const o = C.ctorFunc('foo', 'bar')
cl(o.constructor === C)
cl(o.p0 === 'foo')
cl(o.p1 === 'bar')

const p = new C('foo', 'bar')
cl(p.constructor === C)
cl(p.p0 === 'foo')
cl(p.p1 === 'bar')
