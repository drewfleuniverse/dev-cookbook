const lg = i => console.log(i)

  
const html = '<div class="foo"><p>Foo</p></div>' 

const re = /<(\/?)(\w+)([^>]*?)>/g
const r = html.match(re)

lg(r[0] === '<div class="foo">')
lg(r[1] === '<p>')
lg(r[2] === '</p>')
lg(r[3] === '</div>')