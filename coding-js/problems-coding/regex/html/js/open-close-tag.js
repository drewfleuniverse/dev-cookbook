const lg = i => console.log(i)


const html = '<div class="foo"><p>Foo</p></div>' +
  '<div class="bar"></div>'

const re = /<(\w+)([^>]*)>(.*?)<\/\1>/g
const r = html.match(re)

lg(r[0] === '<div class="foo"><p>Foo</p></div>')
lg(r[1] === '<div class="bar"></div>')