## Split every char - `split` vs `split-regex` vs `match-regex`

- Preference: `split`
- Perf:
  - Chrome: `split` > `match-regex (-86%)` > `split-regex (-91%)`
  - Firefox: `split` > `match-regex (-100%)` > `split-regex (-100%)`
  - [link](https://jsperf.com/regex-split-every-single-char/1)
