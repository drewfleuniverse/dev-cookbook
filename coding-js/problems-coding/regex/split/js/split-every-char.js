const cl = s => console.log(s)
const js = o => JSON.stringify(o)


const split1 = s => s.split('')
const split2 = s => s.split(/(?!^)/)
const split3 = s => s.match(/[^]/g)


let s


s = 'abc'

cl(js(split1(s)) === '["a","b","c"]')
cl(js(split2(s)) === '["a","b","c"]')
cl(js(split3(s)) === '["a","b","c"]')


s = 'ab\ncd'

cl(js(split1(s)) === '["a","b","\\n","c","d"]')
cl(js(split2(s)) === '["a","b","\\n","c","d"]')
cl(js(split3(s)) === '["a","b","\\n","c","d"]')


s = ''

cl(js(split1(s)) === '[]')
cl(js(split2(s)) === '[""]')
cl(js(split3(s)) === 'null')
