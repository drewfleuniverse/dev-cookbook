const js = s => JSON.stringify(s)
const re = /(?:^|\s)foo(?:\s|$)/

console.log(js('foo bar'.match(re))
  === '["foo "]')
console.log(js(' foo bar '.match(re))
  === '[" foo "]')
console.log(js(' bar foo '.match(re))
  === '[" foo "]')
console.log(js(' foobar '.match(re))
  === 'null')