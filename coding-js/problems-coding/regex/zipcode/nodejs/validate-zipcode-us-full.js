function isZip(zip) {
  return /\d{5}-\d{4}/.test(zip)
}


console.log(isZip('12345-6789') === true)
console.log(isZip('2345-678') === false)
console.log(isZip('12345') === false)