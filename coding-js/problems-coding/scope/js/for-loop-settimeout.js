const cl = s => console.log(s)
const js = o => JSON.stringify(o)


const r = [],
  s = [],
  t = []


/*
- `i` is hoisted to and declared in outer scope, i.e. global scope
- `setTimeout`'s callback use the `i` in global scope
 */
for (var i = 0; i < 3; i++) {
  setTimeout(() => r.push(i), 1)
}
setTimeout(() =>
  cl(js(r) === '[3,3,3]')
, 1)
cl(i === 3)


/*
- `i` is declared within for-loop scope
- `setTimeout`'s callback use the `i` in each iteration block
 */
for (let i = 0; i < 3; i++) {
  setTimeout(() => s.push(i), 1)
}
setTimeout(() =>
  cl(js(s) === '[0,1,2]')
, 1)

/*
Explicitly demonstrate `setTimeout` looks for `i` in outer scope
 */
let j
for (j = 0; j < 3; j++) {
  setTimeout(() => t.push(j), 1)
}
setTimeout(() =>
  cl(js(t) === '[3,3,3]')
, 1)
cl(j === 3)


/*
Other solutions
 */

const u = [],
  v = [],
  w = [],
  x = []

for (var i = 0; i < 3; i++) {
  const a = i
  setTimeout(() => u.push(a), 1)
}
setTimeout(() =>
  cl(js(u) === '[0,1,2]')
, 1)


for (var i = 0; i < 3; i++) {
  setTimeout((a =>
    () => v.push(a)
  )(i), 1)
}
setTimeout(() =>
  cl(js(v) === '[0,1,2]')
, 1)


for (var i = 0; i < 3; i++) {
  setTimeout((
    a => w.push(a)
  ).bind(null, i), 1)
}
setTimeout(() =>
  cl(js(w) === '[0,1,2]')
, 1)


for (var i = 0; i < 3; i++) {
  setTimeout((a =>
    () => x.push(a)
  ).call(null, i), 1)
}
setTimeout(() =>
  cl(js(x) === '[0,1,2]')
, 1)
