const cl = s => console.log(s)




/*
`+` as string concatenation operator:
- When one of its operands is
    - A primitive string
    - An object has `toString`
- `null`, `true`, `false`, and `undefined` are implicitly convrted to string
 */

cl('4' + '2' === '42')
cl('4' +  2  === '42')
cl( 4  + '2' === '42')

cl({} + {} === '[object Object][object Object]')
cl({} + 123 === '[object Object]123')
cl(123 + {} === '123[object Object]')

cl(new Number('123') + '456' === '123456')
cl('123' + new Number('456') === '123456')

cl(null + 'foo' === 'nullfoo')
cl(true + 'foo' === 'truefoo')
cl(undefined + 'foo' === 'undefinedfoo')


/*
`+` and `-` as addition and subtraction operators
- Happens when:
    - Applied alone as the first operator or within a parenthesis
    - Any one of its operands:
        - Is primitive number
        - Has `valueOf` returns number
- Convert primitive strings into number
    - Strings not in numeric format result in `NaN`
- `null` and booleans are converted to 0, -0, 1, or -1
 */

cl(+123 === 123)
cl(-123 === -123)
cl(+'123' === 123)
cl(-'123' === -123)
cl(+new String(123) === 123)
cl(-new String(123) === -123)
cl(isNaN(+'foo'))
cl(isNaN(-'foo'))
cl(+null === 0)
cl(-null === -0)
cl(+true === 1)
cl(-true === -1)
cl(+false === 0)
cl(-false === -0)
cl(isNaN(+undefined))
cl(isNaN(-undefined))
cl(isNaN(+{}))
cl(isNaN(-{}))

cl('foo' + (+123) === 'foo123')
cl('foo' + (-123) === 'foo-123')

cl(123 + '123' === '123123')
cl(123 - '123' === 0)

cl(123 + (+'123') === 246)
cl(123 + (-'123') === 0)
cl(123 - (-'123') === 246)

cl(null + 123 === 123)
cl(null - 123 === -123)

cl(true + 123 === 124)
cl(true - 123 === -122)
cl(false + 123 === 123)
cl(false - 123 === -123)

cl(isNaN(undefined + 123))
cl(isNaN(undefined - 123))

cl(123 + null === 123)
cl(123 - null === 123)

cl(123 + true === 124)
cl(123 - true === 122)
cl(123 + false === 123)
cl(123 - false === 123)

cl(isNaN(123 + undefined))
cl(isNaN(123 - undefined))



var a = typeof a
cl(a === 'undefined')

try {
  let b = typeof b
} catch (e) {
  cl(e.name === 'ReferenceError')
}

cl(typeof null === 'object')
cl(3 instanceof Number === false)
cl(null == undefined)
cl(1,2,3 === 3)

cl(2 in [1,2] === false)
cl(2 in [1,2,3] === true)

cl(42. === 42.0)
cl(42 . toString() === '42')
cl(42..toString() === '42')
cl((42.).toString() === '42')


cl(parseFloat('3.14.123') === 3.14)
cl(parseFloat('3.14whatever') === 3.14)

cl(isNaN(Math.max([1,2,3])))


cl(1 && 2 && 3 === 3)
