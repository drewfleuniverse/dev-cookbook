const cl = s => console.log(s);
const js = o => JSON.stringify(o);

var foo = {n: 1};
var bar = foo;
foo.x = foo = {n: 2};

cl(bar !== foo);
cl(js(foo) === '{"n":2}');
cl(js(bar) === '{"n":1,"x":{"n":2}}');
