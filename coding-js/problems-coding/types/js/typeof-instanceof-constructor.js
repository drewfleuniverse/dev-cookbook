const cl = i => console.log(i)


/*
Use typeof with primitive types
 */

cl(typeof 42 === 'number')
cl(typeof '' === 'string')
cl(typeof true === 'boolean')
cl(typeof Symbol() === 'symbol')
cl(typeof undefined === 'undefined')
// null only primitive has type object
cl(typeof null === 'object')


/*
Use conctructor with objects
Notes:
- conctructor points to object's conctructor function
- constructor is writable
 */

cl({}.constructor === Object)
cl([].constructor === Array)
cl((function (){}).constructor === Function)
cl((new Date()).constructor === Date)

function Foo() {}
cl((new Foo()).constructor === Foo)
class Bar {}
cl((new Bar()).constructor === Bar)

Foo.prototype.constructor = Bar
cl((new Foo()).constructor === Bar)
Bar.prototype.constructor = Foo
cl((new Bar()).constructor === Foo)


/*
Use instanceof to lookup constructors in the
prototype chain
 */

cl([] instanceof Array)
cl([] instanceof Object)

let o

o = Object.create({})
cl(o instanceof Object)

function C() {}
o = Object.create(C.prototype)
cl(o instanceof C)
cl(o instanceof Object)
