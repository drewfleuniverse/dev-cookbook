const lg = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const memoize = f => {
  const cache = {}
  return (...args) => {
    const a = args[0]
    if (a in cache) {
      return cache[a]
    } else {
      const r = f(a)
      cache[a] = r
      return r
    }
  }
}


const fib = n => {
  if (n <= 0) return 0
  let p = 1, c = 0
  while (n > 0) {
    c = p + c
    p = c - p
    n--
  }

  return c
}

const memFib = memoize(fib)


const r = []
for (let i = 0; i <= 10; i++)
  r.push(memFib(i))
lg(jsf(r) === `[
  0,
  1,
  1,
  2,
  3,
  5,
  8,
  13,
  21,
  34,
  55
]`)
