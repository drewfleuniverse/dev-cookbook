const cl = i => console.log(i)


class Calc {
  constructor() {
    this.value = 0
  }

  add(v) {
    this.value += v
    return this
  }

  substract(v) {
    this.value -= v
    return this
  }
}


const c = new Calc()

cl(c.add(100).substract(58).value === 42)
