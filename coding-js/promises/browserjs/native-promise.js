const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


const a1 = []
const a2 = []


const p = (willSuccess, a) =>
  new Promise((resolve, reject) => {
  a.push('pStart')

  window.setTimeout(() => {
    a.push('tStart')

    willSuccess
      ? resolve('foo')
      : reject('bar')

    a.push('tEnd')
  }, 1)

  a.push('pEnd')
}).then(result => {
  a.push('then1')

  cl(result === 'foo')

  return result + 'bar'
}).then(result2 => {
  a.push('then2')

  cl(result2 === 'foobar')
}).catch(error => {
  a.push('error')

  cl(error === 'bar')
}).finally(() => {
  a.push('finally')
})


const p1 = p(true, a1)
const p2 = p(false, a2)

cl(js(a1) === '["pStart","pEnd"]')
cl(js(a2) === '["pStart","pEnd"]')

p1.then(r => {
  cl(r === undefined)
  cl(js(a1) === '["pStart","pEnd",' +
    '"tStart","tEnd","then1","then2","finally"]')
})
p2.then(r => {
  cl(r === undefined)
  cl(js(a2) === '["pStart","pEnd",' +
    '"tStart","tEnd","error","finally"]')
})
