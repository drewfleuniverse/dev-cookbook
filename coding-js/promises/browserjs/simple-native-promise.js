const cl = i => console.log(i)


let pro;

pro = new Promise((resolve, reject) => {
  resolve('foo');
});

pro.then(r => cl(r === 'foo'))


pro = new Promise((resolve, reject) => {
  reject('bar');
});

pro
  .then(r => 'not called')
  .catch(e => cl(e === 'bar'))
