import {localSymbol, globalSymbol} from './symbols.mjs'


console.assert(localSymbol === localSymbol)
console.assert(localSymbol !== Symbol('localSymbol'))
console.assert(globalSymbol === globalSymbol)
console.assert(globalSymbol === Symbol.for('globalSymbol'))


const nobody = Symbol()
const localGuy = Symbol('localGuy')
const globalGuy = Symbol.for('globalGuy')

console.assert(nobody !== Symbol())
console.assert(localGuy !== Symbol('localGuy'))
console.assert(globalGuy === Symbol.for('globalGuy'))

console.assert(Symbol.keyFor(globalGuy) === 'globalGuy')


const privateThing = Symbol('privateThing')
const iHaveThings = {}

iHaveThings[privateThing] = 'secret'
iHaveThings.publicThing = 'everybodyKnows'

console.assert(iHaveThings[privateThing] === 'secret')
console.assert(iHaveThings.publicThing === 'everybodyKnows')
console.assert(Object.getOwnPropertyNames(iHaveThings).toString() === 'publicThing')
console.assert(Object.getOwnPropertySymbols(iHaveThings)[0] === privateThing)
