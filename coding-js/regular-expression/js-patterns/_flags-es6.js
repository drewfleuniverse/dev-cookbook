const describe=function(a,b){console.log('%c'+a,'font-weight: bold;'),b()},it=function(a,b){console.log('%c'+a,'color:grey;'),b()},assert=function(a,b=!1,d=''){if(b&&console.log(`Assertion log: ${a}`),!a)throw new Error(d)};


const input = `Lorem hello
world ipsum.`;

describe('s: all dot mode', () => {
  it('should match', () => {
    assert(/hello.world/su.test(input));
  });
});

describe('without all dot mode', () => {
  it('should not match', () => {
    assert(/hello.world/u.test(input) === false);
  });

  it('should match', () => {
    assert(/hello[\s\S]world/u.test(input));
  });

  it('should match', () => {
    assert(/hello[^]world/u.test(input));
  });
});
