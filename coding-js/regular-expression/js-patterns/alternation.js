const js = (s) => JSON.stringify(s)


// Or
console.log(js('ab cd'.match(/cd|ef/)) === '["cd"]');
console.log(js('ef gh'.match(/cd|ef/)) === '["ef"]');
