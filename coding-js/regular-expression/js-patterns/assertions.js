const js = (s) => JSON.stringify(s)


// Only if followed by
console.log(js('abc'.match(/ab(?=c)/)) === '["ab"]');
console.log(js('abx'.match(/ab(?=c)/)) === 'null');

// Only if followed by
console.log(js('abx'.match(/ab(?!c)/)) === '["ab"]');
console.log(js('abc'.match(/ab(?!c)/)) === 'null');
