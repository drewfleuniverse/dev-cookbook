const js = (s) => JSON.stringify(s)

// Beginning
console.log(js('abc'.match(/^a/)) === '["a"]');
console.log(js('bac'.match(/^a/)) === 'null');

// End
console.log(js('abc'.match(/c$/)) === '["c"]');
console.log(js('bca'.match(/c$/)) === 'null');

// Word boundary
console.log(js('abc'.match(/\ba/)) === '["a"]');
console.log(js('abc'.match(/c\b/)) === '["c"]');
console.log(js('abc'.match(/\bb/)) === 'null');
console.log(js('a c'.match(/a\bc/)) === 'null');

// Non-word boundary
console.log(js('abc'.match(/\Bb/)) === '["b"]');
console.log(js('abc'.match(/a\B/)) === '["a"]');
console.log(js('abc'.match(/\Ba/)) === 'null');
