const js = (s) => JSON.stringify(s)


// Char
console.log(js('abc'.match(/.b/)) === '["ab"]')
console.log(js('0bc'.match(/.b/)) === '["0b"]')
console.log(js('£bc'.match(/.b/)) === '["£b"]')
console.log(js('bac'.match(/.b/)) === 'null')

// Digit
console.log(js('abc123'.match(/\d/)) === '["1"]')
console.log(js('abc123'.match(/[0-9]/)) === '["1"]')

// Not digit
console.log(js('abc123'.match(/\D/)) === '["a"]')
console.log(js('abc123'.match(/[^0-9]/)) === '["a"]')

// Alphanumeric and _
console.log(js('abc123!'.match(/\w/)) === '["a"]')
console.log(js('abc123!'.match(/[A-Za-z0-9_]/)) === '["a"]')

// Not alphanumeric and not _
console.log(js('abc123!'.match(/\W/)) === '["!"]')
console.log(js('abc123!'.match(/[^A-Za-z0-9_]/)) === '["!"]')

// White space
console.log(js('foo bar'.match(/\s\w/)) === '[" b"]')

// Escape
console.log(js('foo*'.match(/o\*/)) === '["o*"]')
