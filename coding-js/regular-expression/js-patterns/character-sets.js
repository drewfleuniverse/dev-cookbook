const cl = s => console.log(s)
const js = (s) => JSON.stringify(s)


// Char set
cl(js('abc'.match(/[abc]/)) === '["a"]')
cl(js('abc'.match(/[a-c]/)) === '["a"]')
cl(js('-abc'.match(/[a-]/)) === '["-"]')

// Negated/complemented char set
cl(js('az'.match(/[^abc]/)) === '["z"]')
cl(js('az'.match(/[^a-c]/)) === '["z"]')
cl(js('-az'.match(/[^a-]/)) === '["z"]')

// Dot matches dot in char set
cl(js('x.y'.match(/[.]/g)) === '["."]')

// Use negate to match any single char
// and new lines
cl(js('x.y'.match(/[^]/g)) ===
  '["x",".","y"]')
cl(js('\n\r\u2028\u2029'.match(/[^]/g)) ===
`["\\n","\\r"," "," "]`)
