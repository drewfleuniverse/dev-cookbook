const js = (s) => JSON.stringify(s)


// Capturing group (match and remember)
console.log(js('foo bar'.match(/(foo)/)) === '["foo","foo"]');
console.log('foo bar'.replace(/(foo)/, '*$1*') === "*foo* bar");

// Reference to capturing group
console.log(js('a!a!a!'.match(/(a!)\1/)) === '["a!a!","a!"]')
console.log(js('a!a!a!'.match(/(a!)\1\1/)) === '["a!a!a!","a!"]')

// Non-capturing group
console.log(js('a!a!a!'.match(/(?:a!)/)) === '["a!"]')
console.log(js('a!a!a!'.match(/(?:a!)\1/)) === 'null')
