const log = i => console.log(i)
const js = (s) => JSON.stringify(s)


const s1 = 'foo bar 42'

const re1 = /\w+\s/
const re2 = new RegExp('\\w+\\s')
const re3 = RegExp('\\w+\\s')

log(js(s1.match(re1)) === '["foo "]')
log(js(s1.match(re2)) === '["foo "]')
log(js(s1.match(re3)) === '["foo "]')
log(js(re1.exec(s1)) === '["foo "]')


const re4 = /(\w+)(\s)/g
const re5 = new RegExp('(\\w+\\s)', 'g')

log(js(s1.match(re4)) === '["foo ","bar "]')
log(js(s1.match(re5)) === '["foo ","bar "]')

log(js(re4.exec(s1)) === '["foo ","foo"," "]')
log(js(re4.exec(s1)) === '["bar ","bar"," "]')
log(js(re4.exec(s1)) === 'null')


const r9 = s1.replace(/(\w+)\s(\w+)/, '$2 $1')
log(r9 === 'bar foo 42')
