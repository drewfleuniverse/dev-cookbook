const js = (s) => JSON.stringify(s)


// 0 or more
console.log(js('aaa'.match(/a*/)) === '["aaa"]')
console.log(js(''.match(/a*/)) === '[""]')
console.log(js('aaa'.match(/a{0,}/)) === '["aaa"]')
console.log(js(''.match(/a{0,}/)) === '[""]')

// 1 or more
console.log(js('aaa'.match(/a+/)) === '["aaa"]')
console.log(js(''.match(/a+/)) === 'null')
console.log(js('aaa'.match(/a{1,}/)) === '["aaa"]')
console.log(js(''.match(/a{1,}/)) === 'null')

// 0 or 1
console.log(js('aaa'.match(/a?/)) === '["a"]')
console.log(js(''.match(/a?/)) === '[""]')
console.log(js('aaa'.match(/a{0,1}/)) === '["a"]')
console.log(js(''.match(/a{0,1}/)) === '[""]')

// Exact n times
console.log(js('aaa'.match(/a{2}/)) === '["aa"]');
console.log(js('a'.match(/a{2}/)) === 'null');

// At least n times
console.log(js('aaa'.match(/a{2,}/)) === '["aaa"]');
console.log(js('a'.match(/a{2,}/)) === 'null');

// n to m times
console.log(js('aaa'.match(/a{2,3}/)) === '["aaa"]');
console.log(js('aa'.match(/a{2,3}/)) === '["aa"]');
console.log(js('a'.match(/a{2,3}/)) === 'null');

// Non-greedy
console.log(js('aaa'.match(/a*?/)) === '[""]');
console.log(js('aaa'.match(/a+?/)) === '["a"]');
console.log(js('aaa'.match(/a??/)) === '[""]');
console.log(js('aaa'.match(/a{2}?/)) === '["aa"]');
console.log(js('aaa'.match(/a{2,}?/)) === '["aa"]');
console.log(js('aaa'.match(/a{2,3}?/)) === '["aa"]');
