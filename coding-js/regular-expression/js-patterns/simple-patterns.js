const js = (s) => JSON.stringify(s)

console.log(js(''.match(/a/)) === 'null')
console.log(js('foo bar'.match(/foo bar/)) === '["foo bar"]')
