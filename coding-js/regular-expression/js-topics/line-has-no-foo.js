const js = (s) => JSON.stringify(s)


const re = /^((?!foo).)*$/m;

const r1 = '\n'.match(re)
console.log(js(r1) === '["",null]')
console.log(r1.index === 0)

const r2 = 'foo\nbar'.match(re)
console.log(js(r2) === '["bar","r"]')
console.log(r2.index === 4)

const r3 = 'bar\nfoo'.match(re)
console.log(js(r3) === '["bar","r"]')
console.log(r3.index === 0)

const r4 = 'bar\nbar'.match(re)
console.log(js(r4) === '["bar","r"]')
console.log(r4.index === 0)

console.log('|\\');
