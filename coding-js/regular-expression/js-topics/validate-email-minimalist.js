const js = (s) => JSON.stringify(s)

const re1 = /\S+@\S+\.\S+/
const re2 = /[^\s@]+@[^\s@]+\.[^\s@]+/

console.log(js('foo@bar.com'.match(re1)) === '["foo@bar.com"]');
console.log(js('foo@@bar.com'.match(re1)) === '["foo@@bar.com"]');

console.log(js('foo@bar.com'.match(re2)) === '["foo@bar.com"]');
console.log(js('foo@@bar.com'.match(re2)) === 'null');
