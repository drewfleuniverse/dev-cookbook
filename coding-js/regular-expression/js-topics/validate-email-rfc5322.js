const js = (s) => JSON.stringify(s)

const re = RegExp([
  '(?:',
    "[a-z0-9!#$%&'*+/=?^_`{|}~-]+",
    '(?:',
      "\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+",
    ')*',
    '|',
    '"',
    '(?:',
      '[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]',
      '|',
      '\\',
      '[\x01-\x09\x0b\x0c\x0e-\x7f]',
    ')*',
    '"',
  ')',
  '@',
  '(?:',
    '(?:',
      '[a-z0-9]',
      '(?:',
        '[a-z0-9-]*[a-z0-9]',
      ')?',
      '\.',
    ')+',
    '[a-z0-9]',
    '(?:',
      '[a-z0-9-]*',
      '[a-z0-9]',
    ')?',
    '|',
    '\\',
    '[',
      '(?:',
        '(?:',
          '(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])',
        ')',
        '\.',
      '){3}',
      '(?:',
        '(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])',
        '|',
        '[a-z0-9-]*[a-z0-9]:',
        '(?:',
          '[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]',
          '|',
          '\\',
          '[\x01-\x09\x0b\x0c\x0e-\x7f]',
        ')+',
      ')',
      '\\',
    ']',
  ')'
].join(''))


console.log(
  js('foo@bar.com'.match(re)) ===
  '["foo@bar.com",null,null,null,null]'
)
console.log(
  js('foo@com'.match(re)) ===
  '["foo@com",null,null,null,null]'
)
console.log(
  js('foo@com2'.match(re)) ===
  '["foo@com2",null,null,null,null]'
)
console.log(
  js('2foo@com'.match(re)) ===
  '["2foo@com",null,null,null,null]'
)
console.log(
  js('foo@[66.65.29.229]'.match(re)) ===
  '["foo@[66.65.29.229]","29",null,"229","29"]'
)
console.log(
  js('foo@[2604:2000:1500:23d:1d2e:2ac2:a992:9bb1]'.match(re)) ===
  '["foo@[2604:2000:1500:23d:1d2e:2ac2:a992:9bb1]","20",null,null,null]'
)
