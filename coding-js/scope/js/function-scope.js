const cl = s => console.log(s)


;(() => {
  {
    function f () { return 'f' }
  }
  cl(f() === 'f')

  if (false) {
    function g () { return 'g' }
  }
  cl(g === undefined)

  if (true) {
    function h () { return 'h' }
  }
  cl(h() === 'h')
})()


;(() => {
  'use strict'
  {
    function f () { return 'f' }
  }

  try { f } catch (e) {
    cl(e.name === 'ReferenceError')
  }


  if (true) {
    function g () { return 'g' }
  }

  try { g } catch (e) {
    cl(e.name === 'ReferenceError')
  }
})()
