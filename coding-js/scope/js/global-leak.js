// 'use strict'
// ^same results
const cl = i => console.log(i)


var a, b
let c, d

a = 'g.a'
b = 'g.b'
c = 'g.c'
d = 'g.d'

;(() => {
  var a = b = 'f'
  let c = d = 'f'
  // ^same as:
  // var a = (b = 'f')
  // let c = (d = 'f')
  cl(a === 'f')
  cl(b === 'f')
  cl(c === 'f')
  cl(d === 'f')
})()

cl(a === 'g.a')
cl(b === 'f')
cl(c === 'g.c')
cl(d === 'f')
