const isBrowser = new Function('return typeof window === "object"')
const cl = s => console.log(s)
const hop = (o, p) => Object.prototype
  .hasOwnProperty.call(o, p)

var v = 'g.v'
let l = 'g.l'
const c = 'g.c'
function f () {}
class C {}

if (isBrowser()) {
  cl(hop(window, 'v') === true)
  cl(hop(window, 'l') === false)
  cl(hop(window, 'c') === false)
  cl(hop(window, 'f') === true)
  cl(hop(window, 'C') === false)

  window.x = 'w.x'
  cl(hop(window, 'x') === true)
} else {
  cl(hop(global, 'v') === false)
  cl(hop(global, 'l') === false)
  cl(hop(global, 'c') === false)
  cl(hop(global, 'f') === false)
  cl(hop(global, 'C') === false)

  cl(hop(module.exports, 'v') === false)
  cl(hop(module.exports, 'l') === false)
  cl(hop(module.exports, 'c') === false)
  cl(hop(module.exports, 'f') === false)
  cl(hop(module.exports, 'C') === false)


  global.x = 'gl.x'
  module.exports.y = 'mo.y'

  cl(hop(global, 'x') === true)
  cl(hop(module.exports, 'y') === true)

  cl(hop(global, 'y') === false)
  cl(hop(module.exports, 'x') === false)
}
