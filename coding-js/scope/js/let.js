// 'use strict'
// ^: same results
const cl = s => console.log(s)
const js = o => JSON.stringify(o)


let a = 'g.a'


;(() => {
  cl(a === 'g.a')

  a = 'f.a'
  cl(a === 'f.a')
})()


try { cl(b) } catch (e) {
  cl(e.name === 'ReferenceError')
}

let b


{
  let y
}
try { cl(y) } catch (e) {
  cl(e.name === 'ReferenceError')
}


;(() => {
  let x
})()

try { cl(x) } catch (e) {
  cl(e.name === 'ReferenceError')
}
