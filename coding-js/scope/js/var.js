// 'use strict'
// ^: same results
const cl = s => console.log(s)
const js = o => JSON.stringify(o)


var a = 'g.a'


;(() => {
  cl(a === 'g.a')

  a = 'f.a'
  cl(a === 'f.a')
})()


cl(b === undefined)

b = 'g.b'
cl(b === 'g.b')

var b


{
  var y
}
cl(y === undefined)


;(() => {
	var x
})()

try { cl(x) } catch (e) {
	cl(e.name === 'ReferenceError')
}
