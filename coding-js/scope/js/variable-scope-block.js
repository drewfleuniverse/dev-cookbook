// 'use strict'
// ^: same results
const cl = s => console.log(s)


var a = 'a'
let b = 'b'

{
  cl(a === 'a')
  try { b } catch (e) {
    cl(e.name === 'ReferenceError')
  }

  var a = '_a' // access outer a
  let b = '_b'
  var c = 'c'
  let d = 'd'
}

cl(a === '_a')
cl(b === 'b')
cl(c === 'c')
try { d } catch (e) {
  cl(e.name === 'ReferenceError')
}


if (false) { var e = 'e' }
if (true) { var f = 'f' }
cl(e === undefined)
cl(f === 'f')


// i is hoisted to outer scope of for loop
for (var i = 0; i < 3; i++) {}
cl(i === 3)
// The above is equivalent to this:
var j = 42
for (j = 0; j < 3; j++) {}
cl(j === 3)

for (let k = 0; k < 3; k++) {}
try { k } catch (e) {
  cl(e.name === 'ReferenceError')
}
