// 'use strict'
// ^: same result
const cl = s => console.log(s)


var a = 'a'
let b = 'b'
var c = 'c'
let d = 'd'

;(() => {
  cl(a === 'a')
  cl(b === 'b')
  cl(c === undefined)
  try { d } catch (e) {
    cl(e.name === 'ReferenceError')
  }

  a = '_a'
  b = '_b'
  var c = '_c'
  let d = '_d'
  
  var e = 'e'
  let f = 'f'
})()

cl(a === '_a')
cl(b === '_b')
cl(c === 'c')
cl(d === 'd')

try { e } catch (e) {
  cl(e.name === 'ReferenceError')
}
try { f } catch (e) {
  cl(e.name === 'ReferenceError')
}
