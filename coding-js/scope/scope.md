# Scope

## Lexical scope and dynamic scope

###### Lexical scope

- Creates new scope upon name declaration
- Can be done at compile time, using static analysis (of program text)

###### Dynamic scope

- Creates new scope upon program execution
- Can be done at runtime, using call stack

###### Notes
- When analyzing function declaration and execution, be careful with:
  - Name hoisting
  - Closure support
