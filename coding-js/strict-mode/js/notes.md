Purposes

- Throw errors on silent errors in non-strict mode
- Safer behaviors


Invocation

- Entire script, first line first statement
- Function scope, first line first statement


Throw errors

- Assignment to undeclared variables
- Assignment to non-writable global constants
- Assignment to getter-only properties
- Assignment to add properties on non-extensible objects, i.e. `Object.isExtensible()` returns false
- Delete undeletable properties on an object, i.e. `Object.getOwnPropertyDescriptor()` show a property has `configurable` set to false
- Duplicate function parameter names
- Prefixing octal number with a zero
- Add properties on primitive values
- [things on `with`, tldr;]
- Assignment and bind to `arguments` and `eval`


Behaviors

- `eval` with new `var a = 42` will not access `var a` in its outer or global scope
- [too many things on `eval`, tldr;]
- `arguments` doesn't track parameter values
- `this` is `undefined` in functions, except arrow functions


## Notes

**`babel-plugin-transform-strict-mode`**

- Assignment to a non-writable global doesn't throw TypeError
