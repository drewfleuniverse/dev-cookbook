/*
string.charAt(indexNumber=0): charString | emptyString
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


const s = 'foobar'

cl(s.charAt() === 'f')
cl(s.charAt(0) === 'f')
cl(s.charAt(s.length) === '')
