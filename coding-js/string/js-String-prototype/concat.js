/*
string.concat(string2[, string3, ..., stringN]): string
- slower than +
 */
const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


/*
Assignment operators (+, +=) are prefered.
they are over 10 times faster than concat
and array join
 */


const s = 'foo'
const r = s.concat('bar', '42')

cl(s === 'foo');
cl(r === 'foobar42');

// note: faster than `[...str].join()`
// but much slower than `+` and `+=`
