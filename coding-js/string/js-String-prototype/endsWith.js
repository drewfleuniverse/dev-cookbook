const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'foobar42'

cl(s.endsWith('42') === true)
cl(s.endsWith('42', s.length) === true)

cl(s.endsWith('bar') === false)
cl(s.endsWith('bar', s.length-2)
  === true)