/*
string.includes(subString[, startIndexNumber]): boolean
 */

const s1 = 'foobar'

console.assert(s1.includes('foo') === true)
console.assert(s1.includes('Foo') === false)
console.assert(s1.includes('') === true)
