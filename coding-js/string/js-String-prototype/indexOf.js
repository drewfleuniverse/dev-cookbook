/*
string.indexOf(subString[, startIndexNumber]): indexNumber | -1
 */

const s1 = 'foobar'

console.assert(s1.indexOf('foo') === 0)
console.assert(s1.indexOf('Foo') === -1)

console.assert(s1.indexOf('') === 0)
