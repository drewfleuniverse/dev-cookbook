/*
string.lastIndexOf(subString[, startIndexNumber]): indexNumber | -1
 */

const s1 = 'foobarfoobar'

console.assert(s1.indexOf('foo') === 6)
console.assert(s1.indexOf('Foo') === -1)
