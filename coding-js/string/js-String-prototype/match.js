/*
string.match(regExp): array | null
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'foobar42'

cl(js(s.match()) === '[""]')
cl(js(s.match('foo')) === '["foo"]')
cl(js(s.match(/[a-c]/)) === '["b"]')
cl(js(s.match(/[a-c]/g)) === '["b","a"]')
