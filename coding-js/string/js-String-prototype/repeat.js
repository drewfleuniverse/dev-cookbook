const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'foo'

cl(s.repeat() === '')
cl(s.repeat(0) === '')
cl(s.repeat(1) === 'foo')
cl(s.repeat(2) === 'foofoo')