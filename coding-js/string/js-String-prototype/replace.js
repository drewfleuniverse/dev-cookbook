/*
string.replace(regExp | subString, newSubString | Function): string
 */

const cl = i => console.log(i)


let s, r


s = 'foobar'

r = s.replace('foo', 'FOO')
cl(r !== s)
cl(s === 'foobar')
cl(r === 'FOObar')


s = 'abc123^&*'

const f = (m, p1, p2, p3, o, s) =>
  [p1, p2, p3].join('-')

r = s.replace(/([^\d]*)([\d]*)([^\w]*)/, f)
cl(r === 'abc-123-^&*')


s = 'foo-bar'

r = s.replace(/-/, "$'-$$-$`")
cl(r === 'foobar-$-foobar')


s = 'bar foo'

r = s.replace(/(\w+)\s(\w+)/, '$2$1')
cl(r === 'foobar')
