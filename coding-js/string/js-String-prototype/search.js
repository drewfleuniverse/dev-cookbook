/*
string.search(regExp): indexNumber | -1
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'foobar42'


cl(s.search('') === 0)
cl(s.search(/w*/) === 0)
cl(s.search('foo') === 0)


cl(s.search(/[a-c]/) === 3)
cl(s.search(/[a-c]/g) === 3)

cl(s.search(24) === -1)
