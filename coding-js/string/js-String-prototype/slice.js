/*
string.slice(beginIndexNumber[, endIndexNumber]): string
*/


const cl = i => console.log(i)


const s = 'foobar'


cl(s.slice() === s) // primitive value === primitive value
cl(s.slice() === 'foobar')
cl(s.slice(0, s.length) === 'foobar')

cl(s.slice(3) === 'bar')
cl(s.slice(3, 4) === 'b')
cl(s.slice(3, 3) === '')

cl(s.slice(s.length-1) === 'r')
cl(s.slice(s.length) === '')
cl(s.slice(s.length+1) === '')
cl(s.slice(0, s.length+1) === 'foobar')
// ^: indeces larger than length equal
// to length

cl(s.slice(-1) === 'r')
cl(s.slice(-s.length) === 'foobar')
cl(s.slice(-(s.length+1)) === 'foobar')
cl(s.slice(0, -1) === 'fooba')
cl(s.slice(0, -s.length) === '')
cl(s.slice(0, -(s.length+1)) === '')
// ^: negative indeces count from right
// if exceeds length, equal to 0,
// i.e. -length
