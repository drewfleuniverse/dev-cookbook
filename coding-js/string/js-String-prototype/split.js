/*
string.split([separatorString | separatorRegExp[, limitNumber]]): array
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s
let r


s = 'a b c'

r = s.split(' ')
cl(js(r) === '["a","b","c"]')


let limit

limit = 1
r = s.split(' ', limit)
cl(js(r) === '["a"]')

limit = 3
r = s.split(' ', limit)
cl(js(r) === '["a","b","c"]')


r = s.split(/\ \w /)
cl(js(r) === '["a","c"]')


s = '/foo/bar'

r = s.split('/')
cl(js(r) === '["","foo","bar"]')

r.shift()
cl(js(r) === '["foo","bar"]')
