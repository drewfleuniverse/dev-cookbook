const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'foobar42'

cl(s.startsWith('foo') === true)
cl(s.startsWith('foo', 0) === true)

cl(s.startsWith('bar') === false)
cl(s.startsWith('bar', 3)
  === true)