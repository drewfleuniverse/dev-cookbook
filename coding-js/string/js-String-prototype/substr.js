/*
string.substr(startIndexNumber[, lengthNumber]): string
 */
const cl = i => console.log(i)


const s = 'foobar'


cl(s.substr() === 'foobar')
cl(s.substr(0, s.length) === 'foobar')

cl(s.substr(3) === 'bar')
cl(s.substr(3, 1) === 'b')
cl(s.substr(3, 0) === '')

cl(s.substr(s.length-1) === 'r')
cl(s.substr(s.length) === '')
cl(s.substr(s.length+1) === '')
cl(s.substr(0, s.length+1) === 'foobar')
// ^: indeces larger than length equal
// to length

cl(s.substr(-1) === 'r')
// ^: negative indeces count from right
