/*
string.substring(startIndexNumber[, endIndexNumber]): string
- slower than substr
 */

const cl = i => console.log(i)


const s = 'foobar'


cl(s.substring() === 'foobar')
cl(s.substring(0, s.length) === 'foobar')

cl(s.substring(3) === 'bar')
cl(s.substring(3, 4) === 'b')
cl(s.substring(3, 3) === '')

cl(s.substring(s.length-1) === 'r')
cl(s.substring(s.length) === '')
cl(s.substring(s.length+1) === '')
cl(s.substring(0, s.length+1) === 'foobar')
// ^: indeces larger than length equal
// to length

cl(s.substring(-1) === 'foobar')
cl(s.substring(-1, -1) === '')
// ^: treat negative indeces as 0
