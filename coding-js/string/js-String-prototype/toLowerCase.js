/*
string.toLowerCase(): string
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'FOOBAR'
let r = s.toLowerCase()

cl(r === 'foobar')
