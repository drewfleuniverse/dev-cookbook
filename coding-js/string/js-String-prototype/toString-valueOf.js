const s1 = new String('foo')
const r1 = s1.toString()
const r2 = s1.valueOf()

console.assert(r1 === 'foo')
console.assert(r2 === 'foo')