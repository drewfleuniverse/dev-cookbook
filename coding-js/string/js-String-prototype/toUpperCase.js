/*
string.toUpperCase(): string
 */

const cl = i => console.log(i)
const js = i => JSON.stringify(i)


let s = 'foobar'
let r = s.toUpperCase()

cl(r === 'FOOBAR')
