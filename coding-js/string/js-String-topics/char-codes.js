assert('0'.charCodeAt() === 48);
assert('9'.charCodeAt() === 57);

assert(':'.charCodeAt() === 58);
assert('@'.charCodeAt() === 64);

assert('A'.charCodeAt() === 65);
assert('Z'.charCodeAt() === 90);

assert('['.charCodeAt() === 91);
assert('`'.charCodeAt() === 96);

assert('a'.charCodeAt() === 97);
assert('z'.charCodeAt() === 122);


/*
Utils
 */
function describe(c,e){console.log('%c'+c,'font-weight: bold;'),e()};
function it(c,e){console.log('%c'+c,'color:grey;'),e()};
function assert(c,e=!1,f=''){if(e&&console.log(`Assertion log: ${c}`),!c)throw new Error(f)};
