const cl = i => console.log(i)


let s, r, f


s = 'foo'
s[0] = 'o'

cl(s === 'foo')


f = s => { s += 'bar' }

f(s)
cl(s === 'foo')


f = s => (s += 'bar')

r = f(s)
cl(r === 'foobar')
cl(s === 'foo')
s = r
cl(s === 'foobar')
