const cl = i => console.log(i)
const gp = o => Object.getPrototypeOf(o)
const js = i => JSON.stringify(i)


let s, r, it


s = 'abc'


r = []
for (let i = 0; i < s.length; i++) {
  r.push(s[i])
}
cl(js(r) === '["a","b","c"]')

r = []
for (const c of s) r.push(c)
cl(js(r) === '["a","b","c"]')

r = []
it = s[Symbol.iterator]()
let i = it.next()
while (!i.done) {
  r.push(i.value)
  i = it.next()
}
cl(js(r) === '["a","b","c"]')

// N.b. Spread and destructuring do not work with string

// like Array, `for...in` iterates over a
r = []
for (const c in s) r.push(c)
cl(js(r) === '["0","1","2"]')
