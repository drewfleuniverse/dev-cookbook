const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')




let r, r1, r2, r3, s


s = 'foo `bar` 42'
r = `foo \`bar\` ${21*2}`
cl(r === s)


s = `foo
bar`
r = 'foo\nbar'
cl(r === s)


s = 'foobar'

r1 = `foo${true
  ? 'bar'
  : ''
}`
r2 = 'foo' + (true
  ? 'bar'
  : ''
)
r3 = 'foo' + true
  ? 'bar'
  : ''
cl(r1 === 'foobar')
cl(r2 === 'foobar')
cl(r3 === 'bar')

r1 = `foo${true && 'bar'}`
r2 = 'foo' + (true && 'bar')
r3 = 'foo' + true && 'bar'
cl(r1 === 'foobar')
cl(r2 === 'foobar')
cl(r3 === 'bar')


const tagFunc = (s, o, m) => `\
${s[0]}${o.x}\
${s[1]}${o.y}\
${m}\
`
const o = {
  x: 'foo',
  y: 'bar'
}
r = tagFunc`See that ${o} and ${'!'}`

cl(r === 'See that foo and bar!')
