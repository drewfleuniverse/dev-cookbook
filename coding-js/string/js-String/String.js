const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')


cl(typeof new String("foo") === 'object')
cl(typeof String("foo") === 'string')
cl(typeof "foo" === 'string')
cl(typeof 'foo' === 'string')
cl(typeof `foo` === 'string')

cl(new String('foo') !== String('foo'))
cl(String('foo') === "foo")
cl("foo" === 'foo')
cl('foo' === `foo`)


let r1, r2, s

s = 'foo'
r1 = s + ' ' + 'bar' + ' ' + (21 * 2)
r2 = `${s} bar ${21 * 2}`

cl(r1 === r2)


s = 'foo bar'
r1 = 'foo \
bar'
r2 = `foo \
bar`

cl(r1 === s)
cl(r2 === s)


s = 'foo\nbar'
r1 = 'foo\n\
bar'
r2 = `foo
bar`

cl(r1 === s)
cl(r2 === s)
