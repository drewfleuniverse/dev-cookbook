## `slice` vs `substr` vs `substring`

- Preference: `slice` or `substr`
- Perf:
  - Chrome: `slice` >= `substr` > `substring (-16%)`
  - Firefox: `substr` > `slice (-10%)` > `substring (-61%)`
