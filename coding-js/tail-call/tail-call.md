# Tail Call


## Introduction

## Concerns

- TCO breaks stack trace and likely to make debugging harder

## Language support

- JavaScript:
    - ES6 specification
    - Implemented on: Safari/WebKit, Chrome/V8, Node v6
        - Node v6: need to specify tags `node --harmony --use_strict` to enable TOC
    - N.b. the function needs to be returned, otherwise TOC won't be triggered due to the last statement would be `return undefined;`
- Python: support by using third-party module, [tco]. Stock python encourages using explicit iteration for making debugging easier
- Scala: support.
- C++: gcc with explicit compiler option
- Other languages: Lisp, Lua, Scheme, Racket, Tcl, Kotlin, Elixir, Perl

[tco]: https://github.com/baruchel/tco


## References

**Articles**

- [Tail call](https://en.wikipedia.org/wiki/Tail_call)
- [Tail Call Optimization and Java](http://www.drdobbs.com/jvm/tail-call-optimization-and-java/240167044)

**Forum threads**

- [In Scala, tail recursion is as efficient as iteration. Is it true for any other programming language?](https://www.quora.com/In-Scala-tail-recursion-is-as-efficient-as-iteration-Is-it-true-for-any-other-programming-language/answer/Buddha-Buck?srid=CDgP)
