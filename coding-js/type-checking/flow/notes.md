## Concept of Types and Type System

- Flow tends to favor soundness over completeness
- Flow uses structural typing for objects and functions, but nominal typing for classes.

## Subtypes

- `number` is a type, while `1` is its subtype
- Subtype `1|2` and `2|3` aren't equal
