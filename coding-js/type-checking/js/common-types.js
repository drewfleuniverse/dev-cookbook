
describe('array', () => {
  const x = [];

  assert(
    typeof x === 'object' &&
    x.constructor === Array
  );

  assert(Array.isArray(x));
});


describe('object', () => {
  const x = {};

  assert(
    typeof x === 'object' &&
    x.constructor === Object
  );
});


describe('number', () => {
  it('primitive number has ctor', () => {
    const x = 42;

    assert(
      typeof x === 'number' &&
      x.constructor === Number
    );
  });

  it('Number', () => {
    const x = new Number(42);

    assert(
      typeof x === 'object' &&
      x.constructor === Number
    );
  });
});


describe('string', () => {
  it('primitive string has ctor', () => {
    const x = '';

    assert(
      typeof x === 'string' &&
      x.constructor === String
    );
  });

  it('String', () => {
    const x = new String(42);

    assert(
      typeof x === 'object' &&
      x.constructor === String
    );
  });
});

// console.log(o.constructor)




/*
Utils
 */
function describe(c,e){console.log('%c'+c,'font-weight: bold;'),e()};function it(c,e){console.log('%c'+c,'color:grey;'),e()};function assert(c,e=!1,f=''){if(e&&console.log(`Assertion log: ${c}`),!c)throw new Error(f)};
