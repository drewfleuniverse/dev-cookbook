module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/data/**',
    '!**/config/**',
    '!**/jest.config.js',
    '!**/coverage/**',
    '!**/node_modules/**',
    '!**/vendor/**'
  ],
  verbose: true
};
