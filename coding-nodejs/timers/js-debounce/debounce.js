const debounce = (cb, time) => {
  let timeout;
  return function(...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => cb.apply(this, args), time);
  };
};

module.exports = debounce;
