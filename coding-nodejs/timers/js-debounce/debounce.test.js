const debounce = require('./debounce');

const runNTimes = (n = 1, cb) => {
  for (let i = 0; i < n; i++) {
    cb();
  }
};

describe('debounce', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  describe('usage', () => {
    const cbMock = jest.fn();
    let debounced;

    beforeEach(() => {
      debounced = debounce(cbMock, 100);
    });

    afterEach(() => {
      cbMock.mockClear();
    });

    test('should invoke callback when time is up', () => {
      debounced();
      expect(cbMock).toHaveBeenCalledTimes(0);
      jest.runAllTimers();
      expect(cbMock).toHaveBeenCalledTimes(1);
    });

    test('should debounce callback', () => {
      runNTimes(3, () => debounced());
      jest.runAllTimers();
      expect(cbMock).toHaveBeenCalledTimes(1);

      runNTimes(3, () => debounced());
      jest.runAllTimers();
      expect(cbMock).toHaveBeenCalledTimes(2);
    });

    test('should pass args to callback', () => {
      debounced('foo', 'bar');
      jest.runAllTimers();
      expect(cbMock).toBeCalledWith('foo', 'bar');
    });
  });

  test('should capture context in object with function', () => {
    let context;
    const obj = {
      debounced: debounce(function() {
        context = this;
      }, 100)
    };
    obj.debounced();
    jest.runAllTimers();
    expect(context).toBe(obj);
  });

  test('should not capture context in object with arrow function', () => {
    const current = this;
    let context;
    const obj = {
      debounced: debounce(() => {
        context = this;
      }, 100)
    };
    obj.debounced();
    jest.runAllTimers();
    expect(context).toBe(current);
  });

  test('should capture context in class with arrow function', () => {
    let context;
    class Cls {
      constructor() {
        this.debounced = debounce(() => {
          context = this;
        }, 100);
      }
    }
    const obj = new Cls();
    obj.debounced();
    jest.runAllTimers();
    expect(context).toBe(obj);
  });
});
