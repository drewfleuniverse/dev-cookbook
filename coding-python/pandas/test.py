import unittest
from users import *


class TestUsers(unittest.TestCase):

  def testReadFile(self):
    self.assertEqual(
        users.to_string(),
        '   id     fname    lname sex\n' +
        '0   0  Josefina    Luper   F\n' +
        '1   1    Monroe    Dancy   M\n' +
        '2   2   Gilbert  Minelli   M'
    )

  def testAccessById(self):
    self.assertEqual(
        users[users['id'] == 0].to_string(),
        '   id     fname  lname sex\n' +
        '0   0  Josefina  Luper   F'
    )

  def testPivot(self):
    self.assertEqual(
        users.pivot(index='fname', columns='sex', values='id').to_string(),
        'sex         F    M\n'
        + 'fname             \n'
        + 'Gilbert   NaN  2.0\n'
        + 'Josefina  0.0  NaN\n'
        + 'Monroe    NaN  1.0'
    )


if __name__ == '__main__':
  unittest.main()
