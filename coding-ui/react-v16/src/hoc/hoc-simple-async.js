import React from "react";

export function withData(WrappedComponent) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: "nope"
      };
    }
    async componentDidMount() {
      const data = await this.fetchData();
      this.setState({ data });
    }
    async fetchData() {
      const data = await Promise.resolve("cool");
      return data;
    }
    render() {
      return <WrappedComponent data={this.state.data} {...this.props} />;
    }
  };
}
