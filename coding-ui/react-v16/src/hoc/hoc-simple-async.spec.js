import React from "react";
import { shallow } from "enzyme";
import { withData } from "./hoc-simple-async";

describe("withSimpleAsync", () => {
  const Yo = props => <div>{props.data}</div>;
  const YoWithData = withData(Yo);
  test("should ", async () => {
    const wrapper = shallow(<YoWithData />);
    const instance = wrapper.instance();
    await instance.componentDidMount();
    expect(wrapper.prop("data")).toBe("cool");
  });
});
