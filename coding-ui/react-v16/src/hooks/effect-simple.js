import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

export const ms = 1000;

const createAutoCountEffect = (count, setCount) => () => {
  // callback body upon mount and update
  const id = setInterval(() => {
    setCount(count + 1);
  }, ms);
  // callback upon unmount
  return () => {
    clearInterval(id);
  };
};

export const CountSpan = props => {
  const [count, setCount] = useState(0);
  const autoCountEffect = createAutoCountEffect(count, setCount);
  // By passing the second arg as an empty array, useEffect hook
  // only runs when the component is mounted.
  useEffect(autoCountEffect, []);
  return <span>{count + 1}</span>;
};

// CountSpan.propTypes = {
//   initCount: PropTypes.number.isRequired
// };
