import React from "react";
import { mount } from "enzyme";
import { CountSpan, ms } from "./effect-simple";

jest.useFakeTimers();
const getCountSpan = () => {
  const wrapper = mount(<CountSpan initCount={0} />);
  const span = wrapper.find("span");
  return {
    wrapper,
    span
  };
};

describe("CountSpan", () => {
  describe("autoCountEffect", () => {
    test("updates state", () => {
      const { span } = getCountSpan();
      expect(span.text()).toBe("1");
      jest.advanceTimersByTime(ms);
      expect(span.text()).toBe("2");
    });
    test.skip("not update when initCount is updated", () => {
      const { wrapper, span } = getCountSpan();
      jest.advanceTimersByTime(ms);
      expect(span.text()).toBe("2");
      wrapper.setProps({ initCount: 5 });
      wrapper.update();
      jest.advanceTimersByTime(ms);
      expect(span.text()).toBe("2");
    });
  });
});
