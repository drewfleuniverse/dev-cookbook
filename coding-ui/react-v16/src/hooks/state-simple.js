import React, { useState } from "react";

export const text = "Count";

export const ButtonWithState = () => {
  const [count, setCount] = useState(0);
  const handleClick = () => setCount(count + 1);
  return <button onClick={handleClick}>{`${text} ${count + 1}`}</button>;
};
