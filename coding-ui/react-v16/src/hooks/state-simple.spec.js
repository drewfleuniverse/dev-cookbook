import React from "react";
import { shallow } from "enzyme";
import { ButtonWithState } from "./state-simple";

describe("ButtonWithState", () => {
  test("updates state", () => {
    const wrapper = shallow(<ButtonWithState />);
    const button = wrapper.find("button");
    // It is not possible to access state in function components
    // expect(wrapper.state("count")).toBe(0);
    expect(wrapper.text()).toMatch(/1$/);
    button.simulate("click");
    expect(wrapper.text()).toMatch(/2$/);
  });
});
