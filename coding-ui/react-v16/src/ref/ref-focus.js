import React from "react";

export const Input = React.forwardRef((props, ref) => (
  <input type="text" ref={ref} {...props} />
));

export class InputWithFocus extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }
  setFocus() {
    this.ref.current.focus();
  }
  render() {
    return <Input ref={this.ref} />;
  }
}
