import React from "react";
import { mount } from "enzyme";
import { InputWithFocus, Input } from "./ref-focus";

describe("withSimpleAsync", () => {
  test("sets focus on the input element", async () => {
    const wrapper = mount(<InputWithFocus />);
    const instance = wrapper.instance();
    const inputNode = wrapper.find(Input).getDOMNode();
    const focusSpy = jest.spyOn(inputNode, "focus");
    expect(focusSpy).not.toBeCalled();
    instance.setFocus();
    expect(focusSpy).toBeCalled();
  });
});
