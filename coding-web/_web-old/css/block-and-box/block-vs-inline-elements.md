# Block vs inline elements


[CSS display: inline vs inline-block](https://stackoverflow.com/a/9189873)

Inline elements:
- respect left & right margins and padding, but not top & bottom
- cannot have a width and height set
- allow other elements to sit to their left and right.
- see very important side notes on this here.

Block elements:

- respect all of those
- force a line break after the block element

Inline-block elements:

- allow other elements to sit to their left and right
- respect top & bottom margins and padding
- respect height and width




**List of block-level elements**

```
<address>
<article>
<aside>
<blockquote>
<canvas>
<div>
<dl>
<dt>
<dd>
<fieldset>
<figcaption>
<figure>
<footer>
<form>
<h1>
<h2>
<h3>
<h4>
<h5>
<h6>
<header>
<hgroup>
<hr>
<li>
<main>
<nav>
<noscript>
<ol>
<output>
<p>
<pre>
<section>
<table>
<tfoot>
<ul>
<video>
```


**List of inline-level elements**

N.b. elements such as `button` and `input` are `inline-block`

```
<a>
<abbr>
<acronym>
<b>
<bdo>
<big>
<br>
<button>
<cite>
<code>
<dfn>
<em>
<i>
<img>
<input>
<kbd>
<label>
<map>
<object>
<q>
<samp>
<script>
<select>
<small>
<span>
<strong>
<sub>
<sup>
<textarea>
<time>
<tt>
<var>
```
