# Box Model


**Box parts - edges and areas**

- Content: `width` and `height`, ...etc.
- Padding: `padding` and `padding-*`.
- Border: `border` and `border-*`.
- Margin: `margin` and `margin-*`.


**`box-sizing`**

- `content-box` (default): includes `width` and `height`, excludes everything outside of content area.
- `border-box`: includes everything within border area, excludes margin area.


**Questions**

- What does `line-height` to do with box?
