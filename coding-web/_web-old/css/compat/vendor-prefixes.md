# Vendor Prefixes


- `-webkit-` (Chrome, Safari, newer versions of Opera, almost all iOS browsers (including Firefox for iOS); basically, any WebKit based browser)
- `-moz-` (Firefox)
- `-o-` (Old, pre-WebKit, versions of Opera)
- `-ms-` (Internet Explorer and Microsoft Edge)
