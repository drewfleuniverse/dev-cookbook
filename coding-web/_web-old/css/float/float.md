# Float


## Notes


**Effects of `float`**

- `float` is supposed to use with elements have `display:block`.
- Use `float` on a non-positioned element places itself between the stacking order of non-positioned elements and positioned elements.
- Modifies display value to block or table for most elements except it has value flex or inline-flex.


Use with other `display` values:

- `inline`, `inline-block` and `table*`: have computed value `block`
- `flex` and `inline-flex`: no effect, `float` doesn't work with `flex`
