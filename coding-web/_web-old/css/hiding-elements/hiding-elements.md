[What is the difference between visibility:hidden and display:none?](https://stackoverflow.com/questions/133051/what-is-the-difference-between-visibilityhidden-and-displaynone)

- `display:none`:
    - Triggers repaint and reflow
    - Removes the element from the normal flow of the page, allowing other elements to fill in.

- `visibility:hidden`:
    - Triggers repaint
    - Leaves the element in the normal flow of the page such that is still occupies space.
