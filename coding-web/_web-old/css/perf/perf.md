
**Discourage to use element selectors**

- Class selectors resolve quicker then do element selectors
- Relying on descendent selectors which can cause specificity issues
