# Position


**Positioned elements**


- Relatively positioned element: `position: relative`.
- Absolutely positioned element: `position: absolute` or `position: fixed`.
- Stickily positioned element: `position: sticky`.
- Notes:
    - Does not include `position: static`.
    - The following properties can be applied on a positioned element: `top`, `right`, `bottom`, `left`, and `z-index`.
    - `margin` can further offset `top`, `right`, `bottom`, `left` values.


**`position` values**

- `static` (default):
    - The following properties have no effect on a static element: `top`, `right`, `bottom`, `left`, and `z-index`.
- `relative`:
    - Positioned according to the normal flow of the document.
- `absolute`:
    - The element is removed from the normal document flow.
    - Positioned relative to its closest positioned ancestor if any; otherwise, it is placed relative to the initial containing block.
- `fixed`:
    - The element is removed from the normal document flow.
    - Positioned relative to the screen's viewport and doesn't move when scrolled.
- `sticky`:
    - Positioned according to the normal flow of the document, and then offset relative to its flow root and containing block,
