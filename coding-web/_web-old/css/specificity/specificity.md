# Selector Specificity

*Selector specificity determines selector precedence.*

## Notes

- Neutral / non:
    - Universal selector, `*`
    - Combinators, `+, >, ~, ' '`
    - Negation pseudo class, `:not()`
- Low specificity:
    - Type selectors, e.g. `h1`
    - Pseudo-elements, e.g. `::before`
- Med specificity:
    - Class selectors, e.g. `.foo`
    - Attributes selectors, e.g. `[type="foo"]`
    - Pseudo-classes, e.g. `:hover`
- High specificity:
    - ID selectors, e.g. `#foo`
- N.b. `!important` does not have specificity and breaks CSS cascading order

## References

- [Specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity)
