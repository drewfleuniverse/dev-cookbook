**Avoid extra selectors**

Bad:

```css
article .my-class ul li { /*...*/ }
```

Good:

```css
.my-class li { /*...*/ }
```


**CSS reset**

Use normalize.css, etc. or manual reset:

```css
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
```


**Prefer using image as background**

Original image is 1 to 1 ratio.

Bad:

```css
img {
  width: 300px;
  height: 200px;
}
```
```html
<section>
  <!-- display distorted image -->
  <img src="https://tutorialzine.com/media/2016/08/bicycle.jpg" alt="bicycle">
</section>
```

Good:

```css
div {
  width: 300px;
  height: 200px;
  background: url('https://tutorialzine.com/media/2016/08/bicycle.jpg');
  background-position: center center;
  background-size: cover;
}
```
```html
<section>
  <!-- display cropped image without distortion -->
  <div></div>
</section>
```

**Em, rem, vs px**

- `em`: `1em` is relative to the font-size of its parent, i.e. if a parent has font size `42px`, then children's `1em` equals to `42px`.
- `rem`: `1rem` is relative to the font-size of its `<html>`, easy to scale headings, etc
- `px`: precise but no scaling


**Basic BEM**

```css
.block-name__element-name { /*...*/ }
```

**BEM nesting**

```css
.block { /*...*/ }
.block__group { /*...*/ }
.block__elem1 { /*...*/ }
.block__elem2 { /*...*/ }
```
```html
<div class="block">
  <div class="block__group">
    <div class="block__elem1"></div>
    <div class="block__elem2"></div>
  </div>
</div>
```

**BEM optionality**

```css
.block { /*...*/ }
.optional-elem1 { /*...*/ }
.optional-elem2 { /*...*/ }
```
```html
<div class="block">
  <div class="optional-elem1"></div>
  <div class="optional-elem2"></div>
</div>
```

**ARIA shortcuts**

```css
button,
[role=button] { /*...*/ }
```
```html
<button type="button" name="button">
  Button1
</button>
<span role="button">
  Button2
</span>
```
