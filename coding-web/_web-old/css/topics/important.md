**Third-party stylesheet uses `!important` on `list-style-type`**

Apply another `!important` on `style` attribute:

```css
ul[style] {
  list-style-type: none !important;
}
```
```html
<ul style="list-style-type: none;">
  <li>foo</li>
</ul>
```
