

Preparation code:

```html
<ul id="a"></ul>
<ul id="b"></ul>
<ul id="c"></ul>
```
```js
// setup
const appendFrag = (e, l) => {
  const f = document.createDocumentFragment()

  for (i = 0; i < l.length; i++) {
    const li = document.createElement('li')
    const t = document.createTextNode(l[i])
    li.appendChild(t)
    f.appendChild(li)
  }

  e.appendChild(f)
}

const appendChild = (e, l) => {
  for (i = 0; i < l.length; i++) {
    const li = document.createElement('li')
    const t = document.createTextNode(l[i])
    li.appendChild(t)
    e.appendChild(li)
  }
}

const appendHtml = (e, l) => {
  let t = ''
  for (i = 0; i < l.length; i++) {
    t += `<li>${l[i]}</li>`
  }
  e.innerHTML = t
}

const l = [0,1,2,3,4,5,6,7,8,9]
// tear down

```

Snippets

```js
a = document.getElementById('a')
b = document.getElementById('b')
c = document.getElementById('c')


appendFrag(a, l)
appendChild(b, l)
appendHtml(c, l)
```
