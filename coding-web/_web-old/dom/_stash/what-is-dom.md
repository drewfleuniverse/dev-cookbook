# Document object model

DOM

- DOM is a 'programming interface standard' for HTML and XML documents
- A document is parsed through layout engine into a DOM tree
  - I.e. A DOM tree of HTML -> HTML-Document-OM
- W3C DOM and WHATWG DOM are standards implemented in browsers
- WHATWG DOM is the living standard
- The root node of DOM tree is the Document object, i.e. `document`
- N.b. CSS is independent of HTML and parsed into CSSDOM after DOM tree completed


DOM APIs

- DOM + programming language (i.e. JavaScript) = DOM APIs


DOM APIs, in a browser context, comprise:

- [WebIDL](https://www.w3.org/TR/WebIDL)
- [DOM](https://dom.spec.whatwg.org)
  - Infrastructure
  - Events
  - Aborting ongoing activities
  - Nodes
  - Ranges
  - Traversal
  - Sets
- [HTML](https://html.spec.whatwg.org)


Historical DOM:

- DOM core
- DOM events
- DOM traversal
- DOM ranges


History of DOM

- DOM level 0 was developed in the 1990s by Microsoft and Netscape and loosely implemented in JavaScript and JScript
- DOM level 1-3, is developed from 1998 to 2004 by the World Wide Web Consortium (W3C) and implemented with ECMAScript
- DOM level 4 was published in 2015 by the Web Hypertext Application Technology Working Group (WHATWG)
