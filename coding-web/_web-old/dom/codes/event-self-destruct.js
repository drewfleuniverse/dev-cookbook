<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" href="">
  <title>Document</title>
</head>
<body>

<ul id="p">
  <li>a</li>
</ul>

<script>
const cl = s => console.log(s)
const js = o => JSON.stringify(o)


let p, c, r
p = document.getElementById('p')
c = p.getElementsByTagName('li')

r = []
p.addEventListener('click', function(e) {
  this.removeChild(e.target)
  
  const f = document.createDocumentFragment()
  
  const l1 = document.createElement('li')
  const t1 = document.createTextNode('b')
  l1.appendChild(t1)
  const l2 = document.createElement('li')
  const t2 = document.createTextNode('c')
  l2.appendChild(t2)
  
  f.appendChild(l1)
  f.appendChild(l2)
  this.appendChild(f)
})


c[0].click()

cl(c.length)

</script>
</body>
</html>
