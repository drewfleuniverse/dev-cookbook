# event


## Event propagation

Event propagation has three phases:

- Capturing (bubble down)
- Target
- Bubbling (bubble up)


## Event delegation

By setting event listener on parent element, any of its child element can emit the same event, and identify the emitting element in the parent's listener using conditions, e.g. `event.target.tagName`, `event.target.id`.
