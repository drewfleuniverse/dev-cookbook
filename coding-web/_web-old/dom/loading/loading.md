# Loading


## Running scripts regardless of parsing DOM and loading external media

**Option 1: `<script async>` in `<head>`**

- Loads and runs while browsers are loading other resources
- Cons: Scripts are not executed in order
- State: loading

**Option 2: `<script>` in `<head>`**

- Executes when ready state is loading
- Cons: Blocks browsers to load other resources
- State: loading


## Running scripts after DOM parsed regardless of loading external media

**Option 1: `<script defer>` in `<head>`**

- Loads while browsers are loading other resources
- Runs after DOM is parsed
- State: interactive

**Option 2: `<script>` in the end of`<body>`**

- Loads after all other resources
- Runs after DOM is parsed
- Cons: Slower loading
- State: loading

**Option 3: Run scripts in `DOMContentLoaded` event listener on document**

- Runs after DOM is parsed
- State: interactive


## Running scripts after DOM parsed and external media loaded

**Option 1: Run scripts in `load` event listener on window**

- Runs after everything is loaded
- State: complete
