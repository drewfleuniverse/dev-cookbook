[What is DOM reflow?](https://stackoverflow.com/a/27637245/4632950)

A reflow computes the layout of the page. A reflow on an element recomputes the dimensions and position of the element, and it also triggers further reflows on that element’s children, ancestors and elements that appear after it in the DOM. Then it calls a final repaint. Reflowing is very expensive, but unfortunately it can be triggered easily.

Reflow occurs when you:

- insert, remove or update an element in the DOM
- modify content on the page, e.g. the text in an input box
- move a DOM element
- animate a DOM element
- take measurements of an element such as offsetHeight or getComputedStyle
- change a CSS style
- change the className of an element
- add or remove a stylesheet
- resize the window
- scroll


[What's the difference between reflow and repaint?](https://stackoverflow.com/a/2549317/4632950)

A repaint occurs when changes are made to an elements skin that changes visibly, but do not affect its layout.
Examples of this include outline, visibility, background, or color. According to Opera, repaint is expensive because the browser must verify the visibility of all other nodes in the DOM tree.
A reflow is even more critical to performance because it involves changes that affect the layout of a portion of the page (or the whole page).
Examples that cause reflows include: adding or removing content, explicitly or implicitly changing width, height, font-family, font-size and more.


Other References:

[Minimizing browser reflow ](https://developers.google.com/speed/docs/insights/browser-reflow)
[CSS Triggers](https://csstriggers.com/)
