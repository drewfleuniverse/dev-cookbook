# ARIA


- Short for Accessible Rich Internet Applications.
- Makes web an assistive technology (AT).
- Converts DOM into DOM accessibility tree.


## Accessibility attribute use cases

- Navigation landmarks
- JavaScript widgets
- Form hints and error messages
- Live content updates


## Mapping example: HTML5 elements and ARIA roles

HTML5     | ARIA Role, e.g. `role="banner"`
--------- | --------------------------------
`header`  | `banner`
`nav`     | `navigation`
`main`    | `main`
`footer`  | `contentinfo`
`aside`   | `complementary`
`section` | `region` (non-standard)
`article` | `article` (non-standard)
`form`    | `form`

N.b. `role="none"` or `role="presentation"` make an element a `none` node in ARIA tree
