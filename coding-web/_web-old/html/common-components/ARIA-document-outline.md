# ARIA document outline


Uses for accessibility such as screen readers.

**Explicit sectioning**

`<body>`: Main section

`<section>, <article>, <aside>, <nav>`:
    - Subsections inside `<body>`
    - Uses `<h1>-<h6>` to give one or outlines with heading text

**Implicit sectioning**

Elements introduce implicit sections: `<h1>-<h6>` and non-empty `<hgroup>`

- N.b. `<hgroup>`: renders a single outline entry regardless the number heading elements it contains
- N.b. `<nav>`: links inside `<nav>` are not counted as outline entries


**Sectioning roots**

Sectioning roots can have its own outline. External outline are excluded from main outline

- `<body>`: Contains main outline
- `<blockquote>, <details>, <fieldset>, <figure>, <td>`: Contain external outline
