# Content Sectioning


**`<address>: HTMLElement`**

- Usage: Contact information; email, phone, address
- Style: Block, Italic
- Content:
    - Flow content
    - No nested `<address>`
    - No headings `<hgroup>, <h1>, <h2>, <h3>, <h4>, <h5>, <h6>`
    - No sectioning content `<article>, <aside>, <section>, <nav>`
    - No `<header>, <footer>`
- Style: Italic
- Parent: Any element accepts flow content
- Roles: None
- Attributes: Global attributes

**`<article>: HTMLElement`**

- Usage: Newspaper article, forum post, comments
- Style: Block
- Content: Flow content
- Parent:
    - Any element accepts flow content
    - Not `<address>`
- Roles: `application, document, feed, main, presentation, region`
- Attributes: Global attributes


**`<aside>: HTMLElement`**

- Usage: Indirectly related content, call-out boxes
- Style: Block
- Content: Flow content
- Parent:
    - Any element accepts flow content
    - Not `<address>`
- Roles: `feed, note, presentation, region, search`
- Attributes: Global attributes


**`<footer>: HTMLElement`**

- Usage: Information of its containing section, copyright
- Style: Block
- Content
    - Flow content
    - No `<footer>, <header>`
- Parent:
    - Any element accepts flow content
    - Not `<footer>, <header>, <address>`
- Roles: `group, presentation`
- Attributes: Global attributes

**`<header>: HTMLElement`**

- Usage: Introductory content, typically a group of introductory or navigational aids
- Style: Block
- Content
    - Flow content
    - No `<footer>, <header>`
- Parent:
    - Any element accepts flow content
    - Not `<footer>, <header>, <address>`
- Roles: `group, presentation`
- Attributes: Global attributes

**`<h1>-<h6>: HTMLHeadingElement`**

- Usage: Headings
- Style: Display block, top and bottom margins, bold
- CSS: `font-size`
- Content: Phrasing content
- Parent: Any element accepts flow content
- Restriction:
- Roles: `tab, presentation`
- Attributes: Global attributes

**`<hgroup>: HTMLElement`**

- Usage: Group headings
- Style: Block
- Content: One or more `<h1>-<h6>`
- Parent: Any element accepts flow content
- Roles: `tab, presentation`
- Attributes: Global attributes

**`<nav>: HTMLElement`**

- Usage: Menus, tables of contents, and indexes
- Style: Block
- Content: Flow content
- Parent: Any element accepts flow content
- Roles: None
- Attributes: Global attributes
- Notes:
    - `<footer>` can be used to contain links along instead of `<nav>`
    - A document can have many `<nav>`
    - `<nav>` is a ARIA navigation landmark

**`<section>: HTMLElement`**

- Usage:
    - Includes heading to explicitly add to document outline
    - Groups sectioning content
    - Lists search results
- Style: Block
- Content: Flow content
- Parent:
    - Any element accepts flow content
    - Not `<address>`
- Restriction:
- Roles: `alert, alertdialog, application, banner, complementary, contentinfo, dialog, document, feed, log, main, marquee, navigation, search, status, tabpanel`
- Attributes: Global attributes


## Notes on Content sectioning

**`<section>` vs `<div>`**

- `<section>`: Use when its content would be listed explicitly in the document’s outline, i.e. a thematic grouping of content, *meaningful*.
- `<div>`: Only use for styling purposes or as a convenience for scripting, i.e. *last resort*.

**`<section>` vs `<article>`**

- Use `<section>` when HTML5 outline is the priority concern.
- They can contain each other.

**`<header>` vs `<hgroup>`**

- `<header>`: *section* header containing introductory content
- `<hgroup>`: Use when grouping multiple heading elements, even used inside `<header>`

**`<header>` and `<footer>`**

- They can be contained in `<article>, <section>, <aside>, <nav>`.
- Not necessarily positioned at the beginning or end of the section
