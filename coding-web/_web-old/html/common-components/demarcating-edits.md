# Demarcating edits


**`<del>: HTMLModElement`**

- Usage: Deleted text
- Style: Inline
- Content: Transparent
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes, `cite, datetime`

**`<ins>: HTMLModElement`**

- Usage: Added text
- Style: Inline
- Content: Transparent
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes, `cite, datetime`
