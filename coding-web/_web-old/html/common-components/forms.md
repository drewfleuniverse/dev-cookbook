# Forms


## Form element

**`<form>: HTMLFormElement`**

- Usage: Contains controls or groups of controls
- Style: `display: block`
- CSS:
    - Use `:valid` and `:invalid` to style a form element
    - E.g. `input:invalid {...}`
- Content:
    - Flow content
    - Not `<form>`
- Parent: Any element accepts flow content
- Roles:`group, presentation`
- Attributes: Global attributes, `accept-charset, action, autocomplete, enctype, method, novalidate, target`
- Notes:
    - `name`:
        - Use `name` on a `<form>` was for legacy reasons, use `id` instead
        - `name` is required on form controls as the 'key' for the control's `value`
- Access:


## Control - input element

**`<input>: HTMLInputElement`**

- Usage: Create controls. Default `type` is `text`
- Style: `display: inline-block`...
- Content: None
- Parent: Any element accepts phrasing content
- Roles: Depends on `type`
- Attributes: Global attributes, `type, accept...`
    - `type` values: `button, checkbox, file, hidden, image, password, radio, reset, submit, text`
    - `type` values, HMTL5: `color, date, datetime-local, email, month, number, range, search, tel, time, url, week`

**`<input type="text">`**

- Value: `DOMString` for text field
- Events: `change, input`
- Attributes: `autocomplete, list, maxlength, minlength, pattern, placeholder, required, size`
- Method: `select(), setRangeText(), setSelectionRange()`
- Roles: None

**`<input type="password">`**

- Value: `DOMString`
- Events: `change, input`
- Attributes: `autocomplete, inputmode, maxlength, minlength, pattern, placeholder, readonly, required, and size`
- Method: `select(), setRangeText(), setSelectionRange()`
- Roles: None

**`<input type="button">`**

- Value: `DOMString`
- Events: `click`
- Attributes: `type, value`
- Methods: None
- Roles: `link, menuitem, menuitemcheckbox, menuitemradio, radio, switch, tab`

**`<input type="reset">`**

- Value: `DOMString`, as label
- Events: `click`
- Attributes: `type, value`
- Method: None
- Roles: N/A

**`<input type="submit">`**

- Value: `DOMString`, as label
- Events: `click`
- Attributes: `type, value`
- Method: None
- Roles: None

**`<input type="image">`**

- Value: None
- Events: None
- Attributes: `alt, src, width, height, formaction, formenctype, formmethod, formnovalidate, formtarget`
- Method: None
- Roles: `link, menuitem, menuitemcheckbox, menuitemradio, radio, switch`

**`<input type="checkbox">`**

- Value: `DOMString`
- Events: `change, input`
- Attributes: `checked`
- Method: `select()`
- Roles: `button, menuitemcheckbox, option, switch`

**`<input type="radio">`**

- Value: `DOMString`
- Events: `change, input`
- Attributes: `checked`
- Method: `select()`
- Roles: `menuitemradio`

**`<input type="file">`**

- Value: `DOMString`
- Events: `change, input`
- Attributes: `accept, multiple, required`
- Method: `select()`
- Roles: None

**`<input type="hidden">`**

- Value: `DOMString`
- Events: None
- Attributes: `autocomplete`
- Method: None
- Roles: None


## Other elements

**`<label>: HTMLLabelElement`**

- Usage: Caption for an item in a UI
- Style: `display: inline`
- Content:
    - Phrasing content
    - No `<label>`
- Parent: Any element accepts phrasing content
- Roles: None
- Attributes: Global attributes, `for`
- Events: `click`
- Notes:
    - Without `for`, the control is placed inside `<label>`
    - With `for`, the control is placed outside `<label>`
    - One `<label>` can associate with multiple controls

**`<datalist>: HTMLDataListElement`**

- Usage:
    - Create editable combobox with `<input>`:
      - `<input list="foo" ...`
      - `<datalist id="foo"><option ...`
- Style: `display: none`
- Content: Phrasing content or `<option>`
- Parent: Any element accepts phrasing content
- Roles: None
- Attributes: Global attributes
- Notes:
    - `<option value="foo">` display 'foo' in list
    - `<option value="foo">bar</option>` display 'foo bar' in list
- Access:

**`<fieldset>: HTMLFieldSetElement`**

- Usage: Group form labels and controls
- Style: `display: block`, `border-*`, `min-width: min-content`
- Content: Flow content, optionally, a `<legend>` before all flow content
- Parent: Any element accepts flow content
- Roles: `group, presentation`
- Attributes: Global attributes, `disabled, form, name`
- Notes:
- Access:

**`<legend>: HTMLLegendElement`**

- Usage: Caption for `<fildset>`
- Style: `display: block`
- Content: Phrasing content
- Parent: `<fieldset>`
- Roles: None
- Attributes: Global attributes

**`<select>: HTMLSelectElement`**

- Usage: Select control
- Style: `display: inline-block`
- Content: `<option>, <optgroup>`
- Parent: Any element accepts phrasing content
- Roles: `menu`
- Attributes: Global attributes, `autofocus, disabled, form, multiple, name, required, size`
- Notes:
- Access:

**`<optgroup>: HTMLOptGroupElement`**

- Usage:
- Style: `display: block`, ...
- Content: `<option>`
- Parent: `<select>`
- Roles: None
- Attributes: Global attributes, `disabled, label`
- Notes:
- Access:

**`<option>: HTMLOptionElement`**

- Usage: Option item in `<select>, <optgroup>, <datalist>`
- Style: `display: block`, padding
- Content: Text
- Parent: `<select>, <optgroup>, <datalist>`
- Roles: None
- Attributes: Global attributes, `disabled, label, selected, value`

**`<button>: HTMLButtonElement`**

- Usage:
    - Form button
    - Standalone button
    - The look and feel of a `<button>` depends on user agent
- Style: `display: inline-block`, `box-sizing: border-box`, `border-*`, `font-*`, `color`, `cursor`...
- CSS: `::before, ::after`
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: `checkbox, link, menuitem, menuitemcheckbox, menuitemradio, radio, switch, tab`
- Attributes: Global attributes, `autofocus, disabled, form, formaction, formenctype, formmethod, formnovalidate, formtarget, name, type, value`
- Events: `click`
- Notes:
    - When used in `<form>`, `<button>` may submit different value in older IE.
    - `<button>` allows inline text content, e.g. `<i>, <em>`
    - Click on a `<buttom>` may not focus

**`<textares>: HTMLTextAreaElement`**

- Usage: Textarea
- Style: `display: inline-block`, ...
- Content: Text
- Parent: Any element accepts phrasing content
- Roles: None
- Attributes: Global attributes, `autocomplete, autofocus, cols, disabled, form, maxlength, minlength, name, placeholder, readonly, required, rows, spellcheck, wrap`

**Other elements**

- `<meter>, <output>`







**`<>: HTMLElement`**

- Usage:
- Style: `display: block`
- CSS:
- Content: Flow content
- Parent: Any element accepts flow content
- Roles:
- Attributes: Global attributes
- Notes:
- Access:
