# Image and multimedia


**`<img>: HTMLImageElement`**

- Usage: Embeds image into the document
- Style: Inline
- Content: None
- Parent: Any element accepts embedded content
- Roles: Any
- Attributes: Global attributes, `alt, crossorigin, decoding, height, sizes, src, srcset`
- Notes: No closing tag
- Access:


**Other tags**

- `<map>, <area>`
- `<audio>, <video>, <track>`: Embeds audio;  Display none
