# Inline text semantics


**`<a>: HTMLAnchorElement`**

- Usage: Hyperlink anchor
- Style: `display: inline`, `text-decoration*`, `color`, `cursor`
- CSS: `:link, :visited, :hover, :focus, :active`
- Content: Flow content or phrasing content
- Parent: Any element accepts flow content or phrasing content
- Roles: `button, checkbox, menuitem, menuitemcheckbox, menuitemradio, option, radio, switch, tab, treeitem`
- Attributes: Global attributes, `href, hreflang, type`

**`<abbr>: HTMLElement`**

- Usage: Abbreviation
- Style: Inline, text decorations
- Content: Phrasing content
- Parent: Any element accepts Phrasing content
- Roles: Any
- Attributes: Global attributes, `title`

**`<b>: HTMLElement`**

- Usage:
    - Called *bring attention to* element in HMTL5
    - Only use when `<strong>, <em>, <mark>` do not fit
    - No special semantic information
    - Better use with class to convey semantic information
- Style: Inline, bold
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes

**`<br>: HTMLBRElement`**

- Usage:
    - Line break
    - Useful for poems or addresses
- Style: Inline
- Content: None
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes
- Notes: Do NOT use `<br>` to increase line gaps, use CSS `margin` or `<p>`

**`<cite>: HTMLElement`**

- Usage: Citation
- Style: Inline, italic
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes
- Notes: `<blockquote>, <q>` have their own `cite` attribute

**`<code>: HTMLElement`**

- Usage: Computer code
- Style: Inline, monospace
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes

**`<data>: HTMLDataElement`**

- Usage:
    - Enclose data without using `value` attribute
    - Enclose human readable data name and assign machine readable data to its `value` attribute
- Style: Inline
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: N/A
- Attributes: Global attributes, `value`
- Notes: Do NOT use `<data>` for date and time data, use `<time>` instead.

**`<time>: HTMLTimeElement`**

- Usage:
    - Enclose date/time without using `datetime` attribute
    - Enclose human readable date/time name and assign machine readable date/time to its `datetime` attribute
- Style: Inline
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes, `datetime`

**`<em>: HTMLElement`**

- Usage: Stress emphasis
- Style: Inline, italic
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes

**`<i>: HTMLElement`**

- Usage: Include technical terms, foreign language phrases, or fictional character thoughts in normal text
- Style: Inline, italic
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes


**`<mark>: HTMLElement`**

- Usage: Highlight text
- Style: Inline, background color
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes

**`<q>: HTMLQuoteElement`**

- Usage: Quotation
- Style: Inline, pseudo elements for double quotations
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes
- Notes: For block or longer quotes, use `<blockquote>`

**`<s>: HTMLElement`**

- Usage: Strike through
- Style: Inline, line through text decoration
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes


**`<small>: HTMLElement`**

- Usage: Smaller text
- Style: Inline, smaller font size
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes

**`<span>: HTMLSpanElement`**

- Usage:
   - Generic inline container for phrasing content
   - Like `<div>`, but `<span>` is inlined
- Style: Inline
- Content: Phrasing content
- Parent: Any element accepts phrasing content
- Roles: Any
- Attributes: Global attributes

**Other tags**

- <var>: math variables
- <code>: The HTML Code element
- <kbd>: The HTML Keyboard input element
- <samp>: The HTML Sample Output element
