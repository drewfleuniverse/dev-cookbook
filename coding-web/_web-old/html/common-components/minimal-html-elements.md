# Minimal HTML Elements

## Main root

**`<html>: HTMLHtmlElement`**

- Usage: The root of a document
- Style: Block
- Content: `<head>, <body>`
- Parent: None
- Roles: None
- Attributes: `xmlns`
- Access:
    - `document.documentElement`
    - `document.getElementsByTagName('html')[0]`


## Header

**`<head>: HTMLHeadElement`**

- Usage: Metadata
- Style: None
- Content: Exactly one `<title>`, one or more metadata content
- Parent: `<html>`
- Roles: None
- Access:
    - `document.head`
    - `document.getElementsByTagName('head')[0]`


## Document metadata

**`<link>: HTMLLinkElement`**

- Usage: External resource
- Style: None
- Content: None
- Parent: Any element accept metadata elements
- Roles: None
- Attributes: `crossorigin, href, media, rel, sizes, title, type...`
- Notes: No closing tag

**`<meta>: HTMLMetaElement`**

- Usage: Metadata
- Style: None
- Content: None
- Parent: `<head>`
- Roles: None
- Attributes: `charset, content, http-equiv, name`
- Notes: No closing tag

**`<style>: HTMLStyleElement`**

- Usage: CSS
- Style: None
- Content: `type="text/css"`
- Parent: Any element accept metadata elements
- Roles: None
- Attributes: `type...`
- Access: `document.getElementsByTagName('style')`

**`<title>: HTMLTitleElement`**

- Usage: Title of document
- Style: None
- Content: Text
- Parent: `<head>`
- Roles: None
- Access:
    - `document.title`
    - `document.getElementsByTagName('title')[0]`


## Sectioning root

**`<body>: HTMLBodyElement`**

- Usage: Content of HTML document
- Style: Block
- Content: Flow content
- Parent: `<html>`
- Roles: None
- Attributes: Global attributes
- Access:
    - `document.body`
    - `document.getElementsByTagName('body')[0]`
