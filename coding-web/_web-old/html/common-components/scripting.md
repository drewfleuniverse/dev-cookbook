# Scripting


**`<script>: HTMLScriptElement`**

- Usage: Embed or reference JavaScript.
- Style: None
- Content: Programming scripts
- Parent:
    - Any element that accepts metadata content
    - Any element that accepts phrasing content.
- Roles: None
- Attributes:
    - Global attributes, `async, crossorigin, defer, src, type`
    - `type` values in HTML5 browsers:
        - `text/javascript`: The default value, thus can be omitted.
        - `module`: Enables ES6 module.
- Notes: If set to display block, browser renders the content of `<script>` as text


**Other tags**

- `<canvas>`: Use with the canvas scripting API or the WebGL API; Display inline
- `<noscript>`: Encloses HTML that only show when JavaScript is disabled; Display none when enabled, inline when JS disabled
