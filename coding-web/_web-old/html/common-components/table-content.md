# Table content


**`<table>: HTMLTableElement`**

- Usage: Tabular data
- Style: `display: table`, `border-collapse: separate`, `border-*`
- CSS: `border`, `border-collapse`, `border-color`, `:nth-child`, `text-align`
- Content:
    - Single optional: `<caption>, <thead>, <tfoot>`
    - Either one of:
        - Zero or more `<tbody>`
        - One or more `<tr>`
- Parent: Any element accepts flow content
- Roles: Any
- Attributes: Global attributes

**Table styling example**

```css
table, th, td {
  border-collapse: collapse; /* merge borders between table and cells */
  border: 1px solid black;
  table-layout: fixed; /* all column has the width of widest cell */
  width: 100px; /* cell width, 100% will fill the table to its parent */
  overflow: hidden; /* make text-overflow work*/
  text-overflow: ellipsis;
}
```

**`<thead>: HTMLTableSectionElement`**

- Usage: Table head rows
- Style: `display: table-header-group`, `border-collapse: separate`, `border-*`, `vertical-align: middle`
- CSS: See `<table>`
- Content: Zero or more `<tr>`
- Parent: `<table>`. The <thead> must appear after any <caption> or <colgroup> element, even implicitly defined, but before any <tbody>, <tfoot> and <tr> element.
- Roles: Any
- Attributes: Global attributes

**`<tbody>: HTMLTableSectionElement`**

- Usage: Groups table rows
- Style: `display: table-row-group`, `border-collapse: separate`, `border-*`, `vertical-align: middle`
- CSS: See `<table>`
- Content: Zero or more `<tr>`
- Parent: `<table>`. The <thead> must appear after any <caption> or <colgroup> element, even implicitly defined, but before any <tbody>, <tfoot> and <tr> element.
- Roles: Any
- Attributes: Global attributes
- Notes:
  - If omitted, `<tbody>` will be automatically added by some browser to group `<tr>`
  - Use multiple `<tbody>` to group `<tr>`

**`<tfoot>: HTMLTableSectionElement`**

- Usage: Groups table rows
- Style: `display: table-row-group`, `border-collapse: separate`, `border-*`, `vertical-align: middle`
- CSS: See `<table>`
- Content: Zero or more `<tr>`
- Parent: `<table>`. The <tfoot> must appear after any <caption>, <colgroup>, <thead>, <tbody>, or <tr> element.  Note that this is the requirement as of HTML5.
- Roles: Any
- Attributes: Global attributes

**`<tr>: HTMLTableRowElement`**

- Usage: Group table cells
- Style: `display: table-row`, `border-collapse: separate`, `border-*`, `vertical-align: middle`
- CSS: See `<table>`
- Content: `<td>, <th>, <script>, <template>`
- Parent: `<table>` (only if the table has no child <tbody> element, and even then only after any <caption>, <colgroup>, and <thead> elements); otherwise, the parent must be <thead>, <tbody> or <tfoot>.
- Roles: Any
- Attributes: Global attributes, `rowspan`

**`<td>: HTMLTableDataCellElement`**

- Usage: Table cell
- Style: `display: table-cell`, `border-collapse: separate`, `border-collapse: separate`, `vertical-align: middle`
- CSS: See `<table>`
- Content: Flow content
- Parent: `<tr>`
- Roles: Any
- Attributes: Global attributes, `colspan`

**`<th>: HTMLTableHeaderCellElement`**

- Usage:
    - Table cell in `<thead>`
    - The first column cell in `<tr>`
- Style: `display: table-cell`, `border-collapse: separate`, `border-collapse: separate`, `vertical-align: middle`, `text-align: center`, `font-weight: 700`
- CSS: See `<table>`
- Content: Flow content
- Parent: `<tr>`
- Roles: Any
- Attributes: Global attributes, `colspan, scope`

**`<caption>: HTMLTableCaptionElement`**

- Usage: Table caption
- Style: `display: table-caption`, `border-collapse: separate`
- Content: Flow content
- Parent: `<table>`, must be its first descendant
- Roles: None
- Attributes: Global attributes
- CSS: `caption-size`, `text-align`

**Other tags**

- `<colgroup>, <col>`: Use `span` to specify number of columns and styling `background, and width`
