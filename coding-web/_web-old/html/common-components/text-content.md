# Text content


Important for accessibility and SEO.

**`<blockquote>: HTMLQuoteElement`**

- Usage: Extended quotation, indentation
- Style: Block, margins
- CSS: `margin-left`, `margin-right`, `margin`
- Content: Flow content
- Parent: Any element accepts flow content
- Roles: Any
- Attributes: Global attributes, `cite`
- Notes: For inline or shorter quotes, use `<q>`

**`<dl>: HTMLDListElement`**

- Usage: Description list
- Style: Block
- Content:
    - `<dt>, <dd>`
    - `<dt>, <dd>` grouped by `<div>`
- Parent: Any element accepts flow content
- Roles: `group, presentation`
- Attributes: Global attributes

**`<dt>: HTMLElement`**

- Usage: Description term
- Style: Block
- Content:
    - Flow content
    - No `<header>, <footer>`
- Parent: `<dl>`
- Roles: None
- Attributes: Global attributes
- Notes: Use `::after`, `dt::after { content: ": " }`
**`<dd>: HTMLElement`**

- Usage: Description data
- Style: Block, left margin
- Content: Flow content
- Parent: `<dl>`
- Roles: None
- Attributes: Global attributes

**`<div>: HTMLDivElement`**

- Usage:
    - Style wrapper
    - Scripting wrapper, e.g. in React ecosystem, components are wrapped in `<div>` for styling and scripting purposes.
- Style: Block
- Content: Flow content
- Parent: Any element accepts flow content
- Roles: Any
- Attributes: Global attributes

**`<figure>: HTMLElement`**

- Usage:
    - Self-contained content
    - Contain `<img>`, `<pre>`, `<p>`, etc
    - Markup an image, a poem, or a wisdom quote
- Style: Block, margins
- Content:
    - Flow content
    - Optional `<figcaption>`
- Parent: Any element accepts flow content
- Roles: `group, presentation`
- Attributes: Global attributes

**`<figcaption>: HTMLElement`**

- Usage: Caption or legend
- Style: Block
- Content: Flow content
- Parent: `<figure>`, must be the first or last child
- Restriction:
- Roles: `group, presentation`
- Attributes: Global attributes

**`<hr>: HTMLHRElement`**

- Usage:
    - Thematic break between content topics
    - Do NOT use as horizontal rule, use `<div>` with CSS instead.
- Style: Block, top and bottom margins, borders
- Content: None
- Parent: Any element accepts flow content
- Roles: `presentation`
- Attributes: Global attributes
- Notes: No closing tag

**`<ol>: HTMLOListElement` / `<ul>: HTMLUListElement`**

- Usage: An ordered/unordered list
- Style: Block, top and bottom margins, left padding
- CSS: `list-style*`, `line-height`, `margin`, `padding`
- Content:
    - Zero or more`<li>`
    - Nested `<ol>, <ul>`
- Parent: Any element accepts flow content
- Roles: `directory, group, listbox, menu, menubar, radiogroup, tablist, toolbar, tree, presentation`
- Attributes: Global attributes, `reversed, start, type`
- Note:
    - Use `padding-left: 0` to clear indentation of `<li>`
    - `list-style*` are inherited

**`<li>: HTMLLIElement`**

- Usage: A list item
- Style: `display: list-item`
- CSS: `list-style-type`, `margin`
- Content: Flow content
- Parent: `<ul>, <ol>`
- Roles: `menuitem, menuitemcheckbox, menuitemradio, option, presentation, radio, separator, tab, treeitem`
- Attributes: Global attributes, `value`
- Notes:
    - Use `list-style-type: none` to remove numbering or bullets
    - Preferred to set `list-style*` on `<ol>, <ul>`

**`<main>: HTMLElement`**

- Usage: To contain the central topic of a document
- Style: Block
- Content: Flow content
- Parent:
    - Flow content
    - Not `<article>, <aside>, <footer>, <header>, <nav>`
    - No more than one in a document
- Roles:
    - Default: `main`
    - Permitted: `presentation`
- Attributes: Global attributes

**`<p>: HTMLParagraphElement`**

- Usage: Paragraph
- Style: Block, top and bottom margins
- Content: Phrasing content
- Parent: Any element accepts flow content
- Roles: Any
- Attributes: Global attributes
