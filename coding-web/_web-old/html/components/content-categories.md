# HTML5 Content Categories


No clear line between Flow content and Sectioning Content.

**Flow content**

Flow content elements contain text or embedded content.

- `<a>, <abbr>, <address>, <article>, <aside>, <audio>, <b>,<bdo>, <bdi>, <blockquote>, <br>, <button>, <canvas>, <cite>, <code>, <data>, <datalist>, <del>, <details>, <dfn>, <div>, <dl>, <em>, <embed>, <fieldset>, <figure>, <footer>, <form>, <h1>, <h2>, <h3>, <h4>, <h5>, <h6>, <header>, <hgroup>, <hr>, <i>, <iframe>, <img>, <input>, <ins>, <kbd>, <keygen>, <label>, <main>, <map>, <mark>, <math>, <menu>, <meter>, <nav>, <noscript>, <object>, <ol>, <output>, <p>, <pre>, <progress>, <q>, <ruby>, <s>, <samp>, <script>, <section>, <select>, <small>, <span>, <strong>, <sub>, <sup>, <svg>, <table>, <template>, <textarea>, <time>, <ul>, <var>, <video>, <wbr>`
- Text

**Sectioning content**

- Semantic elements:
    - `<header>, <footer>`
    - `<article>, <aside>, <nav> and <section>`
- `<div>`:
    - The only sectioning element in HTML4.
    - A container for flow content without semantic meaning.



**Heading content**

- `<h1>, <h2>, <h3>, <h4>, <h5>, <h6> and <hgroup>`

**Phrasing content**

- `<abbr>, <audio>, <b>, <bdo>, <br>, <button>, <canvas>, <cite>, <code>, <data>, <datalist>, <dfn>, <em>, <embed>, <i>, <iframe>, <img>, <input>, <kbd>, <keygen>, <label>, <mark>, <math>, <meter>, <noscript>, <object>, <output>, <progress>, <q>, <ruby>, <samp>, <script>, <select>, <small>, <span>, <strong>, <sub>, <sup>, <svg>, <textarea>, <time>, <var>, <video>, <wbr>`
- Plain text

**Embedded content**

- `<audio>, <canvas>, <embed>, <iframe>, <img>, <math>, <object>, <svg>, <video>`


**Palpable content**

-  An element is neither empty or hidden.

## Read

- [Content categories](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Flow_content)
