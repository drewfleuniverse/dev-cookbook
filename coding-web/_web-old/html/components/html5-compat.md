**HTML5 elements - IE9 above**

```css
article,
aside,
figcaption,
figure,
footer,
header,
nav,
section {
  display: block;
}
```

**HTML5 elements - IE9 below**

```html
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->  
```


## Read

- [HTML5 (and Some CSS) Best Practice](https://www.codeproject.com/Tips/666578/HTML-and-Some-CSS-Best-Practice)
