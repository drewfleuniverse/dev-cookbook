
**Closing slash (/)**

Preferred to keep the slash.

- e.g. `<meta attr="foo" />`:
- Not required in HTML5
- Required in XHTML and XML
