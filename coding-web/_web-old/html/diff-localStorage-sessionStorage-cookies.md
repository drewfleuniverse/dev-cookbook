# LocalStorage vs. SessionStorage vs. Cookies


- HTML5 Web Storage
    - Has `localStorage` and `sessionStorage`
    - Accessible by js runs in the domain
    - Stores data as key-value pairs of strings
    - Capacity 10MB per domain
    - `localStorage` is permanent until explicitly cleared using js or manually in browser
    - `sessionStorage` exists until browser closes
    - Vulnerable to cross-site scripting (XSS)
- Cookies
    - A small piece of data that a server sends to the user's web browser
    - Can be set via HTTP header `Set-Cookie` and js
    - If `HttpOnly` is set, cookies are not accessible via js
    - Stores data as key-value pairs of strings
    - Capacity 4093 bytes (0.004MB)
    - Use for session management, personalization, and tracking
    - Vulnerable to cross-site request forgery (CSRF)
    - N.b. StatelessCSRF protection is achievable via JWT claim `xsrfToken` and HTTP header `X-XSRF-TOKEN`
    - E.g. facebook relies more on cookies then on web storage


## References

- [What is the difference between localStorage, sessionStorage, session and cookies?](https://stackoverflow.com/a/19869560/4632950)
- [Where to Store your JWTs – Cookies vs HTML5 Web Storage](https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage)
