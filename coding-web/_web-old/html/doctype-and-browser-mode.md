# Doctype and Browser Mode


## `<!DOCTYPE html>`

- Use when serving page with MIME type `text/html`
- Specify doctype to:
    - Enables standards mode
    - Prevents older browser uses quirks mode


## Notes on serving XHTML

- When serving page with MIME type `application/xhtml+xml`, browsers automatically enables standards mode


## Modes

- Quirks mode: supports legacy for Navigator 4 and IE 5
- Almost standards mode: minimal quirks
- Full standards mode:
    - Disables quirk
    - Ignores unsupported new standards if the browser hasn't supported yet
