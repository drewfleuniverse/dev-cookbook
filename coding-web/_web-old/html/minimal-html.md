**Minimal HTML file - HTML5, preferred**

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
  </body>
</html>
```

**Minimal HTML file - HTML5, backward compatible**

In this example, `<head>` and metadata except `<title>` are omitted

```html
<!DOCTYPE html>
<html>
<title>Page Title</title>

<body>
 <h1>This is a heading</h1>
 <p>This is a paragraph.</p>
</body>

</html>
```

**Minimal HTML file - HTML5, new browser only**

In the HTML5 standard, the <html> tag and the <body> tag can be omitted. Not recommended, due to:

- Omitting <html> or <body> can crash DOM and XML software.
- Omitting <body> can produce errors in older browsers (IE9).

```html
<!DOCTYPE html>
<head>
 <title>Page Title</title>
</head>

<h1>This is a heading</h1>
<p>This is a paragraph.</p>
```
