# Script - Normal vs `async` vs `defer`


## Notes

- Normal: Blocks further HTML parsing when browser reaches `script` until the script is downloaded and finished execution
- `async`: Browser parses HTML and downloads script concurrently and blocks HTML parsing while executing the downloaded script
- `defer`: Browser parses HTML and downloads script concurrently and executes the script when both of HTML and the script are loaded


**Best practices**

- When HTML code is small, use normal `script` and best place it at the end of `body` tag
- Do not use `async` or `defer` when scripts needs to be executed in order, the above two attributes do not guarantee execution order
- Otherwise, use custom js to load scripts asynchronously and run in order

## References
- [Asynchronous vs Deferred JavaScript](https://bitsofco.de/async-vs-defer/)
