# WebSocket Uses

A WebSocket establishes a server connections through an `Upgrade` header in HTTP request. The connection is then bidirectional and peer-to-peer between the client and server.

Uses:

- Social feeds
- Multiplayer games
- Collaborative editing/coding
- Clickstream data
- Financial tickers
- Sports updates
- Multimedia chat
- Location-based apps
- Online education
