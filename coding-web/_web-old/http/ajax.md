# AJAX

- MIME type: `application/json`, `application/xml`, etc
- Short for Asynchronous JavaScript + XML
- Termed in 2005
- Web applications using AJAX allows incremental updates through `XMLHttpRequest` object (XHR)
- AJAX is more likely to use JSON than XML, the X
- By default follows same-origin policy, can bypass the policy using older JSONP or newer CORS


**Conventional model**

- User clicks something
- Browser makes HTTP request
- Server responds with HTML+CSS and entire page is updated

**Ajax model**

- User clicks something
- JavaScript makes HTTP request, i.e. XHR
- Server responds with data, i.e. JSON, XML, etc
- JavaScript updates partial page
