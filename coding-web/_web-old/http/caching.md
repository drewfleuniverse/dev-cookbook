# Caching


Tips below are copied from Google developer website:

- Use consistent URLs: if you serve the same content on different URLs, then that content will be fetched and stored multiple times. Tip: note that URLs are case sensitive.
- Ensure that the server provides a validation token (ETag): validation tokens eliminate the need to transfer the same bytes when a resource has not changed on the server.
- Identify which resources can be cached by intermediaries: those with responses that are identical for all users are great candidates to be cached by a CDN and other intermediaries.
- Determine the optimal cache lifetime for each resource: different resources may have different freshness requirements. Audit and determine the appropriate max-age for each one.
- Determine the best cache hierarchy for your site: the combination of resource URLs with content fingerprints and short or no-cache lifetimes for HTML documents allows you to control how quickly the client picks up updates.
- Minimize churn: some resources are updated more frequently than others. If there is a particular part of a resource (for example, a JavaScript function or a set of CSS styles) that is often updated, consider delivering that code as a separate file. Doing so allows the remainder of the content (for example, library code that doesn't change very often), to be fetched from cache and minimizes the amount of downloaded content whenever an update is fetched.



## Read

- [HTTP Caching ](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching)
- [Redux Patterns: Caching an API Response](https://hackernoon.com/redux-patterns-caching-an-api-response-f85f8d8d73c6)
- [Understanding The Vary Header](https://www.smashingmagazine.com/2017/11/understanding-vary-header/)
