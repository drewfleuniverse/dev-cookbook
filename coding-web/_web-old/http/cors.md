# Cross-Origin Resource Sharing


## Examples

**Simple request**

Request:

```
GET /users HTTP/1.1
Host: bar.com
Origin: http://foo.com
Accept: application/json
# More headers...
```

Response:

```
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Content-Type: application/json
# More headers...
# Response data
```

**Preflight request**

Preflight Request:

```
OPTIONS /users HTTP/1.1
Host: bar.com
Origin: http://foo.com
Access-Control-Request-Method: POST
Access-Control-Request-Headers: Content-Type
# More headers...
```

Preflight Response:

```
HTTP/1.1 200 OK
Access-Control-Allow-Origin: http://foo.com
Access-Control-Allow-Methods: POST, GET
# More headers...
```

Request:

```
POST /users HTTP/1.1
Origin: http://foo.com
Access-Control-Request-Method: POST
Access-Control-Request-Headers: Content-Type
Content-Type: application/json
# More headers...
# Response data
```

Response:

```
HTTP/1.1 201 OK
Access-Control-Allow-Origin: http://foo.com
# More headers...
```


## Studies


**CORS happens when**

- Loading CSS stylesheets, images, and scripts from different domains is allowed, e.g. CDN.
- Most CDN has wildcard or manually listed client domain for `Access-Control-Allow-Origin` header
- CORS is imposed on HTTP requests within scripts, especially for XMLHttpRequest and Fetch API.

**HTTP methods for simple requests**

- `GET`
- `HEAD`
- `POST`

**HTTP methods for preflight requests**

- `PUT`
- `DELETE`
- `CONNECT`
- `OPTIONS`
- `TRACE`
- `PATCH`

**Headers**

- `Access-Control-Allow-Origin`:
    - Only a single value is allowed:
        - W3C allows multiple values
        - Browsers only allow a single value
        - Server has to be able to response with either:
            - Wildcard `*`
            - Fill in the request origin

**CORS flow**

- JavaScript executes a request
- If preflight is needed:
    - Browser sends preflight request to server
    - Server sends preflight response to browser
- Browser sends actual request to server
- Server sends actual response to browser
- Javascript process response

**Browser compatibility**

- Chrome 3+
- Firefox 3.5+
- Opera 12+
- Safari 4+
- Internet Explorer 8+


## Example - CORS Caching

**`foo.com` headers**

```sh
GET /v1/data HTTP/1.1
Host: bar.com
Origin: http://foo.com
```

**`bar.com` headers**

```sh
HTTP/1.1 200 Ok
Content-Type: application/json
Content-Length: 4365
Access-Control-Allow-Origin: http://foo.com
Vary: Origin
Cache-Control: max-age=3600 # TTL, time to live
```

## Example - AWS S3 bucket policy to enable CORS

```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
    <CORSRule>
        <AllowedOrigin>*</AllowedOrigin>
        <AllowedMethod>GET</AllowedMethod>
        <MaxAgeSeconds>3000</MaxAgeSeconds>
        <AllowedHeader>Authorization</AllowedHeader>
        <AllowedHeader>Content-*</AllowedHeader>
        <AllowedHeader>Host</AllowedHeader>
    </CORSRule>
</CORSConfiguration>
```


## Read

- [CORS](https://www.html5rocks.com/en/tutorials/cors/)
- [Cachin CORS](https://www.fastly.com/blog/caching-cors)
