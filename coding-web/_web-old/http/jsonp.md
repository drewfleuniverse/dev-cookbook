# JSONP


- MIME type: `application/json-p`
- Short for JSON with Padding
- For requesting data from different domain
- Unlike Ajax model, JSONP doesn't impose same-origin policy
- Replaced by newer CORS model
- Works on older browsers where CORS have no support

**Example**

```html
<script type="application/javascript"
        src="http://server.example.com/Users/1234?callback=parseResponse">
</script>
```

- `parseResponse` function is the P of JSONP
- In JSONP, server returns JSON data that 'wrapped' in the above JavaScript, once script is received, the callback function `parseResponse` is called to return JSON data
