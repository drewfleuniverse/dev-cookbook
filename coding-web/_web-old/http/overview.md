# HTTP Overview

- HTTP: Hypertext Transfer Protocol
- HTTP as an application-layer protocol
  - HTTP can operate on OSI model layer 7, application layer. HTTP may also use on layer 4 with UDP.
- HTTP as a client-server protocol
- HTTP as a request-response protocol
- HTTP as a stateless protocol
  - Unlike FTP, HTTP doesn't have a session layer, thus HTTP needs additional session management techniques such as cookie, session ID, or web storage.


## Notes

- Distributed computing has four basic architectures: client–server, three-tier, n-tier, and peer-to-peer
