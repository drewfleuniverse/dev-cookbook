# REST


- REpresentational State Transfer
- IS stateless
- REST APIs contain a set of resource URIs
- A REST URI may respond in XML, HTML, JSON, etc
- REST is an architectural style, while SOAP is a protocol


**URI, URN, URL**

- URI contains URN and URL
- URI: `index.html`
- URN: `foo.com/index.html`
- URL: `http://foo.com/index.html`


**HTTP methods - on a collection**

URL: `https://api.foo.com/collection`

- `GET`: list the collection
- `PUT`: replace the collection
- `PATCH`: N/A
- `POST`: create a new entry in the collection
- `DELETE`: delete the collection


**HTTP methods - on an element**

URL: `https://api.foo.com/collection/bar`

- `GET`: retrieve entry bar
- `PUT`: replace entry bar
- `PATCH`: update entry bar
- `POST`: create a new entry in entry bar
- `DELETE`: delete entry bar


**Idempotent**

- `GET`, `PUT`, and `DELETE` are idempotent.
- In latin, *idem* means same, *potent* means power.
- Examples: f(f(x)) = f(X), abs(abs(x)) = abs(x), 1 * 1 = 1, (+2) = 2, max(3,3) = 3
