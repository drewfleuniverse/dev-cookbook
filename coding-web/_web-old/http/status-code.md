# HTTP Status Code


- 1xx: Informational
- 2xx: Success
- 3xx: Redirection
- 4xx: Client Error
- 5xx: Server Error

- *200 (OK)*: GET, POST
- *201 (Created)*
- 202 (Accepted)
- *204 (No Content)*: POST, PUT, DELETE

- *301 (Moved Permanently)*
- 302 (Found)
- 303 (See Other)
- *304 (Not Modified)*: POST, PUT
- 307 (Temporary Redirect)

- *401 (Unauthorized)*
- *403 (Forbidden)*
- *404 (Not Found)*
- *405 (Method Not Allowed)*
- 406 (Not Acceptable)
- 412 (Precondition Failed)
- 415 (Unsupported Media Type)

- *500 (Internal Server Error)*
- *501 (Not Implemented)*


- [What is the difference between HTTP status code 200 (cache) vs status code 304?](https://stackoverflow.com/questions/1665082/what-is-the-difference-between-http-status-code-200-cache-vs-status-code-304)

- [Common codes](https://restfulapi.net/http-status-codes/)
