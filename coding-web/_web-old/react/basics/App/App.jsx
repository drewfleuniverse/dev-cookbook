// @flow
import * as React from 'react';
import Login from './Login';

const App = (): React.Element<'div'> => (
  <div className="App">
    <Login />
  </div>
);


export default App;
