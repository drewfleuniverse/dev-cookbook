// @flow
import * as React from 'react';

type Props = {
  message?: string,
};

type State = {
  email: string,
  password: string
};


export default
class Login extends React.Component<Props, State> {
  static defaultProps = {
    username: 'foo@bar.com',
  };
  constructor(props: any) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }
  handleChange = (event: SyntheticEvent<HTMLInputElement>) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  }
  handleSubmit = (event: SyntheticEvent<HTMLInputElement>) => {
    event.preventDefault();
    console.log(this.state.username);
    console.log(this.state.password);
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label htmlFor="username">
          Username:
          <input
            id="username"
            type="text"
            name="username"
            onChange={this.handleChange}
            value={this.state.username}
          />
        </label>
        <label htmlFor="password">
          Password:
          <input
            id="password"
            type="password"
            name="password"
            onChange={this.handleChange}
            value={this.state.password}
          />
        </label>
        <input type="submit" />
      </form>
    );
  }
}
