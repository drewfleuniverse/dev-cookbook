// @flow
import * as React from 'react';


const simpleElement: React.Element<"div"> = (
  <div
    strAttr="foo"
    expAttr={40 + 2}
  >
    <h1>Title</h1>
    <p>Content</p>
  </div>
);


export default simpleElement;

// export const escape: React.Element<"a"> = (
//   <a
//     href={
//       'http://localhost/page.php?<SCRIPT>alert("XSS!")</SCRIPT>'
//     }
//   >
//     XSS
//   </a>
// );
