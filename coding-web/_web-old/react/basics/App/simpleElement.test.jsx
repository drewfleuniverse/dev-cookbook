// @flow
import * as React from 'react';
// import TestRenderer from 'react-test-renderer';
import simpleElement from './simpleElement';


describe('simpleElement', () => {
  it('can have string literal as attribute', () => {
    const { strAttr } = simpleElement.props;
    expect(strAttr).toBe('foo');
  });
  it('can have JavaScript expression as attribute', () => {
    const { expAttr } = simpleElement.props;
    expect(expAttr).toBe(42);
  });
  it('can have children', () => {
    const { children } = simpleElement.props;
    expect(children).toEqual([
      <h1>Title</h1>,
      <p>Content</p>,
    ]);
  });
  // it('escapes by default', () => {
  //   const testRenderer = TestRenderer.create(escape);
  //   console.log(testRenderer.root);
  // });
});
