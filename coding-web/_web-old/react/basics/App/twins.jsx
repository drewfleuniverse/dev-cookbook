// @flow
import * as React from 'react';


export const twinJsx: React.Element<'p'> = (
  <p className="foo">
    bar
  </p>
);

export const twinJs: React.Element<'p'> = React.createElement(
  'p',
  { className: 'foo' },
  'bar',
);
