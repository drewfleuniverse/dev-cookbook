// @flow
import {
  twinJsx,
  twinJs,
} from './twins';


describe('twinJsx and twinJs', () => {
  it('are objects', () => {
    expect(typeof twinJsx).toEqual('object');
    expect(typeof twinJs).toEqual('object');
  });
  it('are identical', () => {
    const a: string = JSON.stringify(twinJsx);
    const b: string = JSON.stringify(twinJs);
    expect(a).toEqual(b);
  });
});
