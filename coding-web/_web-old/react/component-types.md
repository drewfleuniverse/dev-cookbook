# Component Types


## Stateless Functional Component

Stateless functional component (SFC) is a pure function which renders same component as long as its given props remain unchanged.


## Stateless Component vs Pure Component

A pure component (PC) is a component shipped with its `shouldComponentUpdate` method only does shallow comparison. In contrast, as of React 16, a stateless functional component (SFC) doesn't have a `shouldComponentUpdate` method to prevent itself being re-rendered unnecessarily. Thus, for performance wise, SFCs should be guarded by a component which has a `shouldComponentUpdate` implemented properly.

## Fragments

```jsx
function Glossary(props) {
  return (
    <dl>
      {props.items.map(item => (
        // Without the `key`, React will fire a key warning
        <Fragment key={item.id}>
          <dt>{item.term}</dt>
          <dd>{item.description}</dd>
        </Fragment>
      ))}
    </dl>
  );
}
```
