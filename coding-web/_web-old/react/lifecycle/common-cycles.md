# Common Cycles


**Instantiation**

```
constructor()
+ getDerivedStateFromProps()
- UNSAFE_componentWillMount()
render()
componentDidMount()
```

**Update props**

```
shouldComponentUpdate()
+ getDerivedStateFromProps()
- UNSAFE_componentWillReceiveProps()
- UNSAFE_componentWillUpdate()
render()
componentDidUpdate()
```

**Update state**

```
shouldComponentUpdate()
- UNSAFE_componentWillUpdate()
render()
componentDidUpdate()
```
