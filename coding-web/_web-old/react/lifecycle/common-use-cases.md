# Common use cases


**Update state**

- `componentDidMount()`
- `static getDerivedStateFromProps()`
- `UNSAFE_componentWillMount()`
- `UNSAFE_componentWillReceiveProps()`

**Perform async calls / re-render**

- `componentDidUpdate()`
- `componentDidMount()`

**Cancel async calls / un-subscribe timers**

- `componentWillUnmount()`

**Subscribe timers**

- `componentDidMount()`

**Prepare re-render**

- `getSnapshotBeforeUpdate()`
- `UNSAFE_componentWillUpdate()`
