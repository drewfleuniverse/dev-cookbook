// @flow
import * as React from 'react';


export const lifecycle = {
  count: 0,
  callers: [],
  add(caller: string) {
    this.count += 1;
    this.callers.push(`${this.count}: ${caller}`);
  },
  reset() {
    this.count = 0;
    this.callers.length = 0;
  },
};


type Props = {
  msg: string
};

type State = {
  msg: string
};


class App extends React.Component<Props, State> {
  static defaultProps = {
    msg: 'hello',
  }
  static getDerivedStateFromProps(nextProps: Props, prevState: State) {
    lifecycle.add('getDerivedStateFromProps()');
    if (nextProps.msg === prevState.msg) {
      return null;
    }
    return { msg: nextProps.msg };
  }
  constructor(props: Props) {
    super(props);
    lifecycle.add('constructor()');
    this.state = { msg: props.msg };
  }
  componentDidMount() {
    lifecycle.add('componentDidMount()');
  }
  shouldComponentUpdate(nextProps: Props, nextState: State) {
    lifecycle.add('shouldComponentUpdate()');
    if (nextProps.msg !== this.state.msg) {
      return true;
    }
    if (nextState.msg !== this.state.msg) {
      return true;
    }
    return false;
  }
  componentDidUpdate() {
    lifecycle.add('componentDidUpdate()');
  }
  componentWillUnmount() {
    lifecycle.add('componentWillUnmount()');
  }
  render() {
    lifecycle.add('render()');
    return (
      <div>
        {this.state.msg}
      </div>
    );
  }
}


export default App;
