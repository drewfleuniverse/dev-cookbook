import * as React from 'react';
import { shallow } from 'enzyme';
import App, { lifecycle } from './App';

beforeEach(() => {
  lifecycle.reset();
});

describe('Component App', () => {
  it('renders without crashing', () => {
    shallow(<App />);
  });

  it('initialzes state', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.state('msg')).toBe('hello');
  });

  it('renders', () => {
    shallow(<App />);
    expect(lifecycle.callers).toEqual([
      '1: constructor()',
      '2: getDerivedStateFromProps()',
      '3: render()',
      '4: componentDidMount()',
    ]);
  });

  const initialLifecycle = [
    '1: constructor()',
    '2: getDerivedStateFromProps()',
    '3: render()',
    '4: componentDidMount()',
  ];

  it('updates props.msg with old value', () => {
    const wrapper = shallow(<App />);
    wrapper.setProps({ msg: wrapper.state().msg });
    expect(lifecycle.callers.slice(0, 4)).toEqual(initialLifecycle);
    expect(lifecycle.callers.slice(4)).toEqual([
      '5: shouldComponentUpdate()',
    ]);
  });

  it('updates props.msg with new value', () => {
    const wrapper = shallow(<App />);
    wrapper.setProps({ msg: 'world' });
    expect(lifecycle.callers.slice(0, 4)).toEqual(initialLifecycle);
    expect(lifecycle.callers.slice(4)).toEqual([
      '5: shouldComponentUpdate()',
      '6: getDerivedStateFromProps()',
      '7: render()',
      '8: componentDidUpdate()',
    ]);
  });

  it('updates state.msg with old value', () => {
    const wrapper = shallow(<App />);
    wrapper.setState({ msg: wrapper.state().msg });
    expect(lifecycle.callers.slice(0, 4)).toEqual(initialLifecycle);
    expect(lifecycle.callers.slice(4)).toEqual([
      '5: shouldComponentUpdate()',
    ]);
  });

  it('updates state.msg with new value', () => {
    const wrapper = shallow(<App />);
    wrapper.setState({ msg: 'world' });
    expect(lifecycle.callers.slice(0, 4)).toEqual(initialLifecycle);
    expect(lifecycle.callers.slice(4)).toEqual([
      '5: shouldComponentUpdate()',
      '6: render()',
      '7: componentDidUpdate()',
    ]);
  });

  it('unmounts', () => {
    const wrapper = shallow(<App />);
    wrapper.unmount();
    expect(lifecycle.callers.slice(0, 4)).toEqual(initialLifecycle);
    expect(lifecycle.callers.slice(4)).toEqual([
      '5: componentWillUnmount()',
    ]);
  });
});
