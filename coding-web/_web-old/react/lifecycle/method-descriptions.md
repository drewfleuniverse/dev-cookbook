# Method Descriptions


**Current methods**

`constructor(props): void`:

- Invocation - Mounting:
    - Before all methods
- Uses:
    - Initialize state using `this.state`
    - Bind event handlers
- Cautions:
    - If not initializing state and not binding event handlers, consider omitting `constructor()`
    - If the state depends on `props`, consider:
        - Lifting the state up (preferred)
        - Use `getDerivedStateFromProps()`
    - Don't call `setState()`

`render(): React.Node`:

- Invocation - Mounting:
    - Before `componentDidMount()`
- Invocation - Updating:
    - Before `componentDidUpdate()`
- Uses:
    - Consume `this.props` and `this.state`
    - Returns:
        - React elements using JSX
        - String or number to render text nodes
        - Booleans or `null`
            - If `null` or `false` is returned, node will not be found in DOM
- Cautions:
    - Must be a pure function

`componentDidMount(): void`:

- Invocation - Mounting:
    - The last lifecycle hook in mounting phase
- Uses:
    - Perform async operations
    - Perform subscriptions, e.g. subscribe timers
    - Re-render
    - Access the rendered DOM node
- Cautions:
    - Call `setState()` only when necessary due to triggering an extra *invisible* `render()`, e.g. may be needed for building modal and tooltip that need to find position before visible in browser

`static getDerivedStateFromProps(nextProps, prevState): PartialStateObject | null`:

- Invocation - Mounting:
    - Before `render()`
- Invocation - Updating:
    - Before `render()`
- Uses:
    - Returns state change or `null`
- Uses - Mounting:
    - Use as an alternative to initialize state with props in `constructor()`
- Uses - Updating:
    - Update state from props
- Notes - Updating:
    - Call `setState()` does not trigger this methods
    - Use `shouldComponentUpdate()` to prevent calling this method
    - Current behavior: Once this method is called, even the state is not updated, the entire updating lifecycle will be completed

`shouldComponentUpdate(nextProps, nextState, nextContext): boolean`:

- Invocation - Updating:
    - The first lifecycle hook in updating phase
- Uses:
    - Optimize performance
- Cautions:
    - Defaults to return `true`
- Cautions - `React.PureComponent`:
    - Internally performs shallow comparison
- Notes:
    - Return `false` to prevent calling further lifecycle hooks
    - Call `forceUpdate()` does not trigger this method

`getSnapshotBeforeUpdate(prevProps, prevState): snapshot`

- Uses:
    - Prepare re-render
    - Access the DOM node to be re-rendered

`componentDidUpdate(prevProps, prevState, prevContext): void`:

- Invocation - Updating:
    - The last lifecycle hook in updating phase
- Uses:
    - Perform async operations
    - Re-render
    - Access the rendered DOM node

`componentWillUnmount(): void`:

- Invocation - Un-mounting:
    - The last lifecycle hook
- Uses:
    - Cancel async operations
    - Perform un-subscriptions, e.g. unsubscribe timers

`componentDidCatch(errorString, errorInfo): void`:

- Handle errors

`setState(updater[, callback])`:

- Uses:
    - Enqueue state change
    - Trigger an updating lifecycle
- Cautions:
    - Immediately accessing `this.state` after calling `setState({...})` may not show updated value
    - If accessing `this.state` immediately after calling `setState()` is needed:
        - Access it in the `setState` callback
        - Access it in `componentDidUpdate()`
    - If state change depends on current state, use the function form of updater
- Updater signature:
    - Function: `(prevState, props) => ({...})`:
    - Object: `{...}`:

**Methods to be deprecated in version 17**

`UNSAFE_componentWillMount(): void`:

- Prefer: `componentDidMount()`
- Invocation - Mounting:
    - Before `render()`
- Uses:
    - Call `setState()`
    - The only lifecycle hook called on server rendering

`UNSAFE_componentWillReceiveProps(nextProps): void`:

- Prefer: `static getDerivedStateFromProps()`
- Invocation - Updating:
    - Before `UNSAFE_componentWillUpdate()`
- Uses:
    - Call `setState()`
- Caution:
    - Use `shouldComponentUpdate()` to prevent unnecessary calls to this method
    - Check `nextProps` and `this.props` before calling `setState()`
- Notes:
    - `setState()` doesn't trigger this method

`UNSAFE_componentWillUpdate(nextProps, nextState): void`:

- Prefer: `static getDerivedStateFromProps()`
- Invocation - Mounting:
    - Before `render()`
- Uses:
    - Prepare re-render
    - Access the DOM node to be re-rendered
- Cautions:
    - Do not call `setState()`
    - Do not dispatch Redux action
