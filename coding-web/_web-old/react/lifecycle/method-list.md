# Common Methods


**Methods**

```ts
constructor(props): void
static getDerivedStateFromProps(nextProps, prevState): stateChange | null
render(): React.node
componentDidMount(): void
shouldComponentUpdate(nextProps, nextState, nextContext): boolean
getSnapshotBeforeUpdate(prevProps, prevState): snapshot | null
componentDidUpdate(prevProps, prevState, prevContext): void
componentDidCatch(error, info): void
componentWillUnmount(): void
render(): React.node
```

**Deprecating methods**

```ts
UNSAFE_componentWillMount(): void
UNSAFE_componentWillReceiveProps(nextProps): void
UNSAFE_componentWillUpdate(nextProps, nextState): void
```

**Other methods**

```ts
setState(updater[, callback])
forceUpdate(callback)
```

**Class properties**

```js
defaultProps
displayName
```

**Instance properties**

```js
props
state
```
