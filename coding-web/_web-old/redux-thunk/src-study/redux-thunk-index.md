# Redux Thunk

## Source Code

```js
function createThunkMiddleware(extraArgument) {
  return ({ dispatch, getState }) => next => action => {
    /**
     * Enables the enhanced `dispatch' to be passed with a `function` instead
     * of a plain action object.
     */
    if (typeof action === 'function') {
      return action(dispatch, getState, extraArgument);
    }

    return next(action);
  };
}

/**
 * Points to the middleware to be passed to `applyMiddleware`
 */
const thunk = createThunkMiddleware();
/**
 * Exposing `withExtraArgument` method to allow passing extra argument to
 * the `action` function of thunk middleware
 */
thunk.withExtraArgument = createThunkMiddleware;

export default thunk;
```


## Notes

### Imitating Redux Thunk Implementation

```js
const middlewareReturnsThis = ({ dispatch, getState }) => action => action(dispatch, getState);

const dispatchTakesThatMiddleware = middlewareReturnsThis({
  dispatch: 'dispatch', getState: 'getState'
});


const thunk1 = () => (dispatch) => console.log(`${dispatch}`);
const thunk2 = () => (dispatch, getState) =>
  console.log(`${dispatch}, ${getState}`);

dispatchTakesThatMiddleware(thunk1()); // dispatch
dispatchTakesThatMiddleware(thunk2()); // dispatch, getState
```
