## Reducers

- Redux reduces using reducers
- A reducer suggests by its name that it reduce an output by returning an accumulator

## Lifecycle

**Action**
- An action creator function:
    - Take optional data arguments to become its payload
    - Returns an action object contains:
        - Action type
        - Payload (optional)

**Reducer**

- A reducer function:
    - Takes:
        - The current state belongs to this reducer
        - An action object
    - Reducer contains a set of conditions to decide how to shape the state subtree based action type

**Store**

- The action passed to store as an argument of `store.dispatch`
    - Internally, `currentReducer` represents the reducer function
    - `currentReducer` invokes the reducer function
    - Optionally invokes listeners which subscribes to this state subtree
- The root reducer uses `combineReducers` to update the entire state tree

**View**

- React updates components
