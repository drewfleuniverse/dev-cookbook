# Redux - applyMiddleware()


## Source Code

```js
export default function applyMiddleware(...middlewares) {
  /**
  * When `createStore` is called with an enhancer, `createStore` returns an
  * enhanced self with the following line:
  *   `return enhancer(createStore)(reducer, preloadedState)`
  * Thus `args` are `reducer` and `preloadedState`
  */
  return (createStore) => (...args) => {
    const store = createStore(...args)
    let dispatch = store.dispatch // points to the unenhanced `dispatch`
    let chain = []

    /**
    * `middlewareAPI.dispatch` points to whatever `dispatch` points to
    */
    const middlewareAPI = {
      getState: store.getState,
      dispatch: (...args) => dispatch(...args)
    }
    chain = middlewares.map(middleware => middleware(middlewareAPI))
    /**
     * - Now both of `dispatch` and `middlewareAPI.dispatch` points to the
     *   enhanced self
     * - The chain of middlewares are executed from right to left passed with
     *   the unenhanced dispatch
     *   - For this reason, the top most/left most middleware
     */
    dispatch = compose(...chain)(store.dispatch)

    return {
      ...store,
      dispatch
    }
  }
}
```


## Notes

### Middleware Signature

```js
const middleware = ({ getState, dispatch }) => next => action => {
  // do something
  return next(action);
};
```

`{ getState, dispatch }`:
- `getState`: points to the Store's, i.e. the current store, `getState` method
- `dispatch`: points to the enhanced

`dispatch` method return from `compose`
`next`: points to the original, unenhanced `dispatch` method

`action`: the action to be passed to enhanced `dispatch` method


### Closure with Arrow Functions

```js
const foo = a1 => a2 => {
  console.log(`${a1} ${a2}`);
};

foo(1); // a1=1, returns a2 => { ... };
foo(1)(2); // a1=1, a2=2, output 1 2
```

Both of `a1` and `a2` belong to the lexical environment of the inner function body block.<br/>

### Arrow Function Scope

```js
const foo = () => console.log('foo');
const bar = () => console.log('bar');
const moo = () => console.log('moo');

let c = foo;

const o = {
  f: c,
  g: () => c()
};

c = bar;

o.f(); // 'foo'
o.g(); // 'bar'

c = moo;
o.g(); // 'moo'
```

`{ g: () => c() }` fetches `c` lexically from the scope upon invocation
