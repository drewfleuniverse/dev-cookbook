# Redux - compose()


## Source Code

```js
/**
 * `compose()` returns a function in any calling condition, i.e. from zero or
 * one to many arguments
 */
export default function compose(...funcs) {
  /**
   * `compose()(foo) returns foo`
   * Equal to `(a => a)(foo)`
   */
  if (funcs.length === 0) {
    return arg => arg
  }

  /**
   * `compose(a => a)(foo) returns foo`
   * `compose(fooFunction)() executes fooFunction`
   * `compose(fooFunction)(bar) executes fooFunction with arg bar`
   */
  if (funcs.length === 1) {
    return funcs[0]
  }

  /**
   * Given an array of functions `[a1, a2, a3]`, in each execution, the
   * accumulator `a` is passed with a middleware `b` and returns:
   *   `(...args) => a(b(...args))`
   * The above function can be expanded as below:
   *   `(...args) => a1(a2(a3(...args)))`
   * Thus when invoking `compose([a1, a2, a3])(args)`, the returned value is
   * the immediate invocation of the nested functions:
   *   `a1(a2(a3(...args)))`
   */
  return funcs.reduce((a, b) => (...args) => a(b(...args)))
}
```


## Notes

### Composition order

`compose(f, g, h)(1, 2, 3)` is equivalent to ()

###### Example

```js
const f = (...args) => args,
      g = (...args) => args,
      h = (...args) => args;
(
  (...args) => f(...g(...h(...args)))
)(1, 2, 3); // returns [1, 2, 3]
```


### Array.prototype.reduce()

```js
const A = [1, 2, 3];
/**
 * First execution:
 *   - `accumulator = A[0]`
 *   - `currentValue = A[1]`
 * Second execution and onwards:
 *   - `accumulator` is the return value of first execution
 *   - `currentValue = A[2]`
 */
const foo = A.reduce((accumulator, currentValue) => (
  accumulator + currentValue
)); // foo = 6


const f = arg => arg,
      g = arg => arg,
      h = arg => arg;
const B = [f, g, h];
/**
 * `a(b(arg))` is always evaluated in each execution, thus:
 *   - In each execution, `b(arg)` is evaluated and passed to `a`
 *   - In each execution,`a` is an evaluated function returned from previous
 *     execution
 *   - The returned value of `B.reduce(...)` is a function `arg => ...`
 */
const bar = B.reduce((a, b) => arg => a(b(arg)));
bar('cool'); // 'cool'
```
