# Notes


## Environment Variables

- Environment variables are injected via webpack DefinePlugin and are isolated from the build environment (for Node.js/webpack) and runtime environment (for browser/react)
