const { minimalHtml5Factory } = require("./dom-utils");

/**
 * HTML element terms:
 * `<tag attribute="attribute-value">text</tag>`
 */

const {
  window: { document, Node, DocumentType, HTMLDocument }
} = minimalHtml5Factory({
  body: `
<div>element</div>
<!-- comment -->
text
`
});

const div = document.getElementsByTagName("div")[0];
const comment = div.nextSibling.nextSibling;
const text = comment.nextSibling;
const fragment = document.createDocumentFragment();

describe("DOM node types", () => {
  test("types", () => {
    expect(Node.DOCUMENT_NODE).toBe(9);
    expect(Node.DOCUMENT_TYPE_NODE).toBe(10);
    expect(Node.ELEMENT_NODE).toBe(1);
    expect(Node.TEXT_NODE).toBe(3);
    expect(Node.DOCUMENT_FRAGMENT_NODE).toBe(11);
    expect(Node.COMMENT_NODE).toBe(8);
  });
  describe("DOCUMENT_NODE", () => {
    test("document", () => {
      expect(document.nodeType).toBe(Node.DOCUMENT_NODE);
      expect(document.nodeName).toBe("#document");
      expect(document.constructor).toBe(HTMLDocument);
    });
  });
  describe("DOCUMENT_TYPE_NODE", () => {
    test("doctype", () => {
      expect(document.doctype.nodeType).toBe(Node.DOCUMENT_TYPE_NODE);
      expect(document.doctype.nodeName).toBe("html");
      expect(document.doctype.constructor).toBe(DocumentType);
    });
  });
  describe("ELEMENT_NODE", () => {
    test("documentElement", () => {
      expect(document.documentElement.nodeType).toBe(Node.ELEMENT_NODE);
      expect(document.documentElement.nodeName).toBe("HTML");
    });
    test("head", () => {
      expect(document.head.nodeType).toBe(Node.ELEMENT_NODE);
      expect(document.head.nodeName).toBe("HEAD");
    });
    test("body", () => {
      expect(document.body.nodeType).toBe(Node.ELEMENT_NODE);
      expect(document.body.nodeName).toBe("BODY");
    });
    test("div", () => {
      expect(div.nodeType).toBe(Node.ELEMENT_NODE);
      expect(div.nodeName).toBe("DIV");
      expect(div.textContent).toBe("element");
    });
  });
  describe("COMMENT_NODE", () => {
    test("comment", () => {
      expect(comment.nodeType).toBe(Node.COMMENT_NODE);
      expect(comment.nodeName).toBe("#comment");
      expect(comment.textContent.trim()).toBe("comment");
    });
  });
  describe("TEXT_NODE", () => {
    test("text", () => {
      expect(text.nodeType).toBe(Node.TEXT_NODE);
      expect(text.nodeName).toBe("#text");
      expect(text.textContent.trim()).toBe("text");
    });
  });
  describe("DOCUMENT_FRAGMENT_NODE", () => {
    test("fragment", () => {
      expect(fragment.nodeType).toBe(Node.DOCUMENT_FRAGMENT_NODE);
      expect(fragment.nodeName).toBe("#document-fragment");
    });
  });
});
