const { minimalHtml5Factory } = require("./dom-utils");

const {
  window: { document, Node }
} = minimalHtml5Factory({
  body: `
<h1>heading</h1>
<ul>
  <!-- comment -->
  <li>itemA</li>
  <li>itemB</li>
</ul>
`
});

const { body } = document;
const [html] = document.getElementsByTagName("html");
const [h1] = document.getElementsByTagName("h1");
const [ul] = document.getElementsByTagName("ul");
const [li1, li2] = document.getElementsByTagName("li");
const comment = li1.previousSibling.previousSibling;

describe("DOM traversal", () => {
  describe("parent", () => {
    describe("parentNode", () => {
      test("returns null when no parent", () => {
        expect(document.parentNode).toBe(null);
      });
      test("returns non-element parent", () => {
        expect(html.parentNode).not.toBe(null);
        expect(html.parentNode.nodeType).toBe(Node.DOCUMENT_NODE);
      });
      test("returns parent", () => {
        expect(body.parentNode).toBe(html);
        expect(h1.parentNode).toBe(body);
        expect(ul.parentNode).toBe(body);
        expect(li1.parentNode).toBe(ul);
      });
      test("is readonly", () => {
        li1.parentNode = h1;
        expect(li1.parentNode).toBe(ul);
      });
    });
    describe("parentElement", () => {
      test("returns null when no parent", () => {
        expect(document.parentElement).toBe(null);
        expect(html.parentElement).toBe(null);
      });
      test("returns parent", () => {
        expect(body.parentElement).toBe(html);
        expect(h1.parentElement).toBe(body);
        expect(ul.parentElement).toBe(body);
        expect(li1.parentElement).toBe(ul);
      });
      test("is readonly", () => {
        li1.parentElement = h1;
        expect(li1.parentElement).toBe(ul);
      });
    });
  });
  describe("child", () => {
    /**
     * childNodes returns a NodeList live collection
     */
    describe("childNodes", () => {
      test("returns all nodes ", () => {
        expect(ul.childNodes.length).toBe(7);
        expect(ul.childNodes[0].nodeType).toBe(Node.TEXT_NODE);
        expect(ul.childNodes[0].textContent).toBe("\n  ");
        expect(ul.childNodes[1].nodeType).toBe(Node.COMMENT_NODE);
        expect(ul.childNodes[1]).toBe(comment);
        expect(ul.childNodes[2].nodeType).toBe(Node.TEXT_NODE);
        expect(ul.childNodes[2].textContent).toBe("\n  ");
        expect(ul.childNodes[3].nodeType).toBe(Node.ELEMENT_NODE);
        expect(ul.childNodes[3]).toBe(li1);
        expect(ul.childNodes[4].nodeType).toBe(Node.TEXT_NODE);
        expect(ul.childNodes[4].textContent).toBe("\n  ");
        expect(ul.childNodes[5].nodeType).toBe(Node.ELEMENT_NODE);
        expect(ul.childNodes[5]).toBe(li2);
        expect(ul.childNodes[6].nodeType).toBe(Node.TEXT_NODE);
        expect(ul.childNodes[6].textContent).toBe("\n");
      });
    });
    /**
     * children returns a NodeList live collection
     */
    describe("children", () => {
      test("returns element nodes", () => {
        expect(ul.children.length).toBe(2);
        expect(ul.children[0].nodeType).toBe(Node.ELEMENT_NODE);
        expect(ul.children[0]).toBe(li1);
        expect(ul.children[1].nodeType).toBe(Node.ELEMENT_NODE);
        expect(ul.children[1]).toBe(li2);
      });
    });
    test("firstChild", () => {
      expect(ul.firstChild).toBe(ul.childNodes[0]);
    });
    test("lastChild", () => {
      expect(ul.lastChild).toBe(ul.childNodes[6]);
    });
    test("firstElementChild", () => {
      expect(ul.firstElementChild).toBe(ul.childNodes[3]);
    });
    test("lastElementChild", () => {
      expect(ul.lastElementChild).toBe(ul.childNodes[5]);
    });
  });
  describe("siblings", () => {
    describe("previousSibling", () => {
      test("returns null when no more siblings", () => {
        expect(comment.previousSibling.previousSibling).toBe(null);
      });
      test("can return a non-element node", () => {
        expect(li1.previousSibling.nodeType).toBe(Node.TEXT_NODE);
      });
      test("returns an element node", () => {
        expect(li2.previousSibling.previousSibling).toBe(li1);
      });
    });
    describe("nextSibling", () => {
      test("returns null when no more siblings", () => {
        expect(li2.nextSibling.nextSibling).toBe(null);
      });
      test("can return a non-element node", () => {
        expect(li1.nextSibling.nodeType).toBe(Node.TEXT_NODE);
      });
      test("returns an element node", () => {
        expect(li1.nextSibling.nextSibling).toBe(li2);
      });
    });
    describe("previousElementSibling", () => {
      test("returns null when no more siblings", () => {
        expect(li1.previousElementSibling).toBe(null);
      });
      test("returns an element node", () => {
        expect(li2.previousElementSibling).toBe(li1);
      });
    });
    describe("nextElementSibling", () => {
      test("returns null when no more siblings", () => {
        expect(li2.nextElementSibling).toBe(null);
      });
      test("returns an element node", () => {
        expect(li1.nextElementSibling).toBe(li2);
      });
    });
  });
});
