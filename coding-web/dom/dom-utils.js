const { JSDOM } = require("jsdom");

function minimalHtml5Factory({ title, body } = { title: "title", body: "" }) {
  return new JSDOM(`
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${title}</title>
  </head>
  <body>${body}</body>
</html>
`);
}

module.exports = {
  minimalHtml5Factory
};
