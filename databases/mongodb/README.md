# Database Cookbook

## File and folder structure

- For each topic, create a single file first; only use folder for topics that cannot be contained in a single file

```
mongodb/
  shell/
    configurations.md
    connections.md
    users.md
    databases.md
    collections.md
    crud.md
    aggregation.md
    map-reduce.md
  shell-scripts/
    seeding/
      seeding-1.js
    backup/
      s3.js
  data-modeling/
    schema.md
  drivers/
    java
    nodejs
  optimization-and-indexes
  data-types/
  security/
  operators/
```
