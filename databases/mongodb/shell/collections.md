# Collections

## Basic operations

### List collections

#### Commands

- `show collections`
- `db.getCollectionNames()`
- `db.getCollectionInfos()`
- `db.runCommand( { listCollections: 1 } )`

### Create collections

#### Commands

- `db.createCollection( "<collection>" )`
- `db.runCommand( { create: "<collection>" } )`

#### Notes

- `createCollection()`: Uses `create` command internally

### Drop collections

#### Commands

- `db.collection.drop()`
- `db.runCommand( { drop: "<collection>" } )`

#### Notes

- `drop()`: Uses `drop` command internally
