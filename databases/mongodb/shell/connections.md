# Connections

## Localhost connection examples

**adminUser@admin**

```sh
mongo -u "adminUser" -p "***" --authenticationDatabase "admin"
```

**testUser@test**

```sh
mongo -u "testUser" -p "***" --authenticationDatabase "test"
```
