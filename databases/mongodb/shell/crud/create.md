# Create


## Commands

- `db.collection.insertOne(document, writeConcern)`
- `db.collection.insertMany(arrayOfDocuments, options)`
- `db.collection.insert(documentOrArrayOfDocuments, options)`

## Usage

```js
db.collection.insertOne(
   <document>,
   {
      writeConcern: <document>
   }
)

db.collection.insertMany(
   [ <document 1> , <document 2>, ... ],
   {
      writeConcern: <document>,
      ordered: <boolean>
   }
)

db.collection.insert(
   <document or array of documents>,
   {
     writeConcern: <document>,
     ordered: <boolean>
   }
)
```

## Notes

- Create collection if it does not exist
- Add `_id` field and `ObjectId` if no valid `_id` in the document
- `writeConcern`: default to `w: 1`
- `ordered`:
    - Applies to `insertMany()` and `insert()`
    - Default to `true`
    - `ordered: true`: stops immediately during insertion when an error is encountered
    - `ordered: false`: skips insertion errors, e.g. skips documents have duplicate keys and inserts all valid documents

## Return value

- `insertOne()`:
    - Success: `{ "acknowledged" : <boolean>, "insertedIds" : [<ids>] }`
    - Has error: `WriteError({...})` or `writeConcernError({...})`
- `insertMany()`:
    - Success: `{ "acknowledged" : <boolean>, "insertedIds" : [<ids>] }`
    - Has error: `BulkWriteError( { <fields> } )`
- `insert()`:
    - Insert one document:
        - Success: `WriteResult( { <fields> } )`
        - Has error: `WriteResult( { <fields> } )`
    - Insert multiple documents
        - Success: `BulkWriteResult( { <fields> } )`
        - Has error: `BulkWriteError( { <fields> } )`
- Notes:
    - Field `acknowledged` is `true` if write concern is enabled, `false` otherwise

## Other methods

Methods can do insertion as part of their behavior or supported operations:

- As a behavior, when specified option `upsert: true`:
    - `db.collection.update()`
    - `db.collection.updateOne()`
    - `db.collection.updateMany()`
    - `db.collection.findAndModify()`
    - `db.collection.findOneAndUpdate()`
    - `db.collection.findOneAndReplace()`
- As a behavior:
    - `db.collection.save()`
- As a supported operation:
    - `db.collection.bulkWrite()`
