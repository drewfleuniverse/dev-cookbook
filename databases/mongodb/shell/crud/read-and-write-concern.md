# Read and Write Concern


## Read concern

### Introduction

- Also called read isolation

### Option

**Format**

```js
readConcern: { level: <"majority"|"local"|"linearizable"> }
```


## Write concern

### Introduction

- Also called write acknowledgement
- Uses:
    - Specifies how a write operation to a replica set will persist after the primary node is offline
    - Specifies how a write operation will return the operation status, i.e. acknowledgement versus no acknowledgement
- N.b. Since version 2.6, write concern is part of write operations, no need to call the `getLastError` command to get the write operation results

### Options

#### Format

```js
{ w: <value>, j: <boolean>, wtimeout: <number> }
```

#### `w` option

Specifies which nodes should acknowledge the operation and return the operation status

- `w: 1`:
    - The default write concern
    - Requests acknowledgement of standalone `mongod` or the primary in a replica set
- `w: 0`:
    - Requests no acknowledgement, i.e. fire and forget
    - May return errors messages
- `w: <larger-than-1>`: for replica sets only, e.g. number
- `w: "majority"`:
    - For replica sets only, requests acknowledgement of the majority of voting nodes
    - For read operations, use `majority` read concern to ensure data written during a partial node shutdown can be retrieved
- `w: <tag>`: for replica sets only, requests acknowledgement of the tagged nodes

#### `j` option

- `j: true`: requests acknowledgement of the specified nodes have written to the journal, i.e. on the disk


#### `wtimeout` option

Time limit in milliseconds, applicable only when `w` is not `0`
