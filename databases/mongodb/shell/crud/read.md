# Read


## Commands

- `db.collection.find(query, projection)`
- `db.collection.findOne(query, projection)`


## Return values

- `find()`: a cursor; `cursor.size() != 0` if the query matches, `cursor.size() == 0` otherwise
- `findOne()`: a document matches the query, `null` otherwise


## Query

### Query on documents

**All documents, using shell helper**

`db.foo.find()` or `db.foo.find({})`

- `db.foo.find()` automatically print up to 20 documents based on `shellBatchSize` value

**All documents, using cursor**

`var cursor = db.foo.find()` or `var cursor = db.foo.find({})`

- The above returns a curser points to the entire collection, i.e. `cursor.size() == db.foo.count()`

**Read concern**

`var cursor = ...`, then use `cursor.readConcern()`




### Query on embedded documents

### Query an array

### Query an array of embedded documents

### Query for null or missing fields


## Projection

- https://docs.mongodb.com/manual/tutorial/project-fields-from-query-results/


## Cursor

### Cursor methods

- `cursor.toArray()`: returns all documents in an array

### Read concern

Read `read-and-write-concern.md`


## Other methods

```js
db.collection.findAndModify(optionDocument) // only write on a single document
db.collection.findOneAndDelete(filter, options)
db.collection.findOneAndReplace(filter, replacement, options)
db.collection.findOneAndUpdate(filter, update, options)
```
