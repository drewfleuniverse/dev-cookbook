# Databases


## Basic operations

### List Databases

#### Commands

- `show dbs`
- `show databases`
- `db.adminCommand( { listDatabases: 1 } )`

#### Notes

- `show dbs` and `show databases`:
    - Use `listDatabases` internally
- `listDatabases`:
    - Outputs: `databases` array and `totalSize` number

### Create and use databases

#### Commands

- `use foobar`

#### Notes

- `use foobar` does not create database until the database has at least one collection
- A database will be removed if it becomes empty

### Delete databases

#### Commands

- `db.dropDatabase()`
- `db.runCommand( { dropDatabase: 1 } )`
- `db.runCommand( { dropAllUsersFromDatabase: 1 } )`

#### Notes

- `dropDatabase`: locks global write lock
