# Users

## Enabling access control

- By default, mongoDB doesn't enable access control, e.g. `show users` in `admin` database outputs nothing
- Enabling access control:
    - Procedure 1 - Add user then enable access control:
        - Create a user in `admin` database with `userAdminAnyDatabase` role
        - Enable access control
    - Procedure 2 - Enable access control then add user:
        - Also called *localhost exception*
        - Enable access control
        - Create user with or `userAdmin` or `userAdminAnyDatabase` role in `admin` database

## Examples

### Create user

**Create admin user**

```js
use admin
db.createUser(
  {
    user: "adminUser",
    pwd: "123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
```

**Create database user**

```js
use test
db.createUser(
  {
    user: "testUser",
    pwd: "123",
    roles: [ { role: "readWrite", db: "test" } ]
  }
)
```
