# Security - Users

## User role roles

- Database User Roles:
    - read
    - readWhite
- Database Administration Roles:
    - dbAdmin
    - dbOwner
    - userAdmin
- Cluster Administration Roles:
    - clusterAdmin*
    - clusterManager
    - clusterMonitor
    - hostManager
- Backup and Restoration Roles:
    - backup*
    - restore*
- All-database roles:
    - readAnyDatabase
    - readWriteAnyDatabase*
    - userAdminAnyDatabase*
    - dbAdminAnyDatabase*
- Superuser roles:
    - dbOwner@admin
    - userAdmin@admin
    - userAdminAnyDatabase
    - N.b. Superuser roles can assign any user any privilege on any database. However, it doesn't mean themselves have all privileges
- root:
    - Equivalent to the following roles combined: readWriteAnyDatabase, dbAdminAnyDatabase, userAdminAnyDatabase, clusterAdmin roles, restore, and backup
- Notes:
    - Many roles are able to be created in both of admin and non-admin databases and have different scopes on the roles's privileges
