import java.util.Stack;

class DemoFact {
  static long loopFact(int n) {
    long res = 1;
    if (n <= 1) return 1;
    while (n > 0) {
      assert res > 0 && res <= Long.MAX_VALUE : "Out of range!";
      res *= n;
      n--;
    }
    return res;
  }

  static long recursiveFact(int n) {
    assert n >= 0 && n <= 20 : "Out of range!";
    if (n <= 1) return 1;
    return n * recursiveFact(n-1);
  }

  static long stackFact(int n) {
    assert n >= 0 && n <= 20 : "Out of range!";
    Stack<Integer> S = new Stack<>();
    while (n > 1) S.push(n--);
    long result = 1;
    while (S.size() > 0) result = result * S.pop();
    return result;
  }


  // class type BigInteger
  public static void main(String[] args) {
    System.out.println(loopFact(0));
    System.out.println(loopFact(1));
    System.out.println(loopFact(2));
    System.out.println(loopFact(20));

    System.out.println(recursiveFact(0));
    System.out.println(recursiveFact(1));
    System.out.println(recursiveFact(2));
    System.out.println(recursiveFact(20));

    System.out.println(stackFact(0));
    System.out.println(stackFact(1));
    System.out.println(stackFact(2));
    System.out.println(stackFact(20));
  }
}
