class DemoTOH {
  public static void main(String[] args) {
    RecursiveTOH recursiveTOH = new RecursiveTOH();
    recursiveTOH.toh(3, "S", "G", "T");

    System.out.println();

    StackTOH stackTOH = new StackTOH();
    stackTOH.toh(3, "S", "G", "T");
  }
}
