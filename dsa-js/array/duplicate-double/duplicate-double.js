const dupTwoConcat = arr => [].concat(arr).concat(arr);
const dupTwoSliceConcat = arr => arr.slice().concat(arr);
const dupTwoSpread = arr => [...arr, ...arr];
const dupTwoFor = arr => {
  const len = arr.length;
  const result = [];
  for (let i = 0; i < len; i++) {
    result[i] = arr[i];
    result[i + len] = arr[i];
  }
  return result;
};

module.exports = {
  dupTwoConcat,
  dupTwoSliceConcat,
  dupTwoSpread,
  dupTwoFor
};
