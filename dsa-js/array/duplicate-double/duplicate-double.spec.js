const {
  dupTwoConcat,
  dupTwoSliceConcat,
  dupTwoSpread,
  dupTwoFor
} = require("./duplicate-double");

describe("duplicate-double", () => {
  [dupTwoConcat, dupTwoSliceConcat, dupTwoSpread, dupTwoFor].forEach(dupTwo => {
    test("duplicates an array of even length", () => {
      expect(dupTwo([1, 2])).toEqual([1, 2, 1, 2]);
    });
  });
});
