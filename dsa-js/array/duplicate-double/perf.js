const suite = new Benchmark.Suite;


/*
Chrome:
duplicateLoop x 24,370,202 ops/sec ±2.33% (51 runs sampled)
duplicateSliceConcat x 5,429,149 ops/sec ±3.37% (59 runs sampled)
duplicateConcatConcat x 2,971,906 ops/sec ±1.42% (60 runs sampled)
Fastest is duplicateLoop
 */


const prep = {
  'setup': () => {
    const duplicateLoop = function (arr) {
      const len = arr.length;
      const copy = new Array(len*2);

      for (let i = 0; i < len; i++) {
        const tmp = arr[i];
        copy[i] = tmp;
        copy[len+i] = tmp;
      }

      return copy
    };


    const duplicateSliceConcat = function (arr) {
      return arr.slice().concat(arr);
    };


    const duplicateConcatConcat = function (arr) {
      return [].concat(arr).concat(arr);
    };

    const arr = [0,1,2,3,4,5,6,7,8,9]
  },
  'teardown': () => {

  }
}

suite
.add('duplicateLoop', () => {
  duplicateLoop(arr);
}, prep)
.add('duplicateSliceConcat', () => {
  duplicateSliceConcat(arr);
}, prep)
.add('duplicateConcatConcat', () => {
  duplicateConcatConcat(arr)
}, prep)
.on('cycle', event => {
  console.log(String(event.target));
})
.on('complete', function () {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run({ 'async': true });
