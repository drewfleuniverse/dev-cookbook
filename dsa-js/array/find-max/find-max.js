const findMaxForEach = arr => {
  let max = -Infinity;
  arr.forEach(num => {
    if (max < num) {
      max = num;
    }
  });
  return max;
};

/**
 * Using `Math.max` caveats
 * Slower than using loop or reduce
 * Doesn't work with large input array
 */
const findMaxMathMaxSpread = arr => Math.max(...arr);
const findMaxMathMaxApply = arr => Math.max.apply(undefined, arr);

const findMaxReduceMathMax = arr =>
  arr.reduce((max, num) => Math.max(max, num), -Infinity);
const findMaxReduceTernary = arr =>
  arr.reduce((max, num) => (max < num ? num : max), -Infinity);

module.exports = {
  findMaxForEach,
  findMaxMathMaxSpread,
  findMaxMathMaxApply,
  findMaxReduceMathMax,
  findMaxReduceTernary
};
