const {
  findMaxForEach,
  findMaxMathMaxSpread,
  findMaxMathMaxApply,
  findMaxReduceMathMax,
  findMaxReduceTernary
} = require("./find-max");

describe("find-max", () => {
  Object.entries({
    findMaxForEach,
    findMaxMathMaxSpread,
    findMaxMathMaxApply,
    findMaxReduceMathMax,
    findMaxReduceTernary
  }).forEach(([name, findMax]) => {
    console.log(name, findMax);
    describe(`${name}`, () => {
      test("finds max", () => {
        expect(findMax([-1, 0, 1])).toBe(1);
      });
    });
  });
});
