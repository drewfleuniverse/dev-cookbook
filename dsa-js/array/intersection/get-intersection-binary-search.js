/*
Using sort and binary search
Perf: O(n * log(m))
Space: O(n)
 */

const binarySearch = (arr, val) => {
  let lo = 0;
  let hi = arr.length - 1;
  while (lo <= hi) {
    const mid = Math.floor((lo + hi) / 2);
    if (arr[mid] > val) {
      hi = mid - 1;
    } else if (arr[mid] < val) {
      lo = mid + 1;
    } else {
      return arr[mid];
    }
  }
  return null;
};

const getIntersection = (arr1, arr2) => {
  if (arr1.length === 0 || arr2.length === 0) {
    return [];
  }
  if (arr1.length > arr2.length) {
    let tmp = arr1;
    arr1 = arr2;
    arr2 = arr1;
  }
  const sortedArr1 = [...arr1].sort((a, b) => (a - b));
  const result = [];
  for (const el of arr2) {
    if (binarySearch(arr1, el) !== null) {
      result.push(el);
    }
  }
  return result;
};

describe('getIntersection()', () => {
  it('should return empty arrays', () => {
    assert(getIntersection([], []).toString() === '');
    assert(getIntersection([
      1, 2
    ], [3, 4]).toString() === '');
  });
  it('should return intersction 1', () => {
    const result = getIntersection([
      1, 2, 3, 4
    ], [2, 3]);
    assert(result.toString() === '2,3');
  });
  it('should return intersction 2', () => {
    const result = getIntersection([
      2, 3
    ], [1, 2, 3, 4]);
    assert(result.toString() === '2,3');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
