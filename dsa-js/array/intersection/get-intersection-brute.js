/*
Using brute force
Perf: O(n * m)
Space: O(1)
 */


const getIntersection = (arr1, arr2) => {
  if (arr1.length === 0 || arr2.length === 0) {
    return [];
  }
  const result = [];
  for (const el1 of arr1) {
    for (const el2 of arr2) {
      if (el1 === el2) {
        result.push(el1);
      }
    }
  }
  return result;
};


describe('getIntersection()', () => {
  it('should return empty arrays', () => {
    assert(getIntersection([], []).toString() === '');
    assert(getIntersection([
      1, 2
    ], [3, 4]).toString() === '');
  });
  it('should return intersction 1', () => {
    const result = getIntersection([
      1, 2, 3, 4
    ], [2, 3]);
    assert(result.toString() === '2,3');
  });
  it('should return intersction 2', () => {
    const result = getIntersection([
      2, 3
    ], [1, 2, 3, 4]);
    assert(result.toString() === '2,3');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
