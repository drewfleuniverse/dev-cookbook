const cl = i => console.log(i)
const js = i => JSON.stringify(i)


// time: O(l+m); space O(n+m)
const mergeSorted = (a, b) => (
  [].concat(a)
    .concat(b)
    .sort()
)


let a, b, r


a = [1,2,2]
b = [3,3,5,6]
r = mergeSorted(a, b)
cl(js(r) === '[1,2,2,3,3,5,6]')


a = [1,2,3]
b = []
r =mergeSorted(a, b)
cl(js(r) === '[1,2,3]')
