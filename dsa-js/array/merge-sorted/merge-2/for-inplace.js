const cl = i => console.log(i)
const js = i => JSON.stringify(i)


// time: O(l*m); space O(1)
const mergeSorted = (a, b) => {
  let m = a.length, n = b.length
  for (let i = n-1; i >= 0; i--) {
    let p = a[m-1]
    let j
    for (j = m-2; j >= 0 && a[j] > b[i]; j--)
      a[j+1] = a[j]

    if (j < m-2 || p > b[i]) {
      a[j+1] = b[i]
      b[i] = p
    }
  }
}


let a, b, r


a = [1,2,2]
b = [3,3,5,6]
mergeSorted(a, b)
cl(js(a.concat(b)) === '[1,2,2,3,3,5,6]')


a = [1,2,3]
b = []
mergeSorted(a, b)
cl(js(a.concat(b)) === '[1,2,3]')
