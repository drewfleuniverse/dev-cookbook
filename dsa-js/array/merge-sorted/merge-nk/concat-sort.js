const cl = i => console.log(i)
const js = i => JSON.stringify(i)


// time: O(n); space O(n)
const mergeSorted = function () {
  return [...arguments]
    .reduce((a, b) => a.concat(b))
    .sort()
}


let a, b, c, r


a = [1,2,2]
b = [3,3,5]
c = [2,8,9]
r = mergeSorted(a, b, c)
cl(js(r) === '[1,2,2,2,3,3,5,8,9]')


a = [1,2,3]
b = [3,3,5]
c = []
r = mergeSorted(a, b, c)
cl(js(r) === '[1,2,3,3,3,5]')
