## Array Reverse - `reverse` vs `for-assign` vs `for-pop` vs `for-swap` vs `for-xor-swap`


- Preference: `for-swap`
- Perf:
  - Chrome: for-swap` vs `for-xor-swap (-29%)` > `reverse (-46%)` > `for-assign (-88%)` > `for-pop (-89%)`
  - Firefox:  `for-swap` > `for-xor-swap (-15%)` > `reverse (-78%)` >
    `for-assign (-81%)` > `for-pop (-84%)`
- Jsperf: [](https://jsperf.com/reverse-array-assorted)
