const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const reverse = a => {
  const l = a.length
  let j, t
  for (let i = 0; i < l/2; i++) {
    j = l-i-1
    t = a[i]
    a[i] = a[j]
    a[j] = t
  }

  return a
}


const a = [1, 2, 3, 4, 5]
const r = reverse(a)

cl(r === a)
cl(js(a) === '[5,4,3,2,1]')
