/**
 * Leetcode 167. Two Sum II - Input array is sorted
 * Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.
 * The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.
 * Note:
 *   Your returned answers (both index1 and index2) are not zero-based.
 *   You may assume that each input would have exactly one solution and you may not use the same element twice.
 * Example:
 *   Input: numbers = [2,7,11,15], target = 9
 *   Output: [1,2]
 *   Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @param {number} begin
 * @param {number} end
 * @return {(number[]|undefined)}
 *
 * Time: O(n), visiting n elements up to n times.
 * Space: O(1), using tail recursion.
 */
function twoSumIncrementRecursion(
  nums,
  target,
  begin = 0,
  end = nums.length - 1
) {
  if (begin >= end) {
    return;
  }
  const sum = nums[begin] + nums[end];
  if (target > sum) {
    return twoSumIncrementRecursion(nums, target, begin + 1, end);
  }
  if (target < sum) {
    return twoSumIncrementRecursion(nums, target, begin, end - 1);
  }
  return [begin, end];
}

/**
 * @param {number[]} nums
 * @param {number} target
 * @param {number} begin
 * @param {number} end
 * @return {(number[]|undefined)}
 *
 * Time: O(n), visiting n elements up to n times.
 * Space: O(1), using tail recursion.
 */
function twoSumIncrementLoop(nums, target) {
  let begin = 0;
  let end = nums.length - 1;
  while (begin < end) {
    const sum = nums[begin] + nums[end];
    if (target > sum) {
      begin++;
    } else if (target < sum) {
      end--;
    } else {
      return [begin, end];
    }
  }
}

module.exports = {
  twoSumIncrementRecursion,
  twoSumIncrementLoop
};
