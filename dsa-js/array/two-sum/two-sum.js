/**
 * Leetcode 1. Two Sum
 *
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 *
 * Example:
 *   Given nums = [2, 7, 11, 15], target = 9,
 *   Because nums[0] + nums[1] = 2 + 7 = 9,
 *   return [0, 1].
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {(number[]|undefined)}
 *
 * Time: O(n^2)
 * Space: O(1)
 */
function twoSumBrute(nums, target) {
  const { length: len } = nums;
  for (let l = 0; l < len - 1; l++) {
    for (let r = l + 1; r < len; r++) {
      if (nums[l] + nums[r] === target) {
        return [l, r];
      }
    }
  }
}

function twoSumHashTable(nums, target) {
  const hashTable = nums.reduce((acc, num, index) => {
    acc[num] = index;
    return acc;
  }, {});
  let result;
  nums.forEach((numA, indexA) => {
    const numB = target - numA;
    const indexB = hashTable[numB];
    if (indexB !== undefined && indexB !== indexA) {
      result = [indexA, indexB];
    }
  });
  return result;
}

module.exports = {
  twoSumBrute,
  twoSumHashTable
};
