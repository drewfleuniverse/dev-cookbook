const { twoSumBrute, twoSumHashTable } = require("./two-sum");

describe("twoSum", () => {
  Object.entries({
    twoSumBrute,
    twoSumHashTable
  }).forEach(([name, func]) => {
    describe(`${name}`, () => {
      test("returns two sum when array has even length", () => {
        const sum = 5;
        const arr = [1, 2, 3, 4];
        const [i, j] = func(arr, sum);
        expect(arr[i] + arr[j]).toEqual(sum);
      });
      test("returns two sum when array has odd length", () => {
        const sum = 6;
        const arr = [1, 2, 3, 4, 5];
        const [i, j] = func(arr, sum);
        expect(arr[i] + arr[j]).toEqual(sum);
      });
      test("returns undefined when no matched sums - positive infinity", () => {
        const arr = [1, 2, 3, 4];
        expect(func(arr, Infinity)).toBeUndefined();
      });
      test("returns undefined when no matched sums - negative infinity", () => {
        const arr = [1, 2, 3, 4];
        expect(func(arr, -Infinity)).toBeUndefined();
      });
      test("returns undefined when array has only one element", () => {
        const arr = [1];
        expect(func(arr, 42)).toBeUndefined();
      });
      test("returns undefined when array is empty", () => {
        const arr = [];
        expect(func(arr, 42)).toBeUndefined();
      });
    });
  });
});
