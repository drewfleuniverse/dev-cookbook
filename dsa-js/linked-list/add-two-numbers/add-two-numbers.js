const { ListNode } = require("../../common");

/**
 * Add Two Numbers
 * Leetcode: https://leetcode.com/problems/add-two-numbers/
 */

/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
function addTwoNumbersRecursive(l1, l2) {
  const result = new ListNode(0);
  addTwoNumbersHelper(l1, l2, result);
  return result;
}

function addTwoNumbersHelper(l1, l2, result) {
  if (result === null) {
    return;
  }
  const v1 = l1 !== null ? l1.val : 0;
  const v2 = l2 !== null ? l2.val : 0;
  const nextL1 = l1 !== null ? l1.next : null;
  const nextL2 = l2 !== null ? l2.next : null;
  result.val += v1 + v2;
  if (result.val > 9) {
    result.val = result.val % 10;
    result.next = new ListNode(1);
  } else if (nextL1 !== null || nextL2 !== null) {
    result.next = new ListNode(0);
  }
  addTwoNumbersHelper(nextL1, nextL2, result.next);
}

function addTwoNumbersLoop(l1, l2) {
  const result = new ListNode(0);
  let curResult = result;
  let curL1 = l1;
  let curL2 = l2;

  while (curL1 !== null || curL2 !== null) {
    const v1 = curL1 !== null ? curL1.val : 0;
    const v2 = curL2 !== null ? curL2.val : 0;
    const nextL1 = curL1 !== null ? curL1.next : null;
    const nextL2 = curL2 !== null ? curL2.next : null;
    curResult.val += v1 + v2;
    if (curResult.val > 9) {
      curResult.val = curResult.val % 10;
      curResult.next = new ListNode(1);
    } else if (nextL1 !== null || nextL2 !== null) {
      curResult.next = new ListNode(0);
    }
    curResult = curResult.next;
    curL1 = nextL1;
    curL2 = nextL2;
  }
  return result;
}

module.exports = {
  addTwoNumbersRecursive,
  addTwoNumbersLoop
};
