const { createList } = require("../../utils");
const { ListNode } = require("../../common");
const {
  addTwoNumbersRecursive,
  addTwoNumbersLoop
} = require("./add-two-numbers");

describe("addTwoNumbers", () => {
  describe("utils", () => {
    test("1", () => {
      const l = new ListNode(2);
      expect(l.val).toBe(2);
    });
    test("2", () => {
      const l = createList([2, 4, 3]);
      expect(l.val).toBe(2);
      expect(l.next.val).toBe(4);
      expect(l.next.next.val).toBe(3);
    });
  });
  describe("addTwoNumbersRecursive", () => {
    test("should add two lists", () => {
      const l1 = createList([2, 4, 3]);
      const l2 = createList([5, 6, 4]);
      const l = addTwoNumbersRecursive(l1, l2);
      expect(l.val).toBe(7);
      expect(l.next.val).toBe(0);
      expect(l.next.next.val).toBe(8);
    });
  });
  describe("addTwoNumbersLoop", () => {
    test("should add two lists", () => {
      const l1 = createList([2, 4, 3]);
      const l2 = createList([5, 6, 4]);
      const l = addTwoNumbersLoop(l1, l2);
      expect(l.val).toBe(7);
      expect(l.next.val).toBe(0);
      expect(l.next.next.val).toBe(8);
    });
  });
});
