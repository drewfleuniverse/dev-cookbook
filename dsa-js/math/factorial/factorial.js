/**
 * Factorial:
 * 0 -> 1
 * 1 -> 1
 * 2 -> 2 * 1 = 1
 * 3 -> 3 * 2 * 1 = 6
 * 4 -> 4 * 3 * 2 * 1 = 24
 * 5 -> 5 * 4 * 3 * 2 * 1 = 120
 */

function factWhile(num) {
  let n = num;
  let result = 1;
  while (n > 1) {
    result *= n--;
  }
  return result;
}

function factWhileMaxSafe(num) {
  let n = num;
  let result = 1;
  let prev;
  while (n > 1) {
    prev = result;
    result *= n--;
    if (result > Number.MAX_SAFE_INTEGER) {
      throw new Error(
        `Exceeds maximum safe integer, the last available value was ${prev} at ${n}th factorial.`
      );
    }
  }
  return result;
}

function factStack(num) {
  if (num === 1) {
    return 1;
  }
  const stack = [];
  let n = num;
  while (n >= 1) {
    stack.push(n--);
  }
  let result = 1;
  while (stack.length) {
    result *= stack.pop();
  }
  return result;
}

function factRecursive(num, result = 1) {
  if (num <= 1) {
    return result;
  }
  return factRecursive(num - 1, result * num);
}

function factRecursiveV2(num) {
  if (num <= 1) {
    return 1;
  }
  return num * factRecursive(num - 1);
}

const externalMem = {};
const factRecursiveMemoize = num => {
  const mem = externalMem;
  if (mem[num] !== undefined) {
    return mem[num];
  }
  const result = factRecursive(num);
  mem[num] = result;
  return result;
};

module.exports = {
  factWhile,
  factStack,
  factRecursive,
  factRecursiveV2,
  factRecursiveMemoize,
  externalMem,
  factWhileMaxSafe
};
