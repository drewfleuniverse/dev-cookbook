const {
  factWhile,
  factStack,
  factRecursive,
  factRecursiveV2,
  factRecursiveMemoize,
  externalMem,
  factWhileMaxSafe
} = require("./factorial");

describe("factorial", () => {
  Object.entries({
    factWhile,
    factStack,
    factRecursive,
    factRecursiveV2,
    factRecursiveMemoize,
    factWhileMaxSafe
  }).forEach(([name, func]) => {
    describe(`${name}`, () => {
      test("calculates correct factorial", () => {
        expect(func(0)).toBe(1);
        expect(func(1)).toBe(1);
        expect(func(2)).toBe(2);
        expect(func(3)).toBe(6);
        expect(func(4)).toBe(24);
        expect(func(5)).toBe(120);
        expect(func(10)).toBe(3628800);
      });
    });
  });
  describe(`factRecursiveMemoize`, () => {
    test(`should memoize previous results`, () => {
      expect(externalMem).toEqual({
        0: 1,
        1: 1,
        2: 2,
        3: 6,
        4: 24,
        5: 120,
        10: 3628800
      });
    });
  });
  describe(`factWhileMaxSafe`, () => {
    test(`throws when too much`, () => {
      expect(() => factWhileMaxSafe(1000)).toThrow(
        `Exceeds maximum safe integer, the last available value was 990034950024000 at 1000th factorial.`
      );
    });
  });
});
