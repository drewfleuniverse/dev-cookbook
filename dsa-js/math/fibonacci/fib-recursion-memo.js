/*
Using recursion
Perf: O(n^2)
- T(n) = cn -> O(n^2)
Space: O(n)
*/

const memo = [];
const fib = num => {
  if (num <= 1) {
    return 1;
  }
  if (memo[num] !== undefined) {
    return memo[num];
  }
  memo[num] = fib(num - 1) + fib(num - 2);
  return memo[num];
};


describe('fib()', () => {
  it('should return 1', () => {
    assert(fib(0) === 1);
    assert(fib(1) === 1);
  });
  it('should return 2', () => {
    assert(fib(2) === 2);
  });
  it('should return 3', () => {
    assert(fib(3) === 3);
  });
  it('should return 5', () => {
    assert(fib(4) === 5);
  });
  it('should return 55', () => {
    assert(fib(9) === 55);
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
