const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const countPrime = n => {
  if (n <= 1)
    return false

  const a = new Array(n+1).fill(true)
  const s = Math.floor(Math.sqrt(n))
  for (let i = 2; i <= s; i++) {
    if (a[i]) {
      for (let j = i*i; j <= n; j += i)
        a[j] = false
    }
  }

  let c = 0
  for (let i = 2; i <= n; i++) {
    if (a[i])
      c++
  }

  return c
}

cl(countPrime(100) === 25)
