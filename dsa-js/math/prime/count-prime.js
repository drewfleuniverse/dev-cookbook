const cl = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const isPrime = n => {
  if (n < 2) return false

  const s = Math.floor(Math.sqrt(n))
  for (let d = 2; d <= s; d++) {
    if (n%d === 0)
      return false
  }

  return true
}

const countPrime = n => {
  let c = 0

  for (let i = 2; i <= n; i++) {
    if (isPrime(i))
      c++
  }

  return c
}


cl(countPrime(100) === 25)
