const cl = s => console.log(s)
const js = i => JSON.stringify(i)


const getPrimeFactors = n => {
  const a = []
  for (let d = 2; n > 2; d++) {
    if (n%d === 0) {
      a.push(d)
      n /= d
      d--
    }
  }

  return a
}


cl(js(getPrimeFactors(100)) === '[2,2,5,5]')
