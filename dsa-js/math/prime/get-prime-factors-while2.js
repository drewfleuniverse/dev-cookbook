const cl = s => console.log(s)
const js = i => JSON.stringify(i)


const getPrimeFactors = n => {
  const a = []
  let d = 2
  while (n > 2) {
    if (n%d === 0) {
      a.push(d)
      n /= d
      d--
    }
    d++
  }

  return a
}


cl(js(getPrimeFactors(100)) === '[2,2,5,5]')
