const cl = s => console.log(s)
const jsf = i => JSON.stringify(i, null, '  ')


const isPrime = n => {
  if (n < 2) return false

  const s = Math.floor(Math.sqrt(n))
  let d = 2
  while (d <= s) {
    if (n%d === 0) return false
    else d++
  }

  return true
}


const r = []
for (let i = 1; i <= 100; i++) {
  if (isPrime(i))
    r.push(i)
}
cl(jsf(r) === `[
  2,
  3,
  5,
  7,
  11,
  13,
  17,
  19,
  23,
  29,
  31,
  37,
  41,
  43,
  47,
  53,
  59,
  61,
  67,
  71,
  73,
  79,
  83,
  89,
  97
]`)
