## `isPrime`

- sieve: O(log(log(n)))
- loop: O(log(n))


## `findPrime` and `countPrime`

- sieve: O(n*log(log(n)))
- loop: O(n*log(n))
