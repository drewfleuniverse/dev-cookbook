const { reverse } = require("./revert-int");

const MAX_SAFE_INTEGER = Math.pow(2, 31) - 1;
const MIN_SAFE_INTEGER = -Math.pow(2, 31);

describe("reverse", () => {
  beforeAll(() => {
    expect(MAX_SAFE_INTEGER.toString()).toBe("2147483647");
    expect(MIN_SAFE_INTEGER.toString()).toBe("-2147483648");
    expect([..."2147483647"].reverse().join("")).toBe("7463847412");
    expect(`-${[..."2147483648"].reverse().join("")}`).toBe("-8463847412");
  });
  test("should return correct positive number", () => {
    expect(reverse(123)).toBe(321);
  });
  test("should return correct negative number", () => {
    expect(reverse(-123)).toBe(-321);
  });
  test("should return correct number when input is ending in zeros", () => {
    expect(reverse(12000)).toBe(21);
  });
  test("should return zero when input is larger than 2^31 - 1", () => {
    expect(reverse(MAX_SAFE_INTEGER + 1)).toBe(0);
  });
  test("should return zero when input is smaller than -2^31", () => {
    expect(reverse(MIN_SAFE_INTEGER - 1)).toBe(0);
  });
  test("should return zero when reverted result is larger than 2^31 - 1", () => {
    expect(reverse(8463847412)).toBe(0);
  });
  test("should return zero when reverted result is smaller than -2^31", () => {
    expect(reverse(-9463847412)).toBe(0);
  });
});
