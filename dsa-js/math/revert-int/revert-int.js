/**
 * 7. Reverse Integer
 * Given a 32-bit signed integer, reverse digits of an integer.
 * Example 1:
 *   Input: 123
 *   Output: 321
 * Example 2:
 *   Input: -123
 *   Output: -321
 * Example 3:
 *   Input: 120
 *   Output: 21
 * Note:
 * Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−2^31,  2^31 − 1]. For the purpose of  * this problem, assume that your function returns 0 when the reversed integer overflows.
 *
 * @param {number} num
 * @return {number}
 */
function reverse(num) {
  const MAX_SAFE_INTEGER = Math.pow(2, 31) - 1;
  const MIN_SAFE_INTEGER = -Math.pow(2, 31);
  if (num > MAX_SAFE_INTEGER || num < MIN_SAFE_INTEGER) {
    return 0;
  }
  const MAX_TENTH = Math.floor(MAX_SAFE_INTEGER / 10);
  const MIN_TENTH = Math.ceil(MIN_SAFE_INTEGER / 10);
  const MAX_LAST_DIGIT = MAX_SAFE_INTEGER % 10;
  const MIN_LAST_DIGIT = MIN_SAFE_INTEGER % 10;
  let result = 0;
  let rest = num;
  while (rest !== 0) {
    const digit = rest % 10;
    if (
      result > MAX_TENTH ||
      (result === MAX_TENTH && digit > MAX_LAST_DIGIT)
    ) {
      return 0;
    } else if (
      result < MIN_TENTH ||
      (result === MIN_TENTH && digit < MIN_LAST_DIGIT)
    ) {
      return 0;
    }
    rest = rest > 0 ? Math.floor(rest / 10) : Math.ceil(rest / 10);
    result = result * 10 + digit;
  }
  return result;
}

module.exports = {
  reverse
};
