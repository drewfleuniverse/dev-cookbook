/*
Leetcode 46 Permutations
Using recursion with cloning input
Perf: O(n * n!)
Space: O(n)?
 */


const helper = (result, rest, perm) => {
  if (rest.length === 0) {
    result.push(perm);
    return;
  }
  for (let i = 0; i < rest.length; i++) {
    const newRest = [...rest];
    const temp = newRest.splice(i, 1);
    const newPerm = [...perm, ...temp];
    helper(result, newRest, newPerm);
  }
};


const permute = arr => {
  const result = [];
  helper(result, arr, []);
  return result;
};



describe('permute()', () => {
  it('should return empty array', () => {
    const result = permute([]);
    assert(result.toString() === '');
  });
  it('should return unique permutations', () => {
    const result = permute([1, 2, 3])
    assert(JSON.stringify(result) === '[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]');
  });
  it('should return repeated permutations', () => {
    const result = permute([1, 1, 2])
    assert(JSON.stringify(result) === '[[1,1,2],[1,2,1],[1,1,2],[1,2,1],[2,1,1],[2,1,1]]');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
