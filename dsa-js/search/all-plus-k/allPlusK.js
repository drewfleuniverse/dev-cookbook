const f = (a, k) => {
  const length = a.length;
  const result = [];
  let curr = 0;
  let left = curr - 1;
  let right = curr + 1;

  while (curr < length) {
    let plusK = a[curr] + k;

    if (
      left >= 0 &&
      a[left] === plusK
    ) {
      result.push(a[curr]);
      curr++;
      left = curr-1;
      right = curr+1;
    } else if (
      right < length &&
      a[right] === plusK
    ) {
      result.push(a[curr]);
      curr++;
      left = curr-1;
      right = curr+1;
    } else if (left < 0 && right > length-1) {
      curr++;
      left = curr-1;
      right = curr+1;
    } else {
      left--;
      right++;
    }
  }

  return result;
};

const a = [0,1, 3, 2, 4, 5];
console.log(f(a, 2).toString() === '0,1,3,2');
console.log(f([1], 2).toString() === '');
