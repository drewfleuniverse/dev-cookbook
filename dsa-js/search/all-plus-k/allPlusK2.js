const allPlusK = function (array, k) {
  const result = [];

  const set = new Set(array);
  for (const val of array) {
    const plusK = val + k;
    if (set.has(plusK)) {
      result.push(val);
    }
  }

  return result;
};

const a = [0,1, 3, 2, 4, 5];
console.log(allPlusK(a, 2).toString() === '0,1,3,2');
console.log(allPlusK([1], 2).toString() === '');
