'use strict'
const cl = i => console.log(i)


function findEvenIndex(arr) {
  for (let i = 0; i < arr.length; i++) {
    const l = arr.slice(0, i).reduce(
      (a, b) => (a + b),
      0
    );
    const r = arr.slice(i + 1).reduce(
      (a, b) => (a + b),
      0
    );

    if (l === r) {
      return i;
    }
  }

  return -1;
}


let a


a = [1,2,3,4,3,2,1]
cl(findEvenIndex(a) === 3)
a = [1,100,50,-51,1,1]
cl(findEvenIndex(a) === 1)
a = [1,2,3,4,5,6]
cl(findEvenIndex(a) === -1)
a = [20,10,30,10,10,15,35]
cl(findEvenIndex(a) === 3)
