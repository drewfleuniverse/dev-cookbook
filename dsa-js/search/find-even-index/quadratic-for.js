const cl = i => console.log(i)


function findEvenIndex(arr) {
  const len = arr.length
  let m = 0

  while(m < len) {
    let l = 0, r = 0

    for (let i = 0; i < m; i++)
      l += arr[i]
    for (let j = len-1; j > m; j--)
      r += arr[j]

    if (l === r) return m

    m++
  }

  return -1
}


let a


a = [1,2,3,4,3,2,1]
cl(findEvenIndex(a) === 3)
a = [1,100,50,-51,1,1]
cl(findEvenIndex(a) === 1)
a = [1,2,3,4,5,6]
cl(findEvenIndex(a) === -1)
a = [20,10,30,10,10,15,35]
cl(findEvenIndex(a) === 3)
