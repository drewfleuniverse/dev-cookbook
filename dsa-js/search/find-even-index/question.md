Input:

An integer array of length 0 < arr < 1000. The numbers in the array can be any integer positive or negative.


Output:

The lowest index N where the side to the left of N is equal to the side to the right of N. If you do not find an index that fits these rules, then you will return -1.
