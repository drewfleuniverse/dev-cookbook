const { swap } = require("../../../utils");
const { MaxHeap } = require("../../../tree/max-heap/max-heap");

class MaxHeapSort extends MaxHeap {
  /**
   * @param {number[]} arr
   */
  constructor(arr) {
    super(arr);
  }
  sort() {
    const times = this.size;
    for (let i = 0; i < times; i++) {
      this.removeMax();
    }
  }
  removeMax() {
    swap(this.tree, 0, --this.size);
    if (this.size !== 0) {
      this.siftDown(0);
    }
  }
}

module.exports = { MaxHeapSort };
