const { MaxHeapSort } = require("./max-heap-sort");

const arr = [5, 2, 1, 6, 7, 3, 4, 9, 8];
const maxHeapSort = new MaxHeapSort(arr);

describe("MaxHeapSort", () => {
  test("sort", () => {
    maxHeapSort.sort();
    expect(arr).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });
});
