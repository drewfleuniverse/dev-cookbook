const getParenDepth = str => {
  let curDepth = 0;
  let maxDepth = 0;
  for (const ch of str) {
    if (ch === '(') {
      curDepth += 1;
      if (curDepth > maxDepth) {
        maxDepth = curDepth;
      }
    } else if (ch === ')') {
      curDepth -= 1;
      if (curDepth === -1) {
        return -1;
      }
    }
  }
  if (curDepth !== 0) {
    return -1;
  }
  return maxDepth;
};


describe('getParenDepth()', () => {
  it('should return correct depth', () => {
    assert(getParenDepth('') === 0);
    assert(getParenDepth('()') === 1);
    assert(getParenDepth('()()') === 1);
    assert(getParenDepth('()(())') === 2);
  });
  it('should return -1', () => {
    assert(getParenDepth(')(') === -1);
    assert(getParenDepth('()(') === -1);
    assert(getParenDepth(')()') === -1);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
