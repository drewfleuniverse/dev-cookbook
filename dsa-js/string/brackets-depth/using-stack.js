const getParenDepth = str => {
  const parenStack = [];
  let maxDepth = 0;
  for (const char of str) {
    if (char === '(') {
      parenStack.push(char);
      if (parenStack.length > maxDepth) {
        maxDepth = parenStack.length;
      }
    } else if (char === ')') {
      if (parenStack.length === 0) {
        return -1;
      }
      parenStack.pop();
    }
  }
  if (parenStack.length !== 0) {
    return -1;
  }
  return maxDepth;
};


describe('getParenDepth()', () => {
  it('should return correct depth', () => {
    assert(getParenDepth('') === 0);
    assert(getParenDepth('()') === 1);
    assert(getParenDepth('()()') === 1);
    assert(getParenDepth('()(())') === 2);
  });
  it('should return -1', () => {
    assert(getParenDepth(')(') === -1);
    assert(getParenDepth('()(') === -1);
    assert(getParenDepth(')()') === -1);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
