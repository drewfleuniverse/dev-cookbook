const isAnargram = (str1, str2) => {
  if (str1.length !== str2.length) {
    return false;
  }
  const aCode = 'a'.charCodeAt();
  const zCode = 'z'.charCodeAt();
  const count = new Array(zCode - aCode + 1);
  count.fill(0);
  for (let i = 0; i < str1.length; i++) {
    count[str1[i].charCodeAt() - aCode]++;
    count[str2[i].charCodeAt() - aCode]--;
  }
  for (const el of count) {
    if (el !== 0) {
      return false;
    }
  }
  return true;
};


describe('isAnargram()', () => {
  it('should return true', () => {
    const str1 = 'abbc';
    const str2 = 'bcab';
    assert(isAnargram(str1, str2));
  });
  it('should return false', () => {
    const str1 = 'abcd';
    const str2 = 'cabb';
    assert(isAnargram(str1, str2) === false);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
