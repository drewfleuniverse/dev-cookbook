const swap = (arr, i, j) => {
  const tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
};


const partition = (arr, lo, hi) => {
  const pivot = arr[hi];
  let mid = lo;
  for (let i = lo; i < hi; i++) {
    if (arr[i] < pivot) {
      swap(arr, i, mid);
      mid++;
    }
  }
  swap(arr, hi, mid);
  return mid;
};


const sort = (arr, begin=0, end=arr.length-1) => {
  if (begin >= end) {
    return;
  }
  const mid = partition(arr, begin, end);
  sort(arr, begin, mid - 1);
  sort(arr, mid + 1, end);
};


const isAnargram = (str1, str2) => {
  if (str1.length !== str2.length) {
    return false;
  }
  const charArr1 = str1.split('');
  const charArr2 = str2.split('');
  sort(charArr1);
  sort(charArr2);
  for (let i = 0; i < str1.length; i++) {
    if (charArr1[i] !== charArr2[i]) {
      return false;
    }
  }
  return true;
};


describe('sort()', () => {
  it('should do nothing on an empty array', () => {
    const arr = [];
    sort(arr);
    assert(arr.toString() === '');
  });
  it('should do nothing on an array with one element', () => {
    const arr = [1];
    sort(arr);
    assert(arr.toString() === '1');
  });
  it('should sort', () => {
    const arr = [3,2,1];
    sort(arr);
    assert(arr.toString() === '1,2,3');
  });
});


describe('isAnargram()', () => {
  it('should return true', () => {
    const str1 = 'abbc';
    const str2 = 'bcab';
    assert(isAnargram(str1, str2));
  });
  it('should return false', () => {
    const str1 = 'abcd';
    const str2 = 'cabb';
    assert(isAnargram(str1, str2) === false);
  });
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
