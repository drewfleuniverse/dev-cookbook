const insertSpaces = str => {
  const lastIndex = str.length - 1;
  let lo = 0;
  let hi = lastIndex - 1;

  let left = '';
  let right = '';
  while (lo < hi) {
    left = left + str[lo] + ' ';
    right = str[hi] + ' ' + right;
    lo++;
    hi--;
  }

  if (lo === hi) {
    left += str[lo] + ' ';
  }

  return left + right + str[lastIndex];
};

assert(insertSpaces('abc') === 'a b c');
assert(insertSpaces('abcd') === 'a b c d');



/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
