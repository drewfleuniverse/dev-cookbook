/**
 * 5. Longest Palindromic Substring
 * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
 *
 * Example 1:
 *   Input: "babad"
 *   Output: "bab"
 * Note: "aba" is also a valid answer.
 *
 * Example 2:
 *   Input: "cbbd"
 *   Output: "bb"
 */

/**
 * @param {string} str
 */
function longestPalindromicSubstringCenterExpansion(str) {
  if (str.length === 0) {
    return str;
  }
  let begin = 0;
  let end = 0;
  for (let i = 0; i < str.length; i++) {
    const singleCenterPalLen = expandAroundCenter(str, i, i);
    const doubleCenterPalLen = expandAroundCenter(str, i, i + 1);
    const len = Math.max(singleCenterPalLen, doubleCenterPalLen);
    if (len > end - begin) {
      // baa, 012, len = 2 , i = 1, b = 
      //       ^
      // abba, 0123, len = 4, i = 1, 
      //        ^
      begin = 
    }
  }
  return str.substring(begin, end + 1);
}

/**
 *
 * @param {string} str
 * @param {number} left
 * @param {number} right
 */
function expandAroundCenter(str, left, right) {
  let l = left;
  let r = right;
  while (l > 0 && r < str.length && str[l] === str[r]) {
    l--;
    r++;
  }
  // E.g. 'a' has length 1, len = r - l - 1 = 2 - (-1) + 1 = 1
  return r - l - 1;
}

module.exports = {
  longestPalindromicSubstringCenterExpansion
};
