const funcs = require("./longest-substring");

describe("Longest Palindromic Substring", () => {
  Object.entries(funcs).forEach(([name, func]) => {
    describe(name, () => {
      test(`returns correct values`, () => {
        expect(func(``)).toBe(``);
        expect(func(`a`)).toBe(`a`);
        expect(func(`aa`)).toBe(`aa`);
        expect(func(`ab`)).toBe(`a`);
        expect(func(`aaa`)).toBe(`aaa`);
        expect(func(`aba`)).toBe(`aba`);
        expect(func(`aaaa`)).toBe(`aaaa`);
        expect(func(`abbc`)).toBe(`bb`);
        expect(func(`xxxaba`)).toBe(`xxx`);
        expect(func(`abaxxx`)).toBe(`aba`);
        expect(func(`abaabcba`)).toBe(`abcba`);
      });
    });
  });
});
