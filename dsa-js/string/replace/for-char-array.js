const cl = i => console.log(i)


let s, r


const replace = (s, r) => {
  const sl = s.length
  const rl = r.length
  let n = 0
  for (let i = 0; i < sl; i++) {
    if (s[i] === ' ') n++
  }

  const a = new Array(sl + n*rl)
  let i = 0
  for (let j = 0; j < sl; j++) {
    if (s[j] === ' ') {
      for (let k = 0; k < rl; k++)
        a[i++] = r[k]
    } else {
      a[i++] = s[j]
    }
  }

  return a.join('')
}


s = 'foo bar 42'
r = replace(s, '@@')

cl(r === 'foo@@bar@@42')
