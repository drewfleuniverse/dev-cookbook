const cl = i => console.log(i)


let s, r


const replace = (s, r) => {
  let t = ''
  for (let i = 0; i < s.length; i++) {
    if (s[i] === ' ') t += r
    else t += s[i]
  }

  return t
}


s = 'foo bar 42'
r = replace(s, '@@')

cl(r === 'foo@@bar@@42')
