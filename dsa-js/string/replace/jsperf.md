# Replace String Whitespace


Preference: `replaceConcat`


Chrome:

- `replaceConcat`
- `replace (-34%)`
- `replaceCharArray (-73%)`


Firefox:

- `replaceConcat`
- `replace (-16%)`
- `replaceCharArray (-59%)`
