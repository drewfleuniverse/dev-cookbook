const cl = i => console.log(i)


const reverse = s => {
  const l = s.length
  const a = new Array(l)
  let j
  for (let i = 0; i < l; i++)
    a[l-i-1] = s[i]
  
  return a.join('')
}


let s


s = 'raboof'
cl(reverse(s) === 'foobar')

s = 'rab'
cl(reverse(s) === 'bar')

s = ''
cl(reverse(s) === '')