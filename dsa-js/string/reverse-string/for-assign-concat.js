const cl = i => console.log(i)


const reverse = s => {
  let r = ''
  for (let i = s.length-1; i >= 0; i--)
    r += s[i]

  return r
}


let s


s = 'raboof'
cl(reverse(s) === 'foobar')

s = 'rab'
cl(reverse(s) === 'bar')

s = ''
cl(reverse(s) === '')