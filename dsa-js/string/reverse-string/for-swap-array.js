const cl = i => console.log(i)


const reverse = s => {
  const l = s.length
  const m = Math.floor(l/2)
  const a = new Array(l)
  
  let j
  for (let i = 0; i < m; i++) {
    j = l-i-1
    a[j] = s[i]
    a[i] = s[j]
  }
  
  if (l%2 === 1)
    a[m] = s[m]

  return a.join('')
}


let s


s = 'raboof'
cl(reverse(s) === 'foobar')

s = 'rab'
cl(reverse(s) === 'bar')

s = ''
cl(reverse(s) === '')