## String Reverse

Functions:

- `reverse1`: `split-reverse-join`
- `reverse2`: `for-swap-slice`
- `reverse3`: `for-swap-array`
- `reverse4`: `for-assign-concat`
- `reverse5`: `for-assign-array`


Preference:

- `for-assign-concat`
- `for-swap-array`


Perf:

- Chrome:
  - `for-assign-concat`
  - `for-swap-array (-52%)`
  - `for-assign-array (-53%)`
  - `split-reverse-join (-63%)`
  - `for-swap-slice (-87%)`
- Firefox:
  - `for-swap-array`
  - `for-assign-array (-4%)`
  - `for-assign-concat (-5%)`
  - `split-reverse-join (-55%)`
  - `for-swap-slice (-93%)`
- [link](https://jsperf.com/reverse-string-assorted/1)
