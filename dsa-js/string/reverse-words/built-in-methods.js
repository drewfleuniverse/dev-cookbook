const reverseWords = str => str.split(' ').reverse().join(' ');


describe('reverseWords()', () => {
  it('should return empty string', () => {
    assert(reverseWords('') === '');
  })
  it('should not reverse string without space', () => {
    assert(reverseWords('abc') === 'abc');
  })
  it('should reverse a sentence', () => {
    assert(reverseWords('a bc def') === 'def bc a');
  })
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
