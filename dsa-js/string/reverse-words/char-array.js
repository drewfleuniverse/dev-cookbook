const reverse = (arr, begin=0, end=arr.length) => {
  let i = begin;
  let j = end - 1;
  while (i < j) {
    const tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
    i++;
    j--;
  }
  return arr;
};


const reverseWords = str => {
  const arr = reverse(str.split(''));
  let begin = 0;
  let end = 1;
  while (end < str.length - 1){
    if (arr[end] === ' ') {
      reverse(arr, begin, end);
      begin = end + 1;
    }
    end++;
  }
  if (begin === 0) {
    return reverse(arr).join('');
  }
  return arr.join('');
};


describe('reverse()', () => {
  it('should return empty string', () => {
    const arr = reverse(''.split(''));
    assert(arr.join('') === '');
  })
  it('should reverse string with odd length', () => {
    const arr = reverse('abc'.split(''));
    assert(arr.join('') === 'cba');
  })
  it('should reverse string with even length', () => {
    const arr = reverse('abcd'.split(''));
    assert(arr.join('') === 'dcba');
  })
});

describe('reverseWords()', () => {
  it('should return empty string', () => {
    assert(reverseWords('') === '');
  })
  it('should not reverse string without space', () => {
    assert(reverseWords('abc') === 'abc');
  })
  it('should reverse a sentence', () => {
    assert(reverseWords('a bc def') === 'def bc a');
  })
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
