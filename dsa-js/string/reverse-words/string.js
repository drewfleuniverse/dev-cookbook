const reverse = (str, begin=0, end=str.length) => {
  let i = begin;
  let j = end-1;
  while (i < j) {
    str = str.slice(0, i) +
      str[j] +
      str.slice(i+1, j) +
      str[i] +
      str.slice(j+1);
    i++;
    j--;
  }
  return str;
};


const reverseWords = str => {
  str = reverse(str);
  let begin = 0;
  let end = 1;
  while (end < str.length - 1){
    if (str[end] === ' ') {
      str = reverse(str, begin, end);
      begin = end + 1;
    }
    end++;
  }
  if (begin === 0) {
    return reverse(str);
  }
  return str;
};


describe('reverse()', () => {
  it('should return empty string', () => {
    assert(reverse('') === '');
  })
  it('should reverse string with odd length', () => {
    assert(reverse('abc') === 'cba');
  })
  it('should reverse string with even length', () => {
    assert(reverse('abcd') === 'dcba');
  })
});

describe('reverseWords()', () => {
  it('should return empty string', () => {
    assert(reverseWords('') === '');
  })
  it('should not reverse string without space', () => {
    assert(reverseWords('abc') === 'abc');
  })
  it('should reverse a sentence', () => {
    assert(reverseWords('a bc def') === 'def bc a');
  })
});


/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
