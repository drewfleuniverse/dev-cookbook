const lg = i => console.log(i)
const js = i => JSON.stringify(i)



const isSubstr = (a, b) =>
  a.length > b.length
    ? a.includes(b)
    : b.includes(a)

const isRotation = (a, b) =>
  a.length === b.length &&
  isSubstr(a+a, b)
  
  
let s, t

s = 'abcdef'
t = 'bcdefa'

lg(isRotation(s, t))
lg(isRotation(t, s))

