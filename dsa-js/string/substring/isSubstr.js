const lg = i => console.log(i)
const js = i => JSON.stringify(i)



const isSubstr = (a, b) => {
  return a.length > b.length
    ? a.indexOf(b) !== -1
    : b.indexOf(a) !== -1
}

const a = 'foobar'
const b = 'bar'

lg(isSubstr(a, b))
lg(isSubstr(b, a))
