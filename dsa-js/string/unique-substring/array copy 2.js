/**
 * Leetcode 3. Longest Substring Without Repeating Characters
 * Given a string, find the length of the longest substring without repeating characters.
 * Example 1:
 *   Input: "abcabcbb"
 *   Output: 3
 *   Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 *   Input: "bbbbb"
 *   Output: 1
 *   Explanation: The answer is "b", with the length of 1.
 * Example 3:
 *   Input: "pwwkew"
 *   Output: 3
 *   Explanation: The answer is "wke", with the length of 3.
 *   Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

/**

{
  d: 0,
  v: 1
}
{
  v: 1,
  d: 2,
  f: 3
}

{
  b: 0
}
{
  b: 1
}
{
  b: 3
}

{
  a: 0,
  b: 1,
  c: 2
}
{
  a: 3,
  b: 4,
  c: 5
}

{
  p: 0,
  w: 1
}
{
  w: 1
}
{
  w: 2,
  k: 3,
  e: 4
}
{
  w: 5
}
 */

function substring(str) {
  const chars = {};
  for (let i = 0; i < str.length; i++) {
    const c = str[i];
    if (!chars[c]) {
      chars[c] = [i];
    } else {
      chars[c].push(i);
    }
  }

  let arr = [];
  let curr = 0;

  for (let i = 0; i < str.length; i++) {
    const charIndicesArray = Object.values(chars);
    for (let j = 0; j < charIndicesArray.length; j++) {
      const charIndices = charIndicesArray[j];
      if (charIndices.length === 0) {
        continue;
      }
      const charIndex = charIndices[0];
      if (!arr[curr]) {
        arr[curr] = [charIndex];
        charIndices.shift();
        continue;
      }
      const currLen = arr[curr].length;
      const prevCharIndex = arr[curr][currLen - 1];
      if (
        prevCharIndex + 1 === charIndex &&
        i % (charIndicesArray.length - 1)
      ) {
        arr[curr].push(charIndex);
        charIndices.shift();
        continue;
      } else {
        curr++;
        break;
      }
    }
  }

  const [longest] = arr.reduce((a, b) => (a.length > b.length ? a : b));
  console.log(longest);
  return longest.reduce((a, s) => a + s, "");
}

console.log(substring("pwwkew"));
