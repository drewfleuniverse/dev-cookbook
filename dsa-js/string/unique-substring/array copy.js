/**
 * Leetcode 3. Longest Substring Without Repeating Characters
 * Given a string, find the length of the longest substring without repeating characters.
 * Example 1:
 *   Input: "abcabcbb"
 *   Output: 3
 *   Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 *   Input: "bbbbb"
 *   Output: 1
 *   Explanation: The answer is "b", with the length of 1.
 * Example 3:
 *   Input: "pwwkew"
 *   Output: 3
 *   Explanation: The answer is "wke", with the length of 3.
 *   Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

// getLongestSubStringLength
const getLssl = str => {
  const prevIndices = new Array(128);
  prevIndices.fill(-1);
  let maxLength = 0;
  let cur = 0;
  for (let i = 0; i < str.length; i++) {
    const code = str[i].charCodeAt();
    const prev = prevIndices[code];
    if (prev === -1) {
      cur = i;
    } else {
      cur = Math.max(prev, cur);
    }
    maxLength = Math.max(i - cur + 1, maxLength);
    prevIndices[code] = i;
  }
  return maxLength;
};

describe("getLssl()", () => {
  it("a", () => {
    const str = "a";
    assert(getLssl(str) === 1);
  });
  it("aba", () => {
    const str = "aba";
    assert(getLssl(str) === 2);
  });
  it("aabb", () => {
    const str = "aabb";
    assert(getLssl(str) === 2);
  });
  it("aababcc", () => {
    const str = "aababcc";
    assert(getLssl(str) === 3);
  });
});

/*
Utils
 */

function describe(s, f) {
  console.log(`# ${s} #`);
  f();
}
function it(s, f, r = true) {
  console.log(`${r ? "" : "skip -> "}${s}`);
  r && f();
}
function assert(c, l = false, m = "") {
  l && console.log(`> ${c}`);
  if (!c) {
    throw new Error(m);
  }
}
