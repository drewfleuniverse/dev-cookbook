/**
 * Leetcode 3. Longest Substring Without Repeating Characters
 * Given a string, find the length of the longest substring without repeating characters.
 * Example 1:
 *   Input: "abcabcbb"
 *   Output: 3
 *   Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 *   Input: "bbbbb"
 *   Output: 1
 *   Explanation: The answer is "b", with the length of 1.
 * Example 3:
 *   Input: "pwwkew"
 *   Output: 3
 *   Explanation: The answer is "wke", with the length of 3.
 *   Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

/**
 * @param {string} str
 */
function substring(str) {
  if (str.length === 0) {
    return 0;
  }
  // Group char indices by char
  const charIndices = [...str].reduce((acc, char, i) => {
    if (!acc[char]) {
      acc[char] = [];
    }
    acc[char].push(i);
    return acc;
  }, {});
  console.log(charIndices);
  const chars = Object.keys(charIndices);
  const uniqueStringIndices = [];
  let curr = 0;
  // Iterate up to the length of input string
  for (let i = 0; i < str.length; i++) {
    // Repeat traversal within the list of chars
    for (let j = 0; j < chars.length; j++) {
      const char = chars[j];

      if (charIndices[char].length === 0) {
        continue;
      }

      const [charIndex] = charIndices[char];
      if (!uniqueStringIndices[curr]) {
        uniqueStringIndices[curr] = [charIndex];
        charIndices[char].shift();
        continue;
      }

      const { length: currLen } = uniqueStringIndices[curr];

      const prevCharIndex = uniqueStringIndices[curr][currLen - 1];
      if (
        prevCharIndex + 1 === charIndex &&
        str[prevCharIndex] !== str[charIndex]
      ) {
        uniqueStringIndices[curr].push(charIndex);
        charIndices[char].shift();
      }
      if (j + 1 === chars.length) {
        curr++;
        // break;
      }
    }
  }
  console.log(uniqueStringIndices);
  const longestIndices = uniqueStringIndices.reduce((a, b) =>
    a.length > b.length ? a : b
  );
  return longestIndices.length;
}

substring("");
substring("dvdf");
substring("bbbbbb");
substring("abcabc");
substring("pwwkew");

module.exports = {
  substring
};
