/**
 * Leetcode 3. Longest Substring Without Repeating Characters
 * Given a string, find the length of the longest substring without repeating characters.
 * Example 1:
 *   Input: "abcabcbb"
 *   Output: 3
 *   Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 *   Input: "bbbbb"
 *   Output: 1
 *   Explanation: The answer is "b", with the length of 1.
 * Example 3:
 *   Input: "pwwkew"
 *   Output: 3
 *   Explanation: The answer is "wke", with the length of 3.
 *   Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

/**
 * Creates substring of all lengths from the beginning of the sting. Then find
 * the longest substring by checking if each substring is unique.
 * Time: O(n^3)
 * Space: O(k)
 * @param {string} str
 */
function substringBrute(str) {
  const len = str.length;
  let longest = 0;
  for (let begin = 0; begin < len; begin++) {
    for (let end = begin; end < len; end++) {
      if (isAllUnique(str, begin, end)) {
        longest = Math.max(longest, end - begin + 1);
      }
    }
  }
  return longest;
}

function isAllUnique(str, begin, end) {
  const set = {};
  for (let i = begin; i <= end; i++) {
    const c = str[i];
    if (set[c]) {
      return false;
    }
    set[c] = c;
  }
  return true;
}

/**
 * Sliding window.
 * Time: O(n)
 * Space: O(k)
 * @param {string} str
 */
function substringSliding(str) {
  const { length: len } = str;
  const set = {};
  let longest = 0;
  let begin = 0;
  let end = 0;
  while (begin < len && end < len) {
    const beginChar = str[begin];
    const endChar = str[end];
    if (set[endChar] === undefined) {
      longest = Math.max(longest, end - begin + 1);
      set[endChar] = endChar;
      end++;
    } else {
      delete set[beginChar];
      begin++;
    }
  }
  return longest;
}

/**
 * Sliding window.
 * Time: O(n)
 * Space: O(k)
 * @param {string} str
 */
function substringSlidingOptimized(str) {
  // const log = require("../../utils").logger([
  //   ["map", 13],
  //   "begin",
  //   "end",
  //   "beginChar",
  //   "endChar"
  // ]);
  const { length: len } = str;
  const map = {};
  let longest = 0;
  let begin = 0;
  let end = 0;
  while (end < len) {
    const beginChar = str[begin];
    const endChar = str[end];
    if (map[endChar] !== undefined) {
      begin = Math.max(map[endChar], begin);
      // log([JSON.stringify(map), begin, end, beginChar, endChar], "red");
    }
    longest = Math.max(longest, end - begin + 1);
    map[endChar] = end + 1; // string length from 0 to end
    end++;
    // log([JSON.stringify(map), begin, end, beginChar, endChar]);
  }
  return longest;
}

/**
 * Time: O(n*m)
 * Space:
 * @param {string} str
 */
function substringBruteOneLoop(str) {
  if (str === "") {
    return 0;
  }
  const maps = {};
  let curr = `${str[0]}${0}`;
  let i = 0;
  let j = 0;
  while (i < str.length) {
    const c = str[i];
    if (maps[curr] === undefined) {
      maps[curr] = {};
    }
    if (maps[curr][c] === undefined) {
      maps[curr][c] = i;
      i++;
    } else {
      j++;
      if (i > 0 && c !== str[i - 1]) {
        i = j;
      }
      curr = `${str[j]}${j}`;
    }
  }
  return Object.values(maps)
    .map(m => Object.keys(m).length)
    .reduce((a, b) => (a > b ? a : b), 0);
}

module.exports = {
  substringBruteOneLoop,
  substringBrute,
  substringSliding,
  substringSlidingOptimized
};
