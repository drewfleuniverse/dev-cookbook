const funcs = require("./unique-substring");

describe("Unique substring", () => {
  Object.entries(funcs).forEach(([name, func]) => {
    describe(name, () => {
      test(`returns correct values`, () => {
        expect(func(``)).toBe(0);
        expect(func(`abba`)).toBe(2);
        expect(func(`dvdf`)).toBe(3);
        expect(func(`anviaj`)).toBe(5);
        expect(func(`abcabcbb`)).toBe(3);
        expect(func(`bbbbb`)).toBe(1);
        expect(func(`pwwkew`)).toBe(3);
      });
    });
  });
});
