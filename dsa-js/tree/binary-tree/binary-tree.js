const { swap } = require("../../utils");

class BinaryTree {
  /**
   * @param {number[]} arr
   */
  constructor(arr) {
    this.tree = arr;
    this.size = arr.length;
  }
  get lastChild() {
    return this.firstLeaf - 1;
  }
  get firstLeaf() {
    return Math.floor(this.size / 2);
  }
  get height() {
    return Math.log2(this.size);
  }
  /**
   *
   * @param {number} node
   */
  isLeaf(node) {
    return node >= this.firstLeaf;
  }
  /**
   * @param {number} node
   */
  parent(node) {
    return Math.floor((node - 1) / 2);
  }
  /**
   * @param {number} node
   */
  leftChild(node) {
    return node * 2 + 1;
  }
  /**
   * @param {number} node
   */
  rightChild(node) {
    return node * 2 + 2;
  }
}

module.exports = {
  BinaryTree
};
