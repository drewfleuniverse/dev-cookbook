const { BinaryTree } = require("./binary-tree");

/**
 *          0
 *      1      2
 *    3   4  5   6
 *  7
 */
const binaryTree = new BinaryTree([0, 1, 2, 3, 4, 5, 6, 7]);

describe("BinaryTree", () => {
  test("gets last child", () => {
    expect(binaryTree.lastChild).toBe(3);
  });
  test("gets first leaf", () => {
    expect(binaryTree.firstLeaf).toBe(4);
  });
  test("gets tree height", () => {
    expect(binaryTree.height).toBe(3);
  });
  test("returns parent", () => {
    expect(binaryTree.parent(3)).toBe(1);
    expect(binaryTree.parent(4)).toBe(1);
  });
  test("returns left child", () => {
    expect(binaryTree.leftChild(1)).toBe(3);
  });
  test("returns right child", () => {
    expect(binaryTree.rightChild(1)).toBe(4);
  });
});
