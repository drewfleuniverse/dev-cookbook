/*
Leetcode 606. Construct String from Binary Tree
Using recursion
Perf: O(n)
- Every nodes are visited
Space: O(n)
- Worst case stack height
 */


function TreeNode(val) {
  this.val = val;
  this.left = this.right = null;
}


function tree2str(t) {
  if (t === null) {
    return '';
  }
  if (t.left === null && t.right === null) {
    return t.val;
  }
  if (t.right === null) {
    return `${t.val}(${tree2str(t.left)})`;
  }
  return `${t.val}(${tree2str(t.left)})(${tree2str(t.right)})`;
};


describe('printPreOrder()', () => {
  it('should omit empty leaves', () => {
    const root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.left = new TreeNode(4);
    assert(tree2str(root) === '1(2(4))(3)');
  });
  it('should not omit internal nodes', () => {
    const root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.right = new TreeNode(4);
    assert(tree2str(root) === '1(2()(4))(3)');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
