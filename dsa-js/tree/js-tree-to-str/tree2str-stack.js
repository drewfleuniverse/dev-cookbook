/*
Leetcode 606. Construct String from Binary Tree
Using stack
Perf: O(n)
- Every nodes are visited
Space: O(n)
- Worst case stack height
 */


function TreeNode(val) {
  this.val = val;
  this.left = this.right = null;
}


function tree2str(t) {
  if (t === null) {
    return '';
  }
  const stack = [t];
  const visited = new Set();
  let str = '';
  while (stack.length !== 0) {
    const curr = stack[stack.length - 1];
    if (visited.has(curr)) {
      stack.pop();
      str += ')';
    } else {
      visited.add(curr);
      str += `(${curr.val}`;
      if (curr.left === null && curr.right !== null) {
        str += '()';
      }
      if (curr.right !== null) {
        stack.push(curr.right);
      }
      if (curr.left !== null) {
        stack.push(curr.left);
      }
    }
  }
  return str.slice(1, str.length - 1);
};


describe('printPreOrder()', () => {
  it('should omit empty leaves', () => {
    const root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.left = new TreeNode(4);
    assert(tree2str(root) === '1(2(4))(3)');
  });
  it('should not omit internal nodes 1', () => {
    const root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.right = new TreeNode(4);
    assert(tree2str(root) === '1(2()(4))(3)');
  });
  it('should not omit internal nodes 2', () => {
    const root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(4);
    assert(tree2str(root) === '1(2)(3()(4))');
  });
});


/*
Utils
 */

/* describe('', () => {
  it('', () => {
  });
}); */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
