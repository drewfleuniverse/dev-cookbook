// Utils

function swap(A, i, j) {
  const tmp = A[i];
  A[i] = A[j];
  A[j] = tmp;
}

function assert(cond, message = "") {
  if (!cond) throw new Error(message);
}

// Max Heap

const H = Symbol("H");
const n = Symbol("n");
const siftDown = Symbol("siftDown");
const isLeaf = Symbol("isLeaf");
const leftChild = Symbol("leftChild");
const rightChild = Symbol("rightChild");
const parent = Symbol("parent");
const firstLeaf = Symbol("firstLeaf");
class MaxHeap {
  constructor(_H) {
    this[H] = _H;
    this[n] = _H.length;
    this.buildHeap();
  }

  heapSize() {
    return this[n];
  }

  insert(v) {
    let cur = this[n]++;
    this[H][cur] = v;
    while (cur !== 0 && this[H][cur] > this[H][this[parent](cur)]) {
      swap(this[H], cur, this[parent](cur));
      cur = this[parent](cur);
    }
  }

  buildHeap() {
    for (let i = this[firstLeaf]() - 1; i >= 0; i--) {
      this[siftDown](i);
    }
  }

  removeMax() {
    assert(this[n] !== 0);
    swap(this[H], 0, --this[n]);
    if (this[n] !== 0) this[siftDown](0);
    return this[H][this[n]];
  }

  remove(p) {
    assert(p >= 0 && p < this[n]);
    if (p === this[n] - 1) this[n]--;
    else {
      swap(this[H], p, --this[n]);
      while (p > 0 && this[H][p] > this[H][this[parent](p)]) {
        swap(this[H], p, this[parent](p));
        p = this[parent](p);
      }
      if (this[n] !== 0) this[siftDown](p);
    }
    return this[H][this[n]];
  }

  [siftDown](p) {
    assert(p >= 0 && p < this[n]);
    while (!this[isLeaf](p)) {
      let i = this[leftChild](p);
      if (i < this[n] - 1 && this[H][i] < this[H][i + 1]) i++;
      if (this[H][p] >= this[H][i]) return;
      swap(this[H], p, i);
      p = i;
    }
  }

  [isLeaf](p) {
    return p >= Math.floor(this[n] / 2) && p < this[n];
  }

  [leftChild](p) {
    assert(p < Math.floor(this[n] / 2));
    return p * 2 + 1;
  }

  [rightChild](p) {
    assert(p < Math.floor((this[n] - 1) / 2));
    return p * 2 + 2;
  }

  [parent](p) {
    assert(p > 0);
    return Math.floor((p - 1) / 2);
  }

  [firstLeaf]() {
    return Math.floor(this[n] / 2);
  }
}

// Sort

function sort(H) {
  const m = new MaxHeap(H);
  for (let i = 0; i < H.length; i++) m.removeMax();
}

const _H = [7, 6, 5, 4, 3, 2, 1];
console.log(_H);
sort(_H);
console.log(_H);
