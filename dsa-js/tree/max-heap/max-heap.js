const { BinaryTree } = require("../binary-tree/binary-tree");
const { swap } = require("../../utils");

class MaxHeap extends BinaryTree {
  /**
   * @param {number[]} arr
   */
  constructor(arr) {
    super(arr);
    this.build();
  }
  build() {
    for (let child = this.lastChild; child >= 0; child--) {
      this.siftDown(child);
    }
  }
  /**
   *
   * @param {number} num
   */
  insert(num) {
    this.tree[this.size] = num;
    this.size++;
    let numIndex = this.size - 1;
    let parent = this.parent(numIndex);
    while (parent >= 0 && num > this.tree[parent]) {
      swap(this.tree, numIndex, parent);
      numIndex = parent;
      parent = this.parent(numIndex);
    }
  }
  /**
   * http://www.mathcs.emory.edu/~cheung/Courses/171/Syllabus/9-BinTree/heap-delete.html
   * @param {number} node
   */
  remove(node) {
    swap(this.tree, node, this.size - 1);
    this.size--;
    this.tree.length--;
    const parent = this.parent(node);
    if (parent >= 0 && this.tree[node] > this.tree[parent]) {
      swap(this.tree, node, parent);
    } else {
      this.siftDown(node);
    }
  }
  /**
   * @param {number} node
   */
  siftDown(node) {
    let nd = node; // no need to reassign
    while (!this.isLeaf(nd)) {
      // can be extract to a func
      let maxChild = this.leftChild(nd);
      if (
        maxChild < this.size - 1 &&
        this.tree[maxChild] < this.tree[maxChild + 1]
      ) {
        maxChild++;
      }
      if (this.tree[nd] >= this.tree[maxChild]) {
        return;
      }
      swap(this.tree, nd, maxChild);
      // nd = maxChild; // unneeded
    }
  }
}

module.exports = {
  MaxHeap
};
