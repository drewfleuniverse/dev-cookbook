const { MaxHeap } = require("./max-heap");

/**
 *       5
 *   4      3
 * 1   2
 */
let heap;

describe("MaxHeap", () => {
  beforeEach(() => {
    heap = new MaxHeap([1, 2, 3, 4, 5]);
  });
  test("builds", () => {
    expect(heap.tree).toEqual([5, 4, 3, 1, 2]);
  });
  test("inserts", () => {
    heap.insert(7);
    expect(heap.tree).toEqual([7, 4, 5, 1, 2, 3]);
  });
  describe("removal", () => {
    test("when new node is smaller than parent", () => {
      heap.remove(1);
      expect(heap.tree).toEqual([5, 2, 3, 1]);
    });
    /**
     *        5
     *   2        4
     * 1   1    3
     */
    test("when new node is larger than parent", () => {
      heap = new MaxHeap([5, 2, 4, 1, 1, 3]);
      heap.remove(3);
      expect(heap.tree).toEqual([5, 3, 4, 2, 1]);
    });
    test("works when heap has only one element", () => {
      heap = new MaxHeap([1]);
      heap.remove(0);
      expect(heap.tree).toEqual([]);
    });
  });
});
