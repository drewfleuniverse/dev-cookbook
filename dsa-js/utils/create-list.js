const { ListNode } = require("../common");

/**
 * @param {array} numbers
 */
const createList = numbers => {
  const [firstNumber, ...restNumbers] = numbers;
  const list = new ListNode(firstNumber);
  let tmp = list;
  restNumbers.forEach(n => {
    tmp.next = new ListNode(n);
    tmp = tmp.next;
  });
  return list;
};

module.exports = {
  createList
};
