const { ListNode } = require("../common");
const { createList } = require("./create-list");

describe("utils", () => {
  test("1", () => {
    const l = new ListNode(2);
    expect(l.val).toBe(2);
  });
  test("2", () => {
    const l = createList([2, 4, 3]);
    expect(l.val).toBe(2);
    expect(l.next.val).toBe(4);
    expect(l.next.next.val).toBe(3);
  });
});
