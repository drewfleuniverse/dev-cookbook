const padStart = require("lodash/padEnd");
const chalk = require("chalk");

/**
 * @param {(string | { col: string, pad: number })[]} columns
 */
function logger(columns) {
  const colPadArr = columns.map((column, i) => {
    const col = Array.isArray(column) ? column[0] : column;
    const pad = Array.isArray(column) ? column[1] : column.length;
    return [col, pad];
  });
  const firstRowArr = colPadArr.map(([col, pad]) => padStart(col, pad));
  const firstRow = firstRowArr.join(" | ");
  console.log(firstRow);
  return function(values, color) {
    const resultArr = [];
    values.forEach((val, i) => {
      const pad = colPadArr[i][1];
      resultArr.push(padStart(val === undefined ? "undefined" : val, pad));
    });
    let result = resultArr.join(" | ");
    if (color) {
      result = chalk[color](result);
    }
    console.log(result);
  };
}

module.exports = {
  logger
};
