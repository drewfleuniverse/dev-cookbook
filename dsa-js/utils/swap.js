/**
 *
 * @param {*[]} A
 * @param {number} i
 * @param {number} j
 */
function swap(A, i, j) {
  const tmp = A[i];
  A[i] = A[j];
  A[j] = tmp;
}

module.exports = {
  swap
};
