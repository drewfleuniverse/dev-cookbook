import f from './index';

test('should return 42', () => {
  expect(f()).toBe(42);
});
