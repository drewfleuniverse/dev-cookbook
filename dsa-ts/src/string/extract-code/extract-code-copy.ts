// export const extractCode = (content: string) => {
//   enum Status {
//     SingleLineComments,
//     MultiLineComments,
//     SingleQuotes,
//     DoubleQuotes,
//     BacktikQuotes
//   }

//   const statements: string[] = [];
//   let curr: string;
//   let currTwo: string;
//   let statementStart: number = 0;
//   let statementEnd: number = 0;
//   let status: Status | undefined;
//   let isEscape = false;

//   for (let i = 0; i < content.length; i++) {
//     curr = content[i];
//     currTwo = curr + content[i + 1];

//     if (
//       (status === Status.SingleLineComments && currTwo === "\n") ||
//       (status === Status.MultiLineComments && currTwo === "*/")
//     ) {
//       status = undefined;
//       if (i + 1 < content.length) {
//         statementStart = i + 1;
//       }
//       continue;
//     } else {
//       if (
//         status === Status.SingleQuotes ||
//         status === Status.DoubleQuotes ||
//         status === Status.BacktikQuotes
//       ) {
//         // if (isEscape) {
//         //   isEscape = false;
//         //   continue;
//         // } else if (curr === "\\") {
//         //   isEscape = true;
//         //   continue;
//         // }

//         if (
//           (status === Status.SingleQuotes && curr === `'`) ||
//           (status === Status.DoubleQuotes && curr === `"`) ||
//           (status === Status.BacktikQuotes && curr === "`")
//         ) {
//           status = undefined;
//         }
//       } else {
//         if (status !== Status.MultiLineComments && currTwo === "//") {
//           status = Status.SingleLineComments;
//           statementEnd = i;
//         } else if (status !== Status.SingleLineComments && currTwo === "/*") {
//           status = Status.MultiLineComments;
//           statementEnd = i;
//         }
//         // if (curr === `'`) {
//         //   status = Status.SingleQuotes;
//         // } else if (curr === `"`) {
//         //   status = Status.DoubleQuotes;
//         // } else if (curr === "`") {
//         //   status = Status.BacktikQuotes;
//         // }
//       }
//     }

//     if (
//       status !== Status.SingleLineComments &&
//       status !== Status.MultiLineComments &&
//       statementStart < statementEnd
//     ) {
//       statements.push(content.slice(statementStart, statementEnd));
//     }
//   }
//   return statements;
// };
