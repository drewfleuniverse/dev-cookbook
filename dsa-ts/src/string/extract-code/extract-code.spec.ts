import {
  extractCodeSimple,
  extractCodeWithoutSingleLineComments,
  extractCodeWithoutSingleLineCommentsSimple
} from './extract-code';

type IFn = (arg: string) => string[];

describe('extractCode', () => {
  describe('extractCodeSimple', () => {
    const fn = extractCodeSimple;
    test('should extract statements', () => {
      expect(fn(`\n`)).toMatchObject([]);
      expect(fn(`A;`)).toMatchObject(['A;']);
      expect(fn(`A;\n`)).toMatchObject(['A;']);
      expect(fn(`A;\nB;\n`)).toMatchObject(['A;', 'B;']);
    });
  });
  describe('extractCodeWithoutSingleLineCommentsSimple', () => {
    const fn = extractCodeWithoutSingleLineCommentsSimple;
    test('should extract statements', () => {
      expect(fn(`\n`)).toMatchObject([]);
      expect(fn(`A;`)).toMatchObject(['A;']);
      expect(fn(`A;\n`)).toMatchObject(['A;']);
      expect(fn(`A;\nB;\n`)).toMatchObject(['A;', 'B;']);
    });
    test('should not extract single line comments', () => {
      expect(fn(`//`)).toMatchObject([]);
      expect(fn(`// A;`)).toMatchObject([]);
      expect(fn(`// //`)).toMatchObject([]);
      expect(fn(`A; //`)).toMatchObject(['A;']);
    });
  });
  describe('extractCodeWithoutSingleLineComments', () => {
    const fn = extractCodeWithoutSingleLineComments;
    test('should extract statements', () => {
      expect(fn(`\n`)).toMatchObject([]);
      expect(fn(`A;`)).toMatchObject(['A;']);
      expect(fn(`A;\n`)).toMatchObject(['A;']);
      expect(fn(`A;\nB;\n`)).toMatchObject(['A;', 'B;']);
    });
    test('should not extract single line comments', () => {
      expect(fn(`A; //`)).toMatchObject(['A;']);
      expect(fn(`//`)).toMatchObject([]);
      expect(fn(`// A;`)).toMatchObject([]);
      expect(fn(`// //`)).toMatchObject([]);
    });
    test('should not extract single line comments contain quotes', () => {
      expect(fn(`// ''`)).toMatchObject([]);
      expect(fn(`// ""`)).toMatchObject([]);
      expect(fn('// ``')).toMatchObject([]);
    });
    test('should extract quotes contain single line comments', () => {
      expect(fn(`'//'`)).toMatchObject([`'//'`]);
      expect(fn(`"//"`)).toMatchObject([`"//"`]);
      expect(fn('`//`')).toMatchObject(['`//`']);
    });
  });
});
