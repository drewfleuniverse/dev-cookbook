export const extractCodeSimple = (content: string) => {
  const { length } = content;
  const statements: string[] = [];
  let curr: string;
  let begin: number = 0;

  for (let i = 0; i < length; i++) {
    curr = content[i];
    if (curr === '\n') {
      if (begin < i) {
        statements.push(content.slice(begin, i));
      }
      if (i + 1 < length) {
        begin = i + 1;
      }
    } else if (i === length - 1) {
      statements.push(content.slice(begin));
    }
  }
  return statements;
};

export const extractCodeWithoutSingleLineCommentsSimple = (content: string) => {
  enum Reading {
    SingleLineComments
  }
  const { length } = content;
  const statements: string[] = [];
  let status: Reading | undefined;
  let curr: string;
  let next: string | undefined;
  let begin: number = 0;

  for (let i = 0; i < length; i++) {
    curr = content[i];
    next = i + 1 < length ? content[i + 1] : undefined;
    if (
      status !== Reading.SingleLineComments &&
      next !== undefined &&
      curr + next === '//'
    ) {
      status = Reading.SingleLineComments;
      if (begin < i) {
        statements.push(content.slice(begin, i).trim());
      }
    }
    if (curr === '\n' && status === Reading.SingleLineComments) {
      status = undefined;
      if (i + 1 < length) {
        begin = i + 1;
      }
    } else if (curr === '\n') {
      if (begin < i) {
        statements.push(content.slice(begin, i));
      }
      if (i + 1 < length) {
        begin = i + 1;
      }
    } else if (status !== Reading.SingleLineComments && i === length - 1) {
      statements.push(content.slice(begin));
    }
  }
  return statements;
};

export const extractCodeWithoutSingleLineComments = (content: string) => {
  enum Reading {
    SingleLineComments,
    SingleQuotes,
    DoubleQuotes,
    BacktikQuote
  }
  const { length } = content;
  const statements: string[] = [];
  let status: Reading | undefined;
  let curr: string;
  let next: string | undefined;
  let begin: number = 0;
  // let isEscape: boolean = false;

  for (let i = 0; i < length; i++) {
    curr = content[i];
    next = i + 1 < length ? content[i + 1] : undefined;

    if (status === undefined) {
      if (curr === `'`) {
        status = Reading.SingleQuotes;
        continue;
      } else if (curr === `"`) {
        status = Reading.DoubleQuotes;
        continue;
      } else if (curr === '`') {
        status = Reading.BacktikQuote;
        continue;
      } else if (next !== undefined && curr + next === '//') {
        status = Reading.SingleLineComments;
        if (begin < i) {
          statements.push(content.slice(begin, i).trim());
        }
      }
    }
    if (
      (curr === `'` && status === Reading.SingleQuotes) ||
      (curr === `"` && status === Reading.DoubleQuotes) ||
      (curr === '`' && status === Reading.BacktikQuote)
    ) {
      status = undefined;
    }
    if (curr === '\n' && status === Reading.SingleLineComments) {
      status = undefined;
      if (i + 1 < length) {
        begin = i + 1;
      }
    } else if (curr === '\n') {
      if (begin < i) {
        statements.push(content.slice(begin, i));
      }
      if (i + 1 < length) {
        begin = i + 1;
      }
    } else if (status !== Reading.SingleLineComments && i === length - 1) {
      statements.push(content.slice(begin));
    }
  }
  return statements;
};
