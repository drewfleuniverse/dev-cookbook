# Data Structures and Algorithms

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Data Structures and Algorithms](#data-structures-and-algorithms)
	- [Pseudocode Notation](#pseudocode-notation)
		- [Types](#types)
		- [Class and Abstractions](#class-and-abstractions)
		- [Methods](#methods)
	- [Compilation and Run](#compilation-and-run)
		- [Java Programs](#java-programs)
	- [Sedgewick's Algs4](#sedgewicks-algs4)
		- [Put `algs4.jar` on classpath](#put-algs4jar-on-classpath)
		- [Get data](#get-data)
	- [References](#references)
		- [Books to Read](#books-to-read)
		- [General programming and algorithm interview questions](#general-programming-and-algorithm-interview-questions)

<!-- /TOC -->


## Pseudocode Notation

### Types

Uses C-like data types. No distinction between primitive and class data types.

```
void
int
long
float
double
boolean
...
```

### Class and Abstractions

- Abstract classes and interfaces: In general OO terms, there is no clear line between abstract classes and interfaces. Basically, an abstract class can contain states and method implementations, while an interface cannot contain states. As of Java 8, interfaces are allowed to have default and static methods.

- Visibility on class name:
    - Assumes every class is non-inner class
    - No access modifiers on class name for simpler notation
    - The implementation may improvise depends whether the class is exposed outside of the package
- Visibility on constructors and methods:
    - No omitted modifier for text alignment purpose.
    - E.g. a package visible class may have public methods in its pseudocode notation, while its implementation may omitted the public modifiers to use default visibility instead

```java
/** Class */
Cool<T>
/** Methods */
+ publicMethod(): void
$ protectedMethod(): void // use hash symbol conflicts with markdown toc
- privateMethod(): void
~ defaultSlashPackageMethod(): void
```

```java
/* Class */
Foo<T> : Cool<T>
/* Fields */
- stuff: T
/* Methods */
+ bar(x:int): void
```

### Methods

```js
bar(x)
  //...
```


## Compilation and Run

### Java Programs

Place the following commands in system path:

```
javac *.java && \
java -ea org.junit.runner.JUnitCore Tests
```


## Sedgewick's Algs4


### Put `algs4.jar` on classpath

The program file:
```java
import edu.princeton.cs.algs4.*;
```

Preparation:
  1. Paste the following line in `.bashrc` or `.zshrc`:
```sh
export CLASSPATH=$CLASSPATH:$HOME/Dev/jars/algs4.jar
```
  2. Download [algs4.jar](http://algs4.cs.princeton.edu/code/algs4.jar) and move
    it to `~/Dev/jars/algs4`


### Get data

  - Git clone at [data](https://bitbucket.org/drewfleuniverse/dsa-data)



## References

### Books to Read

- [Introduction to Programming in Java](http://introcs.cs.princeton.edu/java/home/)
- [How to Design Programs, Second Edition](http://www.ccs.neu.edu/home/matthias/HtDP2e/Draft/index.html)
    - Feature a Scheme-Lisp-like language runs in DrRacket

### General programming and algorithm interview questions

- [Java67 - Java Programming tutorials and Interview Questions](http://www.java67.com/)
- [Top 10 Algorithms for Coding Interview](http://www.programcreek.com/2012/11/top-10-algorithms-for-coding-interview/)
- [Geeks for geeks](http://www.geeksforgeeks.org)
- [Read the Slides - CS4104 Data and Algorithm Analysis:
  Calendar](http://courses.cs.vt.edu/~cs4104/shaffer/Fall2010/Calendar.html)
- [Interview Programming Problems Done Right](http://www.cforcoding.com/2012/01/interview-programming-problems-done.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+CForCoding+%28C+for+Coding%29)
