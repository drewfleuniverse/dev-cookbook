## Growth rate

 Growth rate Name | Growth rate   | n = 16 | n = 2^10 (1024)
------------------|---------------|--------|-----------------
log logrithmic    | `log(log(n))` | 2      | ~3.3
logrithmic        | `log(n)`      | 4      | 10
linear            | `n`           | 16     | 2^10
log linear        | `n * log(n)`  | 64     | ~2^13
quadratic         | `n2`          | 2^8    | 2^20
cubic             | `n3`          | 2^12   | 2^30
exponential       | `2^n`         | 2^16   | 2^1024
