# Array


## Time complexity

**Average**

- Access: O(1)
- Search: O(n)
- Insertion: O(n)
- Deletion: O(n)

**Worst**

- Access: O(1)
- Search: O(n)
- Insertion: O(n)
- Deletion: O(n)


## Space complexity

- Worst: O(n)
