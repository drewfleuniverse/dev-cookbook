class AList<E> implements List<E> {
  private static final int defaultSize = 10;
  private int maxSize;
  private int listSize;
  private int curr;
  private E[] list;

  AList() { this(defaultSize); }
  @SuppressWarnings("unchecked")
  AList(int size) {
    maxSize = size;
    listSize = curr = 0;
    list = (E[])new Object[maxSize];
  }

  public void clear() { listSize = curr = 0; }

  public void insert(E it) {
    assert listSize < maxSize : "List capacity exceeded";
    // last iteration: i = curr+1
    for (int i = listSize; i > curr; i--)
      list[i] = list[i-1];
    list[curr] = it;
    listSize++;
  }

  public void append(E it) {
    assert listSize < maxSize : "List capacity exceeded";
    list[listSize++] = it;
  }

  public E remove() {
    if ((curr < 0) || (curr >= listSize)) return null;
    E it = list[curr];
    for (int i = curr; i < listSize-1; i++)
      list[curr] = list[curr+1];
    listSize--;
    return it;
  }

  public void moveToStart() { curr = 0; }
  public void moveToEnd() { curr = listSize; }
  public void prev() { if (curr != 0) curr--; }
  public void next() { if (curr < listSize) curr++; }
  public int length() { return listSize; }
  public int currPos() { return curr; }

  public void moveToPos(int pos) {
    assert (pos>=0) || (pos<=listSize) : "Pos out of range";
    curr = pos;
  }

  public E getValue() {
    assert (curr >= 0) && (curr < listSize) : "No current element";
    return list[curr];
  }

  /*
  Output as:
  (curr)[3][2][1][4][5]: listSize=5, curr=0
  */
  public String toString() {
    int tmpPos = currPos();
    int len = length();
    StringBuffer buf = new StringBuffer();

    for (int i = 0; i < maxSize; i++) {
      if (i == curr ) {
        buf.append("(");
        buf.append("curr");
        buf.append(")");
      }

      buf.append("[");
      if (i < listSize) buf.append(list[i]);
      else buf.append(" ");
      buf.append("]");
    }

    if (curr == maxSize) {
      buf.append("(");
      buf.append("curr");
      buf.append(")");
    }

    buf.append(": ");
    buf.append("listSize=");
    buf.append(listSize);
    buf.append(", ");
    buf.append("curr=");
    buf.append(curr);

    return buf.toString();
  }
}
