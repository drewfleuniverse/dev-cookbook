class DemoUALDict {
  public static void main(String[] args) {
    UALDict<String, Integer> u = new UALDict<String, Integer>(3);
    u.insert("k1", 1);
    u.insert("k2", 2);
    u.insert("k3", 3);
    System.out.println(u);
    u.removeAny();
    u.remove("k1");
    System.out.println(u);
    u.find("k1");
    System.out.println(u);
    /*
    (curr)[<k1, 1>][<k2, 2>][<k3, 3>]: listSize=3, curr=0
    (curr)[<k2, 2>][ ][ ]: listSize=1, curr=0
    [<k2, 2>](curr)[ ][ ]: listSize=1, curr=1
    */
  }
}
