class KVPair<K, E> {
  private K k;
  private E e;

  KVPair() { k = null; e = null; }
  KVPair(K kval, E eval) { k = kval; e = eval; }

  public K key() { return k; }
  public E value() { return e; }

  public String toString() {
    StringBuffer buf = new StringBuffer();
    buf.append("<");
    buf.append(k);
    buf.append(", ");
    buf.append(e);
    buf.append(">");

    return buf.toString();
  }
}
