public interface List<E> {
  public void clear();
  public void insert(E it);
  public void append(E it);
  public E remove();
  public void moveToStart();
  public void moveToEnd();
  public void prev();
  public void next();
  public int length();
  public int currPos();
  public void moveToPos(int pos);
  public E getValue();
}
