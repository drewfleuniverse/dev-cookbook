class UALDict<K, E> implements Dict<K, E> {
  private static final int defaultSize = 10;
  private AList<KVPair<K, E>> list;

  UALDict() { this(defaultSize); }
  UALDict(int size) { list = new AList<KVPair<K, E>>(size); }

  public void clear() { list.clear(); }
  public void insert(K k, E e) { list.append(new KVPair<K, E>(k, e)); }

  public E remove(K k) {
    E temp = find(k);
    if (temp != null) list.remove();
    return temp;
  }

  public E removeAny() {
    if (size() != 0) {
      list.moveToEnd();
      list.prev();
      KVPair<K, E> temp = list.remove();
      return temp.value();
    } else return null;
  }

  public E find(K k) {
    for (list.moveToStart(); list.currPos() < list.length(); list.next()) {
      KVPair<K, E> temp = list.getValue();
      if (k == temp.key()) return temp.value();
    }
    return null;
  }

  public int size() { return list.length(); }

  public String toString() {
    return list.toString();
  }
}
