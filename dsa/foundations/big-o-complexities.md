# Big-O Complexities

## Notation
 - Big-O: Upper bound
 - Big-Theta: Upper and lower bound
 - Big-Omega: Lower bound

## Main References
- [Big-O Cheatsheet](http://bigocheatsheet.com)
  - Read:
    - Big-O Complexity Chart
    - Common Data Structure Operations
    - Array Sorting Algorithms
## Notes
  - Shell sort has performance variants from n^2 to n(log(n))^2 to n^(4/3)
  - If n is large enough, log(n!) is close to n*(log(n))
    - Stirling's approximation: log(n!) = n*(log(n)) - n + O(log(n))
