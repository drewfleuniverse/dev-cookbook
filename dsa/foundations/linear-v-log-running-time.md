## Linear running time


### Simple loop

**Program**

```js
const a = [1,2,3,4,5,6,7,8];
const r = []
for (let i = 0; i < a.length; i++) {
  r.push(a[i]);
}
console.log(r.toString() === '1,2,3,4,5,6,7,8');
```

**Running time**

- O(n)
  - for loop costs n
  - push costs c
  - Total: T(n) = c*n


1 2 3 4 5 6 7 8
^ ^ ^ ^ ^ ^ ^ ^



### Recursive divide

**Program**

```js
const a = [1,2,3,4,5,6,7,8];
const r = [];

const func = (a, lo, hi) => {
  if (lo > hi) {
    return;
  }

  const mid = Math.floor((lo+hi)/2);
  if (lo === hi) {
    r.push(a[mid]);
    return;
  }

  func(a, lo, mid);
  func(a, mid+1, hi);
}

func(a, 0, a.length-1);
console.log(r.toString() === '1,2,3,4,5,6,7,8');
```

**Running time**



```
1 2 3 4 5 6 7 8
       |
   |   |   |
 | | | | | | |
```


## Logarithmic running time example

**Program**

```js
const a = [1,2,3,4,5,6,7,8];
const r = []


const func = (a, p) => {
  while (p < a.length) {
    r.push(p);
    p = 2*p + 1;
  }
};

func(a, 0, a.length-1);
console.log(r.toString() === '0,1,3,7');
```

**Running time**

- O(log2(n))
  - `while` loop: log2(n) + 1
  - push`: c
  - T(n) = c*log2(n) + c
