# Recursion

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Recursion](#recursion)
	- [Introduction](#introduction)
	- [Forms of recursion](#forms-of-recursion)
		- [Recursive functions and algorithms](#recursive-functions-and-algorithms)
		- [Recursive data types](#recursive-data-types)
	- [Types of recursion](#types-of-recursion)
		- [Single and multiple recursion](#single-and-multiple-recursion)
			- [Single recursion](#single-recursion)
			- [Multiple recursion](#multiple-recursion)
			- [Example: Fibonacci functions](#example-fibonacci-functions)
		- [Direct and indirect recursion](#direct-and-indirect-recursion)
			- [Direct recursion](#direct-recursion)
			- [Indirect recursion](#indirect-recursion)
		- [Anonymous recursion](#anonymous-recursion)
		- [Structural and generative recursion](#structural-and-generative-recursion)
	- [Recursive programs](#recursive-programs)
		- [Recursive functions and algorithms](#recursive-functions-and-algorithms)
			- [Factorial](#factorial)
			- [Greatest common divisor](#greatest-common-divisor)
			- [Towers of Hanoi](#towers-of-hanoi)
		- [Recursive data types](#recursive-data-types)
			- [Link lists](#link-lists)
			- [Binary tree](#binary-tree)
	- [Other topics](#other-topics)
		- [Implementation issues](#implementation-issues)
		- [Recursion versus iteration](#recursion-versus-iteration)
		- [Tail-recursive functions](#tail-recursive-functions)
		- [Order of execution](#order-of-execution)
	- [References](#references)

<!-- /TOC -->


## Introduction

- Recursion solves problems by solving smaller instances of the same problem (Knuth, 1)
- A recursive program can describe an infinite number of computations (Wirth, 87)


## Forms of recursion

### Recursive functions and algorithms

- Concepts:
    - Divide and conquer: divide a problem into sub-problems of the same type
    - Dynamic programming and memorization: divide and conquer with pre-/stored results
- A recursive function contains:
    - Base cases:
		    - Produce results without calling the function itself
				- Also called terminating case
    - Recursive cases:
		    - Produce results by calling the function itself
				- Also called reduction step
		- Notes - We can say a base case breaks the chain of recursion:
		 		- when the input converges to the base case
				- or, when the recursive case reaches the base case
- Concerns:
    - When base cases are obvious, e.g. binary tree traversal, they need to be properly tested to avoid infinite loops
    - When base cases aren't obvious, the base case has to be established by explicitly stating a limit of execution times
        - E.g. An infinite series such as e (the base of natural logarithm), the recursive function can accept a parameter to indicate calculating up to n-th term

### Recursive data types

- A data type which is defined with self-referential definition, i.e. use recursion to define a data type
- Regardless of kinds of recursive data types, hereunder are examples:
    - Definition of a linked list:
        - Either an empty linked list,
        - Or an data node and a linked list
    - Definition of natural numbers:
        - Either 1
        - Or n + 1, n is a natural number


## Types of recursion

### Single and multiple recursion

#### Single recursion

- Contains one self-reference
- Can be converted to iteration in linear time and constant space
- E.g. list traversal, linear search, factorial function

#### Multiple recursion

- Contains multiple self-references
- Can be converted to iteration with a stack in exponential time and space
- Can be converted to single recursion with or without iteration
- E.g. Fibonacci function

#### Example: Fibonacci functions

- Fibonacci here defines F_0 = 0, F_1 = 1, F_2 = 1, F_3 = 2, and so on

**Multiple recursive Fibonacci function:**

```js
fib(n)
  if (n < 2) return n;
  return fib(n-1) + fib(n-2)
```

**Single recursive Fibonacci function:**

```js
fib(n)
  return fibHelper(n, 0, 1)

fibHelper(n, p, c) // p: prev; c: curr
  if (n == 0) return p
  if (n == 1) return c
  return fibHelper(n-1, c, p + c)
```

**Iterative Fibonacci function:**

```js
fib(n)
  p = 0 // p: prev
  c = 1 // c: curr
  for (i = 2; i < n; i++)
    temp = p
    p = c
    c = c + temp
  return p + c
```

### Direct and indirect recursion

#### Direct recursion

- A function calls it self

```js
f()
  f()
```

#### Indirect recursion

- A function calls another function which calls itself
- Also called mutual recursion

```js
f()
  g()

g()
  f()
```

### Anonymous recursion

- Anonymous function calls itself
- Rarely used in practice

### Structural and generative recursion

- Structural recursion:
    - Each recursive call processes one or a subset of components of the entire set of input components
    - Likely to have iterative variants
    - E.g. most tree traversals, XML processing, binary tree search
- Generative recursion:
    - Each recursive call processes the entire set of input components
    - Not likely to have iterative variants
    - E.g. gcd, quicksort, binary search, mergesort, fractals


## Recursive programs

### Recursive functions and algorithms

#### Factorial

**Definition, standard/function**

```
fact(n) = 1,               if n = 0
        = n * fact(n - 1), if n > 0
```

**Definition, standard/recurrence relation**

```
b_n = n * b_(n-1)
```

**Definition, accumulator/function**

```
fact(n) = fact_acc(n, 1)
fact_acc(n , t) = t,                      if n = 0
                = fact_acc(n - 1, n * t), if n > 0
```

**Pseudocode, recursive**

```js
fact(n)
  if (n == 0) return 1
	return n * fact(n-1)
```

**Pseudocode, iterative**

```js
fact(n)
	acc = 1
	while (n > 0)
	  acc *= n--
	return acc
```

#### Greatest common divisor

**Definition, function**

```
gcd(x, y) = x,                       if y = 0
          = gcd(y, remainder(x, y)), if y > 0
```

**Definition, recurrence relation**

```
gcd(x, y) = gcd(y, x % y), if y != 0
gcd(x, 0) = x
```

**Pseudocode, tail recursive**

```js
gcd(x, y)
  if (y == 0) return x
	return gcd(y, x % y)
```

**Pseudocode, iterative**

```js
gcd(x, y)
  while (y != 0)
	  temp = x % y
		x = y
		y = temp
	return x
```

#### Towers of Hanoi

**Definition, function**

```
hanoi(n) = 1,                    if n = 1
         = 2 * hanoi(n - 1) + 1, if n > 1
```

**Definition, recurrence relation**

```
h_n = 2 * h_(n-1) + 1
h_1 = 1
```

**Pseudocode, recursive**

```js
hanoi(n)
  if (n == 1) return 1
	return (2 * hanoi(n - 1)) + 1
```

**Formula**

```
h_n = 2^n - 1, for all n >= 1
```

### Recursive data types

#### Link lists

**Component**

```js
class node {
	data
	next // self-reference
}
```

**Traversal**

```js
// usage: traverse(this.head)
traverse(node)
  if (node != null)
  	DO_SOMETHING()
	  traverse(node.next)
```

#### Binary tree

**Component**

```js
class node {
	data
	left  // self-reference
	right // self-reference
}
```

**hasData**

```js
// usage: hasData(this.root, data)
hasData(node, data)
  if (node == null) return false
	else if (node.data == data) return true
	else return hasData(node.left, data) || hasData(node.right, data)
```

**traversal**

```js
// traversal(this.head)
traversal(node)
  if (node != null)
		traversal(node.left)
	  DO_SOMETHING()
		traversal(node.right)
```

## Other topics

### Implementation issues

#### Wrapper function

- Definition:
    - Wrapper function: wraps an auxiliary function; optionally does some computation before calling the auxiliary function
		- Auxiliary function: does the actual recursion
- Uses:
    - Validate parameters
    - Initialize variables and allocate memory
    - Partial compute memorization
    - Handle exceptions
- Implementation concerns:
		- For languages support nested functions: the auxiliary function is nested inside the wrapper function and uses the shared scope
		- For languages doesn't support nested functions:
		    - The auxiliary function is called within the wrapper function
				- In an OO setup, the auxiliary function is mostly a private method

#### Short-circuiting the base case

- Definition:
		- Also called Arm's-length recursion
		- Short-circuiting checks the base case before making a recursive call
		- N.b. Standard recursion makes the recursive call then checks the base case
- Properties:
		- Short-circuiting breaks the chain of recursion *before* making the recursive call, thus saves time for an extra recursive call that only used to reach the base case
- Efficiency:
    - For O(n) algorithms, short-circuiting saves O(n) time, e.g. for tree traversals, avoids the calls on leaves
		- For O(1) algorithms, short-circuiting saves O(1) time

##### Example - Depth-first search

**Standard recursion**

```js
// usage: hasData(this.root, data)
hasData(node, data)
  if (node == null) return false
	else if (node.data == data) return true
	else return hasData(node.left, data) || hasData(node.right, data)
```

**Recursion with short-circuiting**

```js
// usage: hasData(this.root, data)
hasData(node, data)
  if (node == null) return false
	else return hasDataHelper(node, data)

hasDataHelper(node, data)
  if (node.data == data) return true
	else return (node.left && hasData(node.left, data)) ||
              (node.right && hasData(node.right, data))
```

#### Hybrid algorithm

- A hybrid function switches to a faster algorithm when input becomes small enough
- E.g. a merge sort implementation uses non-recursive insertion sort when data is small

### Language limitations

#### Recursion versus iteration

- Recursion:
    - With tail-call optimization (toc): as fast as iteration
		- Without toc:
		    - has overhead of function calls and call stack management
				- about call stack, it is used to trace the function calls for debugging purpose
- Iteration:
    - Preferred by most imperative languages to avoid call stack allocation
- Iteration with an explicit stack:
		- Simulates recursion
		- Mitigates the complexity of iteration-only implementation
		- Has the simplicity of recursion and efficiency of iteration
- Note:
    - Many imperative languages can support toc through third-party packages or compiler options. While languages lean to functional programming have native support of toc, such as Scala and JavaScript

#### Order of execution

**Recursion with tail call**

Uses less call stack space or, with optimization, does not use stack at all

```js
foo(x)
  DO(x)
	if (x < 2)
	  foo(x + 1)
```
```
foo(0)
DO(0)
	foo(0+1)
	DO(1)
		foo(1+1)
		DO(2)
```

**Recursion without tail call**

```js
foo(x)
	if (x < 2)
	  foo(x + 1)
	DO(x)
```
```
foo(0)
	foo(0+1)
		foo(1+1)
		DO(2)
	DO(1)
DO(0)
```

## References

- [Recursion (computer science)](https://en.wikipedia.org/wiki/Recursion_(computer_science))
- [Recursion](http://introcs.cs.princeton.edu/java/23recursion/)
- Wirth, N. (1985) Algorithms and Data Structures
- Knuth et al (1989) Concrete Mathematics. 2 ed
