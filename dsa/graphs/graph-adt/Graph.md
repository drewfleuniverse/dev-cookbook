# Graph ADT

```java
/* Class */
Graph
/* Methods */
+ Init(n:int): void
+ n(): int
+ e(): int
+ first(v:int): int
+ next(v:int, w:int): int
+ setEdge(i:int, j:int, wt:int): void
+ delEdge(i:int, j:int): void
+ isEdge(i:int, j:int): void
+ weight(i:int, j:int): int
+ setMark(v:int, val:int): void
+ getMark(v:int): int
```

## Notes
- `Init(n)`: Initialize a graph object of `n` vertices
- `n()`: Return number of vertices
- `e()`: Return number of edges
- `first(v)`: Return the first neighbor of `v`
- `next(v, w)`:
    - Return the neighbor of `v` next to `w`
    - E.g. next(1,0) returns 3 because the edge list of v_1 is [(1,0), (1,3), (1,4)]
- `setEdge(i, j, wt)`: Set edge (v_i, v_j) with weight `wt`
- `delEdge(i, j)`:
    - Delete edge (v_i, v_j)
    - For adjacent matrix, (v_i, v_j) will be zero
    - For adjacent list, the `j`-th list node at v_i will be removed
- `isEdge(i, j)`: Is (v_i, v_j) an edge?
- `weight(i, j)`: Return the weight at (v_i, v_j)
- `setMark(v, val)`:
    - Set mark `val` at vertex `v`
    - E.g. Set `val=VISITED` if visited, `val=UNVISITED` for reseting the vertex
- `getMark(v)`: Return the mark at vertex `v`
