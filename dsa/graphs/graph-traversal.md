# Graph Traversal


## Complexity

- Time:
    - DFS: Theta(|V| + |E|)


## Shaffer

### Pseudocode

#### Traverse wrapper function

```js
traverse(G)
  for (v = 0; v < G.n(); v++)
    G.setMark(v, UNVISITED)
  for (v = 0; v < G.n(); v++)
    if (G.getMark(v) == UNVISITED)
      DO_TRAVERSE(G, v) // DFS(), BFS()
```

#### Depth-first search

```js
DFS(G, v) // G: graph, v: vertex
  PRE_VISIT(G, v)
  G.setMark(v, VISITED)
  for (w = G.first(v); w < G.n(); w = G.next(v, w))
    if (G.getMark(w) == UNVISITED)
      DFS(G, w)
  POST_VISIT(G, v)
```

**Notes**


**Comments**
