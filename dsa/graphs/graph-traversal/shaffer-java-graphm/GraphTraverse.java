public class GraphTraverse {
  private static final int UNVISITED = 0;
  private static final int VISITED = 1;
  private StringBuffer out = new StringBuffer();

  public void traverseDFS(Graph G) {
    for (int v = 0; v < G.n(); v++)
      G.setMark(v, UNVISITED);
    for (int v = 0; v < G.n(); v++)
      if (G.getMark(v) == UNVISITED)
        DFS(G, v);
  }

  private void DFS(Graph G, int v) {
    preVisit(G, v);
    G.setMark(v, VISITED);
    for (int w = G.first(v); w < G.n(); w = G.next(v, w)) {
      if (G.getMark(w) == UNVISITED)
        DFS(G, w);
    }
    postVisit(G, v);
  }

  private void preVisit(Graph G, int v) { out.append(v + " "); }
  private void postVisit(Graph G, int v) { out.append(v + "`"); }

  public String toString() { return out.toString(); }
}
