import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Tests {
  @Test
  public void evaluate() {
    GraphM G = new GraphM(5);

    // Create a directed graph as ../img/shaffer-java/Figure\ 11.3
    G.setEdge(0, 1, 1);
    G.setEdge(0, 4, 2);
    G.setEdge(1, 3, 3);
    G.setEdge(2, 4, 4);
    G.setEdge(3, 2, 5);
    G.setEdge(4, 1, 6);

    GraphTraverse gt = new GraphTraverse();
    gt.traverseDFS(G);
    System.out.println(gt);
    // assertEquals(1, gm.getMark(2));
  }
}
