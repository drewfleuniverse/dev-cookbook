# GraphL


## GraphL Class

```java
/* Class */
GraphL -> Graph
/* Fields */
- vertex: GraphList[]
- numEdge: int
- Mark: int[]
/* Methods */
+ GraphL(n:int)
+ Init(n:int): void
+ n(): int
+ e(): int
+ first(v:int): int
+ next(v:int, w:int): int
+ setEdge(i:int, j:int, wt:int): void
+ delEdge(i:int, j:int): void
+ isEdge(i:int, j:int): void
+ weight(i:int, j:int): int
+ setMark(v:int, val:int): void
+ getMark(v:int): int
```

### GraphL comments

- `vertex`: List of vertices
- `Mark`: Mark a vertex if visited
- `next()`:
    - Line `Edge e = null;`:
        - is a flag-like variable
        - `e == null` means (v,w) is not an edge or (v, w) doesn't have a next edge
        - `e != null` means has next edge
- `setEdge(i, j, wt)`:
    - Line `if (vertex[i].getValue().vertex() > j)`: because the insertion of `LList` inserts before the current node
- `isEdge(i, j)`:
    - Check if the current node of `vertex[i]` is `j`, if not so lucky traverse the list from beginning
    - As a side effect, if (i,j) is an edge, `vertex[i]` stops at position `j` after execution
    - `isEdge()` is the most important method of `GraphL`, it is used in `next()`, `setEdge()`, `delEdge()`, and `weight()`
- `weight(i, j)`:
    - Line `return 0;`: due to list doesn't contain node without weight as GraphM does, must explicitly return 0

## GraphList Class

```java
/* Class */
GraphList -> LList<Edge>
/* Methods */
+ currLink(): Link<Edge>
+ setCur(c:Link<Edge>): void
```

## Edge Class

```java
/* Class */
Edge
/* Fields */
- vertex: int
- weight: int
/* Methods */
+ Edge(v:int, w:int)
+ vertex(): int
+ weight(): int
```
