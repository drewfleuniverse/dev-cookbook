class Edge {
  private int vertex;
  private int weight;

  Edge(int v, int w) {
    vertex = v;
    weight = w;
  }

  public int vertex() { return vertex; }
  public int weight() { return weight; }
}
