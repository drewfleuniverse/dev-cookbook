public class GraphL implements Graph {
  private GraphList[] vertex;
  private int numEdge;
  private int[] Mark;

  public GraphL(int n) { Init(n); }

  public void Init(int n) {
    vertex = new GraphList[n];
    for (int i = 0; i < vertex.length; i++)
      vertex[i] = new GraphList();
    numEdge = 0;
    Mark = new int[n];
  }

  public int n() { return Mark.length; }
  public int e() { return numEdge; }

  public int first(int v) {
    if (vertex[v].length() == 0)
      return Mark.length;
    vertex[v].moveToStart();
    Edge e = vertex[v].getValue();
    return e.vertex();
  }

  public int next(int v, int w) {
    Edge e = null;
    if (isEdge(v, w)) {
      vertex[v].next();
      e = vertex[v].getValue();
    }
    if (e != null)
      return e.vertex();
    return Mark.length;
  }

  public void setEdge(int i, int j, int wt) {
    assert wt != 0 : "Cannot set weight to 0";
    Edge e = new Edge(j, wt);
    if (isEdge(i, j)) {
      vertex[i].remove();
      vertex[i].insert(e);
    } else {
      numEdge++;
      for (vertex[i].moveToStart();
           vertex[i].currPos() < vertex[i].length();
           vertex[i].next())
        if (vertex[i].getValue().vertex() > j) break;
      vertex[i].insert(e);
    }
  }

  public void delEdge(int i, int j) {
    if (isEdge(i, j)) {
      vertex[i].remove();
      numEdge--;
    }
  }

  public boolean isEdge(int v, int w) {
    Edge e = vertex[v].getValue();
    if ((e != null) && (e.vertex() == w)) return true;
    for (vertex[v].moveToStart();
         vertex[v].currPos() < vertex[v].length();
         vertex[v].next())
      if (vertex[v].getValue().vertex() == w)
        return true;
    return false;
  }

  public int weight(int i, int j) {
    if (isEdge(i, j)) return vertex[i].getValue().weight();
    return 0;
  }

  public void setMark(int v, int val) { Mark[v] = val; }
  public int getMark(int v) { return Mark[v]; }
}
