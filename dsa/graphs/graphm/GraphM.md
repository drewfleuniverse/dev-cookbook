# GraphM

## GraphM Class

```java
/* Class */
GraphM -> Graph
/* Fields */
- Matrix: int[][]
- numEdge: int
- Mark: int[]
/* Methods */
+ GraphL(n:int)
+ Init(n:int): void
+ n(): int
+ e(): int
+ first(v:int): int
+ next(v:int, w:int): int
+ setEdge(i:int, j:int, wt:int): void
+ delEdge(i:int, j:int): void
+ isEdge(i:int, j:int): void
+ weight(i:int, j:int): int
+ setMark(v:int, val:int): void
+ getMark(v:int): int
```

### Comments

- `Matrix`: an adjacency matrix of an directed graph
    - `Matrix[v][w]`:
        - If empty, no edge at (v,w)
        - If not empty:
            - has edge (v,w)
            - `Matrix[v][w]` stores the weight of (v,w)
