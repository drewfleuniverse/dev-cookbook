public class GraphM implements Graph {
  private int[][] Matrix;
  private int numEdge;
  private int[] Mark;

  public GraphM(int n) { Init(n); }

  public void Init(int n) {
    Matrix = new int[n][n];
    numEdge = 0;
    Mark = new int[n];
  }

  public int n() { return Mark.length; }
  public int e() { return numEdge; }

  public int first(int v) {
    for (int i = 0; i < Mark.length; i++)
      if (Matrix[v][i] != 0) return i;
    return Mark.length;
  }

  public int next(int v, int w) {
    for (int i = w+1; i < Mark.length; i++)
      if (Matrix[v][i] != 0) return i;
    return Mark.length;
  }

  public void setEdge(int i, int j, int wt) {
    assert wt != 0 : "Cannot set weight to 0";
    if (Matrix[i][j] == 0) numEdge++;
    Matrix[i][j] = wt;
  }

  public void delEdge(int i, int j) {
    if (Matrix[i][j] != 0) numEdge--;
    Matrix[i][j] = 0;
  }

  public boolean isEdge(int i, int j) { return Matrix[i][j] != 0; }
  public int weight(int i, int j) { return Matrix[i][j]; }
  public void setMark(int v, int val) { Mark[v] = val; }
  public int getMark(int v) { return Mark[v]; }
}
