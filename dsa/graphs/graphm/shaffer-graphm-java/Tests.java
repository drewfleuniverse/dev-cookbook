import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Tests {
  @Test
  public void evaluate() {
    GraphM G = new GraphM(5);

    // Create a directed graph as ../img/shaffer-java/Figure\ 11.3
    G.setEdge(0, 1, 1);
    G.setEdge(0, 4, 2);
    G.setEdge(1, 3, 3);
    G.setEdge(2, 4, 4);
    G.setEdge(3, 2, 5);
    G.setEdge(4, 1, 6);
    assertEquals(1, G.first(0));
    assertEquals(4, G.next(0, 1));
    assertEquals(3, G.first(1));
    assertEquals(5, G.n());
    assertEquals(6, G.e());
    assertEquals(5, G.weight(3, 2));
    assertEquals(true, G.isEdge(4, 1));

    // Delete some
    G.delEdge(2, 4);
    G.delEdge(3, 2);
    G.delEdge(4, 1);
    assertEquals(3, G.e());
    assertEquals(false, G.isEdge(4, 1));
    assertEquals(0, G.weight(3, 2));

    G.setMark(2, 1);
    assertEquals(1, G.getMark(2));
  }
}
