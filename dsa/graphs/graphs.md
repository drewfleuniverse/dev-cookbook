# Graphs


## Introduction

### Uses
- Modeling network
- Mapping locations
- Modeling flow capacities in transportation networks
- AI problem solving
- Finding an order for finishing subtasks in a complex activity
- Modeling organizational relationships

### Overview
- Representations:
    - Adjacency matrix
    - Adjacency list
- Traversal algorithms:
    - Depth-first search
    - Breadth-first search


## Terminology

### Notations
- G = (V, E):
    - G: Graph
    - V: Set of vertices
    - E: Set of edges
- |V|: Number of vertices
- |E|: Number of edges

### Amount of edges
- Sparse: A few edges
- Dense: Many edges
- Complete: All possible edges

### Types
- Directed graph / digraph
- Undirected graph
- Labeled graph

### More Terms
- Adjacent vertices:
    - Two vertices forms an edge are adjacent
    - Also called neighbors
- Edge:
    - (U,V): given two vertices U and V, if they are connected
    - An edge can have cost / weight
        - Such graphs are said to be weighted
- Path:
    - Length: Number of edges
    - Simple: All vertices on the path are distinct
    - Cycle: A path begins and ends at the same vertex, length >= 3
    - Simple cycle: Just a cycle has distinct vertices except the first and last one
- Subgraph: some vertices and their edges in a graph
- Acyclic:
    - A graph without cycles
    - DAG: Directed acyclic graph

### Terms for Undirected Graph
- Connected:
    - Connected graph: when a graph has a path contains all vertices
    - Connected components: when subgraph is connected
- Free tree: Connected and no simple cycles


## Representations

### Adjacency Matrix
- |V|x|V| array:
  - If |V| = n, i and j have range [0, n-1]
      - `V[i][j] != 0`:
        - Means an edge
        - Alternatively can contain edge weight
      - `V[i][j] == 0`: not an edge

#### Complexity
- Time: Theta(|V|^2)
- Space: Theta(|V|^2)
    - Good for dense and complete graphs due to no overhead of pointers
- Notes:
    - Time:
        - Likely to look each array elements
        - Bad for sparse graphs
    - Space: good for dense and complete graphs due to no overhead of pointers

### Adjacency List
- |V| array:
    - Each `V[i]` contains a list of edges
    - Each edge node:
        - Contains corresponding vertex
        - Can also have fields such as weight

#### Complexity
- Time: Theta(|V|+|E|)
- Space: Theta(|V|+|E|)
- Notes:
    - Time: List only contains existing edges
    - Space: Good for sparse graphs
