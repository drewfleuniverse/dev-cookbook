const cl = s => console.log(s);


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


const MAX = 100;


const hash = function (key) {
  assert(
    typeof key === 'number'
    && (key > 0 && key < 1)
  );

  return Math.floor(key * MAX);
}


cl(hash(0.123) === 12)
cl(hash(0.007) === 0)
cl(hash(0.999) === 99)
