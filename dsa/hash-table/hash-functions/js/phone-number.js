const cl = s => console.log(s);


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


const MAX = 100;


const hash = function (key) {
  assert(typeof key === 'string');
  const phone = key.split('-').map(s => Number(s));
  const prime = 73;
  let hash = (phone[0] * prime + phone[1]) % MAX;
  hash = (hash * prime + phone[2]) % MAX;

  return hash;
}


cl(hash('000-000-0000') === 0);
cl(hash('001-001-0001') === 3);
cl(hash('123-456-7890') === 45);
