const cl = s => console.log(s);


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


const MAX = 100;


const hash = function (key) {
  assert(
    typeof key === 'number'
    && (key >= 0 && key < Infinity)
  );

  return key % MAX;
}


cl(hash(42) === 42)
cl(hash(123) === 23)
cl(hash(1024) === 24)
