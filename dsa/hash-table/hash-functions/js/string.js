const cl = s => console.log(s);


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


const MAX = 100;


const hash = function (key) {
  assert(typeof key === 'string');
  const prime = 7;
  let hash = 0;
  for (const ch of key) {
    hash = (prime * hash + ch.charCodeAt()) % MAX;
  }

  return hash;
}


cl(hash('') === 0)
cl(hash('foo') === 86)
cl(hash('bar') === 95)
cl(hash('42') === 14)
