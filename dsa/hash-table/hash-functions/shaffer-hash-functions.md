## Example1

### Pseudo code
```js
// Hash table size is 16 slots
h(x)
  return (x%16)
```

### Comments
- The resulting hash table has 16 slots
- %16 yields values in [0,15]
- Transform input number from decimal to binary, the last 4 bits of a binary
  number has values [0000, 1111], it is [0, (2^4)-1] = [0, 16-1] = [0, 15] in
  its decimal form
- Thus the distribution of the last 4 bis of input number determines the
  performance of h()


## Example2

### Pseudo code
```js
h(s, M)
  c = stringToCharArray(s)
  for (i = 0, sum = 0; i < s.length; i++)
    sum += c[i]
  return sum % M

stringToCharArray(x)
  // return string x as char array
```
### Comments
- This is a basic example of folding approach by summing up the ASCII code of
  each character
- The resulting hash table has M slots
- sum should be reasonably larger than M to produce good distribution

## Example3

### Code
```java
long sfold(String s, int M) {
  int intLength = s.length() / 4;
  long sum = 0;
  for (int j = 0; j < intLength; j++) {
    char c[] = s.substring(j*4,(j*4)+4).toCharArray();
    long mult = 1;
    for (int k = 0; k < c.length; k++) {
      sum += c[k] * mult;
      mult *= 256;
    }
  }
  char c[] = s.substring(intLength * 4).toCharArray();
  long mult = 1;
  for (int k = 0; k < c.length; k++) {
    sum += c[k] * mult;
    mult *= 256;
  }
  return (Math.abs(sum) % M);
}
```
### Comments
- This is an improved example of folding approach
- Code:
  - Suppose s.length= 15, 15=12+3
  - Loop1: Sum up string sections [0,4], [5,8], [9,12]
  - Loop2: Sum up string sections [13,15]
