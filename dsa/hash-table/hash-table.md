# Hash Table


## Time complexity

**Average**

- Access: O(1)
- Search: O(1)
- Insertion: O(1)
- Deletion: O(1)

**Worst**

- Access: O(n)
- Search: O(n)
- Insertion: O(n)
- Deletion: O(n)


## Space complexity

- Worst: O(n)


## Notes

**Time**

- Access and search is the same operation in hash table
