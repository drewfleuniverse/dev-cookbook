const cl = i => console.log(i)
const js = i => JSON.stringify(i)

//LinearProbingHashST
const assert = (cond, msg='') => {
  if (!cond) {
    throw new Error(msg);
  }
}


const max = Symbol('max');
const size = Symbol('size');
const keys = Symbol('keys');
const vals = Symbol('vals');
const hash = Symbol('hash');
const hashString = Symbol('hashString');
const resize = Symbol('resize');


class HashTable {
  constructor(max_) {
    this[max] = max_;
    this[size] = 0;
    this[keys] = new Array(max_);
    this[vals] = new Array(max_);
  }

  size() {
    return this[size];
  }

  isEmpty() {
    return this[size] === 0;
  }

  has(key) {
    assert(typeof key === 'string');
    return this.get(key) !== undefined;
  }

  put(key, val) {
    assert(typeof key === 'string');
    if (val === undefined) {
      this.delete(key);
      return;
    }

    if (this[size] >= Math.floor(this[max]/2)) {
      this[resize](2 * this[max]);
    }

    let i = this[hash](key);

    // Collision resolution - if key already exists
    while (this[keys][i] !== undefined) {
      if (this[keys][i] === key) {
        this[vals][i] = val;
        return;
      }
      i = (i + 1) % this[max];
    }
    // If key doesn't exist
    this[keys][i] = key;
    this[vals][i] = val;
    this[size]++;
  }

  delete(key) {
    assert(typeof key === 'string');
    if (!this.has(key)) {
      return;
    }

    let i = this[hash](key);
    while (key !== this[keys][i]) {
      i = (i + 1) % this[max];
    }

    this[keys][i] = undefined;
    this[vals][i] = undefined;
    this[size]--;

    // Rehash
    i = (i + 1) % this[max];
    while (this[keys][i] !== undefined) {
      const tmpKey = this[keys][i];
      const tmpVal = this[vals][i];

      this[keys][i] = undefined;
      this[vals][i] = undefined;
      this[size]--;

      this.put(tmpKey, tmpVal);
      i = (i + 1) % this[max];
    }

    if (
      this[size] > 0
      && this[size] <= Math.floor(this[max]/8)
    ) {
      this[resize](Math.floor(this[max]/2));
    }
  }

  get(key) {
    let val = undefined;

    for (
      let i = this[hash](key);
      this[keys][i] !== undefined;
      i = (i + 1) % this[max]
    ) {
      if (this[keys][i] === key) {
        val = this[vals][i]
      }
    }

    return val;
  }

  [hash](key) {
    assert(typeof key === 'string');
    const hashCode = this[hashString](key);

    return hashCode % this[max];
  }

  [hashString](str) {
    // Using overkill java implementation
    const primeNum = 31;
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash += Math.pow(
        str.charCodeAt(i)*primeNum,
        str.length-(i+1)
      );
      // bitwise AND in JavaScript converts operands
      // into 32bit integer
      hash = hash & hash;
    }

    return hash;
  }

  [resize](max_) {
    const tmp = new HashTable(max_);

    for (let i = 0; i < this[max]; i++) {
      if (this[keys][i] !== undefined) {
        tmp.put(this[keys][i], this[vals][i]);
      }
    }

    this[keys] = tmp[keys];
    this[vals] = tmp[vals];
    this[max] = max_;
  }

  // [Symbol.iterator]() {
  //
  // }
}


const h = new HashTable(5);


h.put('foo', 1990);
h.put('bar', 2001);
cl(h.get('foo') === 1990);
cl(h.get('bar') === 2001);
cl(h.size() === 2);
cl(h[max] === 5);

h.put('baf', 3900);
cl(h.get('baf') === 3900);
cl(h.size() === 3);
cl(h[max] === 10);

h.delete('baf');
cl(h.get('baf') === undefined);
cl(h.size() === 2);
cl(h[max] === 10);

h.delete('bar');
cl(h.get('bar') === undefined);
cl(h.size() === 1);
cl(h[max] === 5);
