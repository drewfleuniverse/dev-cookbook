public class HashTable<K extends Comparable<? super K>, E> {
  private int M;
  private KVpair<K,E>[] HT;

  @SuppressWarnings("unchecked")
  HashTable(int m) {
    M = m;
    HT = (KVpair<K,E>[])new KVpair[M];
  }

  public void hashInsert(K k, E r) {
    int p, h;
    p = h = h(k);
    for (int i = 1; HT[p] != null; i++) {
      p = (h + p(k, i)) % M;
      assert (HT[p] == null) || (HT[p].key().compareTo(k) != 0) : "Duplicates not allowed";
    }
    HT[p] = new KVpair<K,E>(k, r);
  }

  public E hashSearch(K k) {
    int p, h;
    p = h = h(k);
    for (int i = 1; (HT[p] != null) && (HT[p].key().compareTo(k) != 0); i++) {
      p = (h + p(k, i)) % M;
    }
    if (HT[p] == null) return null;
    else return HT[p].value();
  }

  public E hashRemove(K k) {
    int p, h;
    p = h = h(k);
    for (int i = 1; (HT[p] != null) && (HT[p].key().compareTo(k) != 0); i++) {
      p = (h + p(k, i)) % M;
    }
    if (HT[p] == null) return null;
    else {
      E temp = HT[p].value();
      HT[p] = null;
      return temp;
    }
  }

  private int h(K key) { return M-1; }
  private int p(K key, int slot) { return slot; }
}
