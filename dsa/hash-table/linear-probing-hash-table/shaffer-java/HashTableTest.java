import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class HashTableTest {
  @Before
  public void init() {

  }

  @Test
  public void evaluateHashTable() {
    int M = 5;
    HashTable<Integer, String> hashTable = new HashTable<>(M);
    hashTable.hashInsert(new Integer(8), "Eight");
    hashTable.hashInsert(new Integer(42), "Fourty-Two");
    hashTable.hashInsert(new Integer(19), "Nineteen");
    hashTable.hashInsert(new Integer(13), "Thirteen");
    // N.B. HashTable max size is M-1
    assertEquals("Eight", hashTable.hashSearch(new Integer(8)));
    assertEquals("Fourty-Two", hashTable.hashSearch(new Integer(42)));
    assertEquals("Nineteen", hashTable.hashRemove(new Integer(19)));
    assertEquals(null, hashTable.hashSearch(new Integer(19)));
  }
}
