class KVpair<K, E> {
  private K key;
  private E element;

  KVpair() { key = null; element = null; }
  KVpair(K k, E e) { key = k; element = e; }

  public K key() { return key; }
  public E value() { return element; }
}
