# Shaffer - Collision Resolution


## Introduction

Two collision resolution technique classes:
- Open hashing (separate hashing): collisions are stored outside the table
- Closed hashing: collisions are stored in the table


## Open Hashing (aka closed addressing, separate chaining)

Simple form:
- Each slot is the head of a linked list
- Records within a slot’s list can be ordered in several ways:
  - By insertion order
  - By frequency-of-access order
    - The above two require search through the list if record is not in the
      list
  - By key value order
    - If find key >= the key to find, the search can stop
- Best for in-memory implementation, e.g. in-memory linked list  
- Similar to binsort for placing records in a 'bin'


## Closed Hashing (aka open addressing)


h():
- If slot empty, returns home positions
- If the bucket is not full, returns another empty slot position in the bucket
- If the bucket is full, return a slot in overflow bucket

Forms:
- Bucket Hashing
- Linear Probing

### Closed Hashing - Bucket Hashing:
- For table size M, divide table into B buckets, thus each bucket has B/M
  slots
- The hash table has:
  - Regular buckets
  - An overflow bucket
- Best for implementing hash table stored on disk. Only read entire bucket
  into memory when search or insertion needs to perform

### Closed Hashing - Linear Probing
- Prob function p:
  - Generates prob sequence, a sequence of slots to be filled
  - The first slot in the sequence is home position
- p(K,i) = i
  - K: key value
  - i: i-th slot after home position
- i-th slot position:
  - (h(K) + p(K,i)) % M
  - (h(K) + i) % M
- Cons: the worst collision resolution methods due to tendency to cluster
  records known as primary clustering, which tends to merge small clusters into
  big clusters

### Closed Hashing - Probing Variations
- Linear probing with skipped slots:
  - p(k,i) = ci
  - i-th slot position:
    - (h(K) + p(K,i)) % M
    - (h(K) + ci) % M
  - Requirement: Constant c must be relatively prime toM to generate a linear
    probing sequence
  - Cons: does not solve the problem of primary clustering that visits all slots
    in the table (that is, c and M must share no factors)
- Pseudo-random probing:
  - p(k,i) = Perm[i-1]
    - Perm is an array of length M − 1 containing a random permutation of the
      values from 1 to M − 1
- Quadratic probing:
  - p(K,i) = c1 * i^2 + c2 * i + c3
  - To simplify: p(K,i) = i^2
- Downside of pseudo-random probing and quadratic probing:
  - p() generates home positions and the sequence is the same if p(K1) and p(K2)
    are hashed to the same home position, this is called secondary clustering
- Linear probing with double hashing:
  - p(K,i) = i * h2(K)
  - I.e. a constant i with a second hash function
  - Implementing h2():
    - M to be prime, 1 <= h2(K) <= M-1
    - M = 2^m, 1 <= h2(k) <= (2^m)-1, i.e. odd values

### Closed Hashing - Cost
- Load factor: alpha = N/M
- Probability of 1 collision at the home position: alpha
- Probability of 2 collisions at both the home position and the next slot:
  - N * (N-1) / M * (M-1)
- Probability of i collisions:
  - N * (N-1) ... * (N-i+1) / M * (M-1) ... * (M-i+1)
  - If N and M are large: (N/M)^i


## Perfect Hashing


## Implementation Concerns

### Implementation Concerns - Hashing Method Choices
- If a search is made to a record that is not in its home position, a
  self-organizing list heuristic can be used, e.g.:
  - Open hashing: use self-organizing linked list
  - Closed hashing: linear probing which swaps frequently accessed record
    forward and less otherwise if those records are not in its home position

### Implementation Concerns - Deletion
- On tombstone:
  - Quote from Shaffer[2012]:
    - The tombstone indicates that a record once occupied the slot but does so
      no longer.
    - If a tombstone is encountered when searching along a probe sequence, the
      search procedure continues with the search. When a tombstone is
      encountered during insertion, that slot can be used to store the new
      record. However, to avoid inserting duplicate keys, it will still be necessary for the search procedure to follow the probe sequence until a
      truly empty position has been found, simply to verify that a duplicate is
      not in the table. However, the new record would actually be inserted into
      the slot of the first tombstone encountered.
