# List ADT

```java
/* Class */
List<E>
/* Methods */
+ clear(): void
+ insert(E item): void
+ append(E item): void
+ remove(): E
+ moveToStart(): void
+ moveToEnd(): void
+ prev(): void
+ next(): void
+ length(): int
+ currPos(): int
+ moveToPos(int pos): void
+ getValue(): E
```

## Notes

- Defining current position, assumes there's a `currPos` field:
    - Zero-based position index
    - `currPos=0`: indicates the first element
    - `currPos=list.length()`: indicates null, i.e. one position to the right of last element
