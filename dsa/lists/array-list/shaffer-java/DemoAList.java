class DemoAList {
  public static void main(String[] args) {
    AList<Integer> a = new AList<>(5);

    a.insert(1);
    a.insert(2);
    a.insert(3);
    a.append(4);
    a.append(5);
    System.out.println(a);

    a.moveToEnd();
    a.remove();
    System.out.println(a);

    a.prev();
    a.remove();
    System.out.println(a);

    a.moveToStart();
    a.remove();
    System.out.println(a);

    a.next();
    System.out.println(a);

    a.clear();
    a.remove();
    System.out.println(a);
    /*
    (curr)[3][2][1][4][5]: listSize=5, curr=0
    [3][2][1][4][5](curr): listSize=5, curr=5
    [3][2][1][4](curr)[ ]: listSize=4, curr=4
    (curr)[2][1][4][ ][ ]: listSize=3, curr=0
    [2](curr)[1][4][ ][ ]: listSize=3, curr=1
    (curr)[ ][ ][ ][ ][ ]: listSize=0, curr=0
    */
  }
}
