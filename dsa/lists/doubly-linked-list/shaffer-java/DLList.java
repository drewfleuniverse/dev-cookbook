/*
- In a singly linked list, tail points to the last node;
- In a doubly linked list, tail points to an empty node.
- Using the same definition of curr in other shaffer's lists
  - curr.next() is the current node
  - curr can point to the last node
    - SLL: curr = tail, curr.next() = tail.next() = null
    - DLL: curr = tail.prev(), curr.next() = tail
  - In a DLL, curr is forbidden to point at tail
*/
class DLList<E> implements List<E> {
  private Link<E> head;
  private Link<E> tail;
  private Link<E> curr;
  private int cnt;

  DLList(int size) { this(); }
  DLList() {
    curr = head = new Link<E>(null, null);
    tail = new Link<E>(head, null);
    head.setNext(tail);
    cnt = 0;
  }

  public void clear() {
    head = curr = new Link<E>(null, null);
    tail = new Link<E>(head, null);
    head.setNext(tail);
    cnt = 0;
  }

  public void insert(E it) {
    curr.setNext(new Link<E>(it, curr, curr.next()));
    curr.next().next().setPrev(curr.next());
    cnt++;
  }

  public void append(E it) {
    tail.setPrev(new Link<E>(it, tail.prev(), tail));
    tail.prev().prev().setNext(tail.prev());
    cnt++;
  }

  public E remove() {
    if (curr.next() == tail) return null;
    E it = curr.next().element();
    curr.next().next().setPrev(curr);
    curr.setNext(curr.next().next());
    cnt--;
    return it;
  }

  public void moveToStart() { curr = head; }

  public void moveToEnd() { curr = tail.prev(); }

  public void prev() { if (curr != head) curr = curr.prev(); }

  public void next() { if (curr != tail.prev()) curr = curr.next(); }
  public int length() { return cnt; }
  public int currPos() {
    Link<E> tmp = head;
    int i;
    for (i=0; tmp != curr; i++)
      tmp = tmp.next();
    return i;
  }

  public void moveToPos(int pos) {
    assert (pos>=0) && (pos<=cnt) : "Pos out of range";
    curr = head;
    for (int i = 0; i < pos; i++) curr = curr.next();
  }

  public E getValue() {
    if (curr.next() == tail) return null;
    return curr.next().element();
  }

  /*
  Output as:
  3 2 1
  ^
  Current pos: 0 Length: 3
  */
  public String toString() {
    int tmpPos = currPos();
    int len = length();
    StringBuffer buf = new StringBuffer();

    moveToStart();
    for (int i=0; i<len; i++) {
      buf.append(getValue());
      buf.append(" ");
      next();
    }
    buf.append("\n");
    moveToPos(tmpPos);

    for (int i=0; i<len; i++) {
      if (i != currPos()) buf.append("  ");
      else buf.append("^ ");
    }
    buf.append("\n");

    buf.append("Current pos: ");
    buf.append(currPos());
    buf.append(" Length: ");
    buf.append(length());

    return buf.toString();
  }
}
