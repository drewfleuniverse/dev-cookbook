class Link<E> {
  private E element;
  private Link<E> next;
  private Link<E> prev;

  Link(E it, Link<E> p, Link<E> n){
    element = it; prev = p; next = n;
  }
  // For head and tail nodes
  Link(Link<E> p, Link<E> n) { prev = p; next = n; }

  Link<E> next() { return next; }
  Link<E> setNext(Link<E> nextval) { return next = nextval; }
  Link<E> prev() { return prev; }
  Link<E> setPrev(Link<E> prevval) { return prev = prevval; }
  E element() { return element; }
  E setElement(E it) { return element = it; }
}
