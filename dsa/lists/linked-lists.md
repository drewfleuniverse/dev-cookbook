# Link Lists


## Method Names

- Access: N/A
- Search: N/A
- Insertion:
- Deletion:


## Time complexity

**Average**

- Access: O(n)
- Search: O(n)
- Insertion: O(1)
- Deletion: O(1)

**Worst**

- Access: O(n)
- Search: O(n)
- Insertion: O(1)
- Deletion: O(1)


## Space complexity

- Worst: O(n)


## Notes

**Cons**

- No direct access to elements

**Time**


## Application Examples
