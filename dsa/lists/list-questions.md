# Linked List Questions

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Linked List Questions](#linked-list-questions)
	- [Rearranging Linked List](#rearranging-linked-list)
					- [Write a recursive function to reverse a singly-linked list](#write-a-recursive-function-to-reverse-a-singly-linked-list)
					- [Write a recursive function to reverse a singly-linked list](#write-a-recursive-function-to-reverse-a-singly-linked-list)
	- [Find Element in Linked List](#find-element-in-linked-list)

<!-- /TOC -->

## Rearranging Linked List

###### Write a recursive function to reverse a singly-linked list
```
l1->l2->l3->l4->l5->null
head = l1
```
```js
reverse(head)
  if (head == null || head.next == null)
    return head
  headNext = head.next // store next node in each step
  head.next = null // isolate curr
  tail = reverse(temp) // the last node to be returned
  headNext.next = head // point headNext to head in each step
  return tail
```

###### Write a recursive function to reverse a singly-linked list


## Find Element in Linked List
