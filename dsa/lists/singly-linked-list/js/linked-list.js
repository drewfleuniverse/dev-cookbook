const cl = s => console.log(s)


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}



class Node {
  constructor(data, next=null) {
    this.data = data;
    this.next = next;
  }
}



const _head = Symbol('_head');

class LinkedList {
  constructor() {
    this[_head] = null;
  }

  addFirst(data) {
    this[_head] = new Node(data, this[_head]);
  }

  getFirst() {
    assert(!this.isEmpty());
    return this[_head].data;
  }

  removeFirst() {
    const data = this.getFirst();
    this[_head] = this[_head].next;

    return data;
  }

  addLast(data) {
    if (this[_head] === null) {
      this.addFirst(data);
      return;
    }

    let tmp = this[_head];
    while (tmp.next !== null) {
      tmp = tmp.next;
    }

    tmp.next = new Node(data);
  }

  getLast() {
    assert(!this.isEmpty());
    let tmp = this[_head];
    while (tmp.next !== null) {
      tmp = tmp.next;
    }

    return tmp.data;
  }

  clear() {
    this[_head] = null;
  }

  isEmpty() {
    return this[_head] === null;
  }

  constains(data) {
    for (const key of this) {
      if (key === data) {
        return true;
      }
    }

    return false;
  }

  get(pos) {
    if (this[_head] === null) {
      throw new Error('Index out of bound');
    }

    let tmp = this[_head];
    let i = 0;
    while (
      tmp !== null
      && i < pos
    ) {
      tmp = tmp.next;
      i++;
    }

    if (tmp === null) {
      throw new Error('Index out of bound');
    }

    return tmp.data;
  }

  insertAfter(key, data) {
    let tmp = this[_head];
    while (
      tmp !== null
      && tmp.data !== key
    ) {
      tmp = tmp.next;
    }

    if (tmp !== null) {
      tmp.next = new Node(data, tmp.next);
    }
  }

  insertBefore(key, data) {
    if (this.isEmpty()) {
      return;
    }

    if (this[_head].data = key) {
      this.addFirst(data);
      return;
    }

    let prev = null;
    let cur = this[_head];
    while (
      cur !== null
      && cur.data !== key
    ) {
      prev = cur;
      cur = cur.next;
    }

    if (cur !== null) {
      prev.next = new Node(data, prev.next);
    }
  }

  remove(key) {
    assert(!this.isEmpty());

    if (this[_head].data === key) {
      this[_head] = this[_head].next;
      return;
    }

    let prev = null;
    let cur = this[_head];
    while (
      cur !== null
      && cur.data !== key
    ) {
      prev = cur;
      cur = cur.next;
    }

    if (cur === null) {
      throw new Error(`Key not found`);
    }

    prev.next = cur.next;
  }

  copyQuadratic() {
    const copy = new LinkedList();
    let tmp = this[_head];
    while (tmp !== null) {
      copy.addLast(tmp.data);
      tmp = tmp.next;
    }

    return copy;
  }

  copyLinear() {
    const copy = new LinkedList();
    let tmp = this[_head];
    while (tmp !== null) {
      copy.addFirst(tmp.data);
      tmp = tmp.next;
    }

    return copy.reverse();
  }

  copyLinear_() {
    const copy = this.reverse();

    copy.reverseInternal();

    return copy;
  }

  // O(n)
  reverse() {
    const rev = new LinkedList();
    let tmp = this[_head];
    while (tmp !== null) {
      rev.addFirst(tmp.data);
      tmp = tmp.next;
    }

    return rev;
  }
  reverseInternal() {
    let prev = null;
    let curr = this[_head];

    while(curr !== null) {
      const temp = curr.next;
      curr.next = prev;
      prev = curr;
      curr = temp;
    }

    this[_head] = prev;
  }

  copyLinear2() {
    const copy = new LinkedList();

    if (this.isEmpty()) {
      return copy;
    }

    copy.addFirst(this.getFirst());

    let tmp = this[_head];
    let tmpCopy = copy[_head];
    while (tmp.next !== null) {
      tmpCopy.next = new Node(tmp.next.data, null);
      tmp = tmp.next;
      tmpCopy = tmpCopy.next;
    }

    return copy;
  }

  copyLinear3() {
    const copy = new LinkedList();

    if (this.isEmpty()) {
      return copy;
    }

    let tmp = this[_head];
    let tmpCopy = null;
    while (tmp !== null) {
      if (copy.isEmpty()) {
        copy.addFirst(tmp.data);
        tmpCopy = copy[_head];
        tmp = tmp.next;
      } else {
        tmpCopy.next = new Node(tmp.data, null);
        tmpCopy = tmpCopy.next;
        tmp = tmp.next;
      }
    }

    return copy;
  }

  toString() {
    return [...this].toString();
  }

  *[Symbol.iterator]() {
    let tmp = this[_head];
    while(tmp !== null) {
      yield tmp.data;
      tmp = tmp.next;
    }
  }
}


let l, r;


l = new LinkedList();

l.addFirst('foo');
cl(l.isEmpty() === false);

l.addFirst('bar');
cl(l.toString() === 'bar,foo');

cl(l.getFirst() === 'bar');
cl(l.removeFirst() === 'bar');
cl(l.toString() === 'foo');

l.addLast(42);
cl(l.getLast() === 42);
cl(l.toString() === 'foo,42');

cl(l.constains('foo') === true);

l.insertAfter('foo', 'bar');
l.insertBefore('foo', 'foobar');
cl(l.toString() === 'foobar,foo,bar,42')

cl(l.get(3) === 42);

l.remove('foobar')
cl(l.toString() === 'foo,bar,42')

l.clear();
cl(l.isEmpty() === true)

l = new LinkedList();
l.addLast(1);
l.addLast(2);
l.addLast(3);
cl(l.toString() === '1,2,3');

r = l.copyQuadratic();
cl(r.toString() === '1,2,3');

r = l.copyLinear();
cl(r.toString() === '1,2,3');

r = l.copyLinear_();
cl(r.toString() === '1,2,3');

r = l.copyLinear2();
cl(r.toString() === '1,2,3');

r = l.copyLinear3();
cl(r.toString() === '1,2,3');


l = new LinkedList();
l.addLast(1);
l.addLast(2);
l.addLast(3);
cl(l.toString() === '1,2,3');
l.reverseInternal();
cl(l.toString() === '3,2,1');
