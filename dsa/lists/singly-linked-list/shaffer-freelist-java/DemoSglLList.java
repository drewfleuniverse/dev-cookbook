class DemoSglLList {
  public static void main(String[] args) {
    LList<Integer> list = new LList<>();

    list.insert(3);
    list.insert(2);
    list.insert(1);
    list.append(4);
    list.append(5);
    System.out.println(list);
    /*
    1 2 3 4 5
    ^
    Current pos: 0 Length: 5
    */

    list.moveToEnd();
    list.prev();
    list.remove();
    list.moveToStart();
    list.remove();
    list.next();
    System.out.println(list);
    /*
    2 3 4
      ^
    Current pos: 1 Length: 3
    */

    list.moveToStart();
    System.out.println(list.getValue());
    list.next();
    System.out.println(list.getValue());
    list.moveToEnd();
    list.prev();
    System.out.println(list.getValue());
    /*
    2
    3
    4
    */

    list.clear();
    System.out.println(list);
    /*


    Current pos: 0 Length: 0
    */
  }
}
