/*
- Link nodes of singly linked list reuquire a known next node upon
  initialization
- Link nodes can have element value uninitialized upon creation.
*/
class Link<E> {
  private E element;
  private Link<E> next;

  Link(E it, Link<E> nextval) { element = it; next = nextval; }
  Link(Link<E> nextval) { next = nextval; }

  Link<E> next() { return next; }
  Link<E> setNext(Link<E> nextval) { return next = nextval; }
  E element() { return element; }
  E setElement(E it) { return element = it; }

  // Caution: no type parameter is allowed due to static variable
  // is shared among all instances
  static Link freelist = null;

  // 1. the static keyword followed by an additional <T> is due to static
  // methods are created independently from the actual unstances of Link
  // 2. the type parameter of static methods in generics are different from the
  // the type parameter of Link class
  static <I> Link<I> get(I it, Link<I> nextval) {
    if (freelist == null) return new Link<I>(it, nextval);
    Link<I> tmp = freelist;
    freelist = freelist.next();
    tmp.setElement(it);
    tmp.setNext(nextval);
    return tmp;
  }

  // Note: release doesn't need to be static
  void release() {
    element = null;
    next = freelist;
    freelist = this;
  }
}
