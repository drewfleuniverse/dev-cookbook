# LList

## Link Class

```java
/* Class */
Link<E>
/* Fields */
- element: E
- next: Link<E>
/* Methods */
+ Link(it:E, nextval:Link<E>)
+ Link(nextval:Link<E>)
+ next(): Link<E>
+ setNext(nextval:Link<E>): Link<E>
+ element(): E
+ setElement(it:E): E
```

### Notes

- Link nodes of singly linked list require a known next node upon
  initialization
- Link nodes can have element value uninitialized upon creation.

### Comments

- `Link(nextval)`: Head node constructor
- `Link(it, nextval)`: Data node constructor


## LList Class

```java
/* Class */
LList<E> : List<E>
/* Fields */
- head: Link<E>
- tail: Link<E>
# curr: Link<E>
- cnt: int
/* Methods */
+ LList()
+ LList(size:int)
+ clear(): void
+ insert(it:E): void
+ append(it:E): void
+ remove(): E
+ moveToStart(): void
+ moveToEnd(): void
+ prev(): void
+ next(): void
+ length(): int
+ currPos(): int
+ moveToPos(pos:int): void
+ getValue(): E
+ toString(): String
```

### Notes

- Initial state:
    - `head` points to head node, an empty node that has its next points null
    - `tail` and `curr` points to `head`
- Head refers head node while `head` is the pointer to head
- Nodes refers to data nodes
- Position:
    - To disambiguate: Position can be regular position or current position
    - If list not empty:
        - Position has the same behavior of zero-based array index
            - Position equals to 0 implies the first node
            - Position equals to list length implies at the end of list, i.e nothing to traverse
    - If list empty:
        - Position equals to 0 is legit but meaningless, **do not** use position to test if a list is empty

### Comments - Fields

- `head`: points to head
- `tail`: points to the last node
- `curr`:
    - Points to the node previous to current node
    - Situations:
        - `curr == head`: either the current node is the first or the list is empty
        - `curr.next() != null`: has current node
        - `curr.next() == null`, then:
            - `curr == tail`: `curr` is at the end of list
            - `curr == tail == head`: list is empty

### Comments - Methods

- `clear()`:
    - Step 1: By pointing `head.next` to null, all nodes except the head node are garbage collected
    - Step 2: **IMHO** assigning a new null node to head is meaningless
- `insert(it)`:
    - Insert before current node
    - `curr == tail` means either the list is empty or `curr` points to the last node
- `remove()`:
    - Remove current node
    - `curr.next() == null` checks if list is empty. There's other ways to do the same check of course
- `prev()`:
    - Key step: `while (temp.next() != curr)`
        - `curr` points the node previous to current node
        -  N.b. Thus `while (temp.next() != curr.next())` checks if temp.next() points to the current node
