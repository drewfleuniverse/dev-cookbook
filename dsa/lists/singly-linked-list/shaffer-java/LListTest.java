import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class LListTest {
  @Test
  public void evaluateLList() {
    LList<Integer> list = new LList<>();

    list.insert(3);
    list.insert(2);
    list.insert(1);
    list.append(4);
    list.append(5);
    assertEquals(new Integer(1), list.getValue());
    assertEquals("1 2 3 4 5 ; curr=0, length=5", list.toString());

    list.moveToPos(5);
    assertEquals(null, list.getValue());

    list.moveToPos(0);
    assertEquals(new Integer(1), list.getValue());
    assertEquals("1 2 3 4 5 ; curr=0, length=5", list.toString());

    list.next();
    assertEquals(new Integer(2), list.getValue());
    assertEquals("1 2 3 4 5 ; curr=1, length=5", list.toString());

    list.moveToEnd();
    assertEquals(null, list.getValue());
    assertEquals("1 2 3 4 5 ; curr=5, length=5", list.toString());

    list.prev();
    list.remove();
    list.moveToStart();
    list.remove();
    list.next();
    assertEquals("2 3 4 ; curr=1, length=3", list.toString());

    list.moveToStart();
    assertEquals(new Integer(2), list.getValue());
    list.next();
    assertEquals(new Integer(3), list.getValue());
    list.moveToEnd();
    list.prev();
    assertEquals(new Integer(4), list.getValue());

    list.clear();
    assertEquals("; curr=0, length=0", list.toString());
  }
}
