# Title

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Title](#title)
	- [Introduction](#introduction)
		- [Uses](#uses)
		- [Overview](#overview)
	- [Terminology and Properties](#terminology-and-properties)
		- [Notations](#notations)
		- [Terms](#terms)
		- [Forms\/Types\/Representations](#formstypesrepresentations)
		- [Rules](#rules)
	- [Complexity](#complexity)
	- [Pseudocode](#pseudocode)
		- [Author](#author)
			- [\[Author\] Class](#author-class)
			- [\[Author\] Class - foo()](#author-class-foo)
				- [Description](#description)
				- [Code Comments](#code-comments)

<!-- /TOC -->

## Introduction

### Overview

### Definition

### Uses

### Notes


## Terminology and Properties

### Notations

### Terms

### Forms\/Types\/Representations

### Rules

### Notes

## Complexity

- Time - Average and worst:
- Time:
    - Average:
    - Worst:
    - Best:
    - Average	and worst:
        - Search:
        - Insert:
        - Delete:
- Space:
- Notes:

## Pseudocode - by Type Only

### Foo ADT
**Notes**
**Comments**

### Bar Class -> Foo ADT

#### Bar.foobar()

```js
foobar(A, a, b) // A: ..., a:..., b:...
```

## Pseudocode - by Author

### Author
#### \[Author\] Foo Class
#### \[Author\] Class - bar()


## References

- Author. (year) Title - Subtitle. N-th ed.
- Wikipedia. [Title](url)
