/*
Array queue implementation using array with size plus one:

States:
- init: maxSize = n+1, rear = 0, front = 1
- full: ((rear+2) % maxSize) == front
  - when queue is full, rear is two positions behind front, i.e. move
    rear two positions forward the queue to meet front
- empty: ((rear+1) % maxSize) == front

- maxSize = 4, size = 3
- init: 0:[], 1:[], 2:[], 3:[], rear=0, front=1
- enqueue(1), enqueue(2), enqueue(3):
  - 0:[], 1:[1], 2:[2], 3:[3], rear=3, front=1
- dequeue(), dequeue():
  - 0:[], 1:[], 2:[], 3:[3], rear=3, front=3
- dequeue():
  - 0:[], 1:[], 2:[], 3:[], rear=3, front=0
- enqueue(1), enqueue(2), enqueue(3):
  - 0:[1], 1:[2], 2:[3], 3:[], rear=2, front=3
- dequeue():
  - - 0:[], 1:[2], 2:[3], 3:[], rear=2, front=0
*/
class AQueue<E> implements Queue<E> {
  private static final int defaultSize = 10;
  private int maxSize;
  private int front;
  private int rear;
  private E[] listArray;

  AQueue() { this(defaultSize); }
  @SuppressWarnings("unchecked")
  AQueue(int size) {
    maxSize = size + 1;
    rear = 0; front = 1;
    listArray = (E[])new Object[maxSize];
  }

  public void clear() {
    rear = 0; front = 1;
  }

  public void enqueue(E it) {
    assert ((rear+2) % maxSize) != front : "Queue is full";
    rear = (rear+1) % maxSize;
    listArray[rear] = it;
  }

  public E dequeue() {
    assert length() != 0 : "Queue is empty";
    E it = listArray[front];
    front = (front+1) % maxSize;
    return it;
  }

  public E frontValue() {
    assert length() != 0 : "Queue is empty";
    return listArray[front];
  }

  public int length() { return ((rear+maxSize) - front + 1) % maxSize; }

  /*
  012
  ***_: r=3, f=0
  a**_: r=0, f=0
  ab*_: r=1, f=0
  **_a: r=3, f=3
  ***
  */
  public String toString() {
    StringBuffer buf = new StringBuffer();
    for(int i = 0; i < listArray.length; i++) {
      if (i == ((maxSize + front-1) % maxSize)) {
        buf.append("_");
      } else if (length() != 0 && front <= i && i <= rear) {
        buf.append(listArray[i]);
      } else if (length() != 0 && rear < front &&
        (i <= rear || i >= front)) {
        buf.append(listArray[i]);
      } else {
        buf.append("*");
      }
    }
    return buf.toString();
  }
}
