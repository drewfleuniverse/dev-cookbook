class DemoAQueue {
  public static void main(String[] args) {
    AQueue<Integer> aq = new AQueue<>(3);
    System.out.println(aq);

    aq.enqueue(1);
    aq.enqueue(2);
    aq.enqueue(3);
    System.out.println(aq);

    aq.dequeue();
    aq.dequeue();
    aq.dequeue();
    System.out.println(aq);

    aq.enqueue(4);
    aq.enqueue(5);
    aq.enqueue(6);
    System.out.println(aq);

    aq.dequeue();
    aq.dequeue();
    aq.dequeue();
    System.out.println(aq);
    /*
    _***
    _123
    ***_
    456_
    **_*
    */
  }
}
