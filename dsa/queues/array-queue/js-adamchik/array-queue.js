const cl = s => console.log(s)


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}

/*
`front` and `back` are meant to grow larger than `_cap`, thus by modulo, e.g. `front%cap`, the current element can be retrived
 */

const _arr = Symbol('_arr');
const _cap = Symbol('_cap');
const _size = Symbol('_size');
const _front = Symbol('_front');
const _back = Symbol('_back');
const _isFull = Symbol('_isFull');
const _doubleSize = Symbol('_doubleSize');

class ArrayQueue {
  constructor(cap=10) {
    this[_arr] = new Array(cap);
    this[_cap] = cap;
    this[_size] = 0;
    this[_front] = 0;
    this[_back] = -1;
  }

  enqueue(data) {
    if (this[_isFull]()) {
      this[_doubleSize]();
    }
    this[_back]++;
    this[_arr][this[_back] % this[_cap]] = data;
    this[_size]++;
  }

  dequeue() {
    const data = this.peek();
    this[_arr][this[_front] % this[_cap]] = undefined;
    this[_front]++;
    this[_size]--;

    return data;
  }

  peek() {
    assert(!this.isEmpty());
    return this[_arr][this[_front] % this[_cap]]
  }

  clear() {
    this[_arr].fill(undefined);
    this[_size] = 0;
    this[_front] = 0;
    this[_back] = -1;
  }

  isEmpty() {
    return this[_size] === 0;
  }

  [_isFull]() {
    return this[_size] === this[_cap];
  }

  [_doubleSize]() {
    const newArr = new Array(this[_cap] * 2);
    const front = this[_front];
    const back = this[_back];
    // front and back is allowed to numerically larger than cap
    for (let i = front; i <= back; i++) {
      newArr[i - front] = this[_arr][i % this[_cap]];
    }
    this[_arr] = newArr;
    this[_cap] = this[_cap] * 2;
    this[_front] = 0;
    this[_back] = this[_size]-1;
  }

  toString() {
    return [...this].toString();
  }

  *[Symbol.iterator]() {
    const front = this[_front];
    const back = this[_back];
    for (let i = front; i <= back; i++) {
      yield this[_arr][i % this[_cap]];
    }
  }
}


let q, r;


q = new ArrayQueue(3);

cl(q.isEmpty() === true);

q.enqueue('foo');
q.enqueue('bar');
q.enqueue('42');
cl(q.toString() === 'foo,bar,42');

cl(q.isEmpty() === false);

cl(q.peek() === 'foo');

cl(q.dequeue() === 'foo')

q.enqueue('boo');
q.enqueue('far');
cl(q.toString() === 'bar,42,boo,far')

q.clear();
cl(q.isEmpty() === true);
