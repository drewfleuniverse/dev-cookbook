const cl = s => console.log(s)


const sArr = Symbol('sArr');
const sCap = Symbol('sCap');
const sFront = Symbol('sFront');
const sRear = Symbol('sRear');

class ArrayQueue {
  constructor(cap) {
    this[sArr] = new Array(cap);
    this[sCap] = cap;
    this[sFront] = -1;
    this[sRear] = -1;
  }
  enqueue(key) {
    if (this.isFull()) {
      throw new Error('Queue is full');
    }
    if (this.isEmpty()) {
      this[sFront] = 0;
      this[sRear] = 0;
    } else {
      this[sRear] = (this[sRear] + 1) % this[sCap];
    }
    this[sArr][this[sRear]] = key;
  }
  dequeue() {
    if (this.isEmpty()) {
      throw new Error('Queue is empty');
    }
    const key = this[sArr][this[sFront]];
    if (this.hasOnlyOneElement()) {
      this[sFront] = -1;
      this[sRear] = -1;
    } else {
      this[sFront] = (this[sFront] + 1) % this[sCap];
    }
    return key;
  }
  peek() {
    if (this.isEmpty()) {
      throw new Error('Queue is empty');
    }
    return this[sArr][this[sFront]];
  }
  clear() {
    this[sFront] = -1;
    this[sRear] = -1;
  }
  isEmpty() {
    return (
      this[sFront] === -1 &&
      this[sRear] === -1
    );
  }
  isFull() {
    return (this[sRear] + 1) % this[sCap] === this[sFront];
  }
  hasOnlyOneElement() {
    return this[sRear] === this[sFront];
  }
  size() {
    return (
      (this[sRear] + this[sCap] - this[sFront]) % this[sCap]
    ) + 1;
  }
  toString() {
    return [...this].toString();
  }
  *[Symbol.iterator]() {
    for (let i = 0; i < this.size(); i++) {
      const j = (this[sFront] + i) % this[sCap];
      yield this[sArr][j];
    }
  }
}


describe('ArrayQueue', () => {
  it('isEmpty()', () => {
    const queue = new ArrayQueue(2);
    assert(queue.isEmpty() === true);
  });

  it('more on isEmpty()', () => {
    const queue = new ArrayQueue(2);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.dequeue();
    queue.enqueue(3);
    queue.dequeue();
    queue.dequeue();
    assert(queue.isEmpty() === true);
  });

  it('enqueue()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    assert(queue.toString() === '1,2,3');
  });

  it('dequeue()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    assert(queue.toString() === '1,2,3');

    assert(queue.dequeue() === 1);
    assert(queue.toString() === '2,3');

    queue.enqueue(4);
    assert(queue.toString() === '2,3,4');
  });

  it('peek()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    assert(queue.peek() === 1);
  });

  it('clear()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    queue.clear();
    assert(queue.isEmpty());
  });
});


/*
Utils
 */

function describe (s,f) {console.log(`# ${s} #`);f();};
function it (s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert (c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
