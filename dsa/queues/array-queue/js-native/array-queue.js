const cl = s => console.log(s)


const _arr = Symbol('_arr');

class ArrayQueue {
  constructor() {
    this[_arr] = [];
  }

  enqueue(data) {
    this[_arr].push(data);
  }

  dequeue() {
    const data = this.peek();
    this[_arr].shift();

    return data;
  }

  peek() {
    if (this.isEmpty()) {
      throw new Error('Queue is empty');
    }
    return this[_arr][0];
  }

  clear() {
    this[_arr].splice(0);
  }

  isEmpty() {
    return this[_arr].length === 0;
  }

  toString() {
    return [...this].toString();
  }

  *[Symbol.iterator]() {
    for (const data of this[_arr]) {
      yield data;
    }
  }
}


describe('ArrayQueue', () => {
  it('isEmpty()', () => {
    const queue = new ArrayQueue(2);
    assert(queue.isEmpty() === true);
  });

  it('more on isEmpty()', () => {
    const queue = new ArrayQueue(2);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.dequeue();
    queue.enqueue(3);
    queue.dequeue();
    queue.dequeue();
    assert(queue.isEmpty() === true);
  });

  it('enqueue()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    assert(queue.toString() === '1,2,3');
  });

  it('dequeue()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    assert(queue.toString() === '1,2,3');

    assert(queue.dequeue() === 1);
    assert(queue.toString() === '2,3');

    queue.enqueue(4);
    assert(queue.toString() === '2,3,4');
  });

  it('peek()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    assert(queue.peek() === 1);
  });

  it('clear()', () => {
    const queue = new ArrayQueue(3);
    queue.enqueue(1);
    queue.clear();
    assert(queue.isEmpty());
  });
});


/*
Utils
 */

function describe (s,f) {console.log(`## ${s} ##`);f();};
function it (s,f,r=true) {console.log(s);r&&f();};
function assert (c,l=false,m='') {l&&console.log(`>> ${c}`);if(!c){throw new Error(m);}};
