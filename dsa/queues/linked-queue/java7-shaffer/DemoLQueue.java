class DemoLQueue {
  public static void main(String[] args) {
    LQueue<Integer> lq = new LQueue<>();
    System.out.println(lq);

    lq.enqueue(1);
    lq.enqueue(2);
    lq.enqueue(3);
    System.out.println(lq);

    lq.dequeue();
    lq.dequeue();
    lq.dequeue();
    System.out.println(lq);

    lq.enqueue(4);
    lq.enqueue(5);
    lq.enqueue(6);
    System.out.println(lq);

    lq.dequeue();
    lq.dequeue();
    lq.dequeue();
    System.out.println(lq);
    /*
    (f/r)[]->null, size: 0
    (f  )[]->[1]->[2]->(r)[3]->null, size: 3
    (f/r)[]->null, size: 0
    (f  )[]->[4]->[5]->(r)[6]->null, size: 3
    (f/r)[]->null, size: 0
    */
  }
}
