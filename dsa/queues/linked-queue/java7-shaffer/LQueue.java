/*
init: [0(empty)]->null, f->0, r->0, s=0
enqueue: [0(empty)]->[1]->null, f->0, r->1, s=1
enqueue: [0(empty)]->[1]->[2]->null, f->0, r->2, s=2
dequeue: [0(empty)]->[2]->null, f->0, r->2, s=1
dequeue: [0(empty)]->null, f->0, r->0, s=0
*/
class LQueue<E> implements Queue<E> {
    // front points to an empty node, by doing so, the front node can be
    // accesed by front.next()
    private Link<E> front;
    private Link<E> rear;
    private int size;

    LQueue() { init(); }
    LQueue(int size) { init(); } // Ignore size

    private void init() {
      front = rear = new Link<E>(null);
      size = 0;
    }

    public void clear() { init(); }

    public void enqueue(E it) {
      rear.setNext(new Link<E>(it, null)); // access the last node
      rear = rear.next(); // point rear to the newly enqueued node
      size++;
    }

    public E dequeue() {
      assert size != 0 : "Queue is empty";
      E it = front.next().element();
      front.setNext(front.next().next());
      if (front.next() == null) rear = front;
      size--;
      return it;
    }

    public E frontValue() {
      assert size != 0 : "Queue is empty";
      return front.next().element();
    }

    public int length() { return size; }

    public String toString() {
      StringBuffer buf = new StringBuffer();
      Link<E> temp = front;
      buf.append("(f");
      if (size == 0) buf.append("/r");
      else buf.append("  ");
      buf.append(")[]->");
      while (temp.next() != null) { // while size >= 1
        if (temp.next() == rear) buf.append("(r)");
        buf.append("[");
        buf.append(temp.next().element());
        buf.append("]->");
        temp = temp.next();
      }
      buf.append("null, size: ");
      buf.append(size);

      return buf.toString();
    }
}
