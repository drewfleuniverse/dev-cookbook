class Link<E> {
  private Link<E> next;
  private E element;

  Link(E it, Link<E> nextval) { element = it; next = nextval; }
  Link(Link<E> nextval) { next = nextval; }

  Link<E> next() { return next; }
  Link<E> setNext(Link<E> nextval) { return next = nextval; }
  E element() { return element; }
  E setElement(E it) { return element = it; }
}
