# Queues


## Method Names

- Access: N/A
- Search: N/A
- Insertion: enqueue
- Deletion: dequeue


## Time complexity

**Average**

- Access: O(n)
- Search: O(n)
- Insertion: O(1)
- Deletion: O(1)

**Worst**

- Access: O(n)
- Search: O(n)
- Insertion: O(1)
- Deletion: O(1)


## Space complexity

- Worst: O(n)


## Application Examples
