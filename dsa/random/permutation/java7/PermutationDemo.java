import java.util.*;
import java.math.*;

class PermutationDemo<T> {
  static private Random randomValue = new Random();

  static int random(int n) {
    return Math.abs(randomValue.nextInt()) % n;
  }

  public static <T> void swap(T[] arr, int elem1, int elem2) {
    T temp = arr[elem1];
    arr[elem1] = arr[elem2];
    arr[elem2] = temp;
  }

  public static <T> void loopPermute(T[] arr) {
    for (int i = 0; i < arr.length; i++) {
      swap(arr, i, random(arr.length));
    }
  }

  public static <T> void recursivePermute(T[] arr, int len) {
    if (len == 0) return;
    swap(arr, len-1, random(arr.length));
    recursivePermute(arr, len-1);
  }

  public static void main(String[] args) {
    Integer[] ia1 = {1, 2, 3, 4, 5, 6, 7};
    loopPermute(ia1);
    for (int i: ia1) {
      System.out.print(i);
    }
    System.out.println();

    Integer[] ia2 = {1, 2, 3, 4, 5, 6, 7};
    recursivePermute(ia2, ia2.length);
    for (int i: ia2) {
      System.out.print(i);
    }
    System.out.println();
  }
}
