// 'use strict'
const lg = i => console.log(i)
const js = i => JSON.stringify(i)


const permute = nums => {
  const r = []
  const helper = (n, p=[]) => {
    if (n.length === 0)
      return r.push(p)

    for (let i = 0; i < n.length; i++) {
      const a = n.slice()
      const b = a.splice(i, 1)
      helper(a, p.concat(b))
    }
  }
  helper(nums)

  return r
};

lg(js(permute([1,2,3])) === `[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]`)
