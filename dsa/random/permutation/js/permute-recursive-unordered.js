const lg = i => console.log(i)
const js = i => JSON.stringify(i)



const permute = a => {
  const r = []
  helper(a, a.length, r)
  return r
}

const helper = (a, n, r) => {
  if (n === 1)
    r.push(a.slice())
    
  for (let i = 0; i < n; i++) {
    swap(a, i, n-1)
    helper(a, n-1, r)
    swap(a, i, n-1)
  }
}

const swap = (a, m, n) => {
  const t = a[m]
  a[m] = a[n]
  a[n] = t
}


lg(js(permute([1,2,3])) === `[[2,3,1],[3,2,1],[3,1,2],[1,3,2],[2,1,3],[1,2,3]]`)