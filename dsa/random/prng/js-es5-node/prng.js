function PRNG() {
  PRNG.seed = PRNG.seed === undefined ? 12321 : (32123 * PRNG.seed + 7654321);
  return PRNG.seed % 100;
}

for (var i = 0; i < 3; i++) {
  console.log(PRNG());
}

function makeRandom(seed) {
  var next = 4;
  return function() {
    return seed + next++;
  };
}
var random = makeRandom(492347239);
for (var i = 0; i < 3; i++) {
  console.log(random());
}
