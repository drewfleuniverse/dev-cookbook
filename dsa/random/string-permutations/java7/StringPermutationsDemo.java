/*
Observations:

1. For n distinct elements:
- n=2: ab, ba
  - p=2 = 2!
- n=3: abc, acb, bac, bca, cab, cba
  - p=6
  - each has 2 permutations
  - total = 3*2 = 6 = 3!
- n=4: a[bcd], b[acd], c[abd], d[abc]
  - p=24
  - each has 6 permutations
  - total = 4*6 = 24 =4!
- n=5: a[bcde], b[acde], c[abde], d[abce], e[abcd]
  - p=120
  - each has 24 permutations
  - total = 5*24 = 120 = 51

Thus, for n elements has n! permutations.

2. Using recursion to view the combinations:

2.1 - By fixing the first n terms:

[abc]
- a[bc]
  - ab[c], output 1
  - ac[b], output 2
- b[ac]
  - ba[c], output 3
  - bc[a], output 4
- c[ab]
  - ca[b], output 5
  - cb[a], output 6

p([], [abc])
- p([a], [bc])
  - p([ab], [c])
    - p([abc], []) -> when the second param has length 0, p terminates
  - p([ac], [b])
- p([b], [ac])
- p([c], [ab])

2.2 - Swap elements:

[abc]
- cba // result from swap(0,2), length=3
  - bca // result from swap(0,1, length=2
    - terminate at length=1, output bca
  - cba
- acb
  - cab
  - acb
- abc
 - bac
 - abc

References:
- [](http://introcs.cs.princeton.edu/java/23recursion/Permutations.java.html)
*/

class StringPermutationsDemo {
  // Due to java7 doesn't support default parameter, using function overload
  static void recursiveOrderedPermutations(String str) {
    recursiveOrderedPermutations("", str);
  }
  static void recursiveOrderedPermutations(String prefix, String str) {
    int n = str.length();
    if (n == 0) System.out.println(prefix);
    else {
      for (int i = 0; i < n; i++) {
        recursiveOrderedPermutations(
          prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n));
      }
    }
  }

  static void recursiveUnorderedPermutations(String s) {
    int n = s.length();
    char[] a = new char[n];
    for (int i = 0; i < n; i++)
      a[i] = s.charAt(i);

    recursiveUnorderedPermutations(a, n);
  }
  static void recursiveUnorderedPermutations(char[] a, int n) {
    if (n == 1) {
      System.out.println(a);
      return;
    }
    for (int i = 0; i < n; i++) {
      swap(a, i, n-1);
      recursiveUnorderedPermutations(a, n-1);
      swap(a, i, n-1);
    }
  }
  static void swap(char[] a, int i, int j) {
    char tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }




  public static void main(String[] args) {
    String str = "abc";
    recursiveOrderedPermutations(str);
    recursiveUnorderedPermutations(str);
  }
}
