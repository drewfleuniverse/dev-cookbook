// 'use strict'
const lg = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const permute = s => {
  const r = []
  helper(s, '', r)
  return r
}

const helper = (s, p, r) => {
  if (s.length === 0)
    return r.push(p)

  for (let i = 0; i < s.length; i++) {
    helper(
      s.slice(0, i) + s.slice(i+1),
      p + s[i],
      r
    )
  }
}


lg(jsf(permute('abc')) === `[
  "abc",
  "acb",
  "bac",
  "bca",
  "cab",
  "cba"
]`)
