const lg = i => console.log(i)
const jsf = i => JSON.stringify(i, null, '  ')


const permute = s => {
  const r = []
  helper(s, s.length, r)
  return r
}

const helper = (s, n, r) => {
  if (n === 1)
    return r.push(s)

  for (let i = 0; i < n; i++) {
    if (i !== n-1) {
      helper(
        s.slice(0, i) +
        s[n-1] + s.slice(i+1, n-1) +
        s[i] + s.slice(n),
        n-1,
        r
      )
    } else {
      helper(s, n-1, r)
    }
  }
}


lg(jsf(permute('abc')) === `[
  "bca",
  "cba",
  "cab",
  "acb",
  "bac",
  "abc"
]`)
