
// https://github.com/jasonsjones/set-data-structure/blob/master/index.js
(function() {
  'use strict';
  function Set(args) {
    this._items = [];

    if (args) {
      for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] instanceof Array) {
          for (var j = 0; j < arguments[i].length; j++) {
            this.add(arguments[i][j]);
          }
        } else {
          this.add(arguments[i]);
        }
      }
    }
  }

  Set.prototype = {

    add: function(value) {
      if (!this.has(value)) {
        this._items.push(value);
        return true;
      }
      return false;
    },


    remove: function(value) {
      var idx = this._items.indexOf(value);
      if (idx === -1) {
        return null;
      } else {
        return this._items.splice(idx, 1)[0];
      }
    },


    has: function(value) {
      return this._items.indexOf(value) > -1;
    },


    clear: function() {
      this._items = [];
    },


    size: function() {
      return this._items.length;
    },


    isEmpty: function() {
      return this.size() === 0;
    },


    values: function() {
      return this._items;
    },

    union: function(otherSet) {
      if (!(otherSet instanceof Set)) {
        throw new TypeError('invalid parameter type; a Set is required');
      }

      var unionSet = new Set(this.values());

      var argValues = otherSet.values();

      for (var i = 0; i < argValues.length; i++) {
        unionSet.add(argValues[i]);
      }
      return unionSet;
    },

    intersection: function(otherSet) {
      if (!(otherSet instanceof Set)) {
        throw new TypeError('invalid parameter type; a Set is required');
      }

      var intersectionSet = new Set();
      var theseValues = this.values();

      for (var i = 0; i < theseValues.length; i++) {
        if (otherSet.has(theseValues[i])) {
          intersectionSet.add(theseValues[i]);
        }
      }
      return intersectionSet;
    },

    difference: function(otherSet) {
      if (!(otherSet instanceof Set)) {
        throw new TypeError('invalid parameter type; a Set is required');
      }

      var differenceSet = new Set();
      var theseValues = this.values();

      for (var i = 0; i < theseValues.length; i++) {
        if (!otherSet.has(theseValues[i])) {
          differenceSet.add(theseValues[i]);
        }
      }

      return differenceSet;
    },


    subset: function(otherSet) {
      if (!(otherSet instanceof Set)) {
        throw new TypeError('invalid parameter type; a Set is required');
      }

      if (this.size() > otherSet.size()) {
        return false;
      } else {
        var values = this.values();
        for (var i = 0; i < values.length; i++) {
          if (!otherSet.has(values[i])) {
            return false;
          }
        }
        return true;
      }
    }
  };
  module.exports = Set;
}());

var set = new Set([1,2,3]);
set.add(4);
console.log(set);
