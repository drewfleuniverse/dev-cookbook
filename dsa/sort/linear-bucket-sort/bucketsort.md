# Bucket Sort

*Bucket sort distributes input array into buckets then copies the elements back*

## Pseudocode

```js
bucketsort(arr) {
  buckets = new Array(max(arr) + 1);

  for (i = 0; i < buckets.length; i++) {
    buckets[i] = new List();
  }

  for (i = 0; i < arr.length; i++) {
    num = arr[i];
    buckets[num].push(num);
  }

  i = 0;
  for (j = 0; j < buckets.length; j++) {
    bucket = buckets[j];
    for (k = 0; k < bucket.length; k++) {
      num = bucket[k];
      arr[i] = num;
      i++;
    }
  }
}
```


## Time Complexity

- Worst: O(n^2)
- Average: O(n+k)
- Best: O(n+k)


## Space Complexity

- Worst: O(n*k)


## Notes

**Description**

- Also called bin sort
- Can be optimized with insertion sort by:
  - Copy elements in the buckets back to input array
  - Use insertion sort to sort the partially sorted input array

**Cons**

- Slow when distribution is not even

**Time complexity**

- Worst:
  - Happens when distribution allows different values stored in the same bucket
- N.b.
  - n: number of input
  - k: number of buckets
