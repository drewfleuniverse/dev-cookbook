public class BucketSort {
  @SuppressWarnings("unchecked")
  public static void sort(Double[] A) {
    int n = A.length;
    LList<Double>[] B = (LList<Double>[])new LList[n];
    for (int i = 0; i < n; i++)
      B[i] = new LList<Double>();
    for (int i = 0; i < n; i++)
      B[(int)(n * A[i])].insert(A[i]);
    for (int i = 0; i < n; i++)
      insertionSort(B[i]);
    Double m;
    for (int i = 0, j = 0; i < n; i++)
      for (B[i].moveToStart(); (m = B[i].getValue()) != null; B[i].next())
        A[j++] = m;
  }

  private static void insertionSort(LList<Double> L) {
    for (int i = 1; i < L.length(); i++) {
      for (int j = i; j > 0; j--) {
        L.moveToPos(j);
        Double f2 = L.getValue();
        L.prev();
        Double f1 = L.getValue();
        if (f2.compareTo(f1) < 0)
          L.swapNext();
        else break;
      }
    }
  }
}
