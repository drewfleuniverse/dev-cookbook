import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class BucketSortTest {
  private Double[] A;

  @Before
  public void init() { A = new Double[]{0.92, 0.91, 0.52, 0.51, 0.11, 0.12}; }

  @Test
  public void evaluateBucketSort() {
    assertEquals("0.92 0.91 0.52 0.51 0.11 0.12 ", arrayToString(A, A.length));
    BucketSort.sort(A);
    assertEquals("0.11 0.12 0.51 0.52 0.91 0.92 ", arrayToString(A, A.length));
  }

  private String arrayToString(Double[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Double i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
