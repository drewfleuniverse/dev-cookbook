const cl = s => console.log(s);

// http://www.stoimen.com/blog/2013/01/02/computer-algorithms-bucket-sort/


/*
arr: permutation of numbers, inc. 0
max: the max number in input array
 */
const bucketsort = (arr, max) => {
  const len = max + 1;

  const buckets = new Array(len);
  for (let i = 0; i < len; i++) {
    buckets[i] = [];
  }

  const distribute = key => key;
  arr.forEach(key => {
    buckets[distribute(key)].push(key);
  });

  let i = 0;
  buckets.forEach(bucket => {
    bucket.forEach(key => {
      arr[i++] = key;
    })
  });
};



let a;


a = [5,4,2,1,3,1,0];
bucketsort(a, 5);
cl(a.toString() === '0,1,1,2,3,4,5');
