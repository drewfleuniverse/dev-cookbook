const cl = s => console.log(s);


const swap = (arr, i, j) => {
  const tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
};

/*
'a'.charCodeAt(0) === 97
'z''.charCodeAt(0) === 122
97 - 122 === 25
 */
const bucketsort = arr => {
  const len = 26;

  const buckets = new Array(len);
  for (let i = 0; i < len; i++) {
    buckets[i] = [];
  }

  const distribute = key => (
    key.charCodeAt(0) - 97
  );
  arr.forEach(key => {
    buckets[distribute(key)].push(key);
  });

  let i = 0;
  buckets.forEach(bucket => {
    bucket.forEach(key => {
      arr[i++] = key;
    })
  });
};



let a;


a = ['foo', 'bar', 'foo', 'fourty', 'two'];
bucketsort(a);
cl(a.toString() === 'bar,foo,foo,fourty,two');
