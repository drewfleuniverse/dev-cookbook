public class BucketSort {
  @SuppressWarnings("unchecked")
  public static void sort(Integer[] A, int k) {
    LList<Integer>[] B = (LList<Integer>[])new LList[k+1];
    for (int i = 0; i <= k; i++)
      B[i] = new LList<Integer>();
    for (int i = 0; i < A.length; i++)
      B[A[i]].append(A[i]);
    Integer m;
    int j = 0;
    for (int i = 0; i <= k; i++)
      for (B[i].moveToStart(); (m = B[i].getValue()) != null; B[i].next())
        A[j++] = m;
  }
}
