import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class BucketSortTest {
  private Integer[] A;

  @Before
  public void init() { A = new Integer[]{7, 7, 6, 6, 3, 2, 1}; }

  @Test
  public void evaluateBucketSort() {
    assertEquals("7 7 6 6 3 2 1 ", arrayToString(A, A.length));
    BucketSort.sort(A, 7);
    assertEquals("1 2 3 6 6 7 7 ", arrayToString(A, A.length));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
