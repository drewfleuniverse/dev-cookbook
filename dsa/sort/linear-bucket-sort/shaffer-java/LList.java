
class LList<E> implements List<E> {
  private Link<E> head;
  private Link<E> tail;
  protected Link<E> curr;
  private int cnt;

  LList(int size) { this(); }
  LList() {
    curr = tail = head = new Link<E>(null);
    cnt = 0;
  }

  public void clear() {
    head.setNext(null); // Toss away all nodes except the head node
    curr = tail = head = new Link<E>(null); // Kill the head with new node
    cnt = 0;
  }

  public void insert(E it) {
    curr.setNext(new Link<E>(it, curr.next()));
    // Special case: move tail pointer to the new tail node
    if (curr == tail) tail = curr.next();
    cnt++;
  }

  public void append(E it) {
    tail = tail.setNext(new Link<E>(it, null));
    cnt++;
  }

  public E remove() {
    // Caution: the current node IS curr.next(), not curr
    if (curr.next() == null) return null;
    E it = curr.next().element();
    if (curr.next() == tail) tail = curr;
    curr.setNext(curr.next().next());
    cnt--;
    return it;
  }

  public void moveToStart() { curr = head; }

  public void moveToEnd() { curr = tail; }

  public void prev() {
    if (curr == head) return;
    Link<E> tmp = head;
    // Key Step!
    while (tmp.next() != curr) tmp = tmp.next();
    curr = tmp;
  }

  public void next() { if (curr != tail) curr = curr.next(); }

  public int length() { return cnt; }

  public int currPos() {
    // pos of curr, not curr.next()
    Link<E> tmp = head;
    int i;
    for (i=0; tmp != curr; i++)
      tmp = tmp.next();
    return i;
  }

  public void moveToPos(int pos) {
    assert (pos>=0) && (pos<=cnt) : "Position out of range";
    curr = head;
    for (int i=0; i<pos; i++) curr = curr.next();
  }

  public E getValue() {
    if (curr.next() == null) return null;
    return curr.next().element();
  }

  // swap pos and pos+1
  public void swapNext() {
    if ((cnt < 2) || (curr.next().next() == null)) return;
    Link<E> temp = curr.next();
    curr.setNext(curr.next().next());
    curr.next().setNext(temp);
    temp.setNext(null);
    tail = temp;
  }
  /*
  Output as:
  3 2 1
  ^
  Current pos: 0 Length: 3
  */
  public String toString() {
    int tmpPos = currPos();
    int len = length();
    StringBuffer buf = new StringBuffer();

    moveToStart();
    for (int i=0; i<len; i++) {
      buf.append(getValue());
      buf.append(" ");
      next();
    }
    buf.append("\n");
    moveToPos(tmpPos);

    for (int i=0; i<len; i++) {
      if (i != currPos()) buf.append("  ");
      else buf.append("^ ");
    }
    buf.append("\n");

    buf.append("Current pos: ");
    buf.append(currPos());
    buf.append(" Length: ");
    buf.append(length());

    return buf.toString();
  }
}
