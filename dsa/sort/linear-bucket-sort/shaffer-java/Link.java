/*
- Link nodes of singly linked list reuquire a known next node upon
  initialization
- Link nodes can have element value uninitialized upon creation.
*/
class Link<E> {
  private E element;
  private Link<E> next;

  Link(E it, Link<E> nextval) { element = it; next = nextval; }
  // For head node only
  Link(Link<E> nextval) { next = nextval; }

  Link<E> next() { return next; }
  Link<E> setNext(Link<E> nextval) { return next = nextval; }
  E element() { return element; }
  E setElement(E it) { return element = it; }
}
