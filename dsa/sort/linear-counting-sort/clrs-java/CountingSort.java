public class CountingSort {
  public void sort(int[] A, int[] B, int k) {
    int[] C = new int[k+1]; // initialize k+1 int element to 0
                            // k+1 means [0, 1..k]
    for (int i = 0; i < A.length; i++)
      C[A[i]]++;
    for (int i = 1; i < k+1; i++)
      C[i] = C[i] + C[i-1];
    for (int i = A.length-1; i > -1; i--)
      B[--C[A[i]]] = A[i];
  }
}
