import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class CountingSortTest {
  private int[] A;

  @Before
  public void init() { A = new int[]{7, 6, 5, 4, 3, 2, 1}; }

  @Test
  public void evaluateCountingSort() {
    assertEquals("7 6 5 4 3 2 1 ", arrayToString(A, A.length));
    CountingSort countingSort = new CountingSort();
    int[] B = new int[A.length];
    countingSort.sort(A, B, 7);
    assertEquals("1 2 3 4 5 6 7 ", arrayToString(B, B.length));
  }

  private String arrayToString(int[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (int i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
