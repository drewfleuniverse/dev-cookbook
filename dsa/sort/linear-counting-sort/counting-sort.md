# Countingsort

*Counting sort counts the occurrence of each input values then copies them back*

## Pseudocode

```js
countingsort(arr) {
  max = getMax(arr);
  aux = clone(arr);
  cnt = new Array(max+1);
  fill(cnt, 0);

  for (i = 0; i < aux.length; i++) {
    cnt[aux[i]]++;
  }

  for (i = 1; i < cnt.length; i++) {
    cnt[i] = cnt[i-1] + cnt[i];
  }

  for (let i = aux.length-1; i >= 0; i--) {
    num = aux[i];
    pos = cnt[num]-1;
    arr[pos] = num;
    cnt[num]--;
  }
};
```


## Example

**Counting array**

```
# Init
0 0 0 0 0 0 0
# Counting
1 1 1 1 1 1 1
# Addition
1 2 3 4 5 6 7
# Subtraction
0 1 2 3 4 5 6
```

## Time complexity

- Worst: O(n+k) or O(n)
- Average: O(n+k) or O(n)
- Best: O(n+k) or O(n)

## Space complexity

- Worst: O(n+k) or O(k)


## Note

**Description**

- Stable sort

**Pros**

- Uses less space than bucket sort
- Can be used as a subroutine in radix sort

**Cons**

- Inefficient when input range is too large

**Gotchas**

- Must iterate backwards when copying elements in sorted order from auxiliary array to input array

**Time**

- Worst:
  - Operations:
    - n: clone input array to auxiliary array
    - k+1: init count array
    - n: counting
    - k: addition
    - n: sort elements
  - T(n) = 3n + 2k + 1 = O(n+k)
  - N.b. if input array is significantly larger than k, then k is omitted so that it takes O(n)

**Space**

- Worst:
  - n+k: if both of counting and auxiliary array are used
  - k: if only counting array is used
