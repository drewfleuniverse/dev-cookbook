const cl = s => console.log(s);


const getMax = arr => arr.reduce((a,b) => b > a ? b : a);

const countingsort = arr => {
  const aux = arr.slice();
  const cnt = new Array(getMax(arr)+1);
  cnt.fill(0);

  aux.forEach(num => cnt[num]++);

  for (let i = 1; i < cnt.length; i++) {
    cnt[i] = cnt[i-1] + cnt[i];
  }

  // Iterating through auxiliary array backwards
  // is the key to make counting sort stable
  for (let i = aux.length-1; i >= 0; i--) {
    const num = aux[i];
    const pos = cnt[num]-1;
    arr[pos] = aux[i];
    cnt[num]--;
  }
};


let a, r;


a = [5,4,6,2,1,3,0];
countingsort(a);
cl(a.toString() === '0,1,2,3,4,5,6')
