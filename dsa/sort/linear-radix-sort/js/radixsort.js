const cl = s => console.log(s);


const getMax = arr => arr.reduce((a, b) => b > a ? b : a);


const getDigit = (num, pl, rad=10) => Math.floor(num/pl)%rad;


const countingsort = (arr, rad, plc) => {
  const aux = arr.slice();
  const cnt = new Array(rad).fill(0);

  aux.forEach(num => {
    cnt[getDigit(num, plc)]++
  });

  for (let i = 1; i < cnt.length; i++) {
    cnt[i] = cnt[i-1] + cnt[i];
  }

  // Do NOT iterate arr from front, doing so will reverse
  // the order of same digit numbers and make the sort unstable
  for (let i = aux.length-1; i >= 0; i--) {
    const num = aux[i];
    const dgt = getDigit(num, plc);
    const pos = cnt[dgt]-1;
    arr[pos] = num;
    cnt[dgt]--;
  }
};


const radixsort = (arr, rad=10) => {
  const max = getMax(arr);
  for (let place = 1; place <= max ; place *= rad) {
    countingsort(arr, rad, place);
  }
};


let a;


a = [512,42,29,1,36,18,0];
radixsort(a);
cl(a.toString() === '0,1,18,29,36,42,512');
