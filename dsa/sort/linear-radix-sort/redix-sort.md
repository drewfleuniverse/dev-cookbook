# Redix sort

*Radix sort uses counting sort to sort each radix place*

## Pseudo code

```js
getDigit(num, plc, rad=10) {
  return (num / plc) % rad;
}


countingsort(arr, rad, plc) {
  aux = clone(arr);
  cnt = new Array(rad);
  fill(cnt, 0);

  for (i = 0; i < aux.length; i++) {
    cnt[getDigit(aux[i], plc)]++;
  }

  for (i = 1; i < cnt.length; i++) {
    cnt[i] = cnt[i-1] + cnt[i];
  }

  for (i = aux.length-1; i >= 0; i--) {
    num = aux[i];
    dgt = getDigit(num, plc);
    pos = cnt[dgt]-1;
    arr[pos] = num;
    cnt[dgt]--;
  }
}


radixsort(arr, rad=10) {
  max = getMax(arr);
  for (place = 1; place <= max ; place *= rad) {
    countingsort(arr, rad, place);
  }
}
```


## Time Complexity

- Worst: O(n*k)
- Average: O(n*k)
- Best: O(n*k)


## Space Complexity

- Best: O(n+k)


## Notes

**Pros**

- Can be used with characters, e.g. radix = 26


**Time complexity**

- n: number of input
- k: radix places
- r: radix
- Outer loop costs k
- Each counting sort costs n + r
- r is significantly smaller than n and k thus omitted
- T(n) = k * (n + r) = O(n*k)
