public class RadixSort {
  // A.length == B.length; C.length == r
  public void sort(Integer[] A, Integer[] B, int r, int k, int[] C) {
    for (int i = 0, rtok = 1; i < k; i++, rtok *= r) {
      for (int j = 0; j < r; j++) C[j] = 0;
      for (int j = 0; j < A.length; j++)
        C[(A[j]/rtok)%r]++;
      for (int j = 1; j < C.length; j++)
        C[j] = C[j] + C[j-1];
      for (int j = A.length-1; j >= 0; j--)
        B[--C[(A[j]/rtok)%r]] = A[j];
      for (int j = 0; j < A.length; j++)
        A[j] = B[j];
    }
  }
}
