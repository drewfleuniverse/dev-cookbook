import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class RadixSortTest {
  private Integer[] A;

  @Before
  public void init() { A = new Integer[]{75, 76, 45, 44, 21, 22, 1}; }

  @Test
  public void evaluateRadixSort() {
    assertEquals("75 76 45 44 21 22 1 ", arrayToString(A, A.length));
    Integer[] B = new Integer[A.length];
    int[] C = new int[10];
    RadixSort radixSort = new RadixSort();
    radixSort.sort(A, B, 10, 2, C);
    assertEquals("1 21 22 44 45 75 76 ", arrayToString(A, A.length));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
