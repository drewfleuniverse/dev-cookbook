import java.util.Arrays;

class Heapsort {
  private Heapsort() {};

  private static int heapSize;

  private static void heapsort(int[] A) {
    buildMaxHeap(A);
    for (int i = A.length-1; i > 0; i--) {
      swap(A, 0, i);
      heapSize--;
      maxHeapify(A, 0);
    }
  }

  private static void buildMaxHeap(int[] A) {
    heapSize = A.length;
    for (int i = parent(A.length-1); i >= 0; i--)
      maxHeapify(A, i);
  }

  private static void maxHeapify(int[] A, int i) {
    int l = left(i);
    int r = right(i);
    int largest;
    if (l < heapSize && A[i] < A[l])
      largest = l;
    else
      largest = i;
    if (r < heapSize && A[largest] < A[r])
      largest = r;
    if (largest != i) {
      swap(A, largest, i);
      maxHeapify(A, largest);
    }
  }

  private static int parent(int i) {
    assert i > 0 && i < heapSize;
    return (i-1)/2;
  }

  private static int left(int i) {
    assert i >= 0 && i < heapSize;
    return 2*i + 1;
  }

  private static int right(int i) {
    assert i >= 0 && i < heapSize;
    return 2*i + 2;
  }

  private static void swap(int[] A, int i, int j) {
    int temp = A[i];
    A[i] = A[j];
    A[j] = temp;
  }

  public static void main(String[] args) {
    int[] A = new int[] {7, 6, 5, 4, 3, 2, 1};
    heapsort(A);
    assert Arrays.toString(A).equals("[1, 2, 3, 4, 5, 6, 7]");
  }
}
