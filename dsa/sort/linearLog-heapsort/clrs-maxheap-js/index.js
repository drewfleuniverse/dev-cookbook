// Utils

function swap(A, i, j) {
  const temp = A[i];
  A[i] = A[j];
  A[j] = temp;
}

function assert(cond, message='') {
  if (!cond) throw new Error(message)
}


// Max Heap

const H = Symbol('H')
const heapSize = Symbol('heapSize')
const maxHeapify = Symbol('maxHeapify')
const parent = Symbol('parent')
const left = Symbol('left')
const right = Symbol('right')
class MaxHeap {
  constructor(_H) {
    this[H] = _H
    this[heapSize] = _H.length
    this.buildMaxHeap()
  }

  removeMax() {
    assert(this[heapSize] !== 0)
    swap(this[H], 0, --this[heapSize])
    this[maxHeapify](0)
  }

  buildMaxHeap() {
    for (let i = this[parent](this[heapSize]-1); i >= 0; i--)
      this[maxHeapify](i);
  }

  [maxHeapify](i) {
    const l = this[left](i);
    const r = this[right](i);
    let largest;
    if (l < this[heapSize]
      && this[H][i] < this[H][l])
      largest = l;
    else
      largest = i;
    if (r < this[heapSize]
      && this[H][largest] < this[H][r])
      largest = r;
    if (largest !== i) {
      swap(this[H], i, largest);
      this[maxHeapify](largest);
    }
  }

  [parent](i) {
    assert(i > 0 && i < this[heapSize]);
    return Math.floor((i-1)/2);
  }
  [left](i) {
    assert(i >= 0 && i < this[heapSize]);
    return 2*i + 1;
  }
  [right](i) {
    assert(i >= 0 && i < this[heapSize]);
    return 2*i + 2;
  }
}

// Sort

function sort(A) {
  const m = new MaxHeap(A)
  for (let i = 0; i < A.length-1; i++)
    m.removeMax()
}


A = [7, 6, 5, 4, 3, 2, 1]
sort(A);
assert(A.toString() === `1,2,3,4,5,6,7`)
