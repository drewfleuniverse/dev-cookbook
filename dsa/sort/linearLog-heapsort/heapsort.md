# Heapsort

*Heapsort max heapifies the input array then removes the max element in-place to produce a sorted array*

## Pseudocode

```js
class MaxHeap {
  // ...other methods are omitted
  removeMax() {
    if (this.size === 0) {
      return;
    }

    swap(this.heap, 0, this.size-1);
    this.size--;
    this.bubbleDown(0);

    return this.heap[this.size];
  }
}

heapsort(arr) {
  heap = new MaxHeap(arr);

  for (i = 0; i < arr.length-1; i++) {
    heap.removeMax();
  }  
}
```


## Example

```
6 4 5 2 1 3

5 4 3 2 1 6
4 2 3 1 5 6
3 2 1 4 5 6
2 1 3 4 5 6
1 2 3 4 5 6
1,2,3,4,5,6
```


## Time Complexity

- Worst: O(n*log(n))
- Average: O(n*log(n))
- Best: O(n*log(n))


## Space Complexity

- Worst: O(1)


## Notes

**Pros**

- Guaranteed O(n*log2(n))
- Non-recursive

**Cons**

- Average running time is slower than quicksort

**Running time**

- `heapify`: n * `bubbleDown` = n*log2(n)
- `bubbleDown`: log2(n)
  - i.e. heap height is log2(n)

- Heapsort is not a stable sort
  - i.e. same value results in different order as original input
  - i.e. [1,2,1'] => [1',1,2]
