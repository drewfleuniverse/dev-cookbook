'use strict';
const cl = s => console.log(s);


const swap = (arr, i, j) => {
  const tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
};


class MaxHeap {
  constructor (arr) {
    this.heap = arr;
    this.size = arr.length;
    this.heapify();
  }

  removeMax() {
    if (this.size === 0) {
      return;
    }

    swap(this.heap, 0, this.size-1);
    this.size--;
    this.bubbleDown(0);

    return this.heap[this.size];
  }

  heapify() {
    const lastInternal = this.firstLeaf()-1;
    for (let pos = lastInternal; pos >= 0; pos--) {
      this.bubbleDown(pos);
    }
  }

  bubbleDown(pos_) {
    let pos = pos_;

    while(!this.isLeaf(pos)) {
      let chi = this.leftChild(pos);

      if (
        chi+1 < this.size
        && this.heap[chi] < this.heap[chi+1]
      ) {
        chi++;
      }

      if (this.heap[pos] < this.heap[chi]) {
        swap(this.heap, pos, chi);
        pos = chi;
      } else {
        return;
      }
    }
  }

  firstLeaf() {
    return Math.floor(this.size/2);
  }

  isLeaf(pos) {
    return (
      pos >= this.firstLeaf(pos)
      && pos < this.size
    );
  }

  leftChild(pos) {
    return (pos * 2) + 1;
  }
}


const heapsort = arr => {
  const maxHeap = new MaxHeap(arr);
  for (let i = 0; i < arr.length-1; i++) {
    maxHeap.removeMax();
  }
};


let a;


a = [5,4,6,2,1,3];
heapsort(a);
cl(a.toString() === '1,2,3,4,5,6');
