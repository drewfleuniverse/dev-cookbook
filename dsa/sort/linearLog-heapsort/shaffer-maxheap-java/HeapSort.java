public class HeapSort<E extends Comparable<? super E>> {
  public void sort(E[] A, int length) {
    MaxHeap<E> H = new MaxHeap<>(A, length, length);
    for (int i = 0; i < length; i++)
      H.removeMax();
  }
}
