// Utils

function swap(A, i, j) {
  const tmp = A[i]
  A[i] = A[j]
  A[j] = tmp
}

function assert(cond, message='') {
  if (!cond) throw new Error(message)
}


// Max Heap

const H = Symbol('H')
const n = Symbol('n')
const siftDown = Symbol('siftDown')
const isLeaf = Symbol('isLeaf')
const leftChild = Symbol('leftChild')
const rightChild = Symbol('rightChild')
const parent = Symbol('parent')
const firstLeaf = Symbol('firstLeaf')
const lastFull = Symbol('lastFull')
class MaxHeap {
  constructor(_H) {
    this[H] = _H
    this[n] = _H.length
    this.buildHeap()
  }

  buildHeap() {
    for (let i = this[firstLeaf]()-1; i >= 0; i--) {
      this[siftDown](i)
    }
  }

  removeMax() {
    assert(this[n] !== 0)
    swap(this[H], 0, --this[n])
    if (this[n] !== 0) this[siftDown](0)
    return this[H][this[n]]
  }

  [siftDown](p) {
    assert(p >= 0 && p < this[n])
    while (!this[isLeaf](p)) {
      let i = this[leftChild](p)
      if (i < this[n]-1 && this[H][i] < this[H][i+1]) i++
      if (this[H][p] >= this[H][i]) return;
      swap(this[H], p, i)
      p = i
    }
  }

  [isLeaf](p) {
    return p >= this[firstLeaf]() && p < this[n]
  }

  [leftChild](p) {
    assert(p < this[firstLeaf]())
    return p*2 + 1;
  }

  [rightChild](p) {
    assert(p < this[lastFull])
    return p*2 + 2;
  }

  [parent](p) {
    assert(p > 0)
    return Math.floor((p-1)/2)
  }

  [firstLeaf]() {
    return Math.floor(this[n]/2)
  }

  [lastFull]() {
    return Math.floor((this[n]-1)/2)
  }
}

// Sort

function sort(H) {
  const m = new MaxHeap(H);
  for (let i = 0; i < H.length; i++)
    m.removeMax()
}

const _H = [7, 6, 5, 4, 3, 2, 1]
sort(_H)
assert(_H.toString() === `1,2,3,4,5,6,7`)
