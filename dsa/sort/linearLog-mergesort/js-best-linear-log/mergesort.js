const cl = s => console.log(s);


const merge = (arr, aux, lo, mid, hi) => {
  for (let i = lo; i <= hi; i++) {
    aux[i] = arr[i];
  }

  let left = lo;
  let right = mid+1;
  let i = lo;

  while (left <= mid && right <= hi) {
    if (aux[left] < aux[right]) {
      arr[i++] = aux[left++];
    } else {
      arr[i++] = aux[right++];
    }
  }

  if (left === mid+1) {
    while (i <= hi) {
      arr[i++] = aux[right++];
    }
  } else { // right === hi+1
    while (i <= hi) {
      arr[i++] = aux[left++];
    }
  }
};


const mergesort_ = (arr, aux, lo, hi) => {
  if (lo >= hi) {
    return;
  }

  const mid = Math.floor((hi+lo)/2);
  mergesort_(arr, aux, lo, mid);
  mergesort_(arr, aux, mid+1, hi);

  merge(arr, aux, lo, mid, hi);
};


const mergesort = arr => {
  const len = arr.length;
  const aux = new Array(len);
  mergesort_(arr, aux, 0, len-1);
};


let a;


a = [5,4,6,2,1,3];
mergesort(a);
cl(a.toString() === '1,2,3,4,5,6');
