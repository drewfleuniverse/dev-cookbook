const cl = s => console.log(s);


const merge = (arr, aux, lo, mid, hi) => {
  for (let i = lo; i <= hi; i++) {
    aux[i] = arr[i];
  }

  let left = lo;
  let right = mid+1;
  let i = lo;

  while (left <= mid && right <= hi) {
    if (aux[left] < aux[right]) {
      arr[i++] = aux[left++];
    } else {
      arr[i++] = aux[right++];
    }
  }

  while (left === mid+1 && i <= hi) {
    arr[i++] = aux[right++];
  }

  while (right === hi+1 && i <= hi) {
    arr[i++] = aux[left++];
  }
};


const split = (arr, aux, lo, hi) => {
  if (lo >= hi) {
    return;
  }

  const mid = Math.floor((hi+lo)/2);
  split(arr, aux, lo, mid);
  split(arr, aux, mid+1, hi);

  // This makes best case runs in O(n)
  if (arr[mid] > arr[mid+1]) {
    merge(arr, aux, lo, mid, hi);
  }
};


const mergesort = arr => {
  const len = arr.length;
  const aux = new Array(len);
  split(arr, aux, 0, len-1);
};



let a;


a = [5,4,2,1,3,1,0];
mergesort(a);
cl(a.toString() === '0,1,1,2,3,4,5');
