# Mergesort

*Mergesort divides input array into small chunks then sorts and merges the chunks*

## Pseudocode

```js
merge(arr, aux, lo, mid, hi) {
  if (lo >= hi) {
    return;
  }

  for (i = lo; i <= hi; i++) {
    aux[i] = arr[i];
  }

  left = lo;
  right = mid+1;
  i = lo;
  while (
    left <= mid
    && right <= hi
  ) {
    if (aux[left] < aux[right]) {
      arr[i++] = aux[left++];
    } else {
      arr[i++] = aux[right++];
    }
  }

  if (left === mid+1) {
    while (i <= hi) {
      arr[i++] = aux[right++];
    }
  } else { // right === hi+1
    while (i <= hi) {
      arr[i++] = aux[left++];
    }
  }
}


// To invoke:
// mergesort(arr, new Array(arr.length), 0, arr.length)
mergesort(arr, aux, lo, hi) {
  if (lo > hi) {
    return;
  }

  mid = (hi+lo)/2;
  mergesort(arr, aux, lo, mid);
  mergesort(arr, aux, mid+1, hi);
  merge(arr, aux, lo, mid, hi);
}
```


## Example

```
5 4 6 2 1 3

4 5 6 2 1 3
4 5 6 2 1 3
4 5 6 1 2 3
4 5 6 1 2 3
1 2 3 4 5 6
```


## Time Complexity

- Worst: O(n*log(n))
- Average: O(n*log(n))
- Best: O(n*log(n)) or O(n)


## Space Complexity

- Worst: n or 1


## Notes

**Pros**

- Uses O(1) space when sorting linked list

**Time complexity**

- Worst:
  - Divide, split down: O(n)
    - 2^0 + 2^1 +...+ 2^log2(n)
    - T(n) = 2n - 1 = O(n)
  - Conquer, merge: O(log2(n) * n)
    - merge costs O(n) at each level
    - the divide has log2(n) levels
    - T(n) = n * log2(n)
  - T(n) = n * log2(n) + 2n - 1 = O(n*log2(n))
- Best - O(n):
  - By conditionally merging left and right halves, when the input array is sorted, skip the merge operation.

**Space complexity**

- Worst - n: using auxiliary array
- Worst - 1: using linked list
