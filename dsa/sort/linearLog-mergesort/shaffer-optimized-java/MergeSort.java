public class MergeSort<E extends Comparable<? super E>> {
  private static int THRESHOLD;
  public MergeSort() { THRESHOLD = 0; }
  public MergeSort(int T) { THRESHOLD = T; }

  /**
   * @param E[] A  Array to sort
   * @param E[] T  Array for temporary storage
   * @param l      Leftmost element index
   * @param r      Rightmost element index
   */
  public void sort(E[] A, E[] T, int l, int r) {
    int m = (l+r)/2;
    if (l == r) return;
    if ((m-l+1) > THRESHOLD) sort(A, T, l, m);
    else insertionSort(A, l, m-l+1);
    if ((r-m) > THRESHOLD) sort(A, T, m+1, r);
    else insertionSort(A, m+1, r-m);
    // Clone A to T
    for (int i = l; i <= m; i++) T[i] = A[i];
    for (int j = 1; j <= r-m; j++) T[r-j+1]= A[j+m];
    // Other valid loop for reverse clone the right sublist
    // for (int j = 0; j < r-m; j++) T[r-j]= A[j+m+1]
    // for (int j = m+1; j <= r; j++) T[j] = A[r-j+m+1];
    // for (int j = r; j > m; j--) T[r-j+m+1] = A[j];
    // Merge T to A
    for (int i = l, j = r, c = l; c <= r; c++)
      if (T[i].compareTo(T[j]) < 0) A[c] = T[i++];
      else A[c] = T[j--];
  }

  /**
   * @param E[] A
   * @param int s  Index to start
   * @param int n  Number of elements
   */
  private void insertionSort(E[] A, int s, int n) {
    for (int i = s+1; i < s+n; i++)
      for (int j = i; (j > s) && (A[j].compareTo(A[j-1]) < 0); j--)
        DSUtil.swap(A, j, j-1);
  }
}
