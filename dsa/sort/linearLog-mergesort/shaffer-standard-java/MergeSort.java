public class MergeSort<E extends Comparable<? super E>> {
  public void sort(E[] A, E[] T, int l, int r) {
    if (l == r) return;
    int m = (l+r)/2;
    sort(A, T, l, m);
    sort(A, T, m+1, r);
    for (int i = l; i <= r; i++) T[i] = A[i];
    for (int i = l, j = m+1, c = l; c <= r; c++)
      if (i == m+1) A[c] = T[j++];
      else if (j > r) A[c] = T[i++];
      else if (T[i].compareTo(T[j]) < 0) A[c] = T[i++];
      else A[c] = T[j++];
  }
}
