import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class MergeSortTest {
  private Integer[] A;

  @Before
  public void init() { A = new Integer[]{7, 6, 5, 4, 3, 2, 1}; }

  @Test
  public void evaluateBubbleSort() {
    assertEquals("7 6 5 4 3 2 1 ", arrayToString(A, A.length));
    MergeSort<Integer> merSort = new MergeSort<>();
    Integer[] IT = new Integer[A.length];
    merSort.sort(A, IT, 0, A.length-1);
    assertEquals("1 2 3 4 5 6 7 ", arrayToString(A, A.length));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
