public class QuickSort<E extends Comparable<? super E>> {
  public void sort(E[] A, int p, int r) {
    if (p < r) { // Sort at least 2 elements
      int q = partition(A, p, r);
      sort(A, p, q-1);
      sort(A, q+1, r);
    }
  }

  private int partition(E[] A, int p, int r) {
    E x = A[r];
    int i = p-1;
    for (int j = p; j < r; j++)
      if (A[j].compareTo(x) <= 0) // After each iteration, A[j] must > x
        DSUtil.swap(A, ++i, j); // Initially, A[i] has NO PROPERTY
                                // After each iteration, A[i] <= x
    DSUtil.swap(A, ++i, r);
    return i;
  }
}
