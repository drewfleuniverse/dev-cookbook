function quicksort(A, p, r) {
  if (p < r) {
    const q = partition(A, p, r);
    quicksort(A, p, q - 1);
    quicksort(A, q + 1, r);
  }
}

function partition(A, p, r) {
  const x = A[r];
  let i = p - 1;
  for (let j = p; j < r; j++) {
    if (A[j] <= x)
      swap(A, ++i, j);
  }
  swap(A, ++i, r);
  return i;
}

function swap(A, p, q) {
  const temp = A[p];
  A[p] = A[q];
  A[q] = temp;
}

const A = [5, 4, 3, 2, 1];
quicksort(A, 0, A.length - 1);
console.assert(A.toString() === "1,2,3,4,5");
