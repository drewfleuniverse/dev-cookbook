const cl = s => console.log(s)


const swap = function (arr, p, q) {
  const tmp = arr[p];
  arr[p] = arr[q];
  arr[q] = tmp;
}


const partition = function (arr, lo, hi) {
  const pivot = arr[hi];
  let mid = lo;
  for (let i = lo; i < hi; i++) {
    if (arr[i] < pivot) {
      swap(arr, i, mid);
      mid++;
    }
  }
  swap(arr, mid, hi);

  return mid;
};


const insertionsort = function (arr, lo, hi) {
  for (let i = lo+1; i <= hi; i++) {
    let j = i;
    while (
      j > lo
      && arr[j-1] > arr[j]
    ) {
      swap(arr, j-1, j);
      j--
    }
  }
};


const quicksortSglRecurSimple = function (arr, lo, hi) {
  let i = lo;
  while (i < hi) {
    const mid = partition(arr, i, hi);

    quicksortSglRecurSimple(arr, i, mid-1);
    i = mid + 1;
  }
};


const quicksortSglRecur = function (arr, lo_, hi_) {
  let lo = lo_;
  let hi = hi_;

  while (lo < hi) {
    if (hi-lo < 5) {
      insertionsort(arr, lo, hi);
      break;
    }

    const mid = partition(arr, lo, hi);

    const left = mid - lo;
    const right = hi - mid;
    if (left < right) {
      quicksortSglRecur(arr, lo, mid-1);
      lo = mid + 1;
    } else {
      quicksortSglRecur(arr, mid+1, hi);
      hi = mid - 1;
    }
  }
};


let a = [0,9,8,2,3,4,5,6,9,8,7,5];
quicksortSglRecur(a, 0, a.length-1);
cl(a.toString() === '0,2,3,4,5,5,6,7,8,8,9,9');
