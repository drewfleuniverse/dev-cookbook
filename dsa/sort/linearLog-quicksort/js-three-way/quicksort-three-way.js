const cl = s => console.log(s);

const swap = (a, p, q) => {
  const t = a[p]
  a[p] = a[q]
  a[q] = t
};


const quicksort_ = (arr, begin, end) => {

  if (begin >= end) {
    return;
  }

  const pivot = arr[end];
  let lo = begin;
  let hi = end;
  let i = lo;
  while (i <= hi) {
    if (arr[i] < pivot) {
      swap(arr, i++, lo++);
    } else if (arr[i] > pivot) {
      swap(arr, i, hi--);
    } else {
      i++;
    }
  }

  quicksort_(arr, begin, lo-1);
  quicksort_(arr, hi+1, end);
};


const quicksort = arr => {
  quicksort_(arr, 0, arr.length-1);
};


let a;


a = [5,4,2,1,3,1,0];
quicksort(a);
cl(a.toString() === '0,1,1,2,3,4,5');
