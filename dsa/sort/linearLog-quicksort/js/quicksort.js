const cl = s => console.log(s);

const swap = (a, p, q) => {
  const t = a[p]
  a[p] = a[q]
  a[q] = t
};



const partition = (arr, lo, hi) => {
  const pivot = arr[hi];
  let mid = lo;
  for (let i = lo; i < hi; i++) {
    if (arr[i] < pivot) {
      swap(arr, i, mid);
      mid++;
    }
  }
  swap(arr, mid, hi);

  return mid;
};


const quicksort_ = (arr, begin, end) => {
  if (begin >= end) {
    return;
  }

  const mid = partition(arr, begin, end);
  quicksort_(arr, begin, mid-1);
  quicksort_(arr, mid+1, end);
};


const quicksort = arr => {
  quicksort_(arr, 0, arr.length-1);
};


let a;


a = [5,4,6,2,1,3];
quicksort(a);
cl(a.toString() === '1,2,3,4,5,6');
