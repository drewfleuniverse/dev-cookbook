/*
Chrome, input 500, 0-999:
insertionsort x 659,781 ops/sec ±1.42% (63 runs sampled)
array.sort x 20,130 ops/sec ±2.01% (60 runs sampled)
quicksort x 3,167 ops/sec ±2.86% (58 runs sampled)
quicksortIns x 3,238 ops/sec ±1.80% (21 runs sampled)
quicksortTail x 3,010 ops/sec ±1.93% (20 runs sampled)
quicksortTailIns x 3,015 ops/sec ±1.53% (61 runs sampled)
Fastest is insertionsort

Chrome, input 50000, 0-999:
insertionsort x 0.67 ops/sec ±2.98% (6 runs sampled)
array.sort x 152 ops/sec ±1.66% (49 runs sampled)
quicksort x 14.01 ops/sec ±1.44% (11 runs sampled)
quicksortIns x 13.90 ops/sec ±0.90% (10 runs sampled)
quicksortTail x 12.36 ops/sec ±6.39% (9 runs sampled)
quicksortTailIns x 12.60 ops/sec ±3.02% (9 runs sampled)
Fastest is array.sort
 */



 const swap = function (arr, p, q) {
   const tmp = arr[p];
   arr[p] = arr[q];
   arr[q] = tmp;
 }


 const partition = function (arr, lo, hi) {
   const pivot = arr[hi];
   let mid = lo;
   for (let i = lo; i < hi; i++) {
     if (arr[i] < pivot) {
       swap(arr, i, mid);
       mid++;
     }
   }
   swap(arr, mid, hi);

   return mid;
 };


 const insertionsort = function (arr, lo, hi) {
   for (let i = lo+1; i <= hi; i++) {
     let j = i;
     while (
       j > lo
       && arr[j-1] > arr[j]
     ) {
       swap(arr, j-1, j);
       j--
     }
   }
 };


 const quicksort = function (arr, lo, hi) {
   if (lo >= hi) {
     return;
   }

   const mid = partition(arr, lo, hi);
   quicksort(arr, lo, mid-1);
   quicksort(arr, mid+1, hi);
 };


 const quicksortIns = function (arr, lo, hi) {
   if (lo >= hi) {
     return;
   }

   if (hi-lo < 10) {
     insertionsort(arr, lo, hi);
     return;
   }

   const mid = partition(arr, lo, hi);
   quicksortIns(arr, lo, mid-1);
   quicksortIns(arr, mid+1, hi);
 };


 const quicksortTail = function (arr, lo_, hi_) {
   let lo = lo_;
   let hi = hi_;

   while (lo < hi) {
     const mid = partition(arr, lo, hi);

     const left = mid - lo;
     const right = hi - mid;
     if (left < right) {
       quicksortTail(arr, lo, mid-1);
       lo = mid + 1;
     } else {
       quicksortTail(arr, mid+1, hi);
       hi = mid - 1;
     }
   }
 };


 const quicksortTailIns = function (arr, lo_, hi_) {
   let lo = lo_;
   let hi = hi_;

   while (lo < hi) {
     const mid = partition(arr, lo, hi);

     if (hi-lo < 10) {
       insertionsort(arr, lo, hi);
       break;
     }

     const left = mid - lo;
     const right = hi - mid;
     if (left < right) {
       quicksortTailIns(arr, lo, mid-1);
       lo = mid + 1;
     } else {
       quicksortTailIns(arr, mid+1, hi);
       hi = mid - 1;
     }
   }
 };


const prep = {
  setup () {
    const a = Array.from(new Array(500), () => (
      Math.floor(Math.random() * 1000)
    ));
  }
};


const suite = new Benchmark.Suite;
suite.add('insertionsort', () => {
  insertionsort(a, 0, a.length-1);
}, prep)
.add('array.sort', () => {
  a.sort((a,b) => (a-b));
}, prep)
.add('quicksort', () => {
 quicksort(a, 0, a.length-1);
}, prep)
.add('quicksortIns', () => {
 quicksortIns(a, 0, a.length-1);
}, prep)
.add('quicksortTail', () => {
 quicksortTail(a, 0, a.length-1);
}, prep)
.add('quicksortTailIns', () => {
 quicksortTailIns(a, 0, a.length-1);
}, prep)
.on('cycle', event => {
  console.log(String(event.target));
})
.on('complete', function () {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run();
