# Quicksort

*Quicksort places elements to their correct positions by partitioning*

## Pseudocode

```js
partition(arr, lo, hi) {
  pivot = arr[hi];
  mid = lo;
  for (i = lo; i < hi; i++) {
    if (arr[i] < pivot) {
      swap(arr, i, mid);
      mid++;
    }
  }
  swap(arr, mid, hi);

  return mid;
}


// To invoke:
// quicksort(arr, 0, arr.length-1)
quicksort(arr, begin, end) {
  if (begin >= end) {
    return;
  }

  mid = partition(arr, begin, end);

  quicksort(arr, begin, mid-1);
  quicksort(arr, mid+1, end);
}
```


## Example

```
5 4 6 2 1 3

2 1 3 5 4 6
1 2 3 5 4 6
1 2 3 5 4 6
1 2 3 4 5 6
```


## Time Complexity

- Worst: O(n^2)
- Average: O(n*log2(n))
- Best: O(n*log2(n))


## Space Complexity

- Worst: O(n) or O(log2(n))


## Notes

**Description**

- Not stable

**Pros**

- Fastest average running time in real world
- Fast in-memory sort for large input array
- Tail recursion

**Cons**

- Slow on small inputs less then 10 for stack allocation

**Time complexity**

- Worst: O(n^2)
  - Only if partition always results in a single partition, quicksort has n-1 levels of recursion calls and each level costs from n-1 to 1 times, and only the operation at each level should be summed up, so that:
  - (n-1) + (n-2) +...+ 2 + 1
  - (n-1)(n-1+1)/2
  - n * (n-1)/2
  - Total T(n) = (n^2 - n)/2 = O(n^2)
    - N.b. a simpler solution is n-1 levels times running time O(n) per level results O(n^2)
- Best: O(n*log2(n))
  - Only if the divide phase always breaks the elements into two halves, quicksort has no more than log2(n) levels of recursion calls and each level costs O(n), so that
  - O(n) =  n * log2(n)
  - N.b. T(n) is not calculated here

**Space complexity**

- Worst: O(n)
  - In the worst case quicksort has n-1 nested calls which requires n-1 stack frames.
- Worst: O(log2(n))
  - To limit worst space, use a while loop to execute a single recursive quicksort one at a time
  - Furthermore, in each iteration, only execute recursive quicksort on the smaller half so the call stack height is limited to log2(n)
