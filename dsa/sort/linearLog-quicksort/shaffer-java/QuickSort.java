public class QuickSort<E extends Comparable<? super E>> {
  public void sort(E[] A, int i, int j) {
    int pivotIndex = findPivotIndex(A, i, j);
    DSUtil.swap(A, pivotIndex, j); // pivot is now at j
    int k = partition(A, i-1, j, A[j]);
    DSUtil.swap(A, k, j); // pivot is now at k
    if ((k-i) > 1) sort(A, i, k-1);
    if ((j-k) > 1) sort(A, k+1, j);
  }

  private int findPivotIndex(E[] A, int i, int j) {
    return (i+j)/2;
  }

  private int partition(E[] A, int l, int r, E p) {
    do {
      while (A[++l].compareTo(p) < 0);
      while ((r != 0) && (A[--r].compareTo(p) > 0));
      DSUtil.swap(A, l, r);
    } while (l < r);
    DSUtil.swap(A, l, r);
    return l;
  }
}
