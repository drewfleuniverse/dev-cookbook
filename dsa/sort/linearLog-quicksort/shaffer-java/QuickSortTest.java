import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class QuickSortTest {
  private Integer[] A;

  @Before
  public void init() { A = new Integer[]{7, 6, 5, 4, 3, 2, 1}; }

  @Test
  public void evaluateBubbleSort() {
    assertEquals("7 6 5 4 3 2 1 ", arrayToString(A, A.length));
    QuickSort<Integer> quiSort = new QuickSort<>();
    quiSort.sort(A, 0, A.length-1);
    assertEquals("1 2 3 4 5 6 7 ", arrayToString(A, A.length));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
