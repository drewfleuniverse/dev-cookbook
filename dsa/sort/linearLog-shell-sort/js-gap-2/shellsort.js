const cl = s => console.log(s);


const shellsort = arr => {
  for (
    let incr = Math.floor(arr.length/2);
    incr > 2;
    incr /= 2
  ) {
    for (
      let start = 0;
      start < incr;
      start++
    ) {
      insertionsort(arr, start, incr);
    }
  }
  insertionsort(arr, 0, 1);
};

const swap = (arr, p, q) => {
  const tmp = arr[p];
  arr[p] = arr[q];
  arr[q] = tmp;
};

const insertionsort = (arr, start, incr) => {
  for (
    let i = start + incr;
    i < arr.length;
    i += incr
  ) {
    for (
      let j = i;
      j >= incr
      && arr[j-incr] > arr[j];
      j -= incr
    ) {
      swap(arr, j, j-incr);
    }
  }
};


let a;


a = [5,4,2,1,3,1,0];
shellsort(a, 0, 1);
cl(a.toString())
cl(a.toString() === '0,1,1,2,3,4,5');
