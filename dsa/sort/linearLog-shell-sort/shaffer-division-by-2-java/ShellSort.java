public class ShellSort<E extends Comparable<? super E>> {
  public void sort(E[] A) {
    for (int i = A.length/2; i > 2; i /= 2) {
      for (int j = 0;  j < i; j++)
        insertionSort(A, j, i);
    }
    insertionSort(A, 0, 1);
  }
  private void insertionSort(E[] A, int start, int incr) {
    for (int i = start + incr; i < A.length; i += incr)
      for (int j = i; (j >= incr) && (A[j].compareTo(A[j-incr]) < 0); j -= incr)
        DSUtil.swap(A, j, j-incr);
  }
}
