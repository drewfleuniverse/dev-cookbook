public class ShellSort<E extends Comparable<? super E>> {
  public void sort(E[] A) {
    for (int i = maxSequenceIncrement(A.length); i >= 1; i /= 3) {
      for (int j = 0;  j < i; j++)
        insertionSort(A, j, i);
    }
    insertionSort(A, 0, 1);
  }
  private void insertionSort(E[] A, int start, int incr) {
    for (int i = start + incr; i < A.length; i += incr)
      for (int j = i; (j >= incr) && (A[j].compareTo(A[j-incr]) < 0); j -= incr)
        DSUtil.swap(A, j, j-incr);
  }
  private int maxSequenceIncrement(int length) {
    int h = 1;
    while (h < length/3) h = 3*h + 1;
    return h;
  }
}
