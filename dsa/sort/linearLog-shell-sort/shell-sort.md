# Shell sort

*Shellsort incrementally creates partially sorted subarray using insertion sort*

## Pseudocode

```js
insertionsort(arr, start, inc) {
  for (i = start + inc; i < arr.length; i += inc) {
    for (j = i; j >= inc && arr[j-inc] > arr[j]; j -= inc) {
      swap(arr, j, j-inc);
    }
  }
}

shellsort(arr) {
  for (inc = arr.length/2; inc > 2; inc /= 2) {
    for (let start = 0; start < inc; start++) {
      insertionsort(arr, start, inc);
    }
  }
  insertionsort(arr, 0, 1);
}
```

## Example

```
5 4 6 2 1 3

2 1 3 5 4 6
1 2 3 4 5 6
```


## Time complexity

**Division by 2 - Shell, 1959**

- Worst: O(n^2)
- Average: O(n^(3/2))
- Best: O(n*log2(n))

**Division by 3 - Pratt, 1971**

- Worst: O(n^(3/2))
- Average: O(n^(3/2))
- Best: O(n*log2(n))


## Space complexity

- Worst: 1


## Notes

**Pros**

- Fast for medium input array

**Cons**

- Unstable
- Slower than mergesort, heapsort, and quicksort
- Only good for medium input array
