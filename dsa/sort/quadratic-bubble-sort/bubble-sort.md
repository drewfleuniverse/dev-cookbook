# Bubblesort

*Bubble sort bubbles down larger elements*

## Pseudocode

```js
bubblesort(arr) {
  last = arr.length-1;
  swapped = true;
  while (swapped) {
    swapped = false;
    for (i = 1; i <= last; i++) {
      if (arr[i-1] > arr[i]) {
        swap(arr, i-1, i);
        swapped = true;
      }
    }
    last--;
  }
}
```


## Example

```
// input
5 4 6 2 1 3

// After each outer loop
4 5 2 1 3 6
4 2 1 3 5 6
2 1 3 4 5 6
1 2 3 4 5 6
1 2 3 4 5 6
```


## Time Complexity

- Worst: O(n^2)
- Average: O(n^2)
- Best: O(n)


## Space Complexity

- Worst: 1


## Notes

**Description**

- Also known as sinking sort
- Each outer loop guarantees：
  - Larger numbers are moved toward the end
  - The largest number of current array section are moved to the end
- With swapped check, the outer loop exits if the array section is sorted

**Pros**

- Fast on sorted or partially sorted array

**Cons**

- The slowest sort ever invented
- Lots of swaps

**Time complexity**

- Worst:
  - Outer loop: n-1 times
  - Inner loop: from n-1 to 1
  - Running time:
    - Sigma(i=1, n-1)(i)
    - (n-1) * ((n-1)+1) / 2
    - n*(n-1) / 2
  - T(n) = (n^2 - n)/2 = O(n^2)
- Best:
  - Happens when input array is sorted
  - Outer loop: 1 time
  - Inner loop: n-1 times
  - T(n) = n-1 = O(n)
