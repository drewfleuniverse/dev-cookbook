const cl = s => console.log(s);


const swap = (arr, i, j) => {
  const tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
};


const bubblesort = arr => {
  let last = arr.length-1;
  let swapped = true;
  while (swapped) {
    swapped = false;
    for (let i = 1; i <= last; i++) {
      if (arr[i-1] > arr[i]) {
        swap(arr, i-1, i);
        swapped = true;
      }
    }
    last--;
  }
};



let a;


a = [5,4,6,2,1,3];
bubblesort(a);
cl(a.toString() === '1,2,3,4,5,6');
