public class BubbleSort {
  public static <E extends Comparable<? super E>> void sort(E[] A) {
    for (int i = 0; i < A.length-1; i++)
      for (int j = A.length-1; j > i; j--)
        if (A[j].compareTo(A[j-1]) < 0)
          DSUtil.swap(A, j, j-1);
  }
}
