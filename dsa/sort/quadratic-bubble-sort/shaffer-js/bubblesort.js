const cl = s => console.log(s);


const swap = (arr, i, j) => {
  const tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
};


const bubblesort = arr => {
  const last = arr.length-1;
  for (let i = 0; i < last; i++) {
    for (let j = last; j > i; j--) {
      if (arr[j-1] > arr[j]) {
        swap(arr, j-1, j)
      }
    }
  }
};



let a;


a = [5,4,2,3,1];
bubblesort(a);
cl(a.toString() === '1,2,3,4,5');
