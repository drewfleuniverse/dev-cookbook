# Insertionsort

*Insertionsort bubbles up smaller elements*

## Pseudocode

```js
insertionsort(arr) {
  for (i = 1; i < arr.length; i++) {
    j = i;
    while (
      j > 0
      && arr[j-1] > arr[j]
    ) {
      swap(arr, j-1, j);
      j--;
    }
  }
}
```


## Example

```
5 4 6 2 1 3

4 5 6 2 1 3
4 5 6 2 1 3
2 4 5 6 1 3
1 2 4 5 6 3
1 2 3 4 5 6
```


## Time complexity

- Worst: O(n^2)
- Average: O(n^2)
- Best: O(n)


## Space complexity

- Worst: O(1)


## Notes

**Description**

- Stable sort
- In each iteration, outer loop produces a sorted array section which sized from 2 to n
- Inner loop compares the new element with the sorted section:
  - If the new element is smaller than the last element of previously sorted section, move it to correct position
  - Else, the array section is already sorted

**Pros**

- Fast for sorted and partially sorted array
- Fast for small input array
- Fastest quadratic sort

**Cons**

- Too many swap

**Time complexity**

- Worst
  - At each iteration, the inner loop runs incrementally so that:
    - i = 1, j=1 -> 1 time
    - i = 2, j=2,1 -> 2 times
    - i = n-1, j=n-1, n-2,...,2,1 -> n-1 times
  - 1 + 2 +...+ (n-1)
  - ((n-1) * (n)) / 2
  - T(n) = (n^2 - n)/2 = O(n^2)

- Average:
  - If inner loop has only half iterations
  - (((n/2) - 1) * (n/2)) / 2
  - ((n^2)/4 - n/2) / 2
  - T(n) = (n^2 - 2n)/8 = O(n^2)
- Best: If inner loop has no iteration, then the total iteration is n-1
