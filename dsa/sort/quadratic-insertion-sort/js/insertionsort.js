const cl = s => console.log(s);


const swap = (a, p, q) => {
  const t = a[p];
  a[p] = a[q];
  a[q] = t;
};


const insertionsort = arr => {
  for (let i = 1; i < arr.length; i++) {
    let j = i;
    while (
      j > 0
      && arr[j-1] > arr[j]
    ) {
      swap(arr, j-1, j);
      j--;
    }
  }
};



let a;


a = [5,4,6,2,1,3];
insertionsort(a);
cl(a.toString() === '1,2,3,4,5,6');
