public class InsertionSort<E extends Comparable<? super E>> {
  public void sort(E[] A) {
    for (int i = 1; i < A.length; i++)
      for (int j = i; (j > 0) && (A[j].compareTo(A[j-1]) < 0); j--)
        DSUtil.swap(A, j, j-1);
  }
}
