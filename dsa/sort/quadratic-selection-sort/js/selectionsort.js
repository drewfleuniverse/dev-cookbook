const cl = s => console.log(s);


const swap = (arr, i, j) => {
  const tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
};


const selectionsort = arr => {
  const len = arr.length;
  for (let i = 1; i < len; i++) {
    const begin = i-1;
    let min = begin;
    for (let j = i; j < len; j++) {
      if (arr[min] > arr[j]) {
        min = j;
      }
    }

    swap(arr, begin, min);
  }
};



let a;


a = [5,4,6,2,1,3];
selectionsort(a);
cl(a.toString() === '1,2,3,4,5,6');
