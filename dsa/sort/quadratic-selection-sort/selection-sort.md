# Selectionsort

*Selectionsort finds for the smaller elements and places them to front*

## Pseudocode

```js
selectionsort(arr) {
  for (i = 1; i < arr.length; i++) {
    begin = i-1;
    min = begin;
    for (j = i; j < arr.length; j++) {
      if (arr[min] > arr[j]) {
        min = j;
      }
    }

    swap(arr, begin, min);
  }
}
```

## Example

```
// Input
5 4 6 2 1 3

// After each outer loop
1 4 6 2 5 3
1 2 6 4 5 3
1 2 3 4 5 6
1 2 3 4 5 6
1 2 3 4 5 6
```


## Time complexity

- Worst: O(n^2)
- Average: O(n^2)
- Best: O(n^2)


## Space complexity

- Worst: O(1)


## Notes

**Description**

- The outer loop moves the smallest element of current array section to the front

**Cons**

- Not fast on partially sorted array

**Time complexity**

- Worst:
  - Outer loop: n-1 times
  - Inner loop: n-1 to 1 times
  - Since the inner loop always executes:
    - Sigma(i=1, n-1)(i)
    - (n-1) * ((n-1)+1) / 2
    - n * (n-1) / 2
  - T(n) = (n^2 - n)/2 = O(n^2)
