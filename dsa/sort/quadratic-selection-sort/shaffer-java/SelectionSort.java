public class SelectionSort<E extends Comparable<? super E>> {
  public void sort(E[] A) {
    for (int i = 0; i < A.length-1; i++) {
      int temp = i;
      for (int j = A.length-1; j > i; j--)
        if (A[j].compareTo(A[temp]) < 0)
          temp = j;
      DSUtil.swap(A, i, temp);
    }
  }
}
