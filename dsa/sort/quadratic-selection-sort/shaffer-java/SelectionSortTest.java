import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class SelectionSortTest {
  private Integer[] IA;

  @Before
  public void init() { A = new Integer[]{7, 6, 5, 4, 3, 2, 1}; }

  @Test
  public void evaluateBubbleSort() {
    assertEquals("7 6 5 4 3 2 1 ", arrayToString(IA, IA.length));
    SelectionSort<Integer> selSort = new SelectionSort<>();
    selSort.sort(IA);
    assertEquals("1 2 3 4 5 6 7 ", arrayToString(IA, IA.length));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
