class DemoAStack {
  public static void main(String[] args) {
    AStack<Integer> aStack = new AStack<>();
    aStack.push(1);
    aStack.push(2);
    aStack.push(3);
    System.out.println("Length: " + aStack.length());
    System.out.println("Pop: " + aStack.pop());
    System.out.println("Pop: " + aStack.pop());
    System.out.println("Top value: " + aStack.topValue());
    aStack.clear();
    System.out.println("Length: " + aStack.length());
    /*
    Length: 3
    Pop: 3
    Pop: 2
    Top value: 1
    Length: 0
    */
  }
}
