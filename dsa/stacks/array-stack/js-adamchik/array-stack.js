const cl = s => console.log(s)


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


const _capacity = Symbol('_capacity');
const _stack = Symbol('_stack')
const _top = Symbol('_top');

class ArrayStack {
  constructor(capacity=15) {
    this[_capacity] = capacity;
    this[_stack] = new Array(capacity);
    this[_top] = -1;
  }

  isEmpty() {
    return this[_top] === -1;
  }

  peek() {
    assert(!this.isEmpty());

    return this[_stack][this[_top]];
  }

  pop() {
    const data = this[_stack][this[_top]];
    // If data is object, destroy it
    this[_stack][this[_top]] = undefined;
    this[_top]--;

    return data;
  }

  push(data) {
    assert(
      this[_top]+1 !== this[_stack].length,
      'Stack overflow'
    );

    this[_top]++;
    this[_stack][this[_top]] = data;
  }

  clear() {
    this[_stack].fill(undefined);
    this[_top] = -1;
  }

  *[Symbol.iterator]() {
    for (let i = this[_top]; i >= 0; i--) {
      yield this[_stack][i];
    }
  }
}


let stack;


stack = new ArrayStack(3);

cl(stack.isEmpty() === true);

stack.push('foo');
stack.push('bar');
stack.push('42');

cl(stack.isEmpty() === false);
cl([...stack].toString() === '42,bar,foo');

try {
  stack.push('!');
} catch (e) {
  cl(e.message === 'Stack overflow')
}

cl(stack.peek() === '42');
cl(stack.pop() === '42');

stack.pop();
stack.pop();
cl([...stack].toString() === '');

stack.push('123');
cl(stack.isEmpty() === false);
stack.clear();
cl(stack.isEmpty() === true);
cl([...stack].toString() === '');
cl(stack[_stack].toString() === ',,');
