const cl = s => console.log(s)


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


const _stack = Symbol('_stack')

class ArrayStack {
  constructor(capacity=15) {
    this[_stack] = [];
  }

  isEmpty() {
    return this[_stack].length === 0;
  }

  peek() {
    assert(!this.isEmpty());
    const len = this[_stack].length;

    return this[_stack][len-1];
  }

  pop() {
    return this[_stack].pop();
  }

  push(data) {
    this[_stack].push(data);
  }

  clear() {
    const len = this[_stack].length;

    this[_stack].splice(0, len);
  }

  *[Symbol.iterator]() {
    for (let i = 0; i < this[_stack].length; i++) {
      yield this[_stack][i];
    }
  }
}


let stack;


stack = new ArrayStack(3);
cl(stack.isEmpty() === true);

stack.push('foo');
stack.push('bar');
stack.push('42');

cl(stack.isEmpty() === false);
cl([...stack].toString() === 'foo,bar,42');


cl(stack.peek() === '42');
cl(stack.pop() === '42');

stack.pop();
stack.pop();
cl([...stack].toString() === '');

stack.push('123');
cl(stack.isEmpty() === false);
stack.clear();
cl(stack.isEmpty() === true);
cl([...stack].toString() === '');
cl(stack[_stack].toString() === '');
