class DemoLStack {
  public static void main(String[] args) {
    LStack<Integer> lStack = new LStack<>();
    lStack.push(1);
    lStack.push(2);
    lStack.push(3);
    System.out.println("Length: " + lStack.length());
    System.out.println("Pop: " + lStack.pop());
    System.out.println("Pop: " + lStack.pop());
    System.out.println("Top value: " + lStack.topValue());
    lStack.clear();
    System.out.println("Length: " + lStack.length());
    /*
    Length: 3
    Pop: 3
    Pop: 2
    Top value: 1
    Length: 0
    */
  }
}
