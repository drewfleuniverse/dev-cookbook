/*
top->[null]
top->[n1->null]
top->[n2->n1->null]
*/
class LStack<E> implements Stack<E> {
  private Link<E> top; // top is merely a pointer to the top of node of stack
  private int size;

  LStack() { top = null; size = 0; }

  public void clear() { top = null; size = 0; }

  public void push(E it) { top = new Link<E>(it, top); size++; }

  public E pop() {
    assert top != null : "Stack is empty";
    E it = top.element();
    top = top.next();
    size--;
    return it;
  }

  public E topValue() {
    assert top != null : "Stack is empty";
    return top.element();
  }

  public int length() { return size;}
}
