const cl = s => console.log(s)


const assert = function (cond, msg='') {
  if (!cond) {
    throw new Error(msg);
  }
}


class Node {
  constructor(data, next=null) {
    this.data = data;
    this.next = next;
  }
}


const _top = Symbol('_top');

class ArrayStack {
  constructor() {
    this[_top] = null;
  }

  isEmpty() {
    return this[_top] === null;
  }

  peek() {
    assert(!this.isEmpty());

    return this[_top].data;
  }

  pop() {
    assert(!this.isEmpty());
    const data = this[_top].data;
    this[_top] = this[_top].next;

    return data;
  }

  push(data) {
    this[_top] = new Node(data, this[_top]);
  }

  clear() {
    this[_top] = null;
  }

  *[Symbol.iterator]() {
    let tmp = this[_top];
    while (tmp !== null) {
      yield tmp.data;
      tmp = tmp.next;
    }
  }
}


let stack;


stack = new ArrayStack(3);

cl(stack.isEmpty() === true);

stack.push('foo');
stack.push('bar');
stack.push('42');

cl(stack.isEmpty() === false);
cl([...stack].toString() === '42,bar,foo');

cl(stack.peek() === '42');
cl(stack.pop() === '42');

stack.pop();
stack.pop();
cl([...stack].toString() === '');

stack.push('123');
cl(stack.isEmpty() === false);
stack.clear();
cl(stack.isEmpty() === true);
cl([...stack].toString() === '');
