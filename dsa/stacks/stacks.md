# Stacks


## Method Names

- Access: N/A
- Search: N/A
- Insertion: push
- Deletion: pop


## Time complexity

**Average**

- Access: O(n)
- Search: O(n)
- Insertion: O(1)
- Deletion: O(1)

**Worst**

- Access: O(n)
- Search: O(n)
- Insertion: O(1)
- Deletion: O(1)


## Space complexity

- Worst: O(n)


## Application Examples

- Reverse a word
- Undo mechanism in text editors
- Backtracking: access most recent data, e.g. maze game
- Programming language processing:
    - Params and local variables
    - Syntax check for matching brackets
    - Recursion support
