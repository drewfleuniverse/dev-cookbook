import java.util.Arrays;
import java.util.Collections;

public class ArrayOperations {
  public static int max(int[] arr) {
    int max = arr[0];
    for (int i = 1; i < arr.length; i++)
      if (max < arr[i]) max = arr[i];

    return max;
  }

  public static double avg(int[] arr) {
    double sum = 0.0;
    for(int i = 0; i < arr.length; i++)
      sum += arr[i];
    double avg = sum / arr.length;

    return avg;
  }

  public static int[] dup(int[] arr) {
    int ARR_LEN = arr.length;
    int[] dup = new int[ARR_LEN];

    for(int i = 0; i < ARR_LEN; i++)
      dup[i] = arr[i];

    return dup;
  }

  public static void reverse(int[] arr) {
    int ARR_LEN = arr.length;
    int tmp;
    for(int i = 0; i < ARR_LEN / 2; i++) {
      tmp = arr[i];
      arr[i] = arr[ARR_LEN-i-1];
      arr[ARR_LEN-i-1] = tmp;
    }
  }

  public static int[][] sqmatrices(int[][] arr1, int[][] arr2) {
    int ARR_LEN = arr1.length;
    int[][] sqmt = new int[ARR_LEN][ARR_LEN];

    for(int i = 0; i < ARR_LEN; i++)
      for(int j = 0; j < ARR_LEN; j++)
        sqmt[i][j] = arr1[i][j] * arr2[i][j];

    return sqmt;
  }

  public static String printMatrix(int[][] m) {
    String s = "[ ";

    for(int i = 0; i < m.length; i++) {
      if (i != 0)
        s += ", ";
      s += Arrays.toString(m[i]);
    }

    s += " ]";

    return s;
  }

  public static void main (String[] args) {
    int[] array1 = {0, 1, 2, 3, 4, 5, 6};

    System.out.println("Array1 initialized value: " + Arrays.toString(array1));

    System.out.println("Find the max element of Array1...");
    int max1 = max(array1);
    System.out.println("Max: " + max1);

    System.out.println("Calculate the average value of array1...)");
    double avg = avg(array1);
    System.out.println("Average: " + avg);

    System.out.println("Duplacate Array1 to Array2...");
    int[] array2 = dup(array1);
    System.out.println("Array1 reference: " + array1);
    System.out.println("Array2 reference: " + array2);
    System.out.println("Array1 value: " + Arrays.toString(array1));
    System.out.println("Array2 value: " + Arrays.toString(array2));

    System.out.println("Reverse Array2...");
    reverse(array2);
    System.out.println("Array2 value: " + Arrays.toString(array2));

    System.out.println("Multiply mtarray1 and mtarray2...");
    int[][] mtarray1 = {{1, 2, 3}, {4, 5, 6}};
    int[][] mtarray2 = {{3, 2, 1}, {6, 5, 4}};
    int[][] sqmtarray = sqmatrices(mtarray1, mtarray2);
    System.out.println("Result: " + printMatrix(sqmtarray));
  }
}
