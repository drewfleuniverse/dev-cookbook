// java BinarySearch ../../algs4-data/tinyW.txt < ../../algs4-data/tinyT.txt

import java.util.*;
import edu.princeton.cs.algs4.*;

public class BinarySearch {
  public static int rank(int key, int[] a) {
    int lo = 0;
    int hi = a.length - 1;

    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      int cur = a[mid];

      if      (cur < key) lo = mid + 1;
      else if (key < cur) hi = mid - 1;
      else return mid;
    }
    return -1;
  }
  public static void main(String[] args) {
    int[] whitelist = In.readInts(args[0]);
    Arrays.sort(whitelist);

    while (!StdIn.isEmpty()) {
      int key = StdIn.readInt();
      if (rank(key, whitelist) == -1)
        StdOut.println(key);
    }
  }
}
