import java.util.Arrays;

public class JavaAPIs {
  public static void tryMath() {
    double n = -1.234;
    double a = 1.234;
    double b = 4.321;
    double t = Math.toRadians(35.0);

    System.out.println("Using methods in java.lang.Math..." + '\n');

    System.out.println("abs(" + n + "): " +              Math.abs(n));
    System.out.println("max(" + a + ", " + b + "): " +   Math.max(a, b));
    System.out.println("min(" + a + ", " + b + "): " +   Math.min(a, b));

    System.out.println("sin(" + t + "): " +              Math.sin(t));
    System.out.println("cos(" + t + "): " +              Math.cos(t));
    System.out.println("tan(" + t + "): " +              Math.tan(t));

    System.out.println("exp(1.0): " +                    Math.exp(1.0));
    System.out.println("log(Math.E): " +                 Math.log(Math.E));
    System.out.println("pow(" + a + ", "+ "2): " +       Math.pow(a, 2));

    double p = Math.pow(a, 2);

    System.out.println("random(): " +                    Math.random());
    System.out.println("sqrt(" + p + "): " +             Math.sqrt(p));
    System.out.println("Math.E: " +                      Math.E);
    System.out.println("Math.PI: " +                     Math.PI);
    System.out.println();
  }

  public static void tryArrays() {
    int[] a = {6, 4, 8, 5, 1, 3, 4, 5};
    int[] b;
    int[] c = new int[a.length];

    System.out.println("Using methods in java.util.Arrays..." + '\n');

    System.out.println("Execute Arrays.sort(a)...");
    Arrays.sort(a);

    System.out.println("Execute Arrays.copyOf(a, a.length)...");
    b = Arrays.copyOf(a, a.length);

    System.out.println("Execute Arrays.fill(a)...");
    Arrays.fill(c, 1);

    System.out.println("Print array values with Arrays.toString(int[])...");
    System.out.println("Value of array a: " + Arrays.toString(a));
    System.out.println("Value of array b: " + Arrays.toString(b));
    System.out.println("Value of array c: " + Arrays.toString(c));
    System.out.println();
  }

  public static void tryConversion() {
    String si = "123456";
    int i = 123456;
    String sd = "1.23456";
    double d = 1.23456;

    System.out.println("Integer.parseInt(\"" + si + "\"): "
                       + Integer.parseInt(si));
    System.out.println("Integer.toString(" + i + "): " +  Integer.toString(i));
    System.out.println("Double.parseDouble(\"" + sd + "\"): "
                       + Double.parseDouble(sd));
    System.out.println("Double.toString(" + d + "): " + Double.toString(d));
  }

  public static void main(String[] args) {
    tryMath();
    tryArrays();
    tryConversion();
  }
}
