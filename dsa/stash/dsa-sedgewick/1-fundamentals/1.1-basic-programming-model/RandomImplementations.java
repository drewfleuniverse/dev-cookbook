import java.util.Random;
import java.util.Arrays;

public class RandomImplementations {
  // Produce [a, b)
  public static double uniform(double a, double b) {
    return a + Math.random() * (b-a);
  }

  // Produce [0, N)
  public static int uniform(int N) {
    return (int)(Math.random() * N);
  }

  // Produce [lo, hi)
  public static int uniform(int lo, int hi) {
    return lo + uniform(hi-lo);
  }

  public static void shuffle(int[] a) {
    int N = a.length;
    int temp;
    int r;
    for(int i = 0; i < N; i++) {
      r = i + uniform(N-i);
      temp = a[i];
      a[i] = a[r];
      a[r] = temp;
    }
  }

  public static void main(String[] args) {
    System.out.println(uniform(1.00, 2.00));
    System.out.println(uniform(200));
    System.out.println(uniform(100, 200));

    int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    shuffle(a);
    System.out.println(Arrays.toString(a));
  }
}
