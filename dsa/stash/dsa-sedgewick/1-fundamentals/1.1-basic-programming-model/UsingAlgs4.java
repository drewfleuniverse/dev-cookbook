/*
 * Run this program without input ints: `java UsingAlgs4 < UsingAlgs4-ints.txt`
 */

import java.util.*;
import edu.princeton.cs.algs4.*;

public class UsingAlgs4 {
  public static void tryStdRandom() {
    System.out.println("Using methods in edu.princeton.cs.algs4.StdRandom...");
    System.out.println();

    long l = 123456;
    double[] a = {0.1, 1.1, 2.1, 3.1, 4.1};

    System.out.println("setSeed(" + l + ")");
    StdRandom.setSeed(l);
    System.out.println("random(): " + StdRandom.random());
    System.out.println("setSeed(" + (l + l) + ")");
    StdRandom.setSeed(l+l);
    System.out.println("random(): " + StdRandom.random());
    System.out.println("setSeed(" + l + ")");
    StdRandom.setSeed(l);
    System.out.println("random(): " + StdRandom.random());

    System.out.println("StdRandom.uniform(10): " + StdRandom.uniform(10));
    System.out.println("StdRandom.uniform(0, 10): " + StdRandom.uniform(0, 10));
    System.out.println("StdRandom.uniform(0.0, 10.0): "
                       + StdRandom.uniform(0.0, 10.0));
    System.out.println("StdRandom.uniform(10): " + StdRandom.uniform(10));

    System.out.println("StdRandom.bernoulli(0.5): " + StdRandom.bernoulli(0.5));
    System.out.println("StdRandom.bernoulli(0.4): " + StdRandom.bernoulli(0.4));

    System.out.println("shuffle(a), a = " + Arrays.toString(a));
    StdRandom.shuffle(a);
    System.out.println("a = " + Arrays.toString(a));
    System.out.println();
  }

  public static void tryStdStats() {
    System.out.println("Using methods in edu.princeton.cs.algs4.StdStats...");
    System.out.println();

    double[] a = {1.23, 2.34, 3.45, 4.56, 5.67};

    System.out.println("max(a): " + StdStats.max(a));
    System.out.println("min(a): " + StdStats.min(a));
    System.out.println("mean(a): " + StdStats.mean(a));
    System.out.println("var(a): " + StdStats.var(a));
    System.out.println("stddev(a): " + StdStats.stddev(a));
    // median is not within latest algs4.jar
    // System.out.println("median(a): " + StdStats.median(a));
    System.out.println();
  }

  public static void tryStdOut() {
    System.out.println("Using methods in edu.princeton.cs.algs4.StdOut...");
    System.out.println();

    StdOut.print("print()");
    StdOut.println("println()");
    StdOut.println();

    StdOut.printf("printf(\"%%8d\", 123):      \"%8d\"\n", 123);
    StdOut.printf("printf(\"%%-8d\", 123):     \"%-8d\"\n", 123);
    StdOut.printf("printf(\"%%8.2f\", PI):     \"%8.2f\"\n", Math.PI);
    StdOut.printf("printf(\"%%-8.2f\", PI):    \"%-8.2f\"\n", Math.PI);
    StdOut.printf("printf(\"%%8.2e\", PI):     \"%8.2e\"\n", Math.PI);
    StdOut.printf("printf(\"%%8s\", \"goof\"):   \"%8s\"\n", "goof");
    StdOut.printf("printf(\"%%-8s\", \"goof\"):  \"%-8s\"\n", "goof");
    StdOut.printf("printf(\"%%8.2s\", \"goof\"): \"%8.2s\"\n", "goof");

    System.out.println();
  }

  public static void tryStdIn() {
    System.out.println("Using methods in edu.princeton.cs.algs4.StdIn...");
    System.out.println();

    int sum = 0;
    while (!StdIn.isEmpty()) {
      sum += StdIn.readInt();
    }

    // Press `ctrl-d` to interrupt the input
    System.out.println("sum: " + sum);

    System.out.println();
  }

  public static int[] tryIn() {
    System.out.println("Using methods in edu.princeton.cs.algs4.In...");
    System.out.println();

    int[] ai = In.readInts("UsingAlgs4-ints.txt");

    System.out.println();

    return ai;
  }

  public static void tryOut(int[] ai) {
    System.out.println("Using methods in edu.princeton.cs.algs4.Out...");
    System.out.println();

    // `Our.write` is removed from latest `algs4.jar`
    // Out.write(ai, "UsingAlgs4-ints-out.txt");
    Out o = new Out("UsingAlgs4-ints-out.txt");

    for (int i = 0; i < ai.length; i++)
      o.println(ai[i]);

    o.close();

    System.out.println();
  }

  public static void tryStdDraw() {
    System.out.println("Using methods in edu.princeton.cs.algs4.StdDraw...");
    System.out.println();

    // By default, the canvas is sized as a unit square, with (0.0, 0.0) at the
    // lower left and (1.0, 1.0) at the upper right.

    // By default, pen radius is 0.005
    StdDraw.setPenRadius(0.03);
    StdDraw.point(1, 1);
    // By default, the drawing displays on the canvas until the canvas is
    // closed. `show()` changes the default drawing mode from `defer=false` to
    // `defer=true` for `dt` ms, then change back to `defer=false`.
    StdDraw.show(1000);
    StdDraw.clear();

    StdDraw.setPenColor(StdDraw.RED);
    StdDraw.point(.5, .5);
    StdDraw.setPenColor(StdDraw.GREEN);
    StdDraw.point(.0, .0);
    StdDraw.show(1000);
    StdDraw.clear();

    // StdDraw.setPenRadius(0.005);
    StdDraw.setPenColor(StdDraw.MAGENTA);

    // change scale doesn't change pen radius
    int N = 100;
    StdDraw.setXscale(0, N);
    StdDraw.setYscale(0, N * N);
    StdDraw.setPenRadius(.01);
    for (int i = 1; i <= N; i++) {
      StdDraw.point(i, i);
      StdDraw.point(i, i * i);
      StdDraw.point(i, i * Math.log(i));
    }
    StdDraw.show(1000);
    StdDraw.clear();

    StdDraw.setXscale(0, 1);
    StdDraw.setYscale(0, 1);
    int P = 50;
    double[] a = new double[P];
    for (int i = 0; i < P; i++)
      a[i] = StdRandom.random();
    for (int i = 0; i < P; i++) {
      double x = 1.0*i/P;
      double y = a[i]/2.0;
      double rw = 0.5/P;
      double rh = a[i]/2.0;
      // rectangle(double x, double y, double halfWidth, double halfHeight)
      StdDraw.filledRectangle(x, y, rw, rh);
    }
    StdDraw.show(1000);
    StdDraw.clear();

    int Q = 50;
    double[] b = new double[Q];
    for (int i = 0; i < Q; i++)
      b[i] = StdRandom.random();
    Arrays.sort(b);
    for (int i = 0; i < Q; i++) {
      double x = 1.0*i/Q;
      double y = b[i]/2.0;
      double rw = 0.5/Q;
      double rh = b[i]/2.0;
      StdDraw.filledRectangle(x, y, rw, rh);
    }
    StdDraw.show(1000);
    StdDraw.clear();

    System.out.println();
  }

  public static void main(String[] args) {
    tryStdRandom();
    tryStdStats();
    tryStdOut();
    tryStdIn();
    tryOut(tryIn());
    tryStdDraw();
  }
}
