# B-Tree


## Description

### Introduction
  - Invented by Rudolf Bayer and Edward M. McCreight in 1972
  - Height balanced
  - Large branching factor/lower actual height: Both of B-tree and red-black tree have height O(log(n)). However, a B-tree can have a larger log base to produce lower actual height to minimize disk fetch
  - Minimize disk I/O:
    - Typical node implementation has more then 100 children and fits a node in a disk block
    - Fast update and search, i.e. only require a few disk fetches
    - Store related records on same disk block

### Properties - Standard B-Tree
  - Root:
    - If the root is a leaf: Key >= 1
    - If the root is an internal node:
      - Key >= 1
      - Children >= 2
  - Every internal node, except root:
    - Order m
      - The maximum number of children of each node
      - Ceil(m/2) <= children <= m
    - Minimum degree t
      - The minimum number of keys a node can contain
      - t >= 2
      - t-1 <= keys <= 2t-1
      - t <= children <= 2t
      - E.g. 2-3-4 tree:
        - t = 2; each internal node has 1~3 keys and 2~4 children
  - Worst-case height h:
    - O(log_t(n))
    - N.b. n is the minimum number of keys at each node, n >= 1, t >= 2
  - All leaves are at the same level, i.e. tree height
  - All nodes are at least half full

### Operations - Standard B-Tree
  - Search:
    - Traverses down from the root
    - At each internal node, search procedure makes (n+1)-way branching decision
  - Insert:
    - Traverses down to locate the suitable leaf
    - Once reaches the leaf:
      - If the leaf has space, insert the element to the leaf
      - Otherwise, split the leaf into two with middle element promoted to parent, this process may repeat until:
        - Reaches a parent has space for the promoted element
        - Or, the root is split to accommodate the promoted element

### Properties - B+-Tree  
  - The most implemented B-Tree variant
  - B+-tree stores:
    - Keys in internal nodes
    - Keys with their satellite information, or records, in short, in leaves
  - Leaves may be implemented as double linked list


## Complexity

### Time (all variants)
  - Average	and worst:
    - Search: O(log(n))
    - Insert: O(log(n))
    - Delete: O(log(n))

### Space
  - Average and worst: O(n)

### Note

## B-Tree Pseudocode

### CLRS

#### \[CLRS\] BNode Class
```java
/** Class */
BNode<K,E>
/** Fields */
+ leaf: Boolean
+ n: Integer
+ keys: K[]
+ children: BNode<K,E>[]
/** Methods */
+ BNode()
```
  - keys has size 2t-1
  - children has size 2t


#### \[CLRS\] BTree Class
```java
/** Class */
BTree<K,E>
/** Fields */
- root: BNode<K,E>
/** Methods */
+ BTree()
+ search(k:K): E
+ insert(k:K): Void
- searchHelp(rt:BNode<K,E>, k:K): E
- splitChild()
```

#### \[CLRS\] BTree Class - BTree()
```js
BTree()
  n = new BNode(true)
  diskWrite(n)
  root = n
```
- Disk Operations: O(1)
- Total CPU time: O(1)

#### \[CLRS\] BTree Class - search() and searchHelp()
```js
search(k)
  return searchHelp(root, k)

searchHelp(rt, k)
  i = 0
  while ((i < rt.n) && (k > rt.keys[i]))
    i++
  if ((i < rt.n) && (k == rt.keys[i]))
    return rt.keys[i]
  else if (rt.leaf)
    return null
  else
    diskRead(rt.children[i])
    return search(rt.children[i], k)
```
- Disk Operations: O(h) = O(log_t(n))
- Total CPU time:
  - O(t*h) = O(t*log_t(n))
  - Because rt.n < 2t, each recursion step takes O(t)
- Note: n is the minimum number of keys at each node, n >= 1, t >= 2

#### \[CLRS\] BTree Class - splitChild()
```js
splitChild(p, i)
  // Step 1
  c1 = p.children[i]
  c2 = new new BNode()
  c2.leaf = c1.leaf
  c2.n = t-1
  // Step 2
  for (j = 0; j < t-1; j++)
    c2.keys[j] = c1.keys[j+t]
  // Step 3
  if (!c1.leaf)
    for (j = 0; j < t; j++)
      c2.children[j] = c1.children[j+t]
  c1.n = t-1
  // Step 4
  for (j = p.n; j > i; j--)
    p.children[j+1] = p.children[j]
  p.children[i+1] = c2
  // Step 5
  for (j = p.n-1; j >= i; j--)
    p.keys[j+1] = p.keys[j]
  p.keys[i] = c1.keys[t]
  p.n++
  // Step 6
  diskWrite(c1)
  diskWrite(c2)
  diskWrite(p)
```
  - splitChild(p, i)
    - p: parent, must not full, i.e. p.n < 2t-1
    - Assume p.children is full, i.e. p.children.length() = 2t+2
  - Step 1: Create a new child node c2
  - Step 2: Copy the right half of keys of c1 to c2, i.e. c1[t, 2t-2]
  - Step 3: Copy the right half of children of c1 to c2, i.e. c1.children[t, 2t-1]
  - Step 4: Move parent's children one position to the right from range [i+1, p.n] to [i+2, p.n+1] and insert c2 to p.children[i+1]
  - Step 5: Move parent's keys one position to the right from range [i, p.n-1] to [i+1, p.n] and insert the promoted key to p.keys[i]
  - Disk Operations: O(1)
  - Total CPU time: Theta(t)

#### \[CLRS\] BTree Class - insert()
```js
```

## B+-Tree Pseudocode

### Shaffer

#### \[Shaffer\] Class
```java
/** Class */

/** Fields */

/** Methods */

```

#### \[Author\] Class - foo()
```js
foo()
  // ...
```

###### Description

###### Code Comments


## Other References
  - [\[google\/btree\]BTree implementation for Go](https://github.com/google/btree)
