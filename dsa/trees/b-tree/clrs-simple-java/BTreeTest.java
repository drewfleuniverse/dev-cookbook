import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BTreeTest {
  @Test
  public void evaluatesBTree() {
    // The BTree implementation has bug, e.g. 19 couldn't be found
    Integer[] IA = new Integer[]{5, 0, 4, 2, 7, 1, 9, 6, 8, 3};
    BTree<Integer, Integer> bt = new BTree<>();
    bt.insert(new Integer(5), new Integer(5));
    bt.insert(new Integer(9), new Integer(9));
    bt.insert(new Integer(30), new Integer(30));
    bt.insert(new Integer(28), new Integer(28));
    bt.insert(new Integer(19), new Integer(19));
    bt.insert(new Integer(25), new Integer(25));
    bt.insert(new Integer(6), new Integer(6));
    System.out.println(bt.search(new Integer(6)));
    System.out.println(bt.search(new Integer(19)));
    System.out.println(bt.search(new Integer(12)));
  }
}
