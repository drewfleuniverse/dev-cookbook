// May need import

class BTree<K extends Comparable<? super K>, V> {
  private static final int T = 2;
  private Node<K,V> root;

  private class Record<K,V> {
    K key;
    V value;
    Record(K k, V v) { key = k; value = v; }
  }

  private class Node<K,V> {
    boolean leaf;
    int n;
    Record<K,V>[] records = (Record<K,V>[])new Record[2*T-1];
    Node<K,V>[] children = (Node<K,V>[])new Node[2*T];

    Node(boolean l, int n) { leaf = l; n = n; }
  }

  public BTree() {
    root = new Node(true, 0);
  }

  public V search(K k) {
    return searchHelper(root, k);
  }

  private V searchHelper(Node<K,V> rt, K k) {
    int i = 0;
    while ((i < rt.n) && (k.compareTo(rt.records[i].key) > 0))
      i++;
    if ((i < rt.n) && (k.compareTo(rt.records[i].key) == 0))
      return rt.records[i].value;
    else if (rt.leaf)
      return null;
    else return searchHelper(rt.children[i], k);
  }

  public void insert(K k, V v) {
    if (root.n == (2*T-1)) {
      Node<K,V> temp = root;
      root = new Node(false, 0);
      root.children[0] = temp;
      splitChild(root, 0);
      insertNonfullHelper(root, new Record(k, v));
    } else insertNonfullHelper(root, new Record(k, v));
  }

  private void insertNonfullHelper(Node<K,V> rt, Record<K,V> rec) {
    int i = rt.n - 1;
    K k = rec.key;
    if (rt.leaf) {
      while ((i >= 0) && (k.compareTo(rt.records[i].key) < 0)) {
        rt.records[i+1] = rt.records[i];
        i--;
      }
      // rt.records[i+1] has been copied to rt.records[i+2]
      // rt.records[i+2].key > rec.key
      rt.records[i+1] = rec;
      rt.n++;
      System.out.println(
        "rt.records[i+1].key: " + rt.records[i+1].key + "; rt.n: " + rt.n);
    } else {
      while ((i >= 0) && (k.compareTo(rt.records[i].key) < 0))
        i--;
      // Before i++: k > rt.records[i].key
      // After i++: k < rt.records[i].key
      i++;
      if (rt.children[i].n == (2*T-1)) {
        splitChild(rt, i);
        if (k.compareTo(rt.records[i].key) > 0)
          i++;
      }
      insertNonfullHelper(rt.children[i], rec);
    }
  }

  // rt.records[ci] will have the promoted record from splitting child
  private void splitChild(Node<K,V> rt, int ci) {
    Node<K,V> lc = rt.children[ci];
    Node<K,V> rc = new Node<>(lc.leaf, T-1);
    for (int i = 0; i < T-1; i++)
      rc.records[i] = lc.records[T+i];
    if (!lc.leaf)
      for (int i = 0; i < T; i++)
        rc.children[i] = lc.children[T+i];
    lc.n = T-1; // lc.records[T] is going to be promoted to rt.records[]
    // rt has rt.n+1 children; move rt.children [ci+1, rt.n] to [ci+2, rt.n+1]
    for (int i = rt.n; i > ci; i--)
      rt.children[i+1] = rt.children[i];
    rt.children[ci+1] = rc;
    // Originally, rt.records[ci] > rt.children[ci].records[0..*]
    // If rt.children[ci] is splitted, the promoted record should be placed
    // before rt.record[ci], thus move rt.record [ci, rt.n-1] to [ci+1, rt.n]
    for (int i = rt.n - 1; i >= ci; i--)
      rt.records[i+1] = rt.records[i];
    rt.records[ci] = lc.records[T];
    rt.n++;
  }
}
