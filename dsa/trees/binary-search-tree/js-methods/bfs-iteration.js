/*
Perf: O(n)
Space - Worst: O(n)
Space - Average: O(log(n))
 */

class Node {
  constructor(val, left=null, right=null) {
    this.val = val;
    this.left = left;
    this.right = right;
  }
}


const breadthFirstTraverse = function (root) {
  if (root === null) {
    return '';
  }
  const queue = [root];
  let path = [];
  while (queue.length !== 0) {
    const curr = queue.shift();
    path.push(curr.val);
    if (curr.left !== null) {
      queue.push(curr.left);
    }
    if (curr.right !== null) {
      queue.push(curr.right);
    }
  }
  return path.toString();
};


describe('breadthFirstTraverse()', () => {
  it('should return empty path', () => {
    const tree = null;
    assert(breadthFirstTraverse(tree) === '');
  });
  it('should return correct path', () => {
    const tree = new Node(7);
    tree.left = new Node(3);
    tree.right = new Node(9);
    tree.left.left = new Node(1);
    tree.left.right = new Node(6);
    assert(breadthFirstTraverse(tree) === '7,3,9,1,6');
  });
});


/*
Utils
 */

function describe (s,f) {console.log(`# ${s} #`);f();};
function it (s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert (c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
