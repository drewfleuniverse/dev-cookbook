/*
Node Class
 */

class Node {
  constructor(key, left=null, right=null) {
    this.key = key;
    this.left = left;
    this.right = right;
  }

  toString() {
    return `${this.key}`
  }
}


/*
BST Class
 */

const sRoot = Symbol('sRoot');
const sGetMin = Symbol('sGetMin');
const sGetMax = Symbol('sGetMax');
const sInsert = Symbol('sInsert');
const sSearch = Symbol('sSearch');
const sPreOrder = Symbol('sPreOrder');
const sInOrder = Symbol('sInOrder');
const sPostOrder = Symbol('sPostOrder');
const sDeleteMin = Symbol('sDeleteMin');
const sDeleteMax = Symbol('sDeleteMax');
const sDelete = Symbol('sDelete');
const sClone = Symbol('sClone');
const sGetHeight = Symbol('sGetHeight');
const sGetLeaves = Symbol('sGetLeaves');
const sRestore = Symbol('sRestore');
const sGetWidth = Symbol('sGetWidth');
const sGetDiameter = Symbol('sGetDiameter');


class BST {
  constructor() {
    this[sRoot] = null;
  }
  

  /*
  Access
   */

  getMin() {
    if (this[sRoot] === null) {
      throw new Error('Cannot get min');
    }
    return this[sGetMin](this[sRoot]).key;
  }

  [sGetMin](node) {
    if (node.left === null) {
      return node;
    }
    return this[sGetMin](node.left);
  }

  getMax() {
    if (this[sRoot] === null) {
      throw new Error('Cannot get min');
    }
    return this[sGetMax](this[sRoot]).key;
  }

  [sGetMax](node) {
    if (node.right === null) {
      return node;
    }
    return this[sGetMax](node.right);
  }


  /*
  Insertion
   */

  insert(key) {
    this[sRoot] = this[sInsert](this[sRoot], key);
  }

  [sInsert](node, key) {
    if (node === null) {
      return new Node(key);
    }

    if (node.key > key) {
      node.left = this[sInsert](node.left, key);
    } else if (node.key < key) {
      node.right = this[sInsert](node.right, key);
    } else {
      ;// if node has value, set value here
    }

    return node;
  }

  insertIterative(key) {
    if (this[sRoot] === null) {
      this[sRoot] = new Node(key);
      return;
    }

    let curr = this[sRoot];
    while (curr !== null) {
      if (curr.key > key) {
        if (curr.left === null) {
          curr.left = new Node(key);
          return;
        }
        curr = curr.left;
      } else if (curr.key < key) {
        if (curr.right === null) {
          curr.right = new Node(key);
          return;
        }
        curr = curr.right;
      } else {
        // same key case / infinite loop prevention
        return;
      }
    }
  }

  insertIterative2(key) {
    if (this[sRoot] === null) {
      this[sRoot] = new Node(key);
      return;
    }

    let parent = null;
    let curr = this[sRoot];
    while (curr !== null) {
      parent = curr;
      if (curr.key > key) {
        curr = curr.left;
      } else if (curr.key < key) {
        curr = curr.right;
      } else {
        // same key case / infinite loop prevention
        return;
      }
    }

    if (parent.key > key) {
      parent.left = new Node(key);
    } else {
      parent.right = new Node(key);
    }
  }

  /*
  Search
   */

  search(key) {
    return this[sSearch](this[sRoot], key);
  }

  [sSearch](node, key) {
    if (node === null) {
      return null;
    }
    if (node.key === key) {
      return node.key;
    }

    if (node.key > key) {
      return this[sSearch](node.left, key);
    } else {
      return this[sSearch](node.right, key);
    }
  }

  searchIterative(key) {
    let curr = this[sRoot];

    while (curr !== null) {
      if (curr.key > key) {
        curr = curr.left;
      } else if (curr.key < key){
        curr = curr.right;
      } else {
        return curr.key;
      }
    }

    return null;
  }

  breadthFirstSearch(key, path) {
    if (this[sRoot] === null) {
      return null;
    }

    const queue = [];
    queue.push(this[sRoot]);
    while (queue.length !== 0) {
      const curr = queue.shift();
      if (path !== undefined) {
        path.push(curr.key);
      }
      if (curr.key === key) {
        return curr.key;
      }
      if (curr.left !== null) {
        queue.push(curr.left);
      }
      if (curr.right !== null) {
        queue.push(curr.right);
      }
    }

    return null;
  }

  depthFirstSearch(key, path) {
    // preOrder
    if (this[sRoot] === null) {
      return null
    }

    const stack = [];
    stack.push(this[sRoot]);
    while (stack.length !== 0) {
      const curr = stack.pop();
      if (path !== undefined) {
        path.push(curr.key);
      }
      if (curr.key === key) {
        return curr.key;
      }
      if (curr.right !== null) {
        stack.push(curr.right);
      }
      if (curr.left !== null) {
        stack.push(curr.left);
      }
    }

    return null;
  }

  /*
  Deletion
   */

  deleteMin() {
    if (this[sRoot] === null) {
      throw new Error('Cannot delete');
    }
    this[sRoot] = this[sDeleteMin](this[sRoot]);
  }

  [sDeleteMin](node) {
    if (node.left === null) {
      return node.right;
    }

    node.left = this[sDeleteMin](node.left);
    return node;
  }

  deleteMax() {
    if (this[sRoot] === null) {
      throw new Error('Cannot delete');
    }
    this[sRoot] = this[sDeleteMax](this[sRoot]);
  }

  [sDeleteMax](node) {
    if (node.right === null) {
      return node.left;
    }

    node.right = this[sDeleteMax](node.right);
    return node;
  }

  delete(key) {
    if (this[sRoot] === null) {
      throw new Error('Cannot delete');
    }
    this[sRoot] = this[sDelete](this[sRoot], key);
  }

  [sDelete](node, key) {
    if (node === null) {
      return null;
    }

    if (node.key > key) {
      node.left = this[sDelete](node.left, key);
    } else if (node.key < key) {
      node.right = this[sDelete](node.right, key);
    } else {
      if (node.left === null) {
        return node.right;
      } else if (node.right === null) {
        return node.left;
      }

      const tmp = node;
      node = this[sGetMin](tmp.right);
      node.right = this[sDeleteMin](tmp.right);
      node.left = tmp.left;
    }

    return node;
  }

  deleteIterative(key) {

  }

  /*
  Traversal
   */

  preOrder() {
    const path = [];
    this[sPreOrder](this[sRoot], path);
    return path;
  }

  [sPreOrder](node, path) {
    if (node === null) {
      return [];
    }

    path.push(node.key);
    this[sPreOrder](node.left, path);
    this[sPreOrder](node.right, path);
  }

  preOrderIter() {
    if (this[sRoot] === null) {
      return [];
    }

    const stack = [];
    stack.push(this[sRoot]);

    const path = [];
    while (stack.length !== 0) {
      const curr = stack.pop();
      path.push(curr.key);

      if (curr.right !== null) {
        stack.push(curr.right);
      }
      if (curr.left !== null) {
        stack.push(curr.left);
      }
    }

    return path;
  }

  inOrder() {
    const path = [];
    this[sInOrder](this[sRoot], path);
    return path;
  }

  [sInOrder](node, path) {
    if (node === null) {
      return [];
    }

    this[sInOrder](node.left, path);
    path.push(node.key);
    this[sInOrder](node.right, path);
  }

  postOrder() {
    const path = [];
    this[sPostOrder](this[sRoot], path);
    return path;
  }

  [sPostOrder](node, path) {
    if (node === null) {
      return [];
    }

    this[sPostOrder](node.left, path);
    this[sPostOrder](node.right, path);
    path.push(node.key);
  }

  breadthFirst() {
    if (this[sRoot] === null) {
      return [];
    }

    const queue = [];
    queue.push(this[sRoot]);

    const path = [];
    while (queue.length !== 0) {
      const curr = queue.shift();
      path.push(curr.key);

      if (curr.left !== null) {
        queue.push(curr.left);
      }
      if (curr.right !== null) {
        queue.push(curr.right);
      }
    }

    return path;
  }


  /*
  Utils
   */

  clone() {
    let copy = new BST();
    copy[sRoot] = this[sClone](this[sRoot]);
    return copy;
  }

  [sClone](node) {
    if (node === null) {
      return null;
    }

    const curr = new Node(
      node.key,
      this[sClone](node.left),
      this[sClone](node.right),
    );

    return curr;
  }

  getHeight() {
    // use 0-based count here for tree height
    return this[sGetHeight](this[sRoot]) - 1;
  }

  [sGetHeight](node) {
    if (node === null) {
      return 0;
    }

    return 1 + Math.max(
      this[sGetHeight](node.left),
      this[sGetHeight](node.right),
    );
  }

  getHeightIter() {
    if (this[sRoot] === null) {
      return -1;
    }

    const NodeInfo = function (node, height) {
      this.node = node;
      this.height = height;
    };

    let maxHeight = 0;
    let queue = [];
    queue.push(new NodeInfo(this[sRoot], 0));

    while (queue.length > 0) {
      const { node, height } = queue.shift();

      if (node.left !== null) {
        queue.push(new NodeInfo(node.left, height + 1));
      }
      if (node.right !== null) {
        queue.push(new NodeInfo(node.right, height + 1));
      }

      if (height > maxHeight) {
        maxHeight = height;
      }
    }

    return maxHeight;
  }

  getLeaves() {
    return this[sGetLeaves](this[sRoot]);
  }

  [sGetLeaves](node) {
    if (node === null) {
      return 0;
    }

    if (
      node.left === null
      && node.right === null
    ) {
      return 1;
    }

    return this[sGetLeaves](node.left) + this[sGetLeaves](node.right);
  }

  getLeavesIter() {
    if (this[sRoot] === null) {
      return 0;
    }

    let leaves = 0;
    const queue = [];
    queue.push(this[sRoot]);

    while (queue.length) {
      const curr = queue.shift();

      if (
        curr.left === null &&
        curr.right === null
      ) {
        leaves++;
      }

      if (curr.left !== null) {
        queue.push(curr.left);
      }
      if (curr.right !== null) {
        queue.push(curr.right);
      }
    }

    return leaves;
  }

  restore(preOrder, inOrder) {
    this[sRoot] = this[sRestore](
      preOrder,
      0,
      preOrder.length - 1,
      inOrder,
      0,
      inOrder.length - 1,
    );
  }

  [sRestore](preOrder, preL, preR, inOrder, inL, inR) {
    if (preL > preR) {
      return null;
    }

    let leftCount = 0;
    const parentKey = preOrder[preL];
    const parent = new Node(parentKey);

    while (parentKey !== inOrder[inL + leftCount]) {
      leftCount++;
    }

    parent.left = this[sRestore](
      preOrder,
      preL + 1,
      preL + leftCount,
      inOrder,
      inL,
      inL + leftCount - 1
    );
    parent.right = this[sRestore](
      preOrder,
      preL + leftCount + 1,
      preR,
      inOrder,
      inL + leftCount + 1,
      inR
    );

    return parent;
  }

  getWidth() {
    const height = this.getHeight();
    let max = 0;
    for (let depth = 0; depth <= height; depth++) {
      const temp = this[sGetWidth](this[sRoot], depth);
      if (temp > max) {
        max = temp;
      }
    }

    return max;
  }

  [sGetWidth](node, depth) {
    if (node === null) {
      return 0;
    }
    if (depth === 0) {
      return 1;
    }

    return (
      this[sGetWidth](node.left, depth - 1) +
      this[sGetWidth](node.right, depth - 1)
    );
  }

  /* returns max node count of the path from root to left and right leaves */
  getDiameter() {
    return this[sGetDiameter](this[sRoot]);
  }

  [sGetDiameter](node) {
    if (node === null) {
      return 0;
    }

    const count1 = (
      this[sGetHeight](node.left) +
      this[sGetHeight](node.right) +
      3
    );

    const count2 = Math.max(
      this[sGetDiameter](node.left),
      this[sGetDiameter](node.right),
    );

    // return count1;
    return Math.max(count1, count2);
  }

  toString() {
    return [...this].toString();
  }

  *[Symbol.iterator]() {
    if (this[sRoot] === null) {
      return;
    }

    const queue = [];
    queue.push(this[sRoot]);
    while (queue.length !== 0) {
      const curr = queue.shift();
      yield curr.key;

      if (curr.left !== null) {
        queue.push(curr.left);
      }
      if (curr.right !== null) {
        queue.push(curr.right);
      }
    }
  }
}


/*
Tests
 */

const buildBST = () => {
  const bst = new BST();
  bst.insert(4);
  bst.insert(2);
  bst.insert(5);
  bst.insert(1);
  bst.insert(3);

  return bst; // 4|2,5|1,3
};
assert(buildBST().toString() === '4,2,5,1,3');

const buildBST2 = () => {
  const bst = new BST();
  bst.insert(6);
  bst.insert(2);
  bst.insert(1);
  bst.insert(4);
  bst.insert(3);
  bst.insert(5);
  bst.insert(7);

  return bst; // 6|2,7|1,4|3,5
};
assert(buildBST2().toString() === '6,2,7,1,4,3,5');

/*
Test access
 */

describe('BST.getMin()', () => {
  it('gets min', () => {
    const bst = buildBST(); // 4|2,5|1,3
    assert(bst.getMin() === 1);
  });
});

describe('BST.getMin()', () => {
  it('gets max', () => {
    const bst = buildBST(); // 4|2,5|1,3
    assert(bst.getMax() === 5);
  });
});

/*
Test insertion
 */

describe('BST.insert()', () => {
  it('builds BST', () => {
    const bst = new BST();
    bst.insert(4);
    bst.insert(2);
    bst.insert(5);
    bst.insert(1);
    bst.insert(3);

    assert(bst.toString() === '4,2,5,1,3');
  });

  it('omits duplicates', () => {
    const bst = new BST();
    bst.insert(5);
    bst.insert(5);

    assert(bst.toString() === '5');
  });
});

describe('BST.insertIterative()', () => {
  it('builds BST', () => {
    const bst = new BST();
    bst.insertIterative(4);
    bst.insertIterative(2);
    bst.insertIterative(5);
    bst.insertIterative(1);
    bst.insertIterative(3);

    assert(bst.toString() === '4,2,5,1,3');
  });

  it('omits duplicates', () => {
    const bst = new BST();
    bst.insertIterative(5);
    bst.insertIterative(5);

    assert(bst.toString() === '5');
  });
});

describe('BST.insertIterative2()', () => {
  it('builds BST', () => {
    const bst = new BST();
    bst.insertIterative2(4);
    bst.insertIterative2(2);
    bst.insertIterative2(5);
    bst.insertIterative2(1);
    bst.insertIterative2(3);

    assert(bst.toString() === '4,2,5,1,3');
  });

  it('omits duplicates', () => {
    const bst = new BST();
    bst.insertIterative2(5);
    bst.insertIterative2(5);

    assert(bst.toString() === '5');
  });
});

/*
Test search
 */

describe('BST.search()', () => {
  const bst = buildBST(); // 4|2,5|1,3

  it('returns node if found', () => {
    assert(bst.search(1) === 1);
  });

  it('returns null if not found', () => {
    assert(bst.search(Infinity) === null);
  });
});

describe('BST.searchIterative()', () => {
  const bst = buildBST(); // 4|2,5|1,3

  it('returns node if found', () => {
    assert(bst.searchIterative(1) === 1);
  });

  it('returns null if not found', () => {
    assert(bst.searchIterative(Infinity) === null);
  });
});

describe('BST.breadthFirstSearch()', () => {
  const bst = buildBST(); // 4|2,5|1,3

  it('returns node if found', () => {
    assert(bst.breadthFirstSearch(1) === 1);
  });

  it('returns null if not found', () => {
    assert(bst.breadthFirstSearch(42) === null);
  });
});

describe('BST.depthFirstSearch()', () => {
  const bst = buildBST(); // 4|2,5|1,3

  it('returns node if found', () => {
    assert(bst.depthFirstSearch(1) === 1);
  });

  it('returns null if not found', () => {
    assert(bst.depthFirstSearch(42) === null);
  });
});

/*
Test deletion
 */

describe('BST.deleteMin', ()=> {
  it('deletes min', () => {
    const bst = buildBST(); // 4|2,5|1,3
    bst.deleteMin();
    assert(bst.toString() === '4,2,5,3');
  });

  it('throws error when deleting empty tree', () => {
    const bst = new BST();
    try {
      bst.deleteMin();
    } catch (e) {
      assert(e.message === 'Cannot delete');
    }
  });
});

describe('BST.deleteMax', ()=> {
  it('deletes max', () => {
    const bst = buildBST(); // 4|2,5|1,3
    bst.deleteMax();
    assert(bst.toString() === '4,2,1,3');
  });

  it('throws error when deleting empty tree', () => {
    const bst = new BST();
    try {
      bst.deleteMax();
    } catch (e) {
      assert(e.message === 'Cannot delete');
    }
  });
});

describe('BST.delete', ()=> {
  it('deletes node has no left or right child', () => {
    const bst = buildBST(); // 4|2,5|1,3
    bst.delete(5);
    assert(bst.toString() === '4,2,1,3');
  });

  it('deletes node has left and right children', () => {
    const bst = buildBST(); // 4|2,5|1,3
    bst.delete(2);
    assert(bst.toString() === '4,3,5,1');
  });

  it('throws error when deleting empty tree', () => {
    const bst = new BST();
    try {
      bst.delete();
    } catch (e) {
      assert(e.message === 'Cannot delete');
    }
  });
});

/*
Test traversal
 */

describe('BST.preOrder()', () => {
  it('has path...', () => {
    const bst = buildBST(); // 4|2,5|1,3
    const path = bst.preOrder();
    assert(path.toString() === '4,2,1,3,5');
  });
});

describe('BST.preOrderIter()', () => {
  it('has path...', () => {
    const bst = buildBST(); // 4|2,5|1,3
    const path = bst.preOrderIter();
    assert(path.toString() === '4,2,1,3,5');
  });
});

describe('BST.inOrder()', () => {
  it('has path...', () => {
    const bst = buildBST(); // 4|2,5|1,3
    const path = bst.inOrder();
    assert(path.toString() === '1,2,3,4,5');
  });
});

describe('BST.postOrder()', () => {
  it('has path...', () => {
    const bst = buildBST(); // 4|2,5|1,3
    const path = bst.postOrder();
    assert(path.toString() === '1,3,2,5,4');
  });
});

describe('BST.breadthFirst()', () => {
  it('has path...', () => {
    const bst = buildBST(); // 4|2,5|1,3
    const path = bst.breadthFirst();
    assert(path.toString() === '4,2,5,1,3');
  });

  it('returns empty array if tree empty', () => {
    const bst = new BST();
    assert(bst.breadthFirst().length === 0);
  });
});


/*
Test Utils
 */

describe('BST.clone()', () => {
  it('clones', () => {
    const bst = buildBST().clone();
    const copy = bst.clone()
    assert(copy[sRoot] !== bst[sRoot]);
    assert(copy.toString() === '4,2,5,1,3');
  });
});

describe('BST.getHeight()', () => {
  it('gets height on non empty tree', () => {
    const bst = buildBST2(); // 6|2,7|1,4|3,5
    const height = bst.getHeight();
    assert(height === 3);
  });

  it('returns 0 on one node tree', () => {
    const bst = new BST();
    bst.insert(42);
    const height = bst.getHeight();
    assert(height === 0);
  });

  it('returns -1 on empty tree', () => {
    const bst = new BST();
    const height = bst.getHeight();
    assert(height === -1);
  });
});

describe('BST.getHeightIter()', () => {
  it('gets height on non empty tree', () => {
    const bst = buildBST2(); // 6|2,7|1,4|3,5
    const height = bst.getHeightIter();
    assert(height === 3);
  });

  it('returns 0 on one node tree', () => {
    const bst = new BST();
    bst.insert(42);
    const height = bst.getHeightIter();
    assert(height === 0);
  });

  it('returns -1 on empty tree', () => {
    const bst = new BST();
    const height = bst.getHeightIter();
    assert(height === -1);
  });
});

describe('BST.getLeaves()', () => {
  it('gets leaves on non empty tree', () => {
    const bst = buildBST();
    const leaves = bst.getLeaves();
    assert(leaves === 3);
  });

  it('returns 0 on empty tree', () => {
    const bst = new BST();

    const leaves = bst.getLeaves();
    assert(leaves === 0);
  });
});

describe('BST.getLeavesIter()', () => {
  it('gets leaves on non empty tree', () => {
    const bst = buildBST();
    const leaves = bst.getLeavesIter();
    assert(leaves === 3);
  });

  it('returns 0 on empty tree', () => {
    const bst = new BST();

    const leaves = bst.getLeavesIter();
    assert(leaves === 0);
  });
});

describe('BST.restore()', () => {
  it('restores non-empty tree', () => {
    const preOrder = [4,2,1,3,5];
    const inOrder = [1,2,3,4,5];
    const bst = new BST();
    bst.restore(preOrder, inOrder);
    assert(bst.toString() === '4,2,5,1,3');
  });
});

describe('BST.getWidth()', () => {
  it('gets height on non empty tree', () => {
    const bst = buildBST2(); // 6|2,7|1,4|3,5
    const width = bst.getWidth();
    assert(width === 2);
  });

  it('returns 0 on empty tree', () => {
    const bst = new BST();
    const width = bst.getWidth();
    assert(width === 0);
  });
});

describe('BST.getDiameter()', () => {
  it('gets diameter on non empty tree', () => {
    const bst = buildBST2(); // 6|2,7|1,4|3,5
    // Max count path:
    // `6` + `2,4,3` + `2,4,5`
    const diameter = bst.getDiameter();
    assert(diameter === 7);
  });

  it('returns 0 on empty tree', () => {
    const bst = new BST();
    const diameter = bst.getDiameter();
    assert(diameter === 0);
  });
});

describe('BST.toString()', () => {
  it('prints non-empty bst using breadth first traversal', () => {
    const bst = buildBST();
    assert(bst.toString() === '4,2,5,1,3');
  });

  it('prints empty string on empty bst', () => {
    const bst = new BST();
    assert(bst.toString() === '');
  });
});


/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
