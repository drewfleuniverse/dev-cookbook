import java.lang.Comparable;

/**
 * Attributes:
 *   - private BSTNode<K,E> root
 *   - private int nodeCount
 *   - private StringBuffer out
 * Methods, implements Dictionary<K, E>:
 *   - public void clear()
 *   - public void insert(K k, E e)
 *   - public E remove(K k)
 *   - public E removeAny()
 *   - public E find(K k)
 *   - public int size()
 * Methods:
 *   - BST()
 *   - private BSTNode<K,E> insertHelp(BSTNode<K,E> rt, K k, E e)
 *   - private E findHelp(BSTNode<K,E> rt, K k)
 *   - private BSTNode<K,E> removeHelp(BSTNode<K,E> rt, K k)
 *   - private BSTNode<K,E> getMin(BSTNode<K,E> rt)
 *   - private BSTNode<K,E> deleteMin(BSTNode<K,E> rt)
 *   - private BSTNode<K,E> printHelp(BSTNode<K,E> rt)
 *   - private void printVisit(E it)
 *   - public String toString()
 *
 * Notes:
 *   - `<T extends Comparable<T>>` is equivalent to the expression of
 *     `public class Name implements Comparable<Name> {...}` and indicates T
 *     must have implemented Comparable interface
 */
class BST<K extends Comparable<? super K>, E> implements Dictionary<K, E> {
  private BSTNode<K,E> root;
  private int nodeCount;

  BST() { root = null; nodeCount = 0; }

  /**
   * Reinitialize the tree
   */
  public void clear() { root = null; nodeCount = 0; }

  /**
   * Insert data
   * @param K k  Key to insert
   * @param E e  Value to insert
   */
  public void insert(K k, E e) { root = insertHelp(root, k, e); nodeCount++; }

  /**
   * Remove a node
   * @param  K k  Key of the node to remove
   * @return  Value of the removed node
   */
  public E remove(K k) {
    E temp = findHelp(root, k);
    if (temp != null) {
      root = removeHelp(root, k);
      nodeCount--;
    }
    return temp;
  }

  /**
   * Code:
   *   1. Check if the tree is empty
   *   2. Tasks:
   *     - Get the element value of the root
   *     - Get the new tree by removing the root node through calling removeHelp
   *     - Decrement nodeCount
   *   3. Return the element value of old root node
   * @return The data value of the removed root node; null if the tree is empty
   */
  public E removeAny() {
    if (root == null) return null;
    E temp = root.element();
    root = removeHelp(root, root.key());
    nodeCount--;
    return  temp;
  }

  /**
   * @param  K k  Key to find
   * @return  Element of the matched node
   */
  public E find(K k) { return findHelp(root, k); }

  /**
   * @return Node count
   */
  public int size() { return nodeCount; }

  /**
   * Code:
   *   1. Stop 1: If the tree is empty or the recursion reached the leaf,
   *     return a new node with given KV pair
   *   2. Recursion 1 & 2: Recursively traverses down to the leaf node:
   *     - Compare the given key with the key of current node
   *     - Call itself for comparing the child key and the given key
   *     - Reset its child to the original child node or set its empty child
   *       to the new node to insert
   *   3. Stop 2: When the recursive calls are finished, return the
   *     visited node.
   * @param  BSTNode<K,E> rt  Root
   * @param  K            k   Key of data
   * @param  E            e   Value of data
   * @return  The root of the new tree; the new root node if tree was empty
   */
  private BSTNode<K,E> insertHelp(BSTNode<K,E> rt, K k, E e) {
    if (rt == null) return new BSTNode<K,E>(k, e);
    if (rt.key().compareTo(k) > 0) rt.setLeft(insertHelp(rt.left(), k, e));
    else rt.setRight(insertHelp(rt.right(), k, e));
    return rt;
  }

  /**
   * Code:
   *   1. Stop 1: If the tree is empty or no node was found, return null
   *   2. Recursively traversing down or return the data value of the matched
   *     node:
   *     - Recursion 1 & 2: If the current node has a different key, call itself
   *       to proceed to its child
   *     - Stop 2: If key match, return the element value of the matched node
   * @param  BSTNode<K,E> rt  Root tree
   * @param  K            k   Key
   * @return  The value of the node has been found; null if node key not found
   */
  private E findHelp(BSTNode<K,E> rt, K k) {
    if (rt == null) return null;
    if (rt.key().compareTo(k) > 0) return findHelp(rt.left(), k);
    else if (rt.key().compareTo(k) == 0) return rt.element();
    else return findHelp(rt.right(), k);
  }

  /**
   * Code:
   *   1. Stop 1: If the tree is empty or no node was found, return null
   *   2. Recursively traversing down or remove the node:
   *     - Recursion 1 & 2: Traversing down the tree if key not match:
   *       - Compare the key
   *       - Call removeHelp itself with its child
   *       - Point the child of the current node to the subtree returned by
   *         removeHelp
   *     - Found the node to remove:
   *       - If one of its child is empty, return its only child
   *         - If both of its children are empty, null is implicitly returned
   *       - If the node has two children:
   *         - Get the minimum node from the right subtree
   *         - Copy the key and element value of the minimum node to the current
   *           node
   *         - Remove the minimum node from the right subtree
   *         - Point the right child to the new right subtree
   *   - Stop 2: Return the root node of the new tree or the modified/
   *     unmodified subtree
   * @param  BSTNode<K,E> rt  Root
   * @param  K            k   Key of the data to remove
   * @return  The root of the modified tree; null if node not found
   */
  private BSTNode<K,E> removeHelp(BSTNode<K,E> rt, K k) {
    if (rt == null) return null;
    if (rt.key().compareTo(k) > 0) rt.setLeft(removeHelp(rt.left(), k));
    else if (rt.key().compareTo(k) < 0) rt.setRight(removeHelp(rt.right(), k));
    else {
      if (rt.left() == null) return rt.right();
      else if (rt.right() == null) return rt.left();
      else {
        BSTNode<K,E> temp = getMin(rt.right());
        rt.setKey(temp.key());
        rt.setElement(temp.element());
        rt.setRight(deleteMin(rt.right()));
      }
    }
    return rt;
  }

  /**
   * Return the smallest (leftest) node
   * Code:
   *   1. Stop 1: If the current node has null left child, return the current
   *     node
   *   2. Recursion 1: Traversing down toward and return the smallest node
   * @param  BSTNode<K,E> rt  Root node of subtree; node cannot be null
   * @return  The node with the minimum key
   */
   private BSTNode<K,E> getMin(BSTNode<K,E> rt) {
     // What if the tree is empty?
     if (rt.left() == null) return rt;
     return getMin(rt.left());
   }

   /**
    * Remove the smallest node by replacing it with its right child
    * Code:
    *   1. Stop 1: If the current node has empty left child, return the right
    *     subtree to replace the node
    *   2. Recursion 1: Traversing down the tree toward the leftest (smallest)
    *     node:
    *     - Call deleteMin itself with the current node's left child
    *     - Point the current node's left pointer to the node returned by
    *       deleteMin
    *   3. Stop 2: Return the visiting node after each stage of the recursion
    *     has completed
    * Note:
    *   - Since deleteMin only returns the visiting nodes right child when
    *   the visiting node has the minimum key, the rest of the calls to
    *   deleteMin return the unmodified left child of the visiting node
    * @param  BSTNode<K,E> rt  Root node of subtree; node cannot bew empty
    * @return  The root of the modified tree
    */
   private BSTNode<K,E> deleteMin(BSTNode<K,E> rt) {
     if (rt.left() == null) return rt.right();
     rt.setLeft(deleteMin(rt.left()));
     return rt;
   }

   private StringBuffer out;
   private void printHelp(BSTNode<K,E> rt) {
     if (rt == null) return;
     printHelp(rt.left());
     out.append(rt.element() + " ");
     printHelp(rt.right());
   }
   public String toString() {
     out = new StringBuffer(100);
     printHelp(root);
     return out.toString();
   }

   private void preOrderHelper(BSTNode<K,E> rt) {
     if (rt == null) return;
     out.append(rt.element() + " ");
     preOrderHelper(rt.left());
     preOrderHelper(rt.right());
   }
   public String preOrder() {
     out = new StringBuffer();
     preOrderHelper(root);
     return out.toString();
   }

   private void postOrderHelper(BSTNode<K,E> rt) {
     if (rt == null) return;
     postOrderHelper(rt.left());
     postOrderHelper(rt.right());
     out.append(rt.element() + " ");
   }
   public String postOrder() {
     out = new StringBuffer();
     postOrderHelper(root);
     return out.toString();
   }

   private void inOrderHelper(BSTNode<K,E> rt) {
     if (rt == null) return;
     inOrderHelper(rt.left());
     out.append(rt.element() + " ");
     inOrderHelper(rt.right());
   }
   public String inOrder() {
     out = new StringBuffer();
     inOrderHelper(root);
     return out.toString();
   }
}
