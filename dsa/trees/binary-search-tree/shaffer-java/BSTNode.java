/**
 * `BSTNode` implements`BinNode` using generic subtyping. Additional methods are
 * as below:
 * - key(), setKey(), setLeft(), setRight()
 * @param K The key
 * @param E The data element
 */
class BSTNode<K,E> implements BinNode<E> {
  private K key;
  private E element;
  private BSTNode<K,E> left;
  private BSTNode<K,E> right;

  /**
   * Constructors for `BSTNode`.
   * - `BSTNode()`: [probably]Construct null node
   * - `BSTNode(K,E)`: [probably]Construct leaf node
   * - `BSTNode(K,E,BSTNode,BSTNode)`: Construct internal node
   */
  public BSTNode() { left = right = null; }
  public BSTNode(K k, E e) { left = right = null; key = k; element = e; }
  public BSTNode(K k, E e, BSTNode<K,E> l, BSTNode<K,E> r) {
    key = k; element = e; left = l; right = r;
  }

  /** Additional methods for subtyped `BinNode` */
  public K key() { return key; }
  public void setKey(K k) { key = k; }

  public E element() { return element; }
  public void setElement(E e) { element = e; }

  public BSTNode<K,E> left() { return left; }
  /** Additional method for subtyped `BinNode` */
  public void setLeft(BSTNode<K,E> l) { left = l; }

  public BSTNode<K,E> right() { return right; }
  /** Additional method for subtyped `BinNode` */
  public void setRight(BSTNode r) { right = r; }

  /** True is leaf node, false otherwise */
  /** [what is difference between a leaf node and a null node? Does a BST have
  an additional null node?] */
  public boolean isLeaf() { return (left == null) && (right == null); }
}
