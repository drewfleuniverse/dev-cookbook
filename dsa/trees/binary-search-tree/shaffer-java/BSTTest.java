import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BSTTest {
  @Test
  public void evaluatesExpression() {
    BST<Integer,Integer> bst = new BST<Integer,Integer>();
    /**
     * Create a BST as below:
     *      5
     *     / \
     *   2    5
     *  / \  /
     * 1  3 6
     */
    assertEquals(bst.size(), 0);
    bst.insert(5, 5);
    assertEquals(bst.size(), 1);
    bst.insert(5, 5);
    assertEquals(bst.size(), 2);
    bst.insert(2, 2);
    assertEquals(bst.size(), 3);
    bst.insert(6, 6);
    assertEquals(bst.size(), 4);
    bst.insert(3, 3);
    assertEquals(bst.size(), 5);
    bst.insert(1, 1);
    assertEquals(bst.size(), 6);
    assertEquals("1 2 3 5 5 6 ", bst.toString());

    /**
     * Test bst traversals
     */
    assertEquals("5 2 1 3 5 6 ", bst.preOrder());
    assertEquals("1 3 2 6 5 5 ", bst.postOrder());
    assertEquals("1 2 3 5 5 6 ", bst.inOrder());


    /**
     * Test operations
     */
    assertEquals(bst.find(7), null);
    assertEquals(bst.find(6), new Integer(6));
    assertEquals(bst.find(2), new Integer(2));
    assertEquals(bst.find(3), new Integer(3));

    bst.remove(2);
    assertEquals(bst.size(), 5);
    assertEquals(bst.toString(), "1 3 5 5 6 ");

    bst.removeAny();
    assertEquals(bst.size(), 4);
    assertEquals(bst.toString(), "1 3 5 6 ");

    assertEquals(bst.remove(7), null);
    assertEquals(bst.remove(6), new Integer(6));

    bst.clear();
    assertEquals(bst.size(), 0);
  }
}
