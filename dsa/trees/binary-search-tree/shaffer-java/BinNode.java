/**
 * - As its name implies, `BinNode` is a node interface for binary tree, thus it
 *   has 2 children, i.e. a left child and a right child.
 * - `BinNode` interface also not defines the folowing methods:
 *    - key(), setKey(), setLeft(), setRight()
 */
public interface BinNode<E> {
  public E element();
  public void setElement(E v);

  /**
   * BinNode base class doesn't define `setLeft` and `setRight`, the need of
   * of these methods depends on the implementation of BinNode
   */
  public BinNode<E> left();
  public BinNode<E> right();

  /**
   * @return True if a leaf node, false otherwise
   */
  public boolean isLeaf();
}
