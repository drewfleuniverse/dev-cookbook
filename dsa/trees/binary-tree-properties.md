# Binary Tree Properties

## Terms

```
         [A]
      /       \
   [B]         [C]
  /   \       /
[D]   [E]   [F]
```
  - A:
    - the root;
    - parent of B & C
    - at level 0
    - depth 0
  - C:
    - has two children
    - left child F
    - right child empty
  - B & C:
    - left and right subtree of A
    - children of A;
    - at level 1
    - depth 1
  - D & E: the left and right children of B
  - A & C: ancestors of F
  - C & F: descendant of A
  - A, B, C: internal nodes
  - D, E, & F:
    - at level 2
    - leaves
    - depth 2
  - A-C, C-F:
    - 2 edges;
    - forms a path A-C-F
  - A-C-F:
    - a path;
    - has 3 nodes
    - has length 2, i.e. 3-1
  - Height:
    - 3, i.e. max depth + 1


## Tree Types

### Full Binary Tree

Definition
  - Each node of a full binary tree:
    - Must be an internal node has two non-empty children;
    - Or, a leaf, i.e. one of the non-empty children

Full binary tree theorem
  - Given n internal nodes, the number of leaves is n+1

### Complete Binary Tree

Definition
  - A complete binary tree is filled from left to right starts from the root
