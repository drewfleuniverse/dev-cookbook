# Binary Tree Traversal - Depth First Search


## Example Tree

```
       A
    /     \
   B       C
    \     / \
     D   E   F
```


## Preorder Traversal

- Enumeration: ABDCEF

### Using Stack

###### Pseudocode

```js
preorder(node)
  if (node == null)
    return
  visit(node)
  preorder(node.leftChild)
  preorder(node.rightChild)
```

###### Stack

```js
preorder(A) | visit(A)
preorder(A) | preorder(B) | visit(B)
preorder(A) | preorder(B) | preorder(D) | visit(D)
preorder(A) | preorder(C) | visit(C)
preorder(A) | preorder(C) | preorder(E) | visit(E)
preorder(A) | preorder(C) | preorder(F) | visit(F)
```

### Using Iteration

###### Pseudocode

```js
preorder(node)
  S = []
  S.push(node)
  while(S.length != 0)
    cur = S.pop()
    visit(cur)
    if (cur.rightChild)
      S.push(cur.rightChild)
    if (cur.leftChild)
      S.push(leftChild)
```

###### Iterations

```js
[A]
visit(A), [C, B]
visit(B), [C, D]
visit(D), [C]
visit(C), [F, E]
visit(E), [F]
visit(F)
```


## Postorder Traversal

- Enumeration: `DBEFCA`
- Path: `A B [D] [B] A C [E] C [F] [C] [A]`

### Using Stack

###### Pseudocode

```js
postorder(node)
  if (node == null)
    return
  postorder(node.leftChild)
  postorder(node.rightChild)
  visit(node)
```

###### Stack

```js
postorder(A) | postorder(B) | postorder(D) | visit(D)
postorder(A) | postorder(B) | visit(B)
postorder(A) | postorder(C) | postorder(E) | visit(E)
postorder(A) | postorder(C) | postorder(F) | visit(F)
postorder(A) | postorder(C) | visit(C)
postorder(A) | visit(A)
```

### Using Iteration

###### Pseudocode

```js
postorder(node)
  S = []
  lastVisited = null
  while(S.length != 0 || node != null)
    if (node != null)
      S.push(node)
      node = node.leftChild
    else
      peek = S.peek()
      if (peek.rightChild != null
        && lastVisited != peek.rightChild)
        node = peek.rightChild
      else
        visit(peek)
        lastVisited = S.pop()
```

###### Iterations

```js
node=B, lastVisited=null, [A]
node=null, lastVisited=null, [A, B]
node=D, lastVisited=null, peek=B, [A, B]
node=null, lastVisited=null, [A, B, D]
node=null, lastVisited=D, peek=D, visit(D), [A, B]
node=null, lastVisited=B, peek=B, visit(B), [A]
node=C, lastVisited=B, peek=A, [A]
node=E, lastVisited=B, [A, C]
node=null, lastVisited=B, [A, C, E]
node=null, lastVisited=E, peek=E, visit(E), [A, C]
node=F, lastVisited=E, peek=C, [A, C]
node=null, lastVisited=E, [A, C, F]
node=null, lastVisited=F, peek=F, visit(F), [A, C]
node=null, lastVisited=C, peek=C, visit(C), [A]
node=null, lastVisited=A, peek=A, visit(A), []
```



## Inorder Traversal

- Enumeration: BDAECF

### Using Stack

###### Pseudocode

```js
inorder(node)
  if (node == null)
    return
  inorder(node.leftChild)
  visit(node)
  inorder(node.rightChild)
```

###### Stack

```js
inorder(A) | inorder(B) | visit(B)
inorder(A) | inorder(B) | inorder(D) | visit(D)
inorder(A) | visit(A)
inorder(A) | inorder(C) | inorder(E) | visit(E)
inorder(A) | inorder(C) | visit(C)
inorder(A) | inorder(C) | inorder(F) | visit(F)
```
