# Complete Binary Tree - Array Implementation


## Usage
- Max Heap
- Min Heap
- Heap Sort


## Example

```
Number of nodes: 6

         [0]
      /       \
   [1]         [2]
  /   \       /
[3]   [4]   [5]
```

Position      | 0 | 1 | 2 | 3 | 4 | 5
------------- |---|---|---|---|---|---
Parent        | - | 0 | 0 | 1 | 1 | 2
Left Child    | 1 | 3 | 5 | - | - | -
Right Child   | 2 | 4 | - | - | - | -
Left Sibling  | - | - | 1 | - | 3 | -
Right Sibling | - | 2 | - | 4 | - | -


## Formulae

p, position; n, number of nodes
- Parent: floor( (p-1)/2 ), p != 0
- Left Child: (p*2)+1, (p*2)+1 < n
- Right Child: (p*2)+2, (p*2)+2 < n
- Left Sibling: p-1, p%2 == 0
- Right Sibling: p+1, p%2 == 1 && p+1 < n

Why not to add an extra test for `p!=0`?:
- Left Sibling: p-1, p%2 == 0 && p != 0
- Right Sibling: p+1, p%2 == 1 && (p != 0 || p+1 < n)


## Example

\*, first leaf
-, node next to the node that has last left child
+, node next to the node that has last right child

```
Number of nodes: 1
         [0]*
```
```
Number of nodes: 2
         [0]
      /
   [1]*-
```
```
Number of nodes: 3
         [0]
      /       \
   [1]*-+      [2]
```
```
Number of nodes: 4
         [0]
      /       \
   [1]+        [2]*-
  /
[3]
```
```
Number of nodes: 5
         [0]
      /       \
   [1]         [2]*-+
  /   \
[3]   [4]
```
```
Number of nodes: 6
         [0]
      /       \
   [1]         [2]+
  /   \       /
[3]*- [4]   [5]
```

Number of nodes                         | 1 | 2 | 3 | 4 | 5 | 6
--------------------------------------- |---|---|---|---|---|---
First leaf                              | 0 | 1 | 1 | 2 | 2 | 3
Last internal node                      | - | 0 | 0 | 1 | 1 | 2
Node next to the node has a left child  | - | 1 | 1 | 2 | 2 | 3
Node next to the node has a right child | - | - | 1 | 1 | 2 | 2
Last left child                         | - | 1 | 1 | 3 | 3 | 5
Last right child                        | - | - | 2 | 2 | 4 | 4

n, number of nodes
- First leaf: floor( n/2 ), n > 0
- Last internal node: floor( n/2 )-1 or floor( (n-2)/2 ), n > 1
- Node next to the node has a left child: floor( n/2 ), n > 1
  - The last node has a left child must be an internal node, thus the node must
    be smaller than n/2, the first leaf position
- Node next to the node has a right child: floor( (n-1)/2 ), n > 2
  - The sequence is moved one position further by subtracting 1/2 before being
    floored:<br>
    floor( n/2 - 1/2 ) -> floor( (n-1)/2 )
    - The last node has a right child must be an internal node and one position
      precedes the last node that has a left child when node count is even. To
      find the node next to the last node that has a right child, subtract n by
      1 so the floored result is one position smaller when n is even
- Last left child: 1+2*floor( (n-2)/2 ), n > 1
- Last right child: 2+2*floor( (n-3)/2 ), n > 2


## Height

`height = floor(log2(n))`

Length(l) | Height | Descriptions
----------|--------|---------------------
 1        | 0      | root
 2        | 1      | hight 1, first leaf
 3        | 1      | hight 1, last leaf
 4        | 2      | hight 2, first leaf
 7        | 2      | hight 2, last leaf
 8        | 3      | hight 3, first leaf
 15       | 3      | hight 3, last leaf
 16       | 4      | hight 4, first leaf


## Last node at each level

`last = pow(2, height+1)-2`

Index(i) | Height | Last | Descriptions
---------|--------|------|----------------------
 0       | 0      | 0    | root
 1       | 1      | 2    | height 1, first node
 3       | 2      | 6    | height 2, first node
 7       | 3      | 14   | height 3, first node
