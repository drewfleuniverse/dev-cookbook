/**
 * Fields:
 *   - private GTNode<E> parent
 *   - private GTNode<E> leftChild
 *   - private GTNode<E> rightSibling
 *   - private E element
 * Ctors:
 *   - public GTNode(E value)
 *   - public GTNode(E value, GTNode<E> p, GTNode<E> l, GTNode<E> r)
 * Methods:
 *   - public E value()
 *   - public boolean isLeaf()
 *   - public GTNode<E> parent()
 *   - public GTNode<E> leftChild()
 *   - public GTNode<E> rightSibling()
 *   - public void setValue(E v)
 *   - public void setParent(GTNode<E> p)
 *   - public void inserFirst(GTNode<E> f)
 *   - public void insertRight(GTNode<E> r)
 *   - public void removeFirst()
 *   - public void removeRight()
 */
public class GTNode<E> {
  private GTNode<E> parent;
  private GTNode<E> leftChild;
  private GTNode<E> rightSibling;
  private E element;

  public GTNode(E v) {
    parent = leftChild = rightSibling = null;
    element = v;
  }
  public GTNode(E v, GTNode<E> p, GTNode<E> l, GTNode<E> r) {
    element = v;
    parent = p; leftChild = l; rightSibling = r;
  }

  public E value() { return element; }
  public boolean isLeaf() { return leftChild == null; }
  public GTNode<E> parent() { return parent; }
  public GTNode<E> leftChild() { return leftChild; }
  public GTNode<E> rightSibling() { return rightSibling; }
  public void setValue(E v) { element = v; }
  public void setParent(GTNode<E> p) { parent = p; }

  public void inserFirst(GTNode<E> f) {
    f.parent = this;
    f.rightSibling = leftChild;
    leftChild = f;
  }
  public void insertRight(GTNode<E> r) {
    r.parent = parent;
    r.rightSibling = rightSibling;
    rightSibling = r;
  }
  public void removeFirst() {
    if (leftChild == null) return;
    leftChild = leftChild.rightSibling;
  }
  public void removeRight() {
    if (rightSibling == null) return;
    rightSibling = rightSibling.rightSibling;
  }
}
