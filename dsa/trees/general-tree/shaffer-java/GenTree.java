/**
 * Fields:
 *   - private GTNode<E> rt
 * Ctor:
 *   - public GenTree()
 * Methods:
 *   - public void clear()
 *   - public GTNode<E> root()
 *   - public void newRoot(E value, GTNode<E> firstChild, GTNode<R> secondChild)
 *   - public void newLeftChild(E value)
 */
public class GenTree<E> {
  private GTNode<E> rt;

  public GenTree() { rt = null; }

  public void clear() { rt = null; }
  public GTNode<E> root() { return rt; }

  /**
   * newRoot serves two purposes:
   *   1. Initialize a tree with a root with up to two children
   *   2. Merge two trees as the children of a new root
   */
  public void newRoot(E value, GTNode<E> firstChild, GTNode<E> secondChild) {
    clear();
    rt = new GTNode<E>(value, null, firstChild, secondChild);
    if (firstChild != null) firstChild.setParent(rt);
    if (secondChild != null) secondChild.setParent(rt);
  }

  public void newLeftChild(E value) {
    // GTNode<E> temp = new GTNode<E>(value, rt, null, rt.leftMostChild());
    GTNode<E> temp = new GTNode<E>(value, rt, null, null);
    rt.inserFirst(temp);
  }

  private StringBuffer out;
  public void preOrder(GTNode<E> rt) {
    out.append("(");
    out.append(rt.value() + " ");
    // if (rt.rightSibling() == null) out.append("| ");
    if (!rt.isLeaf()) {
      GTNode<E> child = rt.leftChild();
      while (child != null) {
        preOrder(child);
        child = child.rightSibling();
      }
    }
    out.append(")");
  }
  public String toString() {
    out = new StringBuffer();
    preOrder(rt);
    return out.toString();
  }
}
