import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class GenTreeTest {
  /**
   * Build a general tree as below:
   *        1
   *     /  |  \
   *    4   3   2
   *  / | \
   * 6  7 5
   */
  @Test
  public void evaluateGenTree() {
    GenTree<Integer> GT = new GenTree<>();

    GT.newRoot(new Integer(1), null, null);
    assertEquals("(1 )", GT.toString());

    GT.newLeftChild(new Integer(2));
    GT.newLeftChild(new Integer(3));
    GT.newLeftChild(new Integer(4));
    assertEquals("(1 (4 )(3 )(2 ))", GT.toString());

    GTNode<Integer> firstLevelChild1 = GT.root().leftChild();
    GTNode<Integer> secondLevelChild1 = new GTNode<Integer>(new Integer(5));
    GTNode<Integer> secondLevelChild2 = new GTNode<Integer>(new Integer(6));
    GTNode<Integer> secondLevelChild3 = new GTNode<Integer>(new Integer(7));
    firstLevelChild1.inserFirst(secondLevelChild1);
    firstLevelChild1.inserFirst(secondLevelChild2);
    secondLevelChild2.insertRight(secondLevelChild3);
    assertEquals("(1 (4 (6 )(7 )(5 ))(3 )(2 ))", GT.toString());
  }
}
