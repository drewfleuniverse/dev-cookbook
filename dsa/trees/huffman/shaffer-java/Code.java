/**
 * Fields:
 *   - private char data
 *   - private String code
 * Ctor:
 *   - public Code(char d, String c)
 * Methods:
 *   - public char data()
 *   - public String code()
 */
public class Code {
  /** data field stores character or symbol */
  private char data;
  /** Huffman code representation of the data */
  private String code;

  public Code(char d, String c) { data = d; code = c; }

  public char data() { return data; }
  public String code() { return code; }

  public String toString() {
    StringBuffer buf = new StringBuffer();
    buf.append(data);
    buf.append("  ");
    buf.append(code);
    return buf.toString();
  }
}
