import java.math.*;
import java.util.*;

/**
 * Fields:
 * private static Random value
 *
 * Methods:
 * static <E> void swap(E[] A, int p1, int p2)
 * static void permute(int[] A)
 * static void swap(int[] A, int p1, int p2)
 * static <E> void permute(E[] A)
 * static int random(int n)
 */
class DSUtil<E> {
  static <E> void swap(E[] A, int p1, int p2) {
    E temp = A[p1];
    A[p1] = A[p2];
    A[p2] = temp;
  }

  static void swap(int[] A, int p1, int p2) {
    int temp = A[p1];
    A[p1] = A[p2];
    A[p2]= temp;
  }
  /**
   * Randomly permute array A, i.e. shuffle array elements
   * @param  E[] A
   */
  static <E> void permute(E[] A) {
    for (int i = A.length; i > 0; i--)
     swap(A, i-1, random(i));
  }

  /**
   * Randomly permute array A, , i.e. shuffle array elements
   * @param  int[] A
   */
  static void permute(int[] A) {
    for (int i = A.length; i > 0; i--)
     swap(A, i-1, random(i));
  }

  private static Random value = new Random();
  /**
   * @param  int n
   * @return  A random integer i, 0 <= i <= n-1
   */
  static int random(int n) {
    return Math.abs(value.nextInt()) % n;
  }
}
