public interface HuffBaseNode<E> {
  public boolean isLeaf();
  public int weight();
}
