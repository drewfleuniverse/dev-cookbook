/**
 * Fields:
 *   - private int weight
 *   - private HuffBaseNode<E> left
 *   - private HuffBaseNode<E> right
 * Ctor:
 *   - HuffInternalNode(HuffBaseNode<E> l, HuffBaseNode<E> r, int w)
 * Methods:
 *   - public HuffBaseNode<E> left()
 *   - public HuffBaseNode<E> right()
 * Methods implement HuffBaseNode:
 *   - public int weight()
 *   - public boolean isLeaf()
 */

 public class HuffInternalNode<E> implements HuffBaseNode<E> {
   private int weight;
   private HuffBaseNode<E> left;
   private HuffBaseNode<E> right;

   public HuffInternalNode(HuffBaseNode<E> l, HuffBaseNode<E> r, int w) {
     left = l; right = r; weight = w;
   }

   public HuffBaseNode<E> left() { return left; }
   public HuffBaseNode<E> right() { return right; }

   @Override public boolean isLeaf() { return false; }
   @Override public int weight() { return weight; }
 }
