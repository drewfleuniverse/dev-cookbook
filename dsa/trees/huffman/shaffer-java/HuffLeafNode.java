/**
 * Fields:
 *   - private E element
 *   - private int weight
 * Ctor:
 *   - public HuffLeafNode(E e, int w)
 * Methods:
 *   - public E element()
 * Methods implement HuffBaseNode:
 *   - public int weight()
 *   - public boolean isLeaf()
 */
public class HuffLeafNode<E> implements HuffBaseNode<E> {
  private E element;
  private int weight;

  public HuffLeafNode(E e, int w) { element = e; weight = w; }

  public E element() { return element; }

  @Override public int weight() { return weight; }
  @Override public boolean isLeaf() { return true; }
}
