/**
 * Fields:
 *   - private HuffBaseNode<E> root
 * Ctors:
 *   - public HuffTree(E e, int w)
 *   - public HuffTree(HuffBaseNode<E> l, HuffBaseNode<E> r, int w)
 * Methods:
 *   - public HuffBaseNode<E> root()
 *   - public int weight()
 * Method implements Comparable:
 *   - public int compareTo(HuffTree<E> that)
 */

public class HuffTree<E> implements Comparable<HuffTree<E>> {
  private HuffBaseNode<E> root;

  /** Ctor inits root with HuffLeafNode */
  public HuffTree(E e, int w) { root = new HuffLeafNode<E>(e, w); }

  /** Ctor inits root with HuffInternalNode */
  public HuffTree(HuffBaseNode<E> l, HuffBaseNode<E> r, int w) {
    root = new HuffInternalNode<E>(l, r, w);
  }

  /** @return  Get the actual huffman node pointer */
  public HuffBaseNode<E> root() { return root; }

  /** @return  Get the weight of HuffTree's root */
  public int weight() { return root.weight(); }

  public int compareTo(HuffTree<E> that) {
    if (root.weight() < that.weight()) return -1;
    else if (root.weight() == that.weight()) return 0;
    else return 1;
  }


  private StringBuffer buf;
  public String toString() {
    buf = new StringBuffer();
    printHelp(root());
    return buf.toString();
  }
  private void printHelp(HuffBaseNode<E> rt) {
    if (rt.isLeaf()) {
      buf.append(rt.weight());
      buf.append(" ");
      buf.append(((HuffLeafNode<E>)rt).element());
      buf.append(" | ");
      return;
    }
    printHelp(((HuffInternalNode<E>)rt).left());
    buf.append(rt.weight());
    buf.append(" | ");
    printHelp(((HuffInternalNode<E>)rt).right());
  }
}
