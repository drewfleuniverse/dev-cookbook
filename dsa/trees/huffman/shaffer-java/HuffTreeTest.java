import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;
import java.util.Vector;

public class HuffTreeTest {
  private Vector<Code> codeTable;
  private int totalCodeWeight;
  private HuffTree<Character>[] huffInitNodes;
  private MinHeap<HuffTree<Character>> huffInitHeap;
  private HuffTree<Character> huffTree;

  @Before
  @SuppressWarnings("unchecked")
  public void init() {
    codeTable = new Vector<Code>();
    totalCodeWeight = 0;

    huffInitNodes = (HuffTree<Character>[])new HuffTree[8];
    huffInitNodes[0] = new HuffTree<Character>('C', 32);
    huffInitNodes[1] = new HuffTree<Character>('D', 42);
    huffInitNodes[2] = new HuffTree<Character>('E', 120);
    huffInitNodes[3] = new HuffTree<Character>('K', 7);
    huffInitNodes[4] = new HuffTree<Character>('L', 42);
    huffInitNodes[5] = new HuffTree<Character>('M', 24);
    huffInitNodes[6] = new HuffTree<Character>('U', 37);
    huffInitNodes[7] = new HuffTree<Character>('Z', 2);

    huffInitHeap = new MinHeap<HuffTree<Character>>(huffInitNodes, 8, 8);
  }

  @Test
  public void evaluateHuffTree() {
    /**
     * Build a Huffman code tree as below:
     *         306
     *     0/       \1
     * 120-E         186
     *           0/      \1
     *         79          107
     *       0/  \1      0/    \1
     *    37-U   42-D   42-L    65
     *                        0/  \1
     *                     32-C    33
     *                          0/    \1
     *                          9      24-M
     *                        0/  \1
     *                      2-Z    7-K
     */
    buildTree();
    assertEquals(
      "120 E | 306 | 37 U | 79 | 42 D | 186 | 42 L | 107 | " +
      "32 C | 65 | 2 Z | 9 | 7 K | 33 | 24 M | ", huffTree.toString());

    buildCodeTable();
    assertEquals(
      "[E  0, U  100, D  101, L  110, C  1110, Z  111100, K  111101, M  11111]",
      codeTable.toString());
    assertEquals(785, totalCodeWeight);
    assertEquals(2.565359477124183, averageCodeLength(), 0.0);
    assertEquals("MUZ", decode("11111" + "100" + "111100"));
    assertEquals("11111 | 100 | 111100 | ", encode("MUZ"));
  }

  private void buildTree() {
    HuffTree<Character> left, right, merge = null;
    while (huffInitHeap.heapSize() > 1) {
      left = huffInitHeap.removeMin();
      right = huffInitHeap.removeMin();
      merge = new HuffTree<Character>(left.root(), right.root(),
        left.weight() + right.weight());
      huffInitHeap.insert(merge);
    }
    huffTree = merge;
  }

  private void buildCodeTable() {
    buildCodeTableHelper(huffTree.root(), "");
  }
  private void buildCodeTableHelper(HuffBaseNode<Character> rt, String edge) {
    assert rt != null : "Input tree cannot be empty";
    if (rt.isLeaf()) {
      codeTable.addElement(
        new Code(((HuffLeafNode<Character>)rt).element(), edge));
      totalCodeWeight += edge.length() * rt.weight();
    } else {
      buildCodeTableHelper(
        ((HuffInternalNode<Character>)rt).left(), edge + "0");
      buildCodeTableHelper(
        ((HuffInternalNode<Character>)rt).right(), edge + "1");
    }
  }

  private double averageCodeLength() {
    return (double)totalCodeWeight/(double)huffTree.weight();
  }

  private String decode(String code) {
    HuffBaseNode<Character> rt = huffTree.root();
    String s = "";
    for (int i = 0; i < code.length(); i++) {
      char c = code.charAt(i);

      if (c == '0') rt = ((HuffInternalNode<Character>)rt).left();
      else if (c == '1') rt = ((HuffInternalNode<Character>)rt).right();
      else assert false : "Bad code input";

      if (rt.isLeaf()) {
        s += ((HuffLeafNode<Character>)rt).element();
        rt = huffTree.root();
      }
    }
    return s;
  }

  private String encode(String s) {
    String code = "";
    for (int i = 0; i < s.length(); i++) {
      int codeTableIndex = getCodeTableIndex(s.charAt(i));
      assert codeTableIndex < codeTable.size() :
        "Character does not exist in code table";
      code += codeTable.elementAt(codeTableIndex).code() + " | ";
    }
    return code;
  }
  private int getCodeTableIndex(char c) {
    int i = 0;
    while (i < codeTable.size()) {
      if (c == codeTable.elementAt(i).data()) return i;
      i++;
    }
    return i;
  }
}
