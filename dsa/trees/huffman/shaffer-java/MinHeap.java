import java.lang.Comparable;
/**
 * Fields:
 *   - private E[] heap
 *   - private int n
 *   - private int size
 * Methods:
 *   - public MinHeap(E[] h, int n, int s)
 *   - public int heapSize()
 *   - public boolean isLeaf(int pos)
 *   - public int leftChild(int pos)
 *   - public int rightChild(int pos)
 *   - public int parent(int pos)
 *   - public void insert(E val)
 *   - public void buildHeap()
 *   - private void siftDown(int pos)
 *   - public E removeMax()
 *   - public E remove(int pos)
 *   - public String toSting()
 */
public class MinHeap<E extends Comparable<? super E>> {
  private E[] heap;
  private int n;
  private int size;

  public MinHeap(E[] h, int n, int s) {
    heap = h; this.n = n; size = s; buildHeap();
  }

  public int heapSize() { return n; }

  public boolean isLeaf(int pos) { return (pos >= n/2) && (pos < n); }

  public int leftChild(int pos) {
    assert pos < n/2 : "Position has no left child";
    return 2*pos + 1;
  }

  public int rightChild(int pos) {
    assert pos < (n-1)/2 : "Position has no right child";
    return 2*pos + 2;
  }

  public int parent(int pos) {
    assert pos > 0 : "Position has no parent";
    return (pos-1)/2;
  }

  public void insert(E val) {
    assert n < size : "Heap is full";
    int curr = n++;
    heap[curr] = val;
    while ((curr != 0) && (heap[curr].compareTo(heap[parent(curr)]) < 0)) {
      DSUtil.swap(heap, curr, parent(curr));
      curr = parent(curr);
    }
  }

  public void buildHeap() {
    for (int i = n/2-1; i >= 0; i--) siftDown(i);
  }

  private void siftDown(int pos) {
    assert (pos >= 0) && (pos < n) : "Illegal heap position";
    while (!isLeaf(pos)) {
      int j = leftChild(pos);
      if ((j<(n-1)) && (heap[j].compareTo(heap[j+1])) > 0) j++;
      if (heap[pos].compareTo(heap[j]) <= 0) return;
      DSUtil.swap(heap, pos, j);
      pos = j;
    }
  }

  public E removeMin() {
    assert n > 0 : "Remove from empty heap";
    DSUtil.swap(heap, 0, --n);
    if (n != 0) siftDown(0);
    return heap[n];
  }

  public E remove(int pos) {
    assert (pos >= 0) && (pos < n) : "Illegal heap position";
    if (pos == n-1) n--;
    else {
      DSUtil.swap(heap, pos, --n);
      while ((pos > 0) && (heap[pos].compareTo(heap[parent(pos)]) < 0)) {
        DSUtil.swap(heap, pos, parent(pos));
        pos = parent(pos);
      }
      if (n != 0) siftDown(pos);
    }
    return heap[n];
  }

  public String toString() {
    StringBuffer buf = new StringBuffer();

    int power = 1;
    for (int i = 0; i < n; i++) {
      if (i == Math.pow(2, power)-1) {
        buf.append("| ");
        power++;
      }
      buf.append(heap[i]);
      buf.append(" ");
    }

    return buf.toString();
  }
}
