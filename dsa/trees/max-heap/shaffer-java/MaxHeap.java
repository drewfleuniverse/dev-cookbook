import java.lang.Comparable;

/**
 * Fields:
 * - private E[] heap
 * - private int size
 * - private int n
 * Methods:
 * - public MaxHeap(E[] h, int n, int s)
 * - public int heapSize()
 * - public boolean isLeaf(int pos)
 * - public int leftChild(int pos)
 * - public int rightChild(int pos)
 * - public int parent(int pos)
 * - public void insert(E val)
 * - public void buildHeap()
 * - private void siftdown(int pos)
 * - public E removeMax()
 * - public E remove(int pos)
 * - public String toString()
 */
public class MaxHeap<E extends Comparable<? super E>> {
  /** pointer to heap array */
  private E[] heap;
  /** max capacity of heap */
  private int size;
  /** number of elements in heap */
  private int n;

  /**
   * @param  E[] h  Array to heapify
   * @param  int s  Size of heap array allocation
   * @param  int n  Number of existing elements in array
   */
  public MaxHeap(E[] h, int s, int n) {
    heap = h; size = s; this.n = n; buildHeap();
  }

  /** @return  Number of elements in heap */
  public int heapSize() { return n; }

  /**
   * Code:
   *   - Evaluate and return: If the position is within the range of the
   *     first leaf position (>=(n/2)) and element count (<n)
   * @param  int pos  valid heap position, 0 <= pos < n
   * @return  True if position is leaf, false otherwise
   */
  public boolean isLeaf(int pos) { return (pos >= n/2) && (pos < n); }

  /**
   * Code:
   *   - Assert: Position is smaller than the first leaf position, n/2
   *   - Evaluate and return: The left child position, pos*2 + 1
   * @param  int pos  valid heap position, 0 <= pos < n
   * @return  Left child position
   */
  public int leftChild(int pos) {
    assert pos < n/2 : "Position has no left child";
    return pos*2 + 1;
  }

  /**
   * Code:
   *   - Assert: Position is smaller than the first leaf position when n is odd
   *     and one more position smaller than the first leaf position when n is
   *     even, thus smaller than (n-1)/2
   *   - Evaluate and return: The right child position is pos*2 + 2
   * @param  int pos  Legal heap node position, i.e. 0 <= pos < n
   * @return  Right child position
   */
  public int rightChild(int pos) {
    assert pos < (n-1)/2 : "Position has no right child";
    return pos*2 + 2;
  }

  /**
   * Code:
   *   - Assert: Input position cannot be root, i.e. pos > 0
   *   - Evaluate and return: The parent position is (pos-1)/2
   * @param  int pos  valid heap position
   * @return  Parent position
   */
  public int parent(int pos) {
    assert pos > 0 : "Position has no parent";
    return (pos-1)/2;
  }

  /**
   * Code:
   *   1. Assert: Heap is not full (n < size)
   *   2. Append val to the end of heap and increase element count
   *   3. Loop - Sift up val until it is smaller than or equal to its parent:
   *     - To proceed:
   *       - Current position of val is not root
   *       - And, the element at current position is larger than its parent
   *     - Tasks:
   *       - Swap the element with its parent
   *       - Traverse up by updating the current position to its parent
   * @param E val Value to insert
   */
  public void insert(E val) {
    assert n < size : "Heap is full";
    int curr = n++;
    heap[curr] = val;
    while ((curr != 0) && (heap[curr].compareTo(heap[parent(curr)]) > 0)) {
      DSUtil.swap(heap, curr, parent(curr));
      curr = parent(curr);
    }
  }

  /**
   * Build heap by sifting down each of the internal elements. Start from the last
   * internal node; end with the root
   * Code:
   *   - Loop:
   *     - To start:  Position i is the last internal element (n/2-1)
   *     - To proceed: i is within the heap (>= 0)
   *     - Tasks:
   *       - Sift down element at i
   *       - Decrement i
   */
  public void buildHeap() { for (int i = n/2-1; i >= 0; i--) siftDown(i); }

  /**
   * Sift an element down by swapping element value with one of its child if the
   * element is smaller than its child
   * Code:
   *   1. Assert: Position is valid heap position (0 <= pos < n)
   *   2. Loop - Sift the the visiting element down:
   *     - To proceed: current position is not leaf
   *     - Tasks:
   *       - Get the child of current position that has the largest value
   *       - Break: the element at current position is larger than or equal to
   *         its children
   *       - Swap the current element with its largest child
   *       - Traverse down by updating current position
   * @param int pos  Node position to visit
   */
  private void siftDown(int pos) {
    assert (pos >= 0) && (pos < n) : "Illegal heap position";
    while(!isLeaf(pos)) {
      int j = leftChild(pos);
      if ((j<(n-1)) && (heap[j].compareTo(heap[j+1]) < 0)) j++;
      if (heap[pos].compareTo(heap[j]) >= 0) return;
      DSUtil.swap(heap, pos, j);
      pos = j;
    }
  }

  /**
   * Code:
   *   1. Assert: Tree cannot be empty (n>0)
   *   2. Remove the root:
   *     - Swap the root with the last element and decrease the element count
   *     - n.b. If the heap has only one node, the root swapped with itself and
   *       the element count is decreased to zero
   *   3. Heapify: Sift down if heap is not empty after swap
   *   4. Return the removed element at n
   * @return  The maximum element
   */
  public E removeMax() {
    assert n > 0 : "Removing from empty heap";
    DSUtil.swap(heap, 0, --n);
    if (n != 0) siftDown(0);
    return heap[n];
  }

  /**
   * Code:
   *   1. Assert: Current position is valid heap position (0 <= pos < n)
   *   2. Case 1 - Current position is at the end of heap:
   *     - Romove the element at current position by decreasing the element
   *       count
   *     - n.b. Case includes a heap only has one element.
   *   3. Case 2 - Not the last element:
   *     - Remove current position's element:
   *       - Swap the element with the last element
   *       - Remove it by decreasing element count
   *     - Condition 1 - new element is larger than its parent(s):
   *       - Sift up the new element:
   *         - Loop:
   *           - To proceed:
   *             - Current position is not root
   *             - And, the element at current position is larger than its
   *               parent
   *           - Tasks:
   *             - Swap the element with its parent
   *             - Traverse up by point the current position to its parent
   *     - Condition 2 - New element is smaller than its child/children:
   *       - Sift down the element if the heap is not empty
   *   4. Stop: Return the removed element
   * @param  int pos
   * @return  The removed element
   */
  public E remove(int pos) {
    assert (pos >= 0) && (pos < n) : "Illegal haep position";
    if (pos == n-1) n--;
    else {
      DSUtil.swap(heap, pos, --n);
      while ((pos > 0) && (heap[pos].compareTo(heap[parent(pos)]) > 0)) {
        DSUtil.swap(heap, pos, parent(pos));
        pos = parent(pos);
      }
      if (n != 0) siftDown(pos);
    }
    return heap[n];
  }

  /**
   * Iterations:
   * --------|---|---|---|---|---|---
   *       i | 0 | 1 | 2 | 3 | 4 | 5
   * (2^p)-1 | 1 | 1 | 3 | 3 | 7 | 7
   *       p | 1 | 1 | 2 | 2 | 3 | 3
   *
   * Result - Append when i == p^2-1:
   * 0 | 1 2 | 3
   */
  public String toString() {
    StringBuffer buf = new StringBuffer();

    int power = 1;
    for (int i = 0; i < n; i++) {
      if (i == Math.pow(2, power)-1) {
        buf.append("| ");
        power++;
      }
      buf.append(heap[i]);
      buf.append(" ");
    }
    // System.out.println(buf.toString());
    return buf.toString();
  }
}
