import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class MaxHeapTest {
  private Integer[] IA;

  @Before
  public void init() {
    IA = new Integer[]{5, 0, 4, 2, 7, 1, 9, 6, 8, 3};
    // int[] ia = {5, 0, 4, 2, 7, 1, 9, 6, 8, 3};
    // IA = new Integer[ia.length];
    // for (int i = 0; i < ia.length; i++) IA[i] = new Integer(ia[i]);
  }

  @Test
  public void evaluateDataArrays() {
    assertEquals(arrayToString(IA, IA.length), "5 0 4 2 7 1 9 6 8 3 ");
    assertEquals(IA.length, 10);
  }

  @Test
  public void evaluateMaxHeap() {
    MaxHeap<Integer> H = new MaxHeap<>(IA, 10, 10);

    /** Inspect heapified array */
    assertEquals("9 | 8 5 | 6 7 1 4 | 0 2 3 ", H.toString());
    assertEquals(10, H.heapSize());

    /**
     * Remove max element:
     *   - Decrement heap size
     *   - 3 is swapped to top position
     *   - Sift 3 down
     */
    assertEquals(Integer.valueOf(9), H.removeMax());
    assertEquals("8 | 7 5 | 6 3 1 4 | 0 2 ", H.toString());
    assertEquals(9, H.heapSize());

    /** Remove last element: Decrement heap size */
    assertEquals(Integer.valueOf(2), H.remove(8));
    assertEquals("8 | 7 5 | 6 3 1 4 | 0 ", H.toString());
    assertEquals(8, H.heapSize());

    /** Remove internal element */
    assertEquals(Integer.valueOf(7), H.remove(1));
    assertEquals("8 | 6 5 | 0 3 1 4 ", H.toString());
    assertEquals(7, H.heapSize());

    /** Insert a large element: new element is sifted up from the end of heap */
    H.insert(new Integer(30));
    assertEquals("30 | 8 5 | 6 3 1 4 | 0 ", H.toString());
    assertEquals(8, H.heapSize());

    /** Insert */
    H.insert(new Integer(6));
    assertEquals("30 | 8 5 | 6 3 1 4 | 0 6 ", H.toString());
    assertEquals(9, H.heapSize());

    /**
     * Remove an element so the last element (6) is moved to a position where it
     * is larger than its parent (5):
     * ```
     *     30
     *   8    5
     *  6 3  1 4
     * 0 6
     * ```
     * ```
     *     30
     *   8    5
     *  6 3  6 4
     * 0
     * ```
     * To maintain max heap property, 6 is sifted up first then sifted down
     */
    assertEquals(Integer.valueOf(1), H.remove(5));
    assertEquals("30 | 8 6 | 6 3 5 4 | 0 ", H.toString());
    assertEquals(8, H.heapSize());

    /**
     * Current heap:
     * ```
     *     30
     *   8    6
     *  6 3  5 4
     * 0
     * ```
     * Heap positions:
     * ```
     *      0
     *   1    2
     *  3 4  5 6
     * 7
     * ```
     */
    assertEquals(3, H.leftChild(1));
    assertEquals(4, H.rightChild(1));
    assertEquals(true, H.isLeaf(6));
    assertEquals(false, H.isLeaf(3));
    assertEquals(3, H.parent(7));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
