const cl = s => console.log(s);


const assert = (condition, msg='') => {
  if (!condition) {
    throw new Error(msg)
  }
}

const swap = (arr, i, j) => {
  const tmp = arr[i]
  arr[i] = arr[j]
  arr[j] = tmp
}


const heap = Symbol('heap');
const heapify = Symbol('heapify')
const bubbleUp = Symbol('bubbleUp')
const bubbleDown = Symbol('bubbleDown')
const remove = Symbol('remove')
const parent = Symbol('parent')
const firstLeaf = Symbol('firstLeaf')
const isLeaf = Symbol('isLeaf')
const leftChild = Symbol('leftChild')

class MinHeap {
  constructor(arr=[]) {
    this[heap] = arr;
    this[heapify]();
  }


  /*
  Inspection
   */

  size() {
    return this[heap].length;
  }

  isEmpty() {
    return this.size() === 0;
  }


  /*
  Operations
   */

  insert(val) {
    this[heap].push(val);
    this[bubbleUp](this.size()-1);
  }

  findMin() {
    assert(!this.isEmpty());

    return this[heap][0];
  }

  removeMin() {
    assert(!this.isEmpty());

    swap(this[heap], 0, this.size()-1);
    const min = this[heap].pop();
    this[bubbleDown](0);

    return min;
  }

  // Optional
  replaceMin(val) {
    assert(!this.isEmpty());

    const min = this[heap][0];
    this[heap][0] = val;
    this[bubbleDown](0);

    return min;
  }

  // Optional
  toString() {
    const height = pos => (
      Math.floor(Math.log2(pos+1))
    );
    const isLastAtLevel = pos => (
      2**(height(pos)+1) - 2 === pos
    );

    let str = '';
    for (let p = 0; p < this.size(); p++) {
      str += this[heap][p];
      if (isLastAtLevel(p) && p !== this.size()-1) {
        str += '|';
      }
    }

    return str;
  }


  // Optional
  // static merge(array1, array2) { /*retain dups*/ }

  // Optional
  // static meld() { /*discard dups*/ }


  /*
  Intrnal methods
   */

  [heapify]() {
    if (this.size() <= 1) {
      return;
    }

    const lastInternal = this[firstLeaf]()-1
    for (let i = lastInternal; i >= 0; i--) {
      this[bubbleDown](i);
    }
  }

  [bubbleUp](pos_) {
    assert(pos_ >= 0 && pos_ < this.size());

    let par = this[parent](pos_);
    let pos = pos_;
    while (
      pos !== 0
      && this[heap][pos] < this[heap][par]
    ) {
      swap(this[heap], pos, par);
      pos = par;
      par = this[parent](pos);
    }

    return pos;
  }

  [bubbleDown](pos_) {
    assert(pos_ >= 0 && pos_ < this.size());

    let pos = pos_;
    while (!this[isLeaf](pos)) {
      let chi = this[leftChild](pos);

      if (
        chi+1 < this.size()
        && this[heap][chi] > this[heap][chi+1]
      ) {
        chi++;
      }

      if (this[heap][pos] <= this[heap][chi]) {
        return;
      }

      swap(this[heap], pos, chi);
      pos = chi;
    }
  }

  // Optional
  [remove](pos) {
    assert(pos >= 0 && pos < this.size());

    const val = this[heap][pos];
    if (this.size() === 1) {
      this[heap].pop();
    } else {
      swap(this[heap], pos, this.size()-1);
      this[bubbleDown](pos)
    }

    return val
  }

  // Optional
  // [decreaseKey](pos, key) {}

  [parent](pos) {
    return Math.floor((pos-1)/2);
  }

  [firstLeaf]() {
    return Math.floor(this.size()/2);
  }

  [isLeaf](pos) {
    return (
      pos >= Math.floor(this.size()/2)
      && pos < this.size()
    );
  }

  [leftChild](pos) {
    return (pos * 2) + 1;
  }
}


let a, h, r


a = [6,5,4,3,2,1]
h = new MinHeap(a)
cl(h.toString() === '1|24|356')


h = new MinHeap()
cl(h.toString() === '')
cl(h.isEmpty() === true)
cl(h.size() === 0)

h.insert(5)
h.insert(4)
h.insert(3)
h.insert(2)
h.insert(1)

cl(h.toString() === '1|24|53')
cl(h.isEmpty() === false)
cl(h.size() === 5)

cl(h.findMin() === 1)
cl(h.removeMin() === 1)
cl(h.toString() === '2|34|5')

cl(h.replaceMin(9) === 2)
cl(h.toString() === '3|54|9')
