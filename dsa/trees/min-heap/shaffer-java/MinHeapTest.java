import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class MinHeapTest {
  private Integer[] IA;

  @Before
  public void init() {
    IA = new Integer[]{5, 0, 4, 2, 7, 1, 9, 6, 8, 3};
  }

  @Test
  public void evaluateDataArrays() {
    assertEquals(arrayToString(IA, IA.length), "5 0 4 2 7 1 9 6 8 3 ");
    assertEquals(IA.length, 10);
  }

  @Test
  public void evaluateMinHeap() {
    MinHeap<Integer> H = new MinHeap<>(IA, 10, 10);

    /** Inspect heapified array */
    assertEquals("0 | 2 1 | 5 3 4 9 | 6 8 7 ", H.toString());
    assertEquals(10, H.heapSize());

    /**
     * Remove min element:
     *   - Decrement heap size
     *   - 7 is swapped to top position
     *   - Sift 7 down
     */
    assertEquals(Integer.valueOf(0), H.removeMin());
    assertEquals("1 | 2 4 | 5 3 7 9 | 6 8 ", H.toString());
    assertEquals(9, H.heapSize());

    /** Remove last element: Decrement heap size */
    assertEquals(Integer.valueOf(8), H.remove(8));
    assertEquals("1 | 2 4 | 5 3 7 9 | 6 ", H.toString());
    assertEquals(8, H.heapSize());

    /** Remove internal element */
    assertEquals(Integer.valueOf(2), H.remove(1));
    assertEquals("1 | 3 4 | 5 6 7 9 ", H.toString());
    assertEquals(7, H.heapSize());

    /** Insert a large element: new element is sifted up from the end of heap */
    H.insert(new Integer(0));
    assertEquals("0 | 1 4 | 3 6 7 9 | 5 ", H.toString());
    assertEquals(8, H.heapSize());

    /** Insert */
    H.insert(new Integer(3));
    assertEquals("0 | 1 4 | 3 6 7 9 | 5 3 ", H.toString());
    assertEquals(9, H.heapSize());

    /**
     * Remove an element so the last element (3) is moved to a position where it
     * is larger than its parent (4) and sifted up:
     * ```
     *        0
     *    1      4
     *  3   6  7   9
     * 5 3
     * ```
     * ```
     *        0
     *    1      3
     *  3   6  4   9
     * 5
     * ```
     */
     assertEquals(Integer.valueOf(7), H.remove(5));
     assertEquals("0 | 1 3 | 3 6 4 9 | 5 ", H.toString());
     assertEquals(8, H.heapSize());

     /**
      * Current heap:
      * ```
      *        0
      *    1      3
      *  3   6  4   9
      * 5
      * ```
      * Heap positions:
      * ```
      *      0
      *   1    2
      *  3 4  5 6
      * 7
      * ```
      */
      assertEquals(3, H.leftChild(1));
      assertEquals(4, H.rightChild(1));
      assertEquals(true, H.isLeaf(6));
      assertEquals(false, H.isLeaf(3));
      assertEquals(3, H.parent(7));
  }

  private String arrayToString(Integer[] A, int length) {
    StringBuffer buf = new StringBuffer(length);
    for (Integer i : A) { buf.append(i); buf.append(" "); }
    return buf.toString();
  }
}
