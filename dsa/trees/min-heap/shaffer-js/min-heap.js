const cl = i => console.log(i)
const js = i => JSON.stringify(i)


const assert = (cond, msge) => {
  if (!cond) throw new Error(msge)
}

const swap = (a, p, q) => {
  const t = a[p]
  a[p] = a[q]
  a[q] = t
}


const h = Symbol('h')


class MinHeap {
  constructor(_h = []) {
    this[h] = _h
    this.buildHeap()
  }

  heapSize() { return this[h].length }

  insert(v) {
    this[h].push(v)
    let c = this[h].length-1
    while (c !== 0 && v < this[h][this.parent(c)]) {
      swap(this[h], c, this.parent(c))
      c = this.parent(c)
    }
  }

  remove(p) {
    assert(this[h].length > 0)
    const v = this[h][p]
    if (p === this[h].length-1) this[h].length--
    else {
      swap(this[h], p, this[h].length-1)
      this[h].length--
      while (p !== 0
      && this[h][p] < this[h][this.parent(p)]) {
        swap(this[h], p, this.parent(p))
        p = this.parent(p)
      }
      if (this[h].length !== 0)
        this.siftDown(p)
    }

    return v
  }

  removeMin() {
    assert(this[h].length > 0)
    const v = this[h][0]
    swap(this[h], 0, this[h].length-1)
    this[h].length--
    this.siftDown(0)

    return v
  }

  buildHeap() {
    const firstLeaf = Math.floor(this[h].length/2)
    const lastParent = firstLeaf-1
    for (let p = lastParent; p >= 0; p--)
      this.siftDown(p)
  }

  siftDown(p) {
    assert(p >= 0 && p < this[h].length)
    while (!this.isLeaf(p)) {
      let i = this.leftChild(p)

      // find smallest child
      if (i < this[h].length-1
      && this[h][i] > this[h][i+1])
        i++

      // abort if smaller or equal than smallest child
      if (this[h][p] <= this[h][i])
        return

      swap(this[h], p, i)
      p = i
    }
  }

  isLeaf(p) {
    return p >= Math.floor(this[h].length/2) &&
      p < this[h].length
  }

  leftChild(p) {
    assert(p < Math.floor(this[h].length/2))
    return 2*p + 1
  }

  rightChild(p) {
    assert(p < Math.floor((this[h].length-1)/2))
    return 2*p + 2
  }

  parent(p) {
    assert(p > 0)
    return  Math.floor((p-1)/2)
  }

  toString() {
    const height = n => Math.floor(Math.log2(n))
    const last = i => Math.pow(2, height(i+1)+1)-2

    const l = this[h].length
    let s = ''
    for (let i = 0; i < l; i++) {
      s += this[h][i]
      if (i === last(i) && i != l-1) s += '\n'
    }

    return s
  }
}


let mh, a, r


a = [6,4,3,5,1,2]
mh = new MinHeap(a)
cl(mh.toString() === `1
42
563`)

a = []
mh = new MinHeap(a)
mh.insert(6)
mh.insert(4)
mh.insert(3)
mh.insert(5)
mh.insert(1)
mh.insert(2)
cl(mh.toString() === `1
32
654`)

cl(mh.heapSize() === 6)
cl(mh.isLeaf(0) === false)
cl(mh.isLeaf(3) === true)
cl(mh.leftChild(1) === 3)
cl(mh.rightChild(1) === 4)
cl(mh.parent(5) === 2)

r = mh.remove(1)
cl(r === 3)
cl(mh.heapSize() === 5)
cl(mh.toString() === `1
42
65`)

r = mh.removeMin()
cl(r === 1)
cl(mh.heapSize() === 4)
cl(mh.toString() === `2
45
6`)
