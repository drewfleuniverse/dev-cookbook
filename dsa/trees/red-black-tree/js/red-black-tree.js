/*
Constants
 */

const RED = true;
const BLACK = false;

/*
Node Class
 */

class Node {
  constructor(key, color, left=null, right=null) {
    this.key = key;
    this.left = left;
    this.right = right;
    this.color = color;
  }
}


/*
Red-Black Tree
 */

const sRoot = Symbol('sRoot');
const sInsert = Symbol('sInsert');
const sIsRed = Symbol('sIsRed');
const sRotateLeft = Symbol('sRotateLeft');
const sRotateRight = Symbol('sRotateRight');
const sFlipColor = Symbol('sFlipColor');

class RBT {
  constructor() {
    this[sRoot] = null;
  }

  /*
  Insertion
   */

  insert(key) {
    this[sRoot] = this[sInsert](this[sRoot], key);
    this[sRoot].color = BLACK;
  }

  [sInsert](node, key) {
    if (node === null) {
      return new Node(key, RED);
    }

    if (node.key > key) {
      node.left = this[sInsert](node.left, key);
    } else if (node.key < key) {
      node.right = this[sInsert](node.right, key);
    } else {
      ;//
    }

    if (
      !this[sIsRed](node.left) &&
      this[sIsRed](node.right)
    ) {
      node = this[sRotateLeft](node);
    }

    if (
      this[sIsRed](node.left) &&
      this[sIsRed](node.left.left)
    ) {
      node = this[sRotateRight](node);
    }

    if (
      this[sIsRed](node.left) &&
      this[sIsRed](node.right)
    ) {
      this[sFlipColor](node);
    }

    return node;
  }

  [sIsRed](node) {
    if (node === null) {
      return false;
    }

    return node.color === RED;
  }

  [sRotateLeft](node) {
    const temp = node.right;
    node.right = temp.left;
    temp.left = node;
    temp.color = node.color;
    node.color = RED;

    return temp;
  }

  [sRotateRight](node) {
    const temp = node.left;
    node.left = temp.right;
    temp.right = node.right;
    temp.color = node.color
    node.color = RED;

    return temp;
  }

  [sFlipColor](node) {
    node.color = !node.color;
    node.left.color = !node.left.color;
    node.right.color = !node.right.color;
  }

  /*
  Utilities
   */

  toString() {
    return [...this].toString();
  }

  *[Symbol.iterator]() {
    if (this[sRoot] === null) {
      return;
    }

    const queue = [];
    queue.push(this[sRoot]);
    while (queue.length !== 0) {
      const curr = queue.shift();
      yield `${curr.color ? 'R' : 'B'}${curr.key}`;

      if (curr.left !== null) {
        queue.push(curr.left);
      }
      if (curr.right !== null) {
        queue.push(curr.right);
      }
    }
  }
}


/*
Tests Preps
 */

const buildRBT = () => {
  const rbt = new RBT();
  rbt.insert(6);
  rbt.insert(2);
  rbt.insert(1);
  rbt.insert(4);
  rbt.insert(3);
  rbt.insert(5);
  rbt.insert(7);

  return rbt; // B4,R2,B7,B1,B3,R5
};
assert(buildRBT().toString() === 'B4,R2,B7,B1,B3,R5');

/*
Test Insertion
 */

describe('RBT.insert', () => {
  it('inserts', () => {
    const rbt = new RBT();
    rbt.insert(1);
    rbt.insert(2);
    rbt.insert(3);
    rbt.insert(4);
    rbt.insert(5);

    assert(rbt.toString() === 'B4,R2,B5,B1,B3');
  });
});

/*
Test Utilities
 */

function describe (s,f) {console.log('%c'+s,'font-weight: bold;');f();};
function it (s,f,r=true) {console.log('%c'+s,'color:grey;');r&&f();};
function assert (c,l=false,m='') {l&&console.log(`Assertion log: ${c}`);if(!c){throw new Error(m);}};
