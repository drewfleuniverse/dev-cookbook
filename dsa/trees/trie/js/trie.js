const alphaLength = 26;


class Node {
  constructor() {
    this.children = new Array(alphaLength).fill(null);
    this.isEnd = false;
  }
}

class Trie {
  constructor() {
    this.root = new Node();
  }

  insert(key_) {
    const key = key_.toLowerCase();

    if (key.length === 0) {
      return;
    }

    let curr = this.root;

    for (const c of key) {
      const index = Trie.getIndex(c);

      if (curr.children[index] === null) {
        curr.children[index] = new Node();
      }

      /*
      Do not just point to curr.children
       */
      curr = curr.children[index];
    }

    curr.isEnd = true;
  }


  search(key_) {
    const key = key_.toLowerCase();

    let curr = this.root;

    for (const c of key) {
      const index = Trie.getIndex(c);

      if (curr.children[index] === null) {
        return false;
      }

      curr = curr.children[index];
    }

    return curr.isEnd;
  }


  static getIndex(char) {
    const code = char.charCodeAt() - 'a'.charCodeAt();

    if (code < 0) {
      throw new Error('Invalid char')
    }

    return code;
  }
}


describe(`Node`, () => {
  const node = new Node();
  assert(node.children.length === 26);
});


describe(`Trie`, () => {
  it ('insert()', () => {
    const trie = new Trie();

    trie.insert('foo');
    trie.insert('foobar');
  });

  it ('seaerch()', () => {
    const trie = new Trie();

    trie.insert('foo');
    trie.insert('bar');
    trie.insert('foobar');

    assert(trie.search('foo') === true);
    assert(trie.search('bar') === true);
    assert(trie.search('foobar') === true);

    assert(trie.search('far') === false);
    assert(trie.search('boo') === false);
    assert(trie.search('barfoo') === false);
  });

  it ('getIndex()', () => {
    assert(Trie.getIndex('a') === 0);
    assert(Trie.getIndex('b') === 1);
    assert(Trie.getIndex('z') === 25);
  });
});



/*
Utils
 */
function describe(c,e){console.log('%c'+c,'font-weight: bold;'),e()};function it(c,e){console.log('%c'+c,'color:grey;'),e()};function assert(c,e=!1,f=''){if(e&&console.log(`Assertion log: ${c}`),!c)throw new Error(f)};
