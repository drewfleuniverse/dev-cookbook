# 2-3 Trees



## Introduction
- Invented by John Hopcroft in 1970
- A variant of B-tree, i.e. 2-3 B-tree
    - N.b. Although Knuth considers 2-3 tree is a variant of B-tree, the definition in CLRS implies 2-3-4 tree is the simplest B-tree variant

## Properties
- Element to insert cannot duplicate, otherwise the tree won't be balanced
    - Thus user K-V pair to avoid duplication
      - e.g. given (k1,v1) and (k2,v2), k1 != k2, v1 == v2
- Each node:
    - Has one or two elements
    - Left element < right element
- Each internal node, if the node contains:
    - One element, a:
      - Has two children
      - Left subtree < a <= Center subtree
    - Two elements, a and b:
        - Has three children
        - Left subtree < a <= Center subtree < b <= Right subtree
- All leaves are at the same level
- Number of leaves at height k:
    - At least 2^(k-1) leaves
    - At most 3^(k-1) leaves
- Maximum depth: floor(log(n))


## Complexity

- Time - Average and Worst:
        - Search: log(n)
        - Insert: log(n)
        - Delete: log(n)
- Space - Average and Worst: n
- Notes:
    - 2-3 tree is height balanced and every internal node has at least two children, its maximum depth is floor(log(n))
    - n.b. depth and height are both count from zero, i.e. zero-based


## Pseudocode

### Shaffer

#### \[Shaffer\] TTNode Class
```java
/** Class */
TTNode<K,E>

/** Fields */
- leftKey: K
- leftValue: E
- rightKey: K
- rightValue: E
- left: TTNode<K,E>
- center: TTNode<K,E>
- right: TTNode<K,E>

/** Methods */
+ TTNode()
+ TTNode(lk:K, lv:E, rk:K, rv:V,
         p1:TTNode<K,E>, p2:TTNode<K,E>, p3:TTNode<K,E>)
+ isLeaf(): Boolean
+ left(): TTNode<K,E>
+ center(): TTNode<K,E>
+ right(): TTNode<K,E>
+ leftKey(): K
+ leftValue(): E
+ rightKey(): K
+ rightValue(): E
+ setLeftElement(k:K, v:E): Void
+ setRightElement(k:K, v:E): Void
+ setLeft(it:TTNode<K,E>): Void
+ setCenter(it:TTNode<K,E>): Void
+ setRight(it:TTNode<K,E>): Void
+ add(it:TTNode<K,E>): TTNode<K,E>
```

#### \[Shaffer\] TTNode Class - add(it)
```js
add(it)
  if (rightKey == null)
    if (leftKey < it.leftKey())
      rightKey = it.leftKey()
      rightValue = it.leftValue()
      right = center
      center = it.center()
    else
      rightKey = leftKey
      rightValue = leftValue
      leftKey = it.leftKey()
      leftValue = it.leftValue()
      right = center
      center = it.center()
    return this
  else if (leftKey >= it.leftKey())
    center = new TTNode(rightKey, rightValue, null, null
                        center, right, null)
    rightKey = null
    rightValue = null
    right = null
    it.setLeft(left)
    left = it
    return this
  else if ((rightKey < it.leftKey()) )
    it.setCenter(new TTNode(rightKey, rightValue, null, null,
                            it.center(), right, null))
    it.setLeft(this)
    rightKey = null
    rightValue = null
    right = null
    return it
  else
    it2 = new TTNode(rightKey, rightValue, null, null,
                     this, it, null)
    it.setLeft(right)
    right = null
    rightKey = null
    rightValue = null
    return it2
```

###### Code Comments
- N.b. The following cases assume:
    - The current node may be:
        - a node with children, i.e. root or internal node
        - a node without children, i.e. leaf
    - The item to insert may be:
        - a node only has left element
        - a subtree
- Case 1 - Current node has only one element:
    - Case 1.1 - left element < item:
        - Store item as right element
    - Case 1.2 - left element >= item:
        - Store item as left element
    - Return current node
- Case 2 - Current node has two element, left element >= item:
    - Add new node to center
    - Add item to left
    - Return current node
- Case 3 - Current node has two element, right element < item:
    - Add new node to item center
    - Add current node to item left
    - Return item
- Case 4 - Current node has two element, right element >= item:
    - Create new node points to current node and item
    - Return new node

#### \[Shaffer\] TTTree Class
```java
/** Class */
TTTree<K,E> <- Dictionary<K,E>

/** Fields */
- root: TTNode<K,E>
- count: Integer

/** Methods, Partial */
+ TTTree()
+ insert(k:K, v:E): Void
+ find(k:K): E
- insertHelp(rt:TTNode<K,E>, k:K, v:E): TTNode<K,E>
- findHelp(rt:TTNode<K,E>, k:K): E
```

#### \[Shaffer\] TTTree Class - Methods
```js
TTTree()
  count = 0
  root = null

insert(k, v)
  root = insertHelp(root, k, v)
  count++

find(k)
  return findHelp(root, k)
```

#### \[Shaffer\] TTTree Class - insertHelp(rt, k, v)
```js
insertHelp(rt, k, v)
  if (rt == null)
    return new TTNode(k, v, null, null, null, null, null)
  if (rt.isLeaf())
    return rt.add(new TTNode(k, v, null, null, null, null, null))
  if (k < rt.leftKey())
    node = insertHelp(rt.left(), k, v)
    if (node == rt.left()) return rt
    else return rt.add(node)
  else if ((rt.rightKey() == null) || (k < rt.rightKey()))
    node = insertHelp(rt.center(), k, v)
    if (node == rt.center()) return rt
    else return rt.add(node)
  else
    node = insertHelp(rt.right(), k, v)
    if (node == rt.right()) return rt
    else return rt.add(node)
```

###### Description
- recursively traverses down:
    - Stop: when it reaches leaf, add new element
- recursively traverses back:
    - Move back through its traversal path
        - If the modified leaf has new children and becomes a subtree itself, add the subtree to its parent
            - If the add process results in a splited subtree, recursively add subtrees to each visited node until the newly formed subtree doesn't split, i.e. the parent of subtree is merged with its parent. If the add process reaches the root and the root has two elements, the tree will split and increase height
            - Otherwise, no more add process and inserthelp returns each visited nodes and traverses back
        - If no child added to the modified leaf, i.e. one-element leaf becomes two-element leaf, inserthelp will recursively traverse back

###### Code Comments
- Case 1 - Tree is empty:
  	- Create new node with new element
- Stop/Case 2 - Current node is leaf:
  	- Create new node with new element
  	- Add new node to leaf
- Case 3 - Current node is internal node or root:
  	- Case 3.1 - New element < left element:
    		- Insert new element to left
    		- Case - Left still points to itself:
      			- Return current node
    		- Case - Left points to new node:
      			- Add new node to current node
      			- Return current node
  			- Case 3.2 - Right element == null || new element < right element:
    				- Insert new element to center
    				- (Same as sub-cases in Case 3.1)
		- Case 3.3 - Right element <= new element:
    		- Insert new element to right
    		- (Same as sub-cases in Case 3.1)

#### \[Shaffer\] TTTree Class - findHelp(rt, k)
```js
findHelp(rt, k)
  if (rt == null)
    return null
  if (k == rt.leftKey())
    return rt.leftValue()
  if ((rt.rightKey() != null) && (k == tr.rightKey()))
    return rt.rightValue()
  if (k < rt.leftKey())
    return findHelp(rt.left(), k)
  else if (rt.rightKey() == null)
    return findHelp(rt.center(), k)
  else if (k < rt.rightKey())
    return findHelp(rt.center(), k)
  else
    return findHelp(rt.right(), k)
```
