import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

public class TTTest {
  private Dictionary<Integer, Integer> D1;
  @Test
  public void evaluateTTTree() {
    D1 = new TTTree<Integer, Integer>();
    assertEquals(D1.size(), 0);
    D1.insert(new Integer(10), new Integer(10));
    assertEquals(D1.size(), 1);
    assertEquals(D1.toString(), "10");
    D1.clear();
    assertEquals(D1.size(), 0);
    assertEquals(D1.toString(), "");
    D1.insert(new Integer(10), new Integer(10));
    assertEquals(D1.size(), 1);
    assertEquals(D1.toString(), "10");
    D1.insert(new Integer(15), new Integer(15));
    assertEquals(D1.toString(), "10 15");
    assertEquals(D1.find(10), new Integer(10));
    assertEquals(D1.find(20), null);
    D1.insert(new Integer(35), new Integer(35));
    /**
     *     35
     * 10  15  null
     */
    assertEquals(D1.toString(), "35T10T15T");
    D1.insert(new Integer(1), new Integer(1));
    /**
     *       35
     * 1:10  15  null
     */
    assertEquals(D1.toString(), "35T1 10T15T");
    D1.insert(new Integer(30), new Integer(30));
    /**
     *    30:35
     * 1   10  15
     */
    assertEquals(D1.toString(), "30 35T1T10T15");
    D1.insert(new Integer(17), new Integer(17));
    /**
     *       30:35
     * 1:17   10   15
     */
    assertEquals(D1.toString(), "30 35T1 17T10T15");
    D1.insert(new Integer(19), new Integer(19));
    /**
     *          30
     *   19          35
     * 1 17 null  10 15 null
     */
    assertEquals(D1.toString(), "30T19TT1TT17TTT35TT10TT15TTT");
    D1.insert(new Integer(100), new Integer(100));
    /**
     *                30
     *   19           35          null
     * 1 17 null  10 15:100 null
     */
    assertEquals(D1.toString(), "30T19TT1TT17TTT35TT10TT15 100TTT");
    D1.insert(new Integer(90), new Integer(90));
    /**
     *              30
     *   19        35:100      null
     * 1 17 null  10 90 15
     */
    assertEquals(D1.toString(), "30T19TT1TT17TTT35 100TT10TT90TT15T");
    D1.insert(new Integer(95), new Integer(95));
    /**
     *                30
     *   19          35:100       null
     * 1 17 null  10 90:95 15
     */
    assertEquals(D1.toString(), "30T19TT1TT17TTT35 100TT10TT90 95TT15T");
  }
}
