import static org.junit.Assert.assertFalse;
import org.junit.Test;
import java.util.Arrays;

public class DSUtilTest {
  @Test
  public void doSomeTests() {
    String[] _sa = {"do", "some", "tests", "for", "me"};
    String[] sa = {"do", "some", "tests", "for", "me"};
    int[] _ia = {1, 2, 3, 4, 5};
    int[] ia = {1, 2, 3, 4, 5};

    System.out.println(DSUtil.random(15)); // 13

    DSUtil.permute(sa);
    printArray(sa); // for me some tests do

    DSUtil.permute(ia);
    printArray(ia); // 5 2 1 3 4


    DSUtil.swap(ia, 0, 1);
    printArray(ia); // 2 5 1 3 4

    assertFalse(Arrays.equals(_sa, sa));
    assertFalse(Arrays.equals(_ia, ia));
  }

  private <E> void printArray(E[] A) {
    for (E a : A) System.out.print(a + " ");
    System.out.println();
  }

  private void printArray(int[] A) {
    for (int a : A) System.out.print(a + " ");
    System.out.println();
  }
}
