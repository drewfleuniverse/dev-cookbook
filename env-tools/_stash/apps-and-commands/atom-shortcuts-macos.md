# Atom Shortcuts - macOS






## Usage Note

- One project folder per window: fuzzy finder only either finds buffered files or within entire projects.






## Atom Core

### Application
- add project folder: `cmd-shift-o`

### Window
- increase font size: `cmd-+`
- decrease font size: `cmd--`
- reset font size: `cmd-0`

### Editor
- toggle line comments" `cmd-/`
- move to beginning of line: `cmd-left`
- move to end of line: `cmd-right`
- move line up: `cmd-ctrl-up`
- move line down: `cmd-ctrl-down`
- duplicate line: `cmd-shift-d`
- select line: `cmd-l`
- select to beginning of line: `cmd-shift-left`
- select to end of line: `cmd-shift-right`
- delete to beginning of line: `cmd-backspace`
- delete to end of line: `cmd-delete`

### Pane
- show item 1~9: `cmd-1` ~ `cmd-9`
- show next item: `ctrl-tab` or `cmd-alt-right`
- show previous item: `ctrl-shift-tab` or `cmd-alt-left`
- focus pane above: `cmd-k-up`
- focus pane below: `cmd-k-down`
- focus pane left: `cmd-k-left`
- focus pane right: `cmd-k-right`
- increase size: `cmd-alt-+`
- decrease size: `cmd-alt--`








## Core Packages

### Fuzzy Finder
- toggle buffer finder (current opened files in buffer): `cmd-b`
- toggle file finder (current projects): `cmd-p` or `cmd-t`

### Symbol View
- show all symbols in file: `cmd-R`
- show all symbols in project: `cmd-shift-R`

### Command Palette
- toggle: `cmd-shift-p`

### Tree View
- focus or unfocus: `ctrl-0`
- toggle: `cmd-\`
- add file: `A`
- add folder: `shift-A`
- duplicate file or folder: `D`
- delete file or folder: `delete` or `backspace`
- expand directory: `right` or `ctrl-]`
- collapse directory: `ctrl-[`
- recursive expand directory: `ctrl-alt-]`
- recursive collapse directory: `ctrl-alt-[`
- copy full path: `ctrl-shift-c`

### Find and Replace
- toggle and show find: `cmd-f`
- toggle and show replace: `cmd-alt-f`
- toggle find in projects: `cmd-shift-f`
- replace all: `cmd-enter`

### Markdown Preview
- toggle: `ctrl-shift-m`








## Community Packages

### Git Plus
- Add all, commit, and push: `cmd-shift-P`->`aacp`

### Atom Beautify
- beautify: `ctrl-alt-b`

### Terminal Plus
- toggle: ``ctrl-` ``
- new terminal: `cmd-shift-t`
- close terminal: `cmd-shift-x`
- previous: `cmd-shift-j`
- next: `cmd-shift-k`

### Multi Cursor
- `alt-up` or `alt-down`

### Color Picker
- togggle: `cmd-shift-c`

### Emmet
- expand: `ctrl-e`
