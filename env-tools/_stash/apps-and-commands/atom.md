**style.less**

```less
.tab-bar {
  height: 26px;
  padding: 0px;
  .tab {
    height: 26px;
    .title{
      font-size: 12px;
      margin-top: 1px;
      line-height: 24px;
    }
    .close-icon {
      height: 18px!important;
      width: 18px!important;
      top: 4px!important;
      right: 6px!important;
      font-size: 16px!important;
      line-height: 16px!important;
      padding-top: 2px;
    }
  }
}
```
