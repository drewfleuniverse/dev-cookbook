# mongod

## Usage

### macOS

- Start as foreground process: `mongod`
- Use as a background process:
    - Start: `mongod --fork --logpath ~/.mongod-log/mongodb.log`
    - Stop: `mongo --eval "db.getSiblingDB('admin').shutdownServer()"`

**mongod-start.sh**

```sh
[[ -d ~/.mongod-log ]] && \
mongod --fork --logpath ~/.mongod-log/mongodb.log || \
echo "mongod already running or directory ~/.mongod-log does not exist"
```

**mongod-stop.sh**

```sh
mongo --host localhost \
      --port 27017 \
      --eval "db.getSiblingDB('admin').shutdownServer()"
```
