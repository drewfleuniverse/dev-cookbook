# MongoDB Atlas

## Database user permission

### Permissions to a specific database



#### For Robomongo

Robomongo performs read-only commands on admin database upon login, an user with only `readWrite` permission to a database results in no database shown in Robomongo. The following are workarounds to allow Robomongo work correctly:

**Read and write**

- Add *MongoDB Roles*:
    - clusterMonitor@admin
    - readWrite@<database>

**Read and write with admin permissions**

- Add *MongoDB Roles*:
    - clusterMonitor@admin
    - readWrite@<database>
    - dbAdmin@<database>


## Data recovery

- MongoDB Atlas offers snapshots, there has two ways to restore from save snapshots:
    1. Restore entire cluster
    2. Use snapshot query to retrieve a portion of the cluster

### Snapshot query

- Methods:
    1. Use *Backup Tunnel* with mongo shell: the executable itself is buggy, if not work try the second option
    2. *Connect Manually* through mongo shell with SSL cert and CA
