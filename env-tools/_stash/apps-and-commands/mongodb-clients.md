# MongoDB Clients


## Robomongo

### Connection Settings

#### MongoDB Atlas

In **MongoDB Connections**, right click and select **edit** to edit **Connection Settings**:

- In **Connection** tab:
    - Type: Replica Set
    - Name: <name>
    - Members, e.g. for a shard of 3 nodes:
        - `production-shard-00-00-abcde.mongodb.net:27017`
        - `production-shard-00-01-abcde.mongodb.net:27017`
        - `production-shard-00-02-abcde.mongodb.net:27017`
    - Set Name: <set-name>
- In **Authentication** tab:
    - Check *Perform Authentication*
    - Database: admin
    - User Name: <user-name>
    - Password: <password>
    - Auth Mechanism: SCRAM-SHA-1
- In **SSL** tab:
    - Check *Use SSL Protocol*
    - Authentication Method: Self-signed Certificate
- In **Advanced** tab:
    - Default Database: leave empty to connect to test; otherwise connect to the specified database

### Issues

#### MongoDB Atlas

- For mongodb user doesn't have admin permission, e.g. dbAdmin@<database>, Robomongo cannot show the collection in the permitted database


## Mongoclient

### Installation

Mongoclient has three types of distributions: Desktop App, Docker, and Meteor web app


### Connection Settings

#### MongoDB Atlas

- In **Connection** tab:
    - E.g. for a shard of 3 nodes
    - Host/Port: `production-shard-00-00-abcde.mongodb.net`, `27017`
    - Host/Port: `production-shard-00-01-abcde.mongodb.net`, `27017`
    - Host/Port: `production-shard-00-02-abcde.mongodb.net`, `27017`
    - Database Name: <database>
- In **Authentication** tab:
    - Authentication Type: SCRAM-SHA-1
    - Username: <username>
    - Password: <password>
    - Authentication DB: admin
- In **SSL** tab:
    - Check *Use SSL*
- In **Options** tab:
    - Replica Name: <replica-name>
    - Read Preference: Primary Preferred

### Issues

#### MongoDB Atlas

- Mongoclient cannot establish shell connection for mongodb users at all permission levels
