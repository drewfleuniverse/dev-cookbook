# NPM


## Usage

- List global packages: `npm list -g --depth=0`
- Update npm version: `npm install npm@latest -g`

## Installing packages

### Installing packages from package.json

- Install `devDependencies` and other dependencies: `npm install`
- Only installs `dependencies`: `npm install --production`
-

### Installing packages and save to package.json

- Save to `dependencies`:
    - `npm install <package>`
    - `npm install -P <package>`
- Save to `devDependencies`: `npm install -D <package>`

### Behavior and package argument format

- Install from `package.json` in current directory does not need an argument
- Install packages from source contains `package.json`:
    - `<path>`
    - `<gitRepo>`
    - `<tarBall/urlToTarBall>`
- Install packages from the npm Registry:
    - `<name>`: installs the latest version
    - `<name>@<tag>`
    - `<name>@<version>`

### package.json dependency format

**URLs as Dependencies**

Git URLs:

```
"name": "git+ssh://user@hostname:project.git#<tag/sha/branch>"
"name": "git+ssh://user@hostname/project.git#<tag/sha/branch>"
```

GitHub URLs:

```
"name": "user/repo"
"name": "user/repo/branch"
"name": "user/repo#feature\/branch"
```

### Discussions

#### Global package versus local package

- Global: use as a command
- Local: use as development dependencies in various development stages
