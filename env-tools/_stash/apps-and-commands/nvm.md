# NVM


## Installing node versions

### Migrating global packages

**Default global packages**

Edit file `~/.nvm/default-packages` or `$NVM_DIR/default-packages` with a list of packages in the format of npm package arguments, e.g.

```
<name>
<name>@<version>
<gitRepo>
```

 nvm reinstall-packages <version>
**Manual**

```
nvm install node --reinstall-packages-from=node
```
