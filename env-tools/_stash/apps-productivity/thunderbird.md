# Thunderbird

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Thunderbird](#thunderbird)
	- [Setup](#setup)
		- [Preferences](#preferences)
		- [Add email account](#add-email-account)
		- [Add email alias](#add-email-alias)
	- [Add-ons](#add-ons)

<!-- /TOC -->

## Setup

### Preferences

**Privacy**

- Check *Allow remote content ...*

### Add email account

- Go to *Tools->Account Settings*

### Add email alias

- Go to *Tools->Account Settings*
    - Click email account name and click *Manage Identities...* button
    - Add the email alias

## Add-ons

**Install**

- Theme font and size changer
- Thunderbird conversations

**Disable**

- Lightening
