
**Create key pairs**

```sh
cd ~/.ssh
ssh-keygen # enter name for the key pairs, e.g. aws-<username>
```

**Upload public key**

In AWS IAM, in the user summary page, under 'SSH keys for AWS CodeCommit', paste the public key

**Add key to ssh config**

In AWS IAM, in the user summary page, under 'SSH keys for AWS CodeCommit', find 'SSH key ID'

```
Host git-codecommit.*.amazonaws.com
  User <SSH_key_ID>
  IdentityFile ~/.ssh/<private-key-name>
```
