# Setup Development Environment - OSX Version

## System Prerequisites ########################################################

#### iTerm2

Installation: [iTerm2](https://www.iterm2.com/)

Profiles:

- Name: `Dev - drewfle-universe`
  - Command: Login shell
    - Send text at start: `docker-machine start; docker-machine env; eval $(docker-machine env)`
  - Working Directory:
    - Directory: `/Users/drewfle/Dev/drewfle-universe`
- Name: `Dev - aecit-webdev`
  - Command: Login shell
    - Send text at start: `docker-machine start; docker-machine env; eval $(docker-machine env)`
  - Working Directory:
    - Directory: `/Users/drewfle/Dev/aecit-webdev`
- Name: `SSH - AWS CIC Provision Server`
  - Command: `ssh -i /Users/drewfle/ssh/ec2-cic-provision-web__provision-aecit-key-pair-useast.pem ubuntu@cicusa.org`
  - Working Directory:
    - Directory: Home directory
- Name: `SSH - AWS AECIT Web Server`
  - Command: `ssh -i /Users/drewfle/ssh/ec2-aecit-web__aecit-key-pair-useast.pem ubuntu@www2.cicusa.org`
  - Working Directory:
    - Directory: Home directory
- Name: `Python venv`
  - Command: Login shell
    - Send text at start: `workon`
  - Working Directory:
    - Directory: `/Users/drewfle/PyDev`

#### Oh My Zsh

Installation: [Oh My Zsh](http://ohmyz.sh/)

#### Homebrew

Installation: [Homebrew](http://brew.sh/)

Basic usage:

- [Keeping Your Homebrew Up to Date](https://www.safaribooksonline.com/blog/2014/03/18/keeping-homebrew-date/)
- If `/usr/local` isn't writable after upgrading OSX, run:
  - `sudo chown -R $(whoami):admin /usr/local`

Useful packages with command line instructions:

```bash
# Check if brew is healthy
brew doctor
brew update
brew install gnu-sed
```

`.zshrc`

```bash
# User configuration

export PATH="/usr/local/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"

# for php-fpm
export PATH="/usr/local/sbin:$PATH"
export PATH="$PATH:$HOME/.composer/vendor/bin"
# make `gsed` as `sed` in ZSH
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
export MANPATH="/usr/local/opt/gnu-sed/libexec/gnuman:$MANPATH"

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/PyDev
source /usr/local/bin/virtualenvwrapper.sh

source /usr/local/share/zsh/site-functions/_aws

if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

fpath=(~/.zsh/completion $fpath)

# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh
```

## Packages - Programming Languages ############################################

#### PHP

##### Installing PHP

Installation: [Homebrew/homebrew-php](https://github.com/Homebrew/homebrew-php)

Note: `freetype jpeg libpng gd zlib` may need to be re-compiled with Homebrew to update PHP dependencies.

##### Installing Composer

Might only need to run `brew install composer`. After installation, add `export PATH="$PATH:~/.composer/vendor/bin"` to `.zshrc`.

#### Ruby

##### Installing RBENV, Ruby, and SASS

```bash
brew install rbenv
brew install ruby-build

# put the following line in .zshrc
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

rbenv install -l
rbenv install 2.3.0
rbenv rehash
rbenv global 2.3.0

source ~/.zshrc

gem update --system
gem install compass
```

#### Python

##### Installing Python 2.x and Python 3.x

Installation: `brew install python python3`

Note: Homebrew's Python packages are shipped with `pip` or `pip3`.

##### Installing Python Virtual Env

Installation:
```bash
pip install virtualenv
pip install virtualenvwrapper

#Attach the following in .zshrc
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/PyDev
source /usr/local/bin/virtualenvwrapper.sh

# Create PROJECT_HOME and Source shell profile
mkdir PyDev
source ~/.zshrc

###### Temp Note: creating venv for python3
# virtualenv -p /usr/local/bin/python3 ENV
######
```
Note: The `virtualenv` and `virtualenvwrapper` can be installed with `pip` and can be used to create both of the environments of Python 2.x and Python 3.x.

References:
- [Virtual Environments](http://docs.python-guide.org/en/latest/dev/virtualenvs/#virtualenvironments-ref)
- [virtualenvwrapper doc/Installation](http://virtualenvwrapper.readthedocs.org/en/latest/install.html)

## Packages - Command Line Tools ###############################################

#### Docker

Installation: [Install Docker Engine on Mac OSX](https://docs.docker.com/engine/installation/mac/)
Download: [
Docker Toolbox](https://www.docker.com/products/docker-toolbox)
Basic Usage: [Install Docker Engine on Mac OSX - Running a Docker Container](https://docs.docker.com/engine/installation/mac/#running-a-docker-container)

#### AWS-CLI

Install: `brew install awscli`
Configuration: [Configuring the AWS Command Line Interface](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)

Note: If zsh completion is enabled, the terminal may show error `complete:13: command not found: compdef` during start up.

References:
- [Universal Command Line Interface for Amazon Web Services](https://github.com/aws/aws-cli)
- [Installing the AWS Command Line Interface](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)

#### MySql

##### Installing via Homebrew

The MySql installation via `brew` is not secured by default and the root user doesn't have a password.

Installation: `brew install mysql`
Basic usage without launching at start up:

```bash
mysql.server start
mysql -u root
mysql.server stop
```

##### Installing via Docker

#### MongoDB

##### Installing via Homebrew
```
brew install mongodb
mkdir -p /data/db
sudo chown drewfle /data/db
```

###### Notes
```
To have launchd start mongodb at login:
  ln -sfv /usr/local/opt/mongodb/*.plist ~/Library/LaunchAgents
Then to load mongodb now:
  launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mongodb.plist
Or, if you don't want/need launchctl, you can just run:
  mongod --config /usr/local/etc/mongod.conf
```

## References ##################################################################

- [Install homebrew, nginx, mysql, php55, and composer on Mac OS X](https://gist.github.com/mralexho/6cd3cf8b2ebbd4f33165)
