## Dependencies



### handson-ml

Env:
  - Install `virtualenvwrapper` and have env var `VIRTUALENVWRAPPER_PYTHON`
  points to python3

Prep:

```sh
# Create venv project dir
mkproject handson-ml-examples
# Clone source repo to project dir
git clone https://github.com/ageron/handson-ml.git handson-ml-examples
# Activate venv
workon handson-ml-examples
# Install
# If scipy is below scikit in requirements.txt, reverse their order
pip install --upgrade pip
pip install --upgrade -r requirements.txt
jupyter contrib nbextension install --user
jupyter nbextension enable toc2/main
```







### Anaconda Python
- Suggested by Caffe
#### Install
- [DOWNLOAD ANACONDA NOW](https://www.continuum.io/downloads)
#### For Zsh
Paste the following in `.zshrc` (found in .bash_profile):
```sh
# added by Anaconda3 4.3.1 installer
export PATH="/Users/drewfle/anaconda/bin:$PATH"
```
#### Note
- [ANACONDA PACKAGE LIST](https://docs.continuum.io/anaconda/pkg-docs)

### Caffe
Too hard to install, use docker instead....
```sh
brew install -vd snappy leveldb gflags glog szip lmdb
# need the homebrew science source for OpenCV and hdf5
brew tap homebrew/science
brew install hdf5 opencv
# with Python pycaffe needs dependencies built from source
brew install --build-from-source --with-python -vd protobuf
brew install --build-from-source -vd boost boost-python
```
```sh
brew install numpy
```
#### Compile
```
# Uncomment
CPU_ONLY := 1
WITH_PYTHON_LAYER := 1

# Add this to PYTHON_INCLUDE in Makefile.config
PYTHON_INCLUDE := /usr/local/Cellar/python/2.7.13/bin/python \
    /usr/local/Cellar/numpy/1.12.1/lib/python2.7/site-packages/numpy/core/include \
		/usr/local/Cellar/python/2.7.13/Frameworks/Python.framework/Versions/2.7/include/python2.7

PYTHON_LIB := /usr/local/Cellar/python/2.7.13/Frameworks/Python.framework/Versions/2.7/lib
```
#### Install pycaffe dependencies
```sh
cd caffe/python
pip install -r requirements.txt
```
```
export CAFFE_ROOT=$HOME/ml/caffe
export PYTHONPATH=$HOME/ml/caffe/python:$PYTHONPATH
```
#### References
- [Theory of Building Caffe on OS X](https://gist.github.com/kylemcdonald/0698c7749e483cd43a0e#theory-of-building-caffe-on-os-x)
- [Install Deepdream(Caffe-python) on Mac OS X Yosemite 10.10.3](https://github.com/rainyear/lolita/issues/10)
