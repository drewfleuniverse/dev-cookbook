# OSX

## System

### Shortcuts

#### Zoom:

- System Preferences -> Keyboard -> App Shortcuts
- Click + sign
- Add entry: Zoom, control+command+\

### Configure `ssh-add`

Run:

```sh
ssh-keygen && ssh-add -K ~/.ssh/id_rsa
```

To force `ssh-agent` to use keychain on macOS Sierra or higher, run:

```sh
cp ~/.ssh/config ~/.ssh/config.bak 2>/dev/null || echo '
Host *
  UseKeychain yes
  AddKeysToAgent yes
' >> ~/.ssh/config
```

Note:<br>
Always add keys with `ssh-add -K the_key` so secret phrases are stored in keychain.

## Peripherals

### Logitech G100S

#### Install Logitech Gaming Software

Download at [link](http://support.logitech.com/en_us/product/g100s-optical-gaming-mouse).

### Configuration

In Logitech Gaming Software:

- Double click <b>Middle click<b/>, in Keystroke:
  - Keystroke: `ctrl + up`
- Double click <b>DPI Cycling<b/>, in Keystroke:
  - Keystroke: `Left Cmd + C`

## Prerequisites

### Homebrew

#### Install Xcode

Install Xcode through App Store.
Run:

```sh
xcode-select --install
```

To agree with the license, run:

```sh
sudo xcodebuild -license
```

Note:<br>
Alternatively, open up Xcode to agree its license before using Xcode command tools.

#### Install Zsh, Oh-My-Zsh, and Homebrew

Run:

```sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" && \
brew install \
    zsh \
    zsh-completions && \
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

#### Configure `zsh`

Run:

```sh
sudo sh -c "echo '/usr/local/bin/zsh' >> /etc/shells" && \
chsh -s /usr/local/bin/zsh && \
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# zsh-completions
fpath=(/usr/local/share/zsh-completions $fpath)
' >> ~/.zshrc
```

#### Configure Homebrew

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# Configure Homebrew
export PATH="/usr/local/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
' >> ~/.zshrc
```

Check installation status, run:

```sh
brew doctor
```

### GNU Core Utilities and GNU Sed

#### Install

Run:

```sh
brew install \
    coreutils && \
brew install gnu-sed --with-default-names
```

#### Configure `coreutils`

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# Configure coreutil
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
' >> ~/.zshrc
```

### Reload shell profile:

```
source ~/.zshrc
```

## Install via Homebrew [brew]

### [brew]Install Packages

Run:

```sh
brew install git \
    openssl \
    node \
    python \
    python3 \
    rbenv \
    ctags \
    vim \
    p7zip \
    iperf \
    smartmontools \
    tree \
    thefuck \
    jenv \
    watch
```

### [brew]PHP

#### Installation

Run:

```sh
brew install freetype jpeg libpng gd lzlib && \
brew tap homebrew/dupes && \
brew tap homebrew/versions && \
brew tap homebrew/homebrew-php && \
brew install \
  php71 \
  homebrew/php/php-cs-fixer \
  composer
```

#### Configuration

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# Configure PHP command line mode
export PATH="$(brew --prefix homebrew/php/php71)/bin:$PATH"
' >> ~/.zshrc
```

#### Reinstallation after OS Upgrade

Run:

```sh
brew uninstall php71
brew cleanup
xcode-select --install # Click yes all the way when prompt shows up
brew install php71
```

#### References

- [Homebrew/homebrew-php](https://github.com/Homebrew/homebrew-php)
- [(Re)installing PHP on Mac OS X](http://justinhileman.info/article/reinstalling-php-on-mac-os-x/)

### [brew]Java and jEnv/[manual]JUnit:

#### Installation

Run:

```sh
brew update &&
brew install jenv &&
brew tap caskroom/cask &&
brew install Caskroom/cask/java &&
brew install Caskroom/versions/java8
```

#### Configure

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# Configure Java and jEnv
export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"
' >> ~/.zshrc &&
source .zshrc
```

Do the following:<br>
Add installed jdks to `jenv`. To find installed jdk, do `ls /Library/Java/JavaVirtualMachines` and use the jdk names, e.g.:

```sh
jenv add /Library/Java/JavaVirtualMachines/jdk1.7.0_80.jdk/Contents/Home
```

Run:

```sh
jenv rehash &&
jenv global 1.8 && # set default java
jenv shell 1.8 # set default java
```

Note:<br>
Set local java, which also creates a `.java-version`.

```sh
jenv local 1.7
```

#### JUnit:

Download the following and place them in directory, e.g. `~/jars`:

- [junit.jar](http://search.maven.org/#search%7Cgav%7C1%7Cg:%22junit%22%20AND%20a:%22junit%22)
- [hamcrest-core.jar](http://search.maven.org/#search%7Cgav%7C1%7Cg:%22org.hamcrest%22%20AND%20a:%22hamcrest-core%22)
  Run:

```sh
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# Configure JUnit
export JUNIT_HOME=$HOME/Dev/jars
export HAMCREST_HOME=$HOME/Dev/jars
export CLASSPATH=$CLASSPATH:$JUNIT_HOME/junit-4.12.jar:$HAMCREST_HOME/hamcrest-core-1.3.jar:.
' >> ~/.zshrc &&
source .zshrc
```

### [brew]MongoDB

#### Installation

Run:

```sh
brew install mongodb --with-openssl
```

#### Configuration

Run:

```sh
sudo mkdir -p /data/db && \
sudo chown $USER /data/db
```

### [brew]Vim:

Run:

```sh
cp ~/.vimrc ~/.vimrc.bak 2>/dev/null || echo '
filetype plugin indent on
syntax on
"Indentation with hard tabs
set expandtab
set shiftwidth=2
set softtabstop=2
"Fix disabled backspace
set backspace=indent,eol,start
"Fix disabled backspace
set backspace=indent,eol,start
' >> ~/.vimrc
```

References:

- [Indenting source code](http://vim.wikia.com/wiki/Indenting_source_code)

### [brew]rbenv and Ruby:

#### Installation

Run:

```sh
rbenv init && \
cp ~/.zshrc ~/.zshrc.bak || echo '
# Configure rbenv
eval "$(rbenv init -)"
' >> ~/.zshrc && \
rbenv install -l
```

Find the latest Ruby and run:

```sh
rbenv install [ruby-version] && \
rbenv rehash && \
rbenv global [ruby-version] && \
source ~/.zshrc
```

Example:

```sh
rbenv install 2.6.4 && \
rbenv rehash && \
rbenv global 2.6.4 && \
source ~/.zshrc
```

### [brew]Git:

#### Configuration

Run:

```sh
git config --global user.name "Andrew Liu";
git config --global user.email andrewliustudio@gmail.com;
git config --global color.ui true;
git config --global core.editor vim;
git config --global color.branch auto;
git config --global color.diff auto;
git config --global color.status auto;
git config --global diff.submodule log;
git config --global core.excludesfile ~/.gitignore_global
```

#### Edit global .gitignore

Run:

```sh
cp ~/.gitignore_global ~/.gitignore_global.bak 2>/dev/null || echo '
*.DS_Store
.AppleDouble
.LSOverride

# Icon must end with two \r
Icon


# Thumbnails
._*

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk
' > ~/.gitignore_global
```

#### Add public keys to Git services

```sh
pbcopy < ~/.ssh/id_rsa.pub
```

#### References

- [gitignore/Global/macOS.gitignore](https://github.com/github/gitignore/blob/master/Global/macOS.gitignore)

### [brew]The Fuck:

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak && echo '
# Configure thefuck
eval $(thefuck --alias)
' >> ~/.zshrc && \
source ~/.zshrc
```

### [brew | DO NOT INSTALL]AutoEnv:

Note:

- Too Slow, Don't Install!!!!
  Run:

```sh
cp ~/.zshrc ~/.zshrc.bak && echo '
# Configure autoenv
export AUTOENV_ENV_FILENAME=".autoenv"
source $(brew --prefix autoenv)/activate.sh
' >> ~/.zshrc && \
source ~/.zshrc
```

## Install via Gem [gem]

### [gem]Common gems

Run:

```
gem update --system && \
gem install \
  compass \
  iStats
```

## Install via Pip [pip]:

### [pip]virtualenv and virtualenvwrapper

#### Installation

Run:

```sh
# Install venv using pip3 makes the default python and pip in venv point to
# python3 and pip3
pip3 install virtualenv virtualenvwrapper
```

#### Configuration

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak && echo '
# Configure Python venv
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Dev/py-venv
source /usr/local/bin/virtualenvwrapper.sh
' >> ~/.zshrc && \
source ~/.zshrc
```

Additional steps:

```sh
## Create PROJECT_HOME and Source shell profile
mkdir -p ~/Dev/py-venv
## Note: creating Python3 venv
virtualenv -p /usr/local/bin/python3 [projectname]
```

### [pip]AWS-CLI:

#### Installation

n.b. AWS does not maintain brew awscli package.<br>
Run:

```sh
pip install awscli && \
cp ~/.zshrc ~/.zshrc.bak && echo '
# Configure AWS-CLI
source /usr/local/bin/aws_zsh_completer.sh
' >> ~/.zshrc && \
source ~/.zshrc
```

#### Configure IAM credentials

```sh
aws configure
```

#### References

- [aws/aws-cli](https://github.com/aws/aws-cli)

## Manual Installation and Configurations [manual]

### [manual]NVM and Node.js:

#### Install NVM

n.b. NVM team do not maintain brew nvm package.<br>
Do check [creationix/nvm](https://github.com/creationix/nvm) if the link below is up-to-date. Run:

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
```

Run:

```sh
cp ~/.zshrc ~/.zshrc.bak 2>/dev/null && echo '
# Autoload NVM Node.js Version
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
' >> ~/.zshrc && \
source ~/.zshrc
```

Only available on the master branch

```sh
echo '
nodemon
eslint
chai
mocha
sinon
express
webpack
' > $NVM_DIR/default-packages
```

#### Install and Configure Node.js

Run:

```sh
nvm install --lts && \ #install node lts
nvm install node #install latest node
```

Note:<br>
When installing new versions of Node.js, use `nvm install [node] --reinstall-packages-from=[node]` to migrate packages.

### [manual]Intel® Power Gadget

Download at: [Intel® Power Gadget](https://software.intel.com/en-us/articles/intel-power-gadget-20)

### [manual]Darktable

Download at: [Darktable](http://www.darktable.org/install/#osx)

## Install via NPM [npm]

### [npm]Common packages

Install packages in current Node.js version, run:

```sh
nvm use default && npm i -g \
  babel-cli \
  bower \
  express-generator \
  gulp \
  http-server \
  json-server \
  mocha \
  nodemon \
  webpack &&
nvm install node --reinstall-packages-from=default
```

Migrate the NPM packages to other node versions, run:

```sh
nvm install node --reinstall-packages-from=default
```

## Install linters [linter]

### [linter]Using pip

#### Installation

Run:

```sh
pip install pylint
```

### [linter]Using npm

#### Installation

Run:

```sh
nvm use default && npm i -g \
  eslint \
  eslint-plugin-angular \
  eslint-plugin-react \
  sass-lint \
  json-lint &&
nvm install node --reinstall-packages-from=default
```

## Install via Homebrew Cask

### Configure Homebrew Cask

Run:

```sh
brew tap caskroom/cask
```

### Install

Run:

```sh
brew update && \
brew cask install \
  google-chrome \
  firefox \
  flash-player \
  spotify \
  spotify-notifications \
  whatsapp \
  skype \
  google-drive \
  onedrive \
  dropbox \
  synology-cloud-station-drive \
  microsoft-office \
  libreoffice \
  adobe-reader \
  gimp \
  inkscape \
  freecad \
  blender \
  sketchup \
  sketchupviewer \
  iterm2 \
  atom \
  gedit \
  eclipse-ide \
  docker \
  virtualbox \
  vagrant \
  dash \
  netbeans \
  robomongo \
  github-desktop \
  mysqlworkbench \
  tunnelblick \
  filezilla \
  keka \
  flux \
  osxfuse \
  uncrustify \
  smcfancontrol
```

### Post-Install - Gedit

Note:<br>
Right click files to open with gedit.

### Post-Install - Atom

#### Install `apm`:

Do the following:<br>
Click `Atom>Install Shell Commands`.

#### Install packages

Run:

```sh
apm i \
  angularjs \
  atom-beautify \
  autocomplete-java \
  color-picker \
  emmet \
  file-icons \
  git-plus \
  highlight-selected \
  markdown-themeable-pdf \
  minimap \
  multi-cursor \
  oceanic-reef-syntax \
  pdf-view \
  pigments \
  react \
  platformio-ide-terminal \
  docblockr \
  script \
  dash \
  git-time-machine \
  markdown-toc \
  language-javascript-jsx \
  language-babel \
  autocomplete-modules \
  atom-ternjs \
  flow-ide \
  zen \
  git-time-machine \
  atom-clock \
  linter-stylelint \
  react-snippets \
  atom-jest-snippets \
  tester-jest
```

#### Using `atom-ide-ui`

```sh
# Step 1
apm i atom-ide-ui \
  ide-typescript \
  ide-flowtype \
  ide-java
# Step 2
# Disable packages:
# - react
# - linter
# - flow-ide
# - language-javascript-jsx
# - linter-javac
# - autocomplete-java
```

#### Configure Symbols View

Symbols View is a package uses `ctag` to trace symbols. To enable symbols in PHP projects, use the following command in project root to create file `tags`:

```sh
ctags -R --languages=php
```

#### Configure Atom Linter

##### Install Atom Linter and Linter packages

Run:

```sh
apm install \
  linter \
  linter-docker \
  linter-eslint \
  linter-javac \
  linter-json-lint \
  linter-php \
  linter-pylint \
  linter-sass-lint
```

##### Configure Atom ESLint

Do the following:<br>
In <b>Settings>Packages>Linter Eslint>Settings</b>, check <b>Use global ESLint installation</b>.

##### Notes on Atom ESLint

###### To use `eslint-plugin-angular`

Place `.eslintrc.yaml` in project folder and edit the file as:

```
plugins:
  - angular
```

###### to use `eslint-plugin-react`

Place `.eslintrc.yaml` in project root and edit the file as:

```
plugins:
  -react
extends:
  -eslint:recommended
  -plugin:react/recommended
```

###### To use `linter-sass-lint`:

Place `.sass-lint.yml` in project root and edit the file:

- Using the example configuration [sass-lint/docs/sass-lint.yml](https://github.com/sasstools/sass-lint/blob/master/docs/sass-lint.yml)
- Edit `include` and `ignore` options under `Files`.

### Post-Install - osxfuse

<!-- ## Manual installation through Download
- iterm2 (not installed with brew-cask due to iterm internal upgrage bug when upgrading from 2.x to 3.x)
- docker for mac -->

#### References

- [Get Started with ESLint v1.0](http://devnull.guru/get-started-with-eslint/)

## Install via App Store

- WeChat

## Appendix

### Scripts appended to `.zshrc`

```sh
# Configure Homebrew
export PATH="/usr/local/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"

# Configure coreutil
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"


# Configure Java and jEnv
export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"


# Configure Python venv
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Dev/py-venv
source /usr/local/bin/virtualenvwrapper.sh


# Configure AWS-CLI
source /usr/local/bin/aws_zsh_completer.sh


# Configure thefuck
eval $(thefuck --alias)


# Configure thefuck
eval $(thefuck --alias)


# Configure rbenv
eval "$(rbenv init -)"


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# Autoload NVM Node.js Version
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
```
