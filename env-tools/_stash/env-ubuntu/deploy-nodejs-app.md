# Deploy Node.js app on Ubuntu

## Process manager

### PM2

**Installation**

```sh
npm install pm2 -g
```

**Usage**

N.b. `pm2 start` uses `node` to run application

```sh
# For Express app
pm2 start ./bin/www
# List
pm2 start
```

#### Forever

**Run Express application**

```sh
# For Express app
forever start ./bin/www
# List
forever list
```


## Environment

### NVM

### Environment variable

- Place `export NODE_ENV=production` in `.bashrc` or `.bash_profile`
