######03_display_calibration.markdown

#Display Calibration with Spyder2 Express

###Reference
[Screen Calibration on Ubuntu 12.04 with Spyder2](https://nxadm.wordpress.com/2012/05/06/screen-calibration-on-ubuntu-12-04-with-spyder2-workaround/)

###[For dispcalGUI 2] Get Spyder2 Express Windows binary

- Download `spyd2PLD.bin` at this [link](https://sites.google.com/a/apt-get.be/www/spyd2PLD.bin)
- Move it to `~/.local/share/color`. If the directory doesn't exist, create it.

###Install dispcalGUI 3 for calibration

- `sudo apt-get install gnome-color-manager`
- Download 'dispcalGUI' at [Get dispcalGUI via Zero Install (recommended)](http://dispcalgui.hoech.net/#download)
	- Double click to install with Ubuntu Software Center
- Download 'Argyll CMS' at [Download Argyll Color Management Linux x86 executables](http://www.argyllcms.com/downloadlinux.html)
	- Move to the directory of your choice

###Using dispcalGUI 3
The program will find driver for the calibrator online, even for Spyder 2.

--------------------------------------------------------------------------------

######04_backup.markdown

[Create a virtual hard drive volume within a file in Linux](http://thelinuxexperiment.com/create-a-virtual-hard-drive-volume-within-a-file-in-linux/)

--------------------------------------------------------------------------------

######macbookair_specific.markdown

Disable IPv6 and Comment out "dns=dnsmasq" within /etc/NetworkManager/NetworkManager.conf

--------------------------------------------------------------------------------

###### misc_wine.markdown

##Install Adobe Reader X11

###Reference
[WineHQ-Adobe Reader](https://appdb.winehq.org/objectManager.php?sClass=version&iId=27093&iTestingId=86883)
###Download Adobe Reader 11 Windows 7 Version
- [Adobe Acrobat Reader](https://get.adobe.com/reader/otherversions/)
- The version tested is 'AdbeRdr11010_en_US.exe'

###Install DLLs
```bash
winetricks atmlib
winetricks riched20
winetricks wsh57
winetricks mspatcha
```

###Install Adobe Reader 11
```bash
wine <Adobe Reader 11>.exe
```
