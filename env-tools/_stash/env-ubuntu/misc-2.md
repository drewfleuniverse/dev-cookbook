# Ubuntu Server

## AWS Ubuntu 16.04

### Nginx / Let's Encrypt / NVM

#### Initial setup

```bash
sudo apt-get update &&
sudo apt-get upgrade &&
sudo apt-get dist-upgrade

sudo apt-get install htop nginx

ssh-keygen &&
ps -e | grep [s]sh-agent &&
ssh-agent /bin/bash &&
ssh-add ~/.ssh/id_rsa &&
ssh-add -l
# cat ~/.ssh/id_rsa.pub
```

#### Folder permissions and cloinig projects

- Reference: [What permissions should my website files/folders have on a Linux webserver?](http://serverfault.com/questions/357108/what-permissions-should-my-website-files-folders-have-on-a-linux-webserver)

```bash
sudo chown -R $USER:www-data /var/www &&
sudo chmod g+s /var/www

# clone projects into `/var/www`
```

#### Nginx setup

React

```nginx
server {
  listen 80;
  listen [::]:80;
  server_name sis.cicusa.org;
  return 301 https://$server_name$request_uri;
}
server {
  listen 443;
  listen [::]:443;
  server_name sis.cicusa.org;

  ssl on;
  ssl_certificate     /etc/letsencrypt/live/sis.cicusa.org/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/sis.cicusa.org/privkey.pem;

  ssl_session_timeout 5m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

  root /var/www/sis.cicusa.org-version1/public;
  location / {
    try_files $uri /index.prod.html;
  }
}
```

Node app
Reference: [Nginx with multiple locations](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04)

```nginx
server {
  listen 80;
  server_name api.sis.cicusa.org;
  return 301 https://$server_name$request_uri;
}
server {
  listen 443;
  server_name api.sis.cicusa.org;

  ssl on;
  ssl_certificate     /etc/letsencrypt/live/api.sis.cicusa.org/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/api.sis.cicusa.org/privkey.pem;

  ssl_session_timeout 5m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

  location / {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-NginX-Proxy true;
    proxy_pass http://localhost:3000/;
    proxy_ssl_session_reuse off;
    proxy_set_header Host $http_host;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }
}
```

Symlink nginx conf.
n.b. conf file cannot end in `.conf` or nginx would ignore them.

```bash
sudo ln -s /etc/nginx/sites-available/sis.cicusa.org-version1 /etc/nginx/sites-enabled/sis.cicusa.org-version1
sudo ln -s /etc/nginx/sites-available/api.sis.cicusa.org-version1 /etc/nginx/sites-enabled/api.sis.cicusa.org-version1

sudo service nginx restart
```

#### NVM

```bash
# To mitigate errot: `/usr/bin/env: ‘node’: No such file or directory`
sudo apt install nodejs-legacy

sudo apt-get install build-essential and libssl-dev
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
```

```bash
source ~/.bashrc
nvm install --lts
#nvm install 7.1
nvm alias default lts/*
npm install -g avn avn-nvm avn-n
avn setup
echo "lts/*" > ~/.node-version
#nvm install node --reinstall-packages-from=node
```

```bash
```

#### Let's encrypt

```bash
sudo service nginx stop
sudo apt-get install letsencrypt
sudo letsencrypt certonly --standalone
# enter domains

sudo service nginx start
```
