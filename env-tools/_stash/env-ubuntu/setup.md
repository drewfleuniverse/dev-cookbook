#Ubuntu Setup Guide

##Stage 1 - System

###Install Language(s)
Go to "System Settings->Language Support" to install language(s)

###Change swappiness and add more workspaces

```bash
# add line 'vm.swappiness = 5' to change swappiness in the following file
sudo nano /etc/sysctl.conf
# add additional workspaces
gsettings set org.compiz.core:/org/compiz/profiles/unity/plugins/core/ vsize 3
gsettings set org.compiz.core:/org/compiz/profiles/unity/plugins/core/ hsize 3
```
### Inotify Watches Limit
```bash
# add line 'fs.inotify.max_user_watches = 524288' to change swappiness in the following file
sudo nano /etc/sysctl.conf
sudo sysctl -p
```

### Fix no swap on Ubuntu 14.04 when home is encrypted
[Enable Hibernate With Encrypted Swap](https://help.ubuntu.com/community/EnableHibernateWithEncryptedSwap)

##Stage 2 - Utilities

###Reference
[INSTALL ORACLE JAVA 8 IN UBUNTU VIA PPA REPOSITORY](http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html)

``` bash
# add repositories
sudo add-apt-repository ppa:webupd8team/java
sudo add-apt-repository ppa:openjdk-r/ppa
sudo add-apt-repository ppa:no1wantdthisname/openjdk-fontfix
sudo add-apt-repository ppa:nilarimogard/webupd8
sudo add-apt-repository ppa:tsbarnes/indicator-keylock
sudo apt-get update

sudo apt-get install build-essential powertop htop unzip vim byobu nautilus-open-terminal git subversion ssh filezilla exfat-fuse exfat-utils tree php-pear gimp gparted indicator-multiload indicator-cpufreq indicator-keylock sqlite3 libsqlite3-dev php5-sqlite

# install Oracle JDK 7/8 and OpenJDK 8
sudo apt-get install oracle-java7-installer oracle-java8-installer openjdk-8-jdk

# restart nautilus
nautilus -q

# configure git
git config --global user.name "Name Surname"
git config --global user.email nick@company.com
git config --global color.ui true
git config --global core.editor vim
git config --global color.branch auto
git config --global color.diff auto
git config --global color.status auto
git config --global diff.submodule log

# install PulseAudio equalizer
sudo apt-get install pulseaudio-equalizer

# install spotify
# editing your /etc/apt/sources.list
deb http://repository.spotify.com stable non-free
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 94558F59
sudo apt-get update
sudo apt-get install spotify-client

# for using NetBeans and PhpStorm
## using OpenJDK 8
## make sure ppa:no1wantdthisname/openjdk-fontfix is added to apt repositories and updated
sudo update-alternatives --config java
sudo update-alternatives --config javac
## add OpenJDK 8 to path
cd ~
vim .bashrc
### append the following lines
# ------------------
JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/bin/java"
JRE_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"
PATH=$PATH:$HOME/bin:JAVA_HOME:JRE_HOME
# ------------------
source .bashrc

### if font issue not resolved, add the following lines to 'bin/phpstorm64.vmoptions'
# ------------------
-Dawt.useSystemAAFontSettings=lcd
-Dsun.java2d.xrender=true
-Dawt.useSystemAAFontSettings=on
-Dsun.java2d.xrender=true
# ------------------
```

##Stage 3 - Tools

###Install Development Tools

```bash
# add repositories
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo add-apt-repository ppa:linuxgndu/sqlitebrowser
sudo apt-get update

# install sublime text 3
sudo apt-get install sublime-text-installer

# install MySQL Workbench
wget http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community-6.3.3-1ubu1404-amd64.deb
# double click the deb to install it via Ubuntu Software Center.
# DO NOT use dpkg to install, otherwise the dependent pakages won't be correctly installed.

# install SQLite Browser 3
sudo apt-get install sqlitebrowser


# install Eclipse
# Note 1: Eclipse can be installed as multiple executables for Java, JEE, C/C++, etc.
# The solution is: change the Eclipse folder name in /opt, change the name of the .desktop,
# and change the name of and path to Eclipse executable
# Note 2: Make sure eclipse is at least u+x
wget [eclipse]
tar -xzf [eclipse...tar.gz]
sudo mv eclipse /opt
sudo ln -s /opt/eclipse/eclipse /usr/local/bin/eclipse
sudo gedit /usr/share/applications/eclipse.desktop
# paste and/or edit the following config options
#------------------------------------
[Desktop Entry]
Name=Eclipse
Type=Application
Exec=/opt/eclipse/eclipse
Terminal=false
Icon=/opt/eclipse/icon.xpm
Comment=Integrated Development Environment
NoDisplay=false
Categories=Development;IDE;
Name[en]=Eclipse Luna
#-----------------------------------
```

### Installiling NetBeans

#### Installation
Download NetBeans and do `chmod +x` to the installer script. For NetBeans 8.0.2, select OpenJDK for default Java during installation dialogues may fix font rendering bugs.



### Customize Development Tools

####Sublime Text 3
#####Generic Packages

- Package Control
- Git
- GitGutter
- Emmet
- AllAutocomplete
- Terminal
- SublimeREPL
- ColorPicker
- MarkdownPreview
- DocBlockr
- JSLint
- PHPCS
- SublimeLinter
- PHPUnit Completions
- PHPUnit
- Soda Theme
- Colorsublime
- Knockdown
- SublimeCodeIntel

#####Fix key bindings

Go to 'Perferences->Key Bindings - User' and paste the following:

```json
[
    { "keys": ["alt+shift+pageup"], "command": "select_lines", "args": {"forward": false} },
    { "keys": ["alt+shift+pagedown"], "command": "select_lines", "args": {"forward": true} },
	{ "keys": ["ctrl+shift+k"], "command": "toggle_side_bar" }
]
```


##Stage 5 - Environment for Web Development

###Install LAMP Stack with PhpMyAdmin
```bash
sudo apt-get install lamp-server^
sudo mv /etc/php5/apache2/php.ini /etc/php5/apache2/php.ini.bak
sudo cp -s /usr/share/php5/php.ini-development /etc/php5/apache2/php.ini
sudo apt-get install php5-mcrypt php5-json
sudo php5enmod mcrypt
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/fqdn.conf
sudo a2enconf fqdn
sudo a2enmod rewrite
sudo apt-get install phpmyadmin
sudo vim /etc/apache2/apache2.conf
# Add the following line to the bottom
# --------------------------------
# phpMyAdmin Configuration
Include /etc/phpmyadmin/apache.conf
# --------------------------------
sudo service apache2 restart

# Change permission for '/var/www'
sudo usermod -a -G www-data $USER
sudo chgrp -R www-data /var/www
sudo chmod -R 2775 /var/www
```

###If MySQL is not required
```bash
sudo apt-get install php5-mysql
sudo service apache2 restart
```

###Install Additional Packages for PHP5
```bash
sudo apt-get install php5-sqlite php5-curl
```

###Enable Nautilus Trash for Users in `/var/www`

Note: this workaround is per user basis; use the uid of `www-data` MAY work as well.

```bash
# find current user uid
id $USER
# create trash folder for the user, e.g. `.Trash-1000`
sudo mkdir .Trash-[uid]
sudo chown -R $USER:$USER .Trash-[uid]
sudo chmod -R 700 .Trash-[uid]
# enable trash in nautilus in the folder which has `$USER` permission, e.g. `/var/www`
nautilus /var/www
# creater a file or floder and move it to trash with right click
```

###Install Android Studio

Reun `sudo apt-get install lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6`

After downloading android studio, edit `android-studio/bin/studio.sh` add the line above `Run the IDE.` section:
```bash
# Disable IBUS
XMODIFIERS= ./bin/studio.sh
export XMODIFIERS
```
During installation, make sure Oracle JDK is choosen as the Java compiler.



###Install Composer

```bash
cd
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
vim .bashrc
## put the following line in bashrc
# ------------------------------
export PATH="$PATH:~/.composer/vendor/bin"
# ------------------------------
exec $SHELL
```

###Install Node and npm

####Reference
[NodeSource Node.js Binary Distributions](https://github.com/nodesource/distributions)

``` bash
# add the repository of node.js 0.12 deb build
curl -sL https://deb.nodesource.com/setup_dev | sudo bash -
# install
sudo apt-get install -y nodejs
# if not able to use npm without sudo, try reclaiming permissions
sudo chown -R $USER:$USER ~/.npm
sudo chown -R root:$USER /usr/lib/node_modules
sudo chmod g+s /usr/lib/node_modules
```

###Install Ruby with RBENV
####Reference
[Setup Ruby On Rails on Ubuntu 14.04 Trusty Tahr](https://gorails.com/setup/ubuntu/14.04)

``` bash
# install dependencies for ruby
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

# install rbenv with ~/.bashrc
cd
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash
git clone https://github.com/ianheggie/rbenv-binstubs.git ~/.rbenv/plugins/rbenv-binstubs

# restart shell
exec $SHELL

# select and build ruby
rbenv install -l
rbenv install 2.2.2
rbenv global 2.2.2
ruby -v

# set gem not to install documentation for each package
echo "gem: --no-ri --no-rdoc" > ~/.gemrc

# install rails
gem install rails
rbenv rehash
rails -v
```

### Install Additional Packages via `composer`, `npm`, and `gem`

#### Composer

```bash
# Install PHP CodeSniffer and Standards
composer global require "squizlabs/php_codesniffer=*"
composer global require "fabpot/php-cs-fixer=*"
composer global require "phpmd/phpmd=*"
composer global require "wp-coding-standards/wpcs=*"
phpcs --config-set installed_paths /home/drewfle/.composer/vendor/wp-coding-standards/wpcs

# Install Laravel Installer
composer global require "laravel/installer=~1.1"
```

#### NPM

```bash
# Install Gulp and Bower
sudo npm install -g gulp
sudo npm install -g bower
```

#### Pear

```bash
# Install PhpDocumentor
# Due to composer is ubable to install the dependencies for phpdoc in one go, here uses pear instead.
sudo pear channel-discover pear.phpdoc.org
sudo apt-get install graphviz
sudo pear install phpdoc/phpDocumentor
```

#### Gem

```bash
# Install Compass
gem update --system
gem install compass
```
### Python things
Further tutorial on py2/3 venv, read:
  - [Python Development Environment on Mac OS X Yosemite 10.10](http://hackercodex.com/guide/python-development-environment-on-mac-osx/)
  - [Virtual Environments](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
  - [Virtualenvwrapper Doc Installation](http://virtualenvwrapper.readthedocs.org/en/latest/install.html)
```python
sudo apt-get install python-pip python3-pip
sudo apt-get install python-virtualenv
pip install virtualenvwrapper
# for some pip package dependency
sudo apt-get install python-dev python3-dev libxml2-dev libxslt-dev
```
Add the lines to `.bashrc` then `source ~/.bashrc`:
```bash
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/VenvProjects
source /usr/local/bin/virtualenvwrapper.sh
```
n.b. new project with dedicated venv using (pip3 will be automatically installed): `mkproject -p python3 [project_name]`
n.b. new venv using (pip3 will be automatically installed): `mkvirtualenv -p python3 [venv_name]`


### Install sendmail [Obsolete, install poftfix instead]

sendmail requires a FDNQ as sender's hostname. When FDNQ is not available on a development system, the FDNQ aliases should fall in `/etc/hosts`.
Note: hosts aliases of 127.0.0.1 can not be written in multiple lines.

```bash
sudo vim /etc/hosts
# edit the 127.0.0.1 as below
------------
127.0.0.1	localhost.localdomain localhost <yourHostname>
------------
sudo apt-get install sendmail
```
### Other Links
[VirtualBox 4.3.28 released and ubuntu installation instructions included](http://www.ubuntugeek.com/virtualbox-4-3-28-released-and-ubuntu-installation-instructions-included.html)
