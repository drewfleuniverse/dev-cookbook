## Generic Usage

```bash
# check artisan usage
# e.g. php artisan help make:controller
php artisan help [commandName]
```

## Display routes

```bash
# list all routes
php artisan route:list
```


## Create Controller

```bash
# create controller with methods
php artisan make:controller NewController

# create controller without methods
php artisan make:controller NewController --plain
```

## Create Middleware

```bash
# create a middleware
php artisan make:middleware NewMiddleware
```

## Manage Database

```bash
# Creating migrations
# n.b. this command won't touch the migration repository.
# n.b. the migration created with this command can be manually edited or deleted
# if the migration repository doesn't have it.
# n.b. when encountering artisan complains about deleted migration(s), the solution is as below:
# 1. delete the row related to thedeleted migration(s).
# 2. run 'composer dump-autoload', due to hidden command history of artisan may be stored in memory

# create migration without Schema methods
php artisan make:migration [name_of_the_table]

# create migration with Schema methods to create new table
php artisan make:migration [name_of_the_table] --create=[table]

# create migration with Schema methods to alter existing table
php artisan make:migration [name_of_the_table] --table=[table]

# create table(s) in database
php artisan migrate

# create table migrations, i.e. the migration repositories
php artisan migrate:install

# drop table(s) created last time in database
php artisan migrate:rollback

# drop all tables created with artisan migrate
php artisan migrate:reset

# drop all tables created with artisan migrate and recreate
php artisan:migrate refresh
```

## Create Eloquent Model

```bash
# create model with migration
# n.b. this command produces files in both of 'app' and 'database/migrations'
# n.b. the case of the name of model file is according to the case used in the command
php artisan make:model [ModelName]

# create model without migration or the table already exists
php artisan make:model [ModelName] --no-migration
```
## Using Aartisan Tinker

### Strating Tinker

```bash
php artisan tinker
```

### Using Tinker

```php
# tips on working with Laravel objects

# show object status with toArray()
# n.b. most Laravel classes have toArray() method
# for example, a newly initialized model object may be empty, i.e. reference to nothing, and, when its
# data are saved to the table, the model object should contain all properties it should have.
$obj = new App\Class;
$obj->toArray();

# show all data in the table with all()
# n.b. most Laravel classes have all() method
App\ModelClass::all()->toArray();

# show object and class properties using PHP functions
get_object_vars($obj);
get_class_vars('App\Class');

# remove accidentally added property to an object
$obj->porpertyNotExist = whatever;
unset($obj->propertyNotExist);

# create a new model
$myModel = new App\MyModel;
$myModel->foo = 'bar';
$myModel->did_it_at = 'Carbon\Carbon::now();
$myModel->save();

# create a new model with mass assignment, i.e. App\MyModel->fillable()
$myModel = App\MyModel::create(['attr1' => 'value1', 'attr2' => foo(), ...]);

# delete a model
# n.b. make sure what you're deleting with: 'App\MyModel::all()->toArray()'
$myModel->delete();
$myModel->destroy(that_id);
$myModel = App\MyModel::find(that_id)->delete();
# or,
$myModel = App\MyModel::where(...)->delete();
```

## Maintenance Mode

```bash
# enable maintenance mode
php artisan down
# disable maintenance mode
php artisan up
```

## Creating middleware

```bash
php artisan make:middleware [MiddlewareName]
```
