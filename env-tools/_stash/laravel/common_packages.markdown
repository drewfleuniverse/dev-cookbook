Some packages are not installed by default in Laravel.
Hint: install packages via `composer` with command line, run: `composer require [vendorName]/[packageName]`
## IDE

- `barryvdh/laravel-ide-helper`: register the service as `Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,` and run `artisan ide-helper:generate` to generate Laravel namespaces. The complete steps are as below:
	- `php artisan clear-compiled`
	- `php artisan ide-helper:generate`
	- `php artisan ide-helper:meta`
	- `php artisan optimize`
	- Reference:
	    - [Laravel 5 IDE Helper Generator](https://github.com/barryvdh/laravel-ide-helper)
		- [Laravel Development using PhpStorm](http://blog.jetbrains.com/phpstorm/2015/01/laravel-development-using-phpstorm)

## Migration

- `doctrine/dbal`: dropping column support for Sqlite

## Forms

- `illuminate/html`: form package for Laravel 4 and 5
