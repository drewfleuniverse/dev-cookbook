```bash
# In Laravel root, 
npm install
bower init

# Create and paste the following to '.bowerrc'
vim .bowerrc
# ----------------
{
  "directory": "vendor/bower_components"
}
# ----------------
bower install bootstrap-sass-official --save

# Edit 'gulpfile.js' as below
# ----------------
var paths = {
	'jquery': './vendor/bower_components/jquery/',
	'bootstrap': './vendor/bower_components/bootstrap-sass-official/assets/'
}
 
elixir(function(mix) {
    mix.sass("style.scss", 'public/css/', {includePaths: [paths.bootstrap + 'stylesheets/']})
        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts')
        .scripts([
            paths.jquery + "dist/jquery.js",
            paths.bootstrap + "javascripts/bootstrap.js"
        ], 'public/js/app.js', './');
});
# ----------------
```