## Versioning Issues

- For Laravel version, at least, >5.0.31, newly created model object does not contain any property until manually added or saved to database.
