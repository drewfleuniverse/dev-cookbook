## Basic Usage


### Directories

- `~/Homestead`: Laravel Homestead installation, i.e. scrips.
- `~/.homestead`: Homestead configuration files.
- `~/.vagrant.d`: stores Homestead box.
- `~/Code`: the default sync folder for Homestead and home to Laravel applications.
- `VirtualBox VMs`: temporarily stores the launched Homestead VM.

### Configuration

Edit `~/.homestead/Homestead.yaml`:

```yaml
---
ip: "192.168.10.10"
memory: 2048
cpus: 1
provider: virtualbox

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/Code
      to: /home/vagrant/Code

sites:
    - map: [projectDomainName]
      to: /home/vagrant/Code/[projectName]/public

databases:
    - homestead

variables:
    - key: APP_ENV
      value: local

# blackfire:
#     - id: foo
#       token: bar
#       client-id: foo
#       client-token: bar

# ports:
#     - send: 93000
#       to: 9300
#     - send: 7777
#       to: 777
#       protocol: udp
```

### Run, Stop, and Destroy a Homestead Instance

```bash
# check if any box is up. Use globally, not restricted within '~/Homestead'
# state has 'running', 'stop', and nothing if destroyed
vagrant global-status

# in '~/Homestead'
cd ~/Homestead

# start Homestead
vagrant up

# ssh into Homestead
vagrant ssh

# launch Laravel application with the domain name(s) in '~/.homestead/Homestead.yaml':
# n.b. the domain name must be listed in the host's '/etc/hosts', e.g. 192.168.10.10  [projectDomainName]
# n.b. the domain name will be configured in the folder`/etc/nginx/sites-available` in Homestead after 'vagrant up'.
# Go to browser and navigate to [projectDomainName]

# launch Laravel application without looking into `~/.homestead/Homestead.yaml`:
serve [WHATEVERDomainName] [absolutePathToTheLaravelApplication]/public

# suspend Homestead
vagrant suspend

# resume Homestead. Alternatively, 'vagrant up' will do.
vagrant resume

# destroy Homestead
vagrant destroy --force
```