[Composer](https://getcomposer.org/)
[Packagist](https://packagist.org/)
[Laravel 5 Fundamentals](https://laracasts.com/series/laravel-5-fundamentals)
[Creating a Basic ToDo Application in Laravel 5](https://www.flynsarmy.com/2015/02/creating-a-basic-todo-application-in-laravel-5-part-1/)
[Laravel 5 and AngularJS Tutorial](http://www.codetutorial.io/laravel-5-angularjs-tutorial/)

## References
- [10 Must-have Laravel 5.1 Packages](https://laraveltips.wordpress.com/2015/06/25/10-must-have-laravel-5-1-packages/)
- [spatie/laravel-medialibrary](https://github.com/spatie/laravel-medialibrary)
- [Going Async With ES6 Generators](http://davidwalsh.name/async-generators)
- [Single Page Applications with AngularJS 2.0](http://www.slideshare.net/xmlking/spa-20)
- [marmelab/ng-admin](https://github.com/marmelab/ng-admin)
- [Makes Angular JS works offline](http://stackoverflow.com/questions/23652183/makes-angular-js-works-offline)
- [Safely Prevent Template Caching in AngularJS](http://opensourcesoftwareandme.blogspot.com/2014/02/safely-prevent-template-caching-in-angularjs.html)

## Front-end: Gulp, Angular, etc
- [osscafe/gulp-cheatsheet](https://github.com/osscafe/gulp-cheatsheet)
- [A Healthy Gulp Setup for AngularJS Projects](http://paislee.io/a-healthy-gulp-setup-for-angularjs-projects/)
- [Using GulpJS to Generate Environment Configuration Modules](http://atticuswhite.com/blog/angularjs-configuration-with-gulpjs/)
- [merge-stream](https://www.npmjs.com/package/merge-stream)
- [addyosmani/es6-equivalents-in-es5](https://github.com/addyosmani/es6-equivalents-in-es5#classes)

## Notes
- Using the gulpfile in Laravel root enables laveraging elixir's hash string css files.