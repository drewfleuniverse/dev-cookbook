```
# `gnu-sed` must be installed
APP=official-tutorial-52 && \
composer create-project laravel/laravel $APP --prefer-dist && \
cd $APP && \
composer require doctrine/dbal && \
composer require barryvdh/laravel-ide-helper --dev && \
composer require squizlabs/php_codesniffer --dev && \
sed -i "s/APP_URL=http:\/\/localhost/APP_URL=http:\/\/dockerstead.app/g" .env && \
sed -i "s/DB_CONNECTION=mysql/DB_CONNECTION=sqlite/g" .env && \
sed -i "s/DB_HOST=127.0.0.1/#DB_HOST=127.0.0.1/g" .env && \
sed -i "s/DB_PORT=3306/#DB_PORT=3306/g" .env && \
sed -i "s/DB_DATABASE=homestead/DB_DATABASE=storage\/db.sqlite/g" .env && \
sed -i "s/DB_USERNAME=homestead/#DB_USERNAME=homestead/g" .env && \
sed -i "s/DB_PASSWORD=secret/#DB_PASSWORD=secret/g" .env && \
touch storage/db.sqlite && \
chmod -R 775 storage && \
chmod -R 775 bootstrap/cache && \
sed -i "/RouteServiceProvider/ a \        \\/\*  Development Tool Service Providers \*\/\n\        \'SocialEngine\\\SnifferRules\\\ServiceProvider',\n\        \Barryvdh\\\LaravelIdeHelper\\\IdeHelperServiceProvider::class," \
    config/app.php
```
# Laravel 5.2 Official Tutorial



## Project Setup with Per-Project Homestead

###### Setting Project Up and Running
```bash
composer install
php vendor/bin/homestead make
vagrant up
vagrant ssh
cd official-tutorial-52
php artisan migrate
```

********************************************************************************

## Tools for Development

###### Using PhpCS with SquizLabs Code Sniffer 2.0 artisan command
The `sniff` artisan command
```bash
php artisan sniff
```

###### phpDocumentor
As of March 3, 2016, `phpDocumentor.phar` cannot be executed in php7.
```bash
# To create documentation
php phpDocumentor.phar
# To serve the doc output
http-server output
```

********************************************************************************

## Preparing Project
```bash
composer require socialengine/sniffer-rules
wget http://www.phpdoc.org/phpDocumentor.phar
composer require doctrine/dbal
composer require barryvdh/laravel-ide-helper --dev
php artisan ide-helper:generate
```
`socialengine/sniffer-rules` needs further configuration as stated in [socialengine/sniffer-rules](https://packagist.org/packages/socialengine/sniffer-rules). And it is better to set the `phpcs` standard to `PSR2` in `config/sniffer-rules.php`.

###### Configuring Development Environment
On OSX, the following PHP dependencies need to be updated:
```bash
brew rm freetype jpeg libpng gd zlib
brew install freetype jpeg libpng gd zlib
xcode-select --install
# The following may not needed
#brew install phpunit
```

********************************************************************************

## References

- [Laravel 5 IDE Helper Generator](https://github.com/barryvdh/laravel-ide-helper#automatic-phpdocs-for-models)
- [socialengine/sniffer-rules](https://packagist.org/packages/socialengine/sniffer-rules)
