# Mongo Shell 3.2+

## CRUD Operations

### Search

- `db.collection.find(query)`
- `db.collection.find(query, projection)`


### Insert

- `db.collection.insertOne(document)`
- `db.collection.insertMany(array-of-documents)`
- `db.collection.insert(document)`
- `db.collection.insert(array-of-documents)`

## Remove

- `db.collection.remove(query)`: remove each documents which fits deletion criteria.
- `db.collection.drop()`: remove a collection

## Update

- `db.collection.updateOne(...)`
- `db.collection.updateMany(...)`
- `db.collection.replaceOne(...)`
- `db.collection.updateMany(...)`
- `db.collection.update(query, update)`: replace a single document or the first document matched query criteria.
- `db.collection.update(query, update, {multi: 1})`: replace multiple documents; modifier expression must be used in update.
- `db.collection.findAndModify({query: {document}, update: {document})`: modify a single document or the first document matched query criteria.git
- `db.collection.findAndModify({query: {document}, remove: {document})`: remove a single document.

### Update with Modifiers

Fields:

- `$inc`: change the value for existing key or create a new key with assigned value.



## Query Techniques

- `db.foo.find({}, {_id: 0})`: show all documents exclude the `_id` field.
- `var bar = db.foo.findOne(query)`: assign document to a variable.
  - equivalent to: `var bar = db.foo.find(query).limit(1).toArray()[0]`

## Deprecated and Unfound Methods
