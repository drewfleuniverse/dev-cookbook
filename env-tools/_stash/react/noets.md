Authentication
- [Secure Your React and Redux App with JWT Authentication](https://auth0.com/blog/secure-your-react-and-redux-app-with-jwt-authentication/)


Immutable
- [cheatsheets for Immutable.js](http://ricostacruz.com/cheatsheets/immutable-js.html)
- [Immutable.js 101 – Maps and Lists](http://thomastuts.com/blog/immutable-js-101-maps-lists.html)

Local Storage
- [STEP 7: MAKE TODOS PERSISTENT WITH LOCAL STORAGE](http://yeoman.io/codelab/local-storage.html)

Basics
- [A very simple introduction to JS-only React](https://github.com/davidmashe/react_demo)
