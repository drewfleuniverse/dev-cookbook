# Synology DSM CA cert issues

## Problem

The stock self-signed certificate has an invalid CA cert that cannot be recognized by Chrome

## Create a new self-signed certificate

Go to **Control Panel > Security > Certificate** to create a new self-signed certificate

- In **Create root certificate**, i.e. the CA cert:
    - Common name: <name>
        - E.g. mynasCA
    - Fill out the rest

- In **Create certificate**, i.e. the self-signed cert, fill out every slot, note that:
    - Common name: <fdnq>
        - E.g. mynas.synology.me
    - Subject alternative name: <name1>;<name2>;...
        - E.g. mynas.synology.me;192.168.1.2
    - Fill out the rest

## Import the CA to OS

### macOS

- In **Control Panel > Security > Certificate**, click **Export certificate** to download the cert and CA cert pairs:
    - Move the CA cert to **Keychain access > System**
    - Double click the CA cert:
        - In **Trust**, select *Always Trust* for *When using this certificate*
- N.b. no need to do any modification to the self-signed certs due to the CA cert already get the self-signed certs verified
