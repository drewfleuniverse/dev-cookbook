# AESCLI Notes

## AWSCLI basics

### Authentication

#### Using awscli with different profile
```bash
# Method 1
aws [command] --profile [profile-name]

# Method 2 - Linux, Mac OSX
export AWS_DEFAULT_PROFILE=[profile-name]

# Method 2 - Windows
set AWS_DEFAULT_PROFILE=[profile-name]
```
