
---

### Problem
Unable to create views and functions during importing database.

### Reference
[Remove DEFINER clauses from MySQL dumps](http://xplus3.net/2013/10/10/remove-definer-clause-mysqldump/)

### Solution

```bash
mysqldump > non_portable_dump.sql
sed -E 's/DEFINER=`[^`]+`@`[^`]+`/DEFINER=CURRENT_USER/g' non_portable_dump.sql > portable_dump.sql
```
---