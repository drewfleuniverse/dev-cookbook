# Deployd

################################################################################
## Project Setup
################################################################################

### Create Deployd project #####################################################

```bash
dpd create dpd.api.example.com
```

### Create key for accessing Deployd dashboard #################################

```bash
cd dpd.api.example.com
dpd keygen
dpd showkey
```

### Create script for running as daemon process ################################

#### Create script

```bash
vim production-version.js
```

#### Edit the script file

```js
var deployd = require('deployd');
var server = deployd({
  port: 12345,
  env: 'production',
  db: {
    host: 'localhost',
    port: 27017,
    name: 'mongodb-database-name',
    credentials: {
      username: 'mongodb-user-name',
      password: 'password'
    }
  }
});

server.listen();

server.on('listening', function() {
  console.log("Server is listening");
});

server.on('error', function(err) {
  console.error(err);
  process.nextTick(function() { // Give the server a chance to return an error
    process.exit();
  });
});
```

### Create database user in MongoDB ############################################

```js
use mongodb-database-name
db.createUser({user: "mongouser",pwd: "someothersecret",roles: ["readWrite"]})
```

### Run Deployd project as a daemon ############################################

```bash
forever start production-version.js
```

### Host the project with Apache using internal port forwarding ################

Note: the port forwarding only applied within localhost, thus there's no need to
open the port which Deployd project is listening.

#### Enable mode `proxy` and `proxy_http`

```bash
sudo a2enmod proxy proxy_http
```

#### Create and enable Apache virtual host

Create file:

```bash
sudo vim /etc/apache2/sites-available/dpd.api.example.com.conf
```

Edit the file as following:

```apacheconf
<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName dpd.api.example.com
        DocumentRoot /var/www/dpd.api.example.com/public

        ProxyPass / http://localhost:12345/
        ProxyPassReverse / http://localhost:12345/

        <Directory "/var/www/dev2.dpd.api.cicusa.org/public">
                Options FollowSymLinks
                AllowOverride All
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Enable dpd.api.example.com:

```bash
sudo a2ensite dpd.api.example.com.conf
sudo service apache2 restart
```

################################################################################
## Database Migration
################################################################################

### Migrate Deployd APIs and database with dpdmgr ##############################

#### Drop target database

In MongoDB console:

```js
// Drop the database after running `show dbs` and `use [db-name]`
db.runCommand( { dropDatabase: 1 } )
```

#### Run dpdmgr

Once the source and target url endpoints are configured in `conf.yaml`:

```bash
workon dpdmgr
python dpdmgr.py -c uri
deactivate
```
