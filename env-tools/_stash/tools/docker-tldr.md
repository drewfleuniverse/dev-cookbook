# Docker



## Basic Usage #################################################################

### Startup a Docker machine on Mac OSX or Windows ###

The bast way to start a docker machine is using Docker Quickstart Terminal. The Docker application starts the default Docker machine or loads the environment of the running machine in a new terminal.

When a terminal is not started with Docker Quickstart Terminal, run `eval $(docker-machine env)`, which runs againsㄔ the default Docekr machine, or `eval $(docker-machine env [machine-name]])` is needed to load docker machine's environment. Command `docker-machine env` only sets and returns default Docker machine's environment variables.

Note: If no machine is listed with `docker-machine ls` or the default machine was accidentally removed, run `docker-machine create --driver virtualbox default` to reinstall.

[codekitchen/dinghy](https://github.com/codekitchen/dinghy)

### Project structure ###

```bash
/my-docker-project
 |-- Dockerfile
 |-- [other dockerfiles ...]
 |-- [other files ...]
 \-- .dockerignore
```

### Basic Usage ###

```bash
# Run a container with specified container name
docker run --name [app-name] [account-name]/[image-name]:[tag]

# Start a bash session
docker exec -it [container-name/container-id] bash


```

### Run, Stop, and Restart a Container ####

#### Run a container

```bash
# Run a container with specified container name
docker run --name [app-name] [account-name]/[image-name]:[tag]

# Rename a container
docker rename [old-app-name] [new-app-name]

# Run with an interactive terminal
docker run -it [account-name]/[image-name]:[tag]

#  Run a daemon process with port mapped to the container
docker run -d -p 80:80 [account-name]/[image-name]:[tag]
```

#### Stop

#### Run Commands in a Container

```bash
docker exec -it [container-name/container-id] bash
```

### Build, tag, push, and pull a Docker image ###

#### Basic work flow for new project

```bash
# Build a Docker image for the first time.
docker build -t [image-name] .

# 1. Namespace Docker image with Docker Hub account name.
# 2. If the image is tagged with a specific version label, then
# the latest tag will be automatically assigned and result in two images shares
# the same id with different tags.
docker tag [image-id] [account-name]/[image-name]:[tag, e.g. latest]

# Login to Docker Hub
docker login

# Push image to Docker Hub
docker push [account-name]/[image-name]

# Remove local copies of the image and containers
docker rmi -f [image-id]
docker rmi -f [image-name]

# Pull the image from Docker Hub
docker pull [account-name]/[image-name]
# or
docker pull [account-name]/[image-name]:[tag]
```

#### Basic work flow for existing project

```bash
# Build image tagged with latest by default
# This step should repeat several times
docker build -t [image-name] .

docker tag [image-name]:latest [account-name]/[image-name]:[tag, e.g. v1.0 or latest]
docker push [account-name]/[image-name]:[tag, e.g. v1.0 or latest]
```

#### Using the repository on ECS with AWSCLI

```bash
# Run the docker login command with credentials returned by the following command
aws ecr get-login --region us-east-1
```



## Common Commands #############################################################

##### Remove all images

```bash
docker rmi `docker images -aq`
# or
docker rmi -f `docker images -aq`
```

##### Remove all containers

```bash
docker rm `docker ps -aq`
```

##### Remove specific container

```bash
docker rm `docker kill [container-id]`
```



## Project structure and development guidelines ################################

### References ###

- [Docker Official Images - Review Guidelines](https://github.com/docker-library/official-images#review-guidelines)



## References ##################################################################

### Basic Usage ###

- [Get Started with Docker for Mac OS X](https://docs.docker.com/mac/)
- [Docker Engine | Engine reference | Command line reference | build](https://docs.docker.com/engine/reference/commandline/build/)
- [moxiegirl/whalesay](https://github.com/moxiegirl/whalesay)

### MISC ###

#### Tutorials

- [Docker Basics: A Practical Starter Guide](https://blog.codeship.com/docker-basics-a-practical-starter-guide/)
- [Docker Basics: Linking and Volumes](https://blog.codeship.com/docker-basics-linking-and-volumes/)
- [A Better Dev/Test Experience: Docker and AWS](https://medium.com/aws-activate-startup-blog/a-better-dev-test-experience-docker-and-aws-291da5ab1238#.j09ojksnw)
- [Using Containers to Build a Microservices Architecture](https://medium.com/aws-activate-startup-blog/using-containers-to-build-a-microservices-architecture-6e1b8bacb7d1#.sbbjda2ax)
- [Cluster-Based Architectures Using Docker and Amazon EC2 Container Service](https://medium.com/aws-activate-startup-blog/cluster-based-architectures-using-docker-and-amazon-ec2-container-service-f74fa86254bf#.w8nt8wj87)
- [Running Services Using Docker and Amazon EC2 Container Service](https://medium.com/aws-activate-startup-blog/running-services-using-docker-and-amazon-ec2-container-service-bde16b099cb9#.6tojgwmkl)

#### Examples with source repositories
- [training/webapp](https://hub.docker.com/r/training/webapp/)
- [gitlab/gitlab-ce](https://hub.docker.com/r/gitlab/gitlab-ce/)
