# Virtual Box Notes

################################################################################
## Network
################################################################################

### SSH

#### SSH with NAT

1. Install `openssh-server` in the client, e.g. `sudo apt-get install openssh-server`.ß
2. Right click the vm, in 'Network -> Adapter1 -> Advanced', click 'Port Forwarding', then add a rule for SSH, e.g. 'SSH Rule, TCP, 127.0.0.1, 2222, 10.0.2.15, 22'.
3. Since VBox is listening on port 2222, type `ssh -p 2222 [user_name]@127.0.0.1` to connect to the client.
