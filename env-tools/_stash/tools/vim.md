# VIM

## Moving Around

- left: `h`
- right: `l`
- up: `k`
- down: `j`
- move forward to the beginning of next word: `w`
- move forward to the beginning of n next word: `[number]-w`
- move backward to the beginning of previous word: `b`
- move backward to the beginning of n previous word: `[number]-b`
- move forward to the end of n next word: `e`
- move backward to the end of previous word: `ge`
- move to the beginning of line, exclude whitespaces: `^`
- move to the beginning of line, include whitespaces: `0`
- move to the end of line: `$`
- move to the start of a file: `gg`
- move to the end of a file: `G`
- move to nth line: '[number]-G'

## Deleting Characters

- delete char: `x`
- delete multiple chars: repeat `x`
- delete n chars: `[number]-x`
- delete line: `dd`
- delete line break: `J` or `shift-j`

## Undo and Redo

- undo: `u`
- redo: `ctrl-r`

## Other Editing Commands

- insert character: `i`
- insert character at the beginning of line: `I`
- append character: `a`
- append character at the end of line: `A`
- open new line below: `o`
- open new line above: `O`

## Other Commands

- show line number: `:set number`
- hide line number: `:set nonumber`
- set tab width: `:set tabstop=[number]`
- change all tab to current setting: `:retab`
- use space instead of tab: `:set expandtab`
- change
- scroll up: `ctrl-u`
- scroll down: `ctrl-d`
- scroll the line which cursor is on to the middle position: `zz`
- search: `/`

## Getting Out

- save and exit: "ZZ" or "shift+z-z"
- quit: `q`
- quit and discard: `q!`
