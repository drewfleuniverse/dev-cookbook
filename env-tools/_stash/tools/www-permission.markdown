### Reference

 - [What permissions should my website files/folders have on a Linux webserver?](http://serverfault.com/questions/357108/what-permissions-should-my-website-files-folders-have-on-a-linux-webserver)

### Setup Permissions for Single User/EC2 Ubuntu

Assuming the only user has admin rights is user `ubuntu` and the web server uses group `www-data`:

```bash
sudo chown -R ubuntu:www-data example.com
sudo chmod -R 750 example.com
sudo chmod g+s example.com

# Do Not Use The Following, it results in making git unable to index changes
cd example.com
sudo find -type d -exec chmod 750 {} \;
sudo find -type f -exec chmod 640 {} \;
```

For folders require group write permission:

```bash
sudo chmod g+w the-writable
```