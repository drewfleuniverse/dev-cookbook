###Reference
[OpenFrameworks Building but Not Running in Xcode?](http://danielmclaren.com/node/188)

Quote:
The solution is to navigate to "Product > Edit scheme...", select "Run" in the bar on the left of the dialogue window that pops up, and select your app file from the "Executable" dropdown.