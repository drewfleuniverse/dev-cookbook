##Backup Database with sqldump

`mysqldump --add-drop-table -h <hostUrl> -u <userName> -p <dbName> > <dumpName, e.g. Dump20150430>.sql bzip2 <dumpName, e.g. Dump20150430>.sql`

##Backup Database with MySQL Workbench

In 'Management' pane:

- Click 'Data Export'
- Under 'Object Selection'
    - Under 'Tables to Export' select the database
    - Under 'Export Options' select 'Export to Self-Contained File'
- Click 'Start Export'

##Backup Database with PhpMyAdmin

Go the page under 'Export' of the wordpress database:

- Under 'Format', select 'SQL'
- Under 'Export Method', select 'Custom'
- Now the custom options is shown.
- Under 'Object creation options', check 'Add DROP TABLE / VIEW / PROCEDURE / FUNCTION / EVENT statement'
- Click 'Go' to export the database.