###Call Graph
```
# Call: add_action( 'admin_menu', 'fbiz_menu' );
add_action
`-- fbiz_menu
    `-- add_theme_page
        `-- fbiz_page
            |-- _e
            |-- esc_url
            |-- esc_attr_e
            |-- php::isset
            |-- settings_fields
            `-- do_settings_sections

# Call: add_filter( 'option_page_capability_fbiz-options', 
#                   'fbiz_get_options_page_cap' );
add_filter
`-- fbiz_get_options_page_cap

# Def: function fbiz_get_options()
fbiz_get_options
`-- wp_parse_args
    |-- get_option
    `-- fbiz_get_option_defaults
        |-- get_template_directory_uri
        `-- apply_filters

# Call: add_action( 'admin_init', 'fbiz_register_settings' );
add_action
`-- fbiz_register_settings
    |-- register_setting
    |   `-- fbiz_sanitize_callback
    |       |-- php::trim
    |       |-- esc_url_raw
    |       |-- sanitize_text_field
    |       `-- force_balance_tags
    |-- add_settings_section
    |   |-- __
    |   `-- fbiz_display_settings_section
    `-- add_settings_field
        `-- fbiz_display_setting
            |-- php::extract
            |-- fbiz_get_options
            |-- php::array_key_exists
            |-- php::stripslashes
            `-- esc_attr

# Call: add_action( 'admin_print_scripts-appearance_page_options', 
#                   'fbiz_settings_enqueue_scripts' );
add_action
`-- fbiz_settings_enqueue_scripts
    |-- wp_enqueue_script
    |-- wp_enqueue_style
    |-- wp_register_script
    |   `-- get_template_directory_uri
    `-- wp_localize_script
```

###Function Definitions

```php
<?php
// Call: add_action( 'admin_menu', 'fbiz_menu' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function );
_e( $text, $domain );
esc_url( $url, $protocols, $_context );
esc_attr_e( $text, $domain );
settings_fields( $option_group );
do_settings_sections( $page );

// Call: add_filter( 'option_page_capability_fbiz-options', 
//                   'fbiz_get_options_page_cap' );
add_filter( $tag, $function_to_add, $priority, $accepted_args );

// Def: function fbiz_get_options()
$args = wp_parse_args( $args, $defaults );
echo get_option( $option, $default );
get_template_directory_uri();
echo get_template_directory_uri();
apply_filters( $tag, $value, $var ... );

// Call: add_action( 'admin_init', 'fbiz_register_settings' );
register_setting( $option_group, $option_name, $sanitize_callback );
php:: string trim ( string $str [, string $character_mask = " \t\n\r\0\x0B" ] )
esc_url_raw( $url, $protocols );
sanitize_text_field( $str );
force_balance_tags( $text );
add_settings_section( $id, $title, $callback, $page );
add_settings_field( $id, $title, $callback, $page, $section, $args );
php:: int extract ( array &$array [, int $flags = EXTR_OVERWRITE 
                                  [, string $prefix = NULL ]] )
php:: bool array_key_exists ( mixed $key , array $array )
php:: string stripslashes ( string $str )
$fname = esc_attr( $text );

// Call: add_action( 'admin_print_scripts-appearance_page_options', 
//                   'fbiz_settings_enqueue_scripts' );
wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
wp_enqueue_style( $handle, $src, $deps, $ver, $media );
wp_register_script( $handle, $src, $deps, $ver, $in_footer );
wp_localize_script( $handle, $name, $data );
```