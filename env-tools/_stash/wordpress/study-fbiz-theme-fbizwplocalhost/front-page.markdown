###Program Structure

```
front-page.php
|-- get_header()
|-- fbiz_display_slider()
|-- get_sidebar( 'home' )
`-- get_footer()
```

###Function Definitions

```php
get_header( $name );
get_sidebar( $name );
get_footer( $name );
```