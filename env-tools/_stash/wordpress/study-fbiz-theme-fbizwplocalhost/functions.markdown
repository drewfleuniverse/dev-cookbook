###Call Graph
```
# Condition: if ( ! function_exists( 'fbiz_setup' ) ) : ...
php::function_exists

# Call: add_action( 'after_setup_theme', 'fbiz_setup' );
add_action
`-- fbiz_setup
    |-- load_theme_textdomain
    |   `-- get_template_directory
    |-- register_nav_menus
    |-- # add_action( 'wp_enqueue_scripts', 'fbiz_load_scripts' );
    |   add_action
    |   `-- fbiz_load_scripts
    |       |-- (1) wp_enqueue_style
    |       |   `-- get_stylesheet_uri
    |       |-- (2) wp_enqueue_style
    |       |   `-- fbiz_fonts_url
    |       |       |-- _x
    |       |       `-- php::urlencode
    |       |           `-- php::implode
    |       |-- is_singular
    |       `-- wp_enqueue_script
    |           `-- get_template_directory_uri
    `-- # add_action( 'widgets_init', 'fbiz_widgets_init' );
        add_action
        `-- fbiz_widgets_init
            |-- (1) register_sidebar
            |   `-- __
            `-- (2) register_sidebar
                `-- php::sprintf
                    `-- __

# Call: add_filter( 'post_class', 'fbiz_post_classes' );
add_filter
`-- fbiz_post_classes
    |-- post_password_required
    |-- is_attachment
    `-- has_post_thumbnail

# Def: function fbiz_show_copyright_text()
fbiz_show_copyright_text
|-- fbiz_get_options
|-- php::array_key_exists
`-- esc_html

# Def: function fbiz_show_page_header_section()
fbiz_show_page_header_section
|-- is_single
|-- is_page
|-- single_post_title
|-- is_home
|-- php::sprintf
|   |-- __
|   |-- single_post_title
|   `-- php::max
|-- is_404
`-- get_the_archive_title

# Def: function fbiz_show_website_logo_image_or_title()
fbiz_show_website_logo_image_or_title
|-- fbiz_get_options
|-- get_header_image
|-- get_bloginfo
|-- get_custom_header
|-- home_url
|-- esc_url
`-- esc_attr

# Def: function fbiz_show_pagenavi( $p = 2 )
fbiz_show_pagenavi
|-- is_singular
|-- php::empty
|   # function fbiz_p_link( $i, $title = '' )
`-- fbiz_p_link
    |-- php::sprintf
    |   `-- __
    |-- esc_url
    |   `-- get_pagenum_link
    `-- esc_attr

# Def: function fbiz_the_content()
fbiz_the_content
|-- has_post_thumbnail
|-- esc_url
|   `-- get_permalink
|-- esc_attr
|   `-- get_the_title
|-- the_post_thumbnail
`-- the_content

# Def: function fbiz_the_content_single()
fbiz_the_content_single
|-- has_post_thumbnail
|-- the_post_thumbnail
`-- the_content
    `-- __

# Def: function fbiz_display_slider()
fbiz_display_slider
|-- fbiz_get_options
`-- php::array_key_exists
```
###Hooks and Tags
```
```
###Function Definitions
```php
// Condition: if ( ! function_exists( 'fbiz_setup' ) ) : ...
php:: bool function_exists ( string $function_name )

// Call: add_action( 'after_setup_theme', 'fbiz_setup' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
load_theme_textdomain( $domain, $path );
echo get_template_directory();
register_nav_menus( $locations );
wp_enqueue_style( $handle, $src, $deps, $ver, $media );
get_stylesheet_uri();
_x( $text, $context, $domain );
php:: string urlencode ( string $str )
php:: string implode ( string $glue , array $pieces )
is_singular( $post_types );
wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
get_template_directory_uri();
echo get_template_directory_uri();
register_sidebar( $args );

// Call: add_filter( 'post_class', 'fbiz_post_classes' );
add_filter( $tag, $function_to_add, $priority, $accepted_args );
post_password_required( $post );
is_attachment();
has_post_thumbnail( $post_id );

// Def: function fbiz_show_copyright_text()
php:: bool array_key_exists ( mixed $key , array $array )
esc_html( $text )

// Def: function fbiz_show_page_header_section()
is_single($post);
is_page($page);
single_post_title( $prefix, $display );
single_post_title();
is_home();
php:: mixed max ( array $values )
php:: mixed max ( mixed $value1 , mixed $value2 [, mixed $... ] )
is_404();
post_type_archive_title( $prefix, $display );

// Def: function fbiz_show_website_logo_image_or_title()
get_header_image();
$bloginfo = get_bloginfo( $show, $filter );
add_theme_support( 'custom-header' );
home_url( $path, $scheme );
echo esc_url( home_url( '/' ) );

// Def: function fbiz_show_pagenavi( $p = 2 )
echo paginate_links( $args );

// Def: function fbiz_the_content()
$permalink = get_permalink( $id, $leavename );
echo get_the_title( $ID );
the_post_thumbnail( $size, $attr );
the_content( $more_link_text, $stripteaser );

// Def: function fbiz_the_content_single()
```