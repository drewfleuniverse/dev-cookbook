###Program Structure

```
page.php
|-- get_header
|-- fbiz_show_page_header_section
|-- < have_posts() >
|   |-- == true
|   |   |-- have_posts
|   |   |-- the_post
|   |   |-- get_template_part
|   |   |-- < comments_open() || get_comments_number() >
|   |   |   `-- comments_template
|   |   `-- wp_link_pages
|   `-- == false
|       `-- get_template_part
|-- get_sidebar
`-- get_footer
```

###Function Definitions

```php
get_header( $name );
have_posts();
the_post();
get_template_part( $slug );
get_template_part( $slug, $name );
comments_open( $post_id );
$my_var = get_comments_number( $post_id );
comments_template( $file, $separate_comments );
wp_link_pages( $args );
get_sidebar( $name );
get_footer( $name );
```