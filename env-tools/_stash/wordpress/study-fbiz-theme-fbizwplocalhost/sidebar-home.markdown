###Program Structure
```
sidebar-home.php
|-- < !dynamic_sidebar( 'homepage-column-1-widget-area' ) >
|   `-- _e
|-- < !dynamic_sidebar( 'homepage-column-2-widget-area' ) >
|   `-- _e
`-- < !dynamic_sidebar( 'homepage-column-3-widget-area' ) >
    `-- _e
```

###Function Definitions

```php
dynamic_sidebar( $index );
```