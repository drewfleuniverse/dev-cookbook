#WordPress Developer Resources

##Installation and Deployment

##Getting Started with Official Documentations

###Coding Standards

- [WordPress Coding Standards](https://make.wordpress.org/core/handbook/coding-standards)

###Theme Development

- [Theme Development](https://codex.wordpress.org/Theme_Development#Theme_Templates)

###Global Variables, Hooks, and Filters

- [Global Variables](https://codex.wordpress.org/Global_Variables)
- [Plugin API/Action Reference](http://codex.wordpress.org/Plugin_API/Action_Reference)
- [Plugin API/Filter Reference](http://codex.wordpress.org/Plugin_API/Filter_Reference)

###Pages and Templates

- [Page Templates](https://codex.wordpress.org/Page_Templates)


###Retrieving Data Using The Loop

- [The Loop](https://codex.wordpress.org/The_Loop)
- [Function Reference/have posts](https://codex.wordpress.org/Function_Reference/have_posts)
- [Class Reference/WP Query](https://codex.wordpress.org/Class_Reference/WP_Query)
- [Function Reference/wp reset postdata](https://codex.wordpress.org/Function_Reference/wp_reset_postdata)

###Widgets

- [Widgets API](https://codex.wordpress.org/Widgets_API)
- [Sidebars](https://codex.wordpress.org/Sidebars)
- [Function Reference/register sidebar](https://codex.wordpress.org/Function_Reference/register_sidebar)
- [Function Reference/dynamic sidebar](https://codex.wordpress.org/Function_Reference/dynamic_sidebar)

###Pagination

- [Styling Page-Links](https://codex.wordpress.org/Styling_Page-Links)