###Call Graph

```
# call: register_activation_hook( __FILE__, 'halloween_store_install' );
register_activation_hook
`-- halloween_store_install
    `-- update_option

# call: add_action( 'init', 'halloween_store_init' );
add_action
`-- halloween_store_init
    |-- __
    `-- register_post_type

# call: add_action( 'admin_menu', 'halloween_store_menu' );
add_action
`-- halloween_store_menu
    |-- add_options_page
    |   `-- __
    `-- halloween_store_settings_page
        |-- get_option
        |-- php::empty
        |-- _e
        |-- checked
        `-- esc_attr

# call: add_action( 'admin_init', 'halloween_store_register_settings' );
add_action
`-- halloween_store_register_settings
    `-- register_setting
        `-- halloween_sanitize_options
            |-- php::empty
            `-- sanitize_text_field

# call: add_action( 'add_meta_boxes', 'halloween_store_register_meta_box' );
add_action
`-- halloween_store_register_meta_box
    `-- add_meta_box
        |-- __
        `-- halloween_meta_box
            |-- get_post_meta
            |-- wp_nonce_field
            |-- __
            |-- esc_attr
            `-- selected

# call: add_action( 'save_post','halloween_store_save_meta_box' );
add_action
`-- halloween_store_save_meta_box
    |-- get_post_type
    |-- php::isset
    |-- php::defined
    |-- check_admin_referer
    |-- update_post_meta
    `-- sanitize_text_field

# call: add_shortcode( 'hs', 'halloween_store_shortcode' );
add_shortcode
`-- halloween_store_shortcode
    |-- php::extract
    |   `-- shortcode_atts
    |-- get_option
    `-- get_post_meta

# call: add_action( 'widgets_init', 'halloween_store_register_widgets' );
add_action
`-- halloween_store_register_widgets
    `-- register_widget
        `-- class hs_widget: WP_Widget
 ___________/
/
class hs_widget: WP_Widget
|-- hs_widget
|   |-- __
|   `-- WP_Widget::WP_Widget
|       `-- __
|-- WP_Widget::hs_widget::form
|   |-- __
|   |-- wp_parse_args
|   |-- _e
|   |-- WP_Widget::get_field_name
|   `-- esc_attr
|-- WP_Widget::hs_widget::update
|   |-- sanitize_text_field
|   `-- absint
`-- WP_Widget::hs_widget::widget
    |-- php::extract
    |-- apply_filters
    |-- php::empty
    |-- esc_html
    |-- absint
    |-- WP_Query::WP_Query
    |-- WP_Query::query
    |-- WP_Query::have_posts
    |-- WP_Query::the_post
    |-- get_option
    |-- get_post_meta
    |-- the_permalink
    |-- the_title_attribute
    |-- the_title
    |-- __
    `-- wp_reset_postdata
```

###WordPress Hooks Used in This Example

```
# add_action
init
admin_menu
admin_init
add_meta_boxes
save_post
widgets_init

# add_shortcode
hs
```

###WordPress Function Definitions Used in This Example

####Reference

[WP_Widget ()](https://developer.wordpress.org/reference/classes/wp_widget/)
[WP_Query ()](https://developer.wordpress.org/reference/classes/wp_query/)

```php
<?php
// call: register_activation_hook( __FILE__, 'halloween_store_install' );
register_activation_hook( $file, $function );
update_option( $option, $new_value );

// call: add_action( 'init', 'halloween_store_init' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
$translated_text = __( $text, $domain );
register_post_type( $post_type, $args );

// call: add_action( 'admin_menu', 'halloween_store_menu' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
add_options_page( $page_title, $menu_title, $capability, $menu_slug, 
                  $function);
echo get_option( $option, $default );
php:: bool empty ( mixed $var )
_e( $text, $domain );
checked( $checked, $current, $echo );
$fname = esc_attr( $text );

// call: add_action( 'admin_init', 'halloween_store_register_settings' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
register_setting( $option_group, $option_name, $sanitize_callback );
php:: bool empty ( mixed $var )
sanitize_text_field( $str );

// call: add_action( 'add_meta_boxes', 'halloween_store_register_meta_box' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
add_meta_box( $id, $title, $callback, $screen, $context, 
              $priority, $callback_args );
$meta_values = get_post_meta( $post_id, $key, $single );
wp_nonce_field( $action, $name, $referer, $echo );
$translated_text = __( $text, $domain );
$fname = esc_attr( $text );
selected( $selected, $current, $echo);

// call: add_action( 'save_post','halloween_store_save_meta_box' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
echo get_post_type( $post );
php:: bool isset ( mixed $var [, mixed $... ] )
php:: bool defined ( string $name )
check_admin_referer();
check_admin_referer( $action, $query_arg );
update_post_meta($post_id, $meta_key, $meta_value, $prev_value);
sanitize_text_field( $str );

// call: add_shortcode( 'hs', 'halloween_store_shortcode' );
add_shortcode( $tag , $func );
php:: int extract ( array &$array [, int $flags = EXTR_OVERWRITE 
                            [, string $prefix = NULL ]] )
shortcode_atts( $pairs , $atts, $shortcode );
echo get_option( $option, $default );
$meta_values = get_post_meta( $post_id, $key, $single );


// call: add_action( 'widgets_init', 'halloween_store_register_widgets' );
add_action( $hook, $function_to_add, $priority, $accepted_args );
// def: halloween_store_register_widgets
register_widget( $widget_class );
// def: WP_Widget::hs_widget::hs_widget
$translated_text = __( $text, $domain );
WP_Widget::WP_Widget ( string $id_base, string $name, 
            array $widget_options = array(), array $control_options = array() )
// def: WP_Widget::hs_widget::form
$args = wp_parse_args( $args, $defaults );
_e( $text, $domain );
WP_Widget::get_field_name ( string $field_name )
$fname = esc_attr( $text );
// def: WP_Widget::hs_widget::update
sanitize_text_field( $str )
absint( $maybeint );
// def: WP_Widget::hs_widget::widget
php:: int extract ( array &$array [, int $flags = EXTR_OVERWRITE 
                                  [, string $prefix = NULL ]] )
apply_filters( $tag, $value, $var ... );
php:: bool empty ( mixed $var )
esc_html( $text )
absint( $maybeint );
WP_Query::__construct ( string $query = '' )
WP_Query::query ( string $query )
WP_Query::have_posts ()
WP_Query::the_post ()
echo get_option( $option, $default );
$meta_values = get_post_meta( $post_id, $key, $single );
the_permalink();
the_title_attribute( $args );
the_title( $before, $after, $echo );
$translated_text = __( $text, $domain );
wp_reset_postdata();
```

###Database Interaction

####Initialization

```javascript
// Table: wp_options
[
  {
    "option_id": 33,
    "option_name": "active_plugins",
    "option_value": "
    a:1:{
      i:0;
      s:34:\"Chapter 8 Code/halloween-store.php\";
    }",
    "autoload": "yes"
  }, 
  {
    "option_id": 148,
    "option_name": "halloween_options",
    "option_value": "
    a:1:{
      s:13:\"currency_sign\";
      s:1:\"$\";
    }",
    "autoload": "yes"
  }
]
```

####Add a New Product under 'Products' in Admin Menu

```javascript
// Table: wp_posts
[
  {
    "ID": 4,
    "post_author": 1,
    "post_date": "2015-04-20 18:30:46",
    "post_date_gmt": "2015-04-20 18:30:46",
    "post_content": "Content of Drewfle Product 1",
    "post_title": "Drewfle Product 1",
    "post_excerpt": "",
    "post_status": "publish",
    "comment_status": "closed",
    "ping_status": "closed",
    "post_password": "",
    "post_name": "drewfle-product-1",
    "to_ping": "",
    "pinged": "",
    "post_modified": "2015-04-20 18:30:46",
    "post_modified_gmt": "2015-04-20 18:30:46",
    "post_content_filtered": "",
    "post_parent": 0,
    "guid": "http://prowp.localhost/?post_type=halloween-products&#038;p=4",
    "menu_order": 0,
    "post_type": "halloween-products",
    "post_mime_type": "",
    "comment_count": 0
  }
]

// Table: wp_postmeta
[
  {
    "meta_id": 4,
    "post_id": 4,
    "meta_key": "_halloween_product_sku",
    "meta_value": "111"
  }, 
  {
    "meta_id": 5,
    "post_id": 4,
    "meta_key": "_halloween_product_price",
    "meta_value": "222"
  }, 
  {
    "meta_id": 6,
    "post_id": 4,
    "meta_key": "_halloween_product_weight",
    "meta_value": "333"
  }, 
  {
    "meta_id": 7,
    "post_id": 4,
    "meta_key": "_halloween_product_color",
    "meta_value": "red"
  }, 
  {
    "meta_id": 8,
    "post_id": 4,
    "meta_key": "_halloween_product_inventory",
    "meta_value": "In Stock"
  }
]
```

####Enable 'Show Product Inventory'

```javascript
// Table: wp_options
[
  {
    "option_id": 148,
    "option_name": "halloween_options",
    "option_value": "
    a:2:{
      s:14:\"show_inventory\";
      s:2:\"on\";
      s:13:\"currency_sign\";
      s:1:\"$\";
    }",
    "autoload": "yes"
  }
]
```

####Standalone Test: Enable 'Products Widget'
```javascript
// Table: wp_options
[
  {
    "option_id": 147,
    "option_name": "widget_hs_widget",
    "option_value": "
    a:2:{
      i:2;
      a:2:{
        s:5:\"title\";
        s:8:\"Products\";
        s:15:\"number_products\";
        i:3;
      }
      s:12:\"_multiwidget\";
      i:1;
    }",
    "autoload": "yes"
  }
]
```