###Program Structure in Call Graph

```
add_action
`-- prowp_meta_box_init
    `-- add_meta_box
        `-- prowp_meta_box
            |-- get_post_meta
            |-- wp_nonce_field
            |   `-- plugin_basename
            |-- esc_attr
            `-- selected
add_action
`-- prowp_save_meta_box
    |-- isset
    |-- defined
    |-- check_admin_referer
    |   `-- plugin_basename
    `-- update_post_meta
        `-- sanitize_text_field
```

###WordPress Hooks Used in This Example

```
add_meta_boxes
save_post
```

###WordPress Function Definitions Used in This Example

```php
<?php
add_action( $hook, $function_to_add, $priority, $accepted_args );

add_meta_box( $id, $title, $callback, $screen, $context, 
              $priority, $callback_args );
$meta_values = get_post_meta( $post_id, $key, $single );
wp_nonce_field( $action, $name, $referer, $echo );
plugin_basename( $file );
$fname = esc_attr( $text );
selected( $selected, $current, $echo);

bool isset ( mixed $var [, mixed $... ] ) // php
bool defined ( string $name ) // php
check_admin_referer( $action, $query_arg );
plugin_basename( $file );
update_post_meta($post_id, $meta_key, $meta_value, $prev_value);
sanitize_text_field( $str );
```

###Database Interaction

####Initialization

```javascript
// Table: wp_options
[
  {
    "option_id": 33,
    "option_name": "active_plugins",
    "option_value": "
    a:4:{
      i:0;
      s:41:\"Chapter 8 Code/prowp2-custom-meta-box.php\";
      i:1;
      s:39:\"Chapter 8 Code/prowp2-custom-widget.php\";
      i:2;
      s:49:\"Chapter 8 Code/prowp2-reading-settings-plugin.php\";
      i:3;
      s:45:\"Chapter 8 Code/prowp2-settings-api-plugin.php\";
    }",
    "autoload": "yes"
  }
]
```

####Stored/Updated Data

```javascript
// Table: wp_posts
[
  {
    "ID": 6,
    "post_author": 1,
    "post_date": "2015-04-20 17:23:46",
    "post_date_gmt": "2015-04-20 17:23:46",
    "post_content": "Content of Drewfle ProWP MetaBox",
    "post_title": "Drewfle ProWP MetaBox",
    "post_excerpt": "",
    "post_status": "publish",
    "comment_status": "open",
    "ping_status": "open",
    "post_password": "",
    "post_name": "drewfle-prowp-metabox",
    "to_ping": "",
    "pinged": "",
    "post_modified": "2015-04-20 17:23:46",
    "post_modified_gmt": "2015-04-20 17:23:46",
    "post_content_filtered": "",
    "post_parent": 0,
    "guid": "http://prowp.localhost/?p=6",
    "menu_order": 0,
    "post_type": "post",
    "post_mime_type": "",
    "comment_count": 0
  }, 
  {
    "ID": 7,
    "post_author": 1,
    "post_date": "2015-04-20 17:23:46",
    "post_date_gmt": "2015-04-20 17:23:46",
    "post_content": "Content of Drewfle ProWP MetaBox",
    "post_title": "Drewfle ProWP MetaBox",
    "post_excerpt": "",
    "post_status": "inherit",
    "comment_status": "open",
    "ping_status": "open",
    "post_password": "",
    "post_name": "6-revision-v1",
    "to_ping": "",
    "pinged": "",
    "post_modified": "2015-04-20 17:23:46",
    "post_modified_gmt": "2015-04-20 17:23:46",
    "post_content_filtered": "",
    "post_parent": 6,
    "guid": "http://prowp.localhost/?p=7",
    "menu_order": 0,
    "post_type": "revision",
    "post_mime_type": "",
    "comment_count": 0
  }
]

// Table: wp_postmeta
[
  {
    "meta_id": 6,
    "post_id": 6,
    "meta_key": "_prowp_type",
    "meta_value": "special"
  }, 
  {
    "meta_id": 7,
    "post_id": 6,
    "meta_key": "_prowp_price",
    "meta_value": "999"
  }
]
```
