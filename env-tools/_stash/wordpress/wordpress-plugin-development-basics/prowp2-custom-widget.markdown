###Program Structure in Call Graph

```
add_action
`-- prowp_register_widgets
    `-- register_widget
        `--class prowp_widget: WP_Widget

class prowp_widget: WP_Widget
|-- prowp_widget
|   `-- WP_Widget::WP_Widget
|-- WP_Widget::form
|   |-- wp_parse_args
|   |-- WP_Widget::get_field_name
|   |-- esc_attr
|   `-- esc_textarea
|-- WP_Widget::update
|   `-- sanitize_text_field
`-- WP_Widget::widget
    |-- php::extract
    |-- apply_filters
    |-- php::empty
    `-- esc_html
```

###WordPress Hooks Used in This Example

```
widgets_init
```

###WordPress Function Definitions Used in This Example

```php
<?php
add_action( $hook, $function_to_add, $priority, $accepted_args );

register_widget( $widget_class );

WP_Widget::WP_Widget ( string $id_base, string $name, 
            array $widget_options = array(), array $control_options = array() );

WP_Widget::form ( array $instance );
wp_parse_args( $args, $defaults = '' );
WP_Widget::get_field_name ( string $field_name );
$fname = esc_attr( $text );
esc_textarea( $text );

WP_Widget::update ( array $new_instance, array $old_instance );
sanitize_text_field( $str );

WP_Widget::widget ( array $args, array $instance );
php::int extract ( array &$array [, int $flags = EXTR_OVERWRITE [
                                  , string $prefix = NULL ]] );
apply_filters( $tag, $value, $var ... );
php::bool empty ( mixed $var );
esc_html( $text );
```

###Database Interaction

####Initialization

```javascript
// Table: wp_options
[
  {
    "option_id": 33,
    "option_name": "active_plugins",
    "option_value": "
    a:3:{
      i:0;
      s:39:\"Chapter 8 Code/prowp2-custom-widget.php\";
      i:1;
      s:49:\"Chapter 8 Code/prowp2-reading-settings-plugin.php\";
      i:2;
      s:45:\"Chapter 8 Code/prowp2-settings-api-plugin.php\";
    }",
    "autoload": "yes"
  }
]
```

####Stored/Updated Data

```javascript
// Table: wp_options
[
  {
    "option_id": 95,
    "option_name": "sidebars_widgets",
    "option_value": "
    a:3:{
      s:19:\"wp_inactive_widgets\";
      a:0:{
      }
      s:9:\"sidebar-1\";
      a:7:{
        i:0;
        s:14:\"prowp_widget-2\";
        i:1;
        s:8:\"search-2\";
        i:2;
        s:14:\"recent-posts-2\";
        i:3;
        s:17:\"recent-comments-2\";
        i:4;
        s:10:\"archives-2\";
        i:5;
        s:12:\"categories-2\";
        i:6;
        s:6:\"meta-2\";
      }
      s:13:\"array_version\";
      i:3;
    }",
    "autoload": "yes"
  }, 
  {
    "option_id": 154,
    "option_name": "widget_prowp_widget",
    "option_value": "
    a:2:{
      i:2;
      a:3:{
        s:5:\"title\";
        s:11:\"Drewfle Bio\";
        s:4:\"name\";
        s:11:\"Drewfle Fle\";
        s:3:\"bio\";
        s:16:\"A drewfle eater.\";
      }
      s:12:\"_multiwidget\";
      i:1;
    }",
    "autoload": "yes"
  }
]
```
