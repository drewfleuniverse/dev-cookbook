###Program Structure in Call Graph

```
add_action
`-- prowp_settings_init
    |-- add_settings_section
    |   `-- prowp_setting_section
    |-- add_settings_field
    |   `-- prowp_setting_enabled
    |       |-- get_option
    |       `-- checked
    |-- add_settings_field
    |   `-- prowp_setting_name
    |       |-- get_option
    |       `-- esc_attr
    `-- register_setting
        `-- prowp_sanitize_settings
            `-- sanitize_text_field
```

###WordPress Hooks Used in This Example

```
admin_init
```

###WordPress Function Definitions Used in This Example

```php
<?php
add_action( $hook, $function_to_add, $priority, $accepted_args );
add_settings_section( $id, $title, $callback, $page );
add_settings_field( $id, $title, $callback, $page, $section, $args );
register_setting( $option_group, $option_name, $sanitize_callback );

echo get_option( $option, $default );
checked( $checked, $current, $echo );
$fname = esc_attr( $text );
```

###Database Interaction

####Initialization

```javascript
// Table: wp_options
[
  {
    "option_id": 33,
    "option_name": "active_plugins",
    "option_value": "
    a:2:{
      i:0;
      s:49:\"Chapter 8 Code/prowp2-reading-settings-plugin.php\";
      i:1;
      s:45:\"Chapter 8 Code/prowp2-settings-api-plugin.php\";
    }",
    "autoload": "yes"
  }
]
```

####Stored/Updated Data
```javascript
// Table: wp_options
[
  {
    "option_id": 153,
    "option_name": "prowp_setting_values",
    "option_value": "
    a:2:{
      s:7:\"enabled\";
      s:2:\"on\";
      s:4:\"name\";
      s:18:\"Drewfle in Reading\";
    }",
    "autoload": "yes"
  }
]
```