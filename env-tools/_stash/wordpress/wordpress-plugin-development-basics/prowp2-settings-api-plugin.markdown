###Program Structure in Call Graph

```
# call: add_action( 'admin_menu', 'prowp_create_menu' );
add_action
`-- prowp_create_menu
    |-- add_menu_page
    |   |-- prowp_settings_page
    |   |   |-- settings_fields
    |   |   |-- get_option
    |   |   |-- esc_attr
    |	  |   `-- esc_url
    |   `-- plugins_url
    `-- add_action
        `--	prowp_register_settings
            `-- register_setting
                `-- prowp_sanitize_options
                    |-- sanitize_text_field
                    |-- sanitize_email
                    `-- esc_url
```


###WordPress Hooks Used in This Example

```
admin_menu
admin_init
```


###WordPress Function Definitions Used in This Example

```php
<?php
// call: add_action( 'admin_menu', 'prowp_create_menu' );
add_action( $hook, $function_to_add, $priority, $accepted_args );

// def: prowp_create_menu()
add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function,
               $icon_url, $position );
plugins_url( $path, $plugin );
add_action( $hook, $function_to_add, $priority, $accepted_args );

// def: prowp_settings_page()
settings_fields( $option_group );
echo get_option( $option, $default );
$fname = esc_attr( $text );
esc_url( $url, $protocols, $_context );

// def: prowp_register_settings()
register_setting( $option_group, $option_name, $sanitize_callback );

// def: prowp_sanitize_options()
sanitize_text_field( $str );
sanitize_email( $email );
esc_url( $url, $protocols, $_context );
```

###Database Interaction

####Initialization

```javascript
// Table: wp_options
[
  {
    "option_id": 33,
    "option_name": "active_plugins",
    "option_value": "
    a:1:{
      i:0;
      s:45:\"Chapter 8 Code/prowp2-settings-api-plugin.php\";
    }",
    "autoload": "yes"
  }
]
```

####Stored/Updated Data

```javascript
// Table: wp_options
[
  {
    "option_id": 148,
    "option_name": "prowp_options",
    "option_value": "
    a:3:{
      s:11:\"option_name\";
      s:7:\"Drewfle\";
      s:12:\"option_email\";
      s:19:\"drewfle@drewfle.fle\";
      s:10:\"option_url\";
      s:18:\"http://drewfle.fle\";
    }",
    "autoload": "yes"
  }
]
```