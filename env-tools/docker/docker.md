# Docker

## Docker CLI

### Delete images

```sh
# Remove all dangling and unused images
docker system prune -a
```

## Docker compose

### Troubleshooting

#### Changes are not updated

Remove outdated volumes, because those volumes are not reflecting the changes made in `docker-compose.yaml`:

```sh
docker-compose rm -fv
docker-compose rm -fv <image-volume-to-remove>
```
