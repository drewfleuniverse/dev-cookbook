# R

## Prerequisite

`env-tools/nteract/r.md`

## Installation

```sh
apm install atom-language-r hydrogen ide-r rbox
```
