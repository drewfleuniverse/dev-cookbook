# VS Code

## Setup

### Mac

#### User Settings

```json
{
  "editor.minimap.enabled": false,
  "typescript.validate.enable": false,
  "javascript.validate.enable": false,
  "editor.tabSize": 2,
  "[javascript]": {
    "editor.formatOnSave": true
  },
  "window.zoomLevel": 0
}
```
