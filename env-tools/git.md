# git

## Usages

```sh
# Remove added files
git rm -r --cached .
# Output log to a csv file
git log --pretty=format:"%cd,%cn,%s" > \
  <path>/<csv_name>.csv
```

## Tools

### Rebase editor

```sh
npm install -g rebase-editor
git config --global sequence.editor rebase-editor
```

## Alias

In `~/.gitconfig`:

```conf
[alias]
  a = add
  aa = add -A
  aaca = !git add -A && git commit --amend
  ac = !git add -A && git commit -m
  aacm = !git add -A && git commit -m
  b = branch
  bd = branch -d
  bdf = branch -d -f
  bm = branch -m
  cl = clean
  cln = clean -n
  clfd = clean -fd # untracked files and directories
  clfdx = clean -fdx # untracked files, directories, and ignored files
  co = checkout
  co-d = checkout dev
  co-m = checkout master
  cob = checkout -b
  cm = commit
  cma = commit -amend
  cmm = commit -m
  f = fetch
  # gc = gc
  l = log
  lg = log --graph --pretty='%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
  m = merge
  pl = pull
  p = push
  ps = push
  pf = push --force
  psf = push --force
  rb = rebase
  rbi = rebase -i
  rbi-h10 = rebase -i HEAD~10
  rbi-h50 = rebase -i HEAD~50
  rba = rebase --abort
  rbc = rebase --continue
  rf = reflog
  rs = reset
  rsh = reset --hard
  rp-h = rev-parse HEAD # show full git hash
  st = state
  ss = stash
  ssl = stash list
  sss = stash show
  ssp = stash pop
  ssa = stash apply
  ssplssp = !git stash && !git pull && git stash pop
```
