# iTerm

| Shortcut | Action  |
| -------- | ------- |
| Cmd + T  | New tab |
