# Installation

## Using global install

```sh
pip install -r requirements.txt
```

## Using pyenv + venv

```sh
pyenv virtualenv 3.7.1 py371
pyenv activate py371
pip install --upgrade pip
pip install -r requirements.txt
```
