# Jupyter

## Installation

### Simple

```sh
pip install jupyter
```

### Pyenv + Venv

```sh
# Installation

## Create venv
pyenv virtualenv <pyhthon-version> <venv-name>
## Use venv
pyenv activate <venv-name>
## Upgrade pip
pip install --upgrade pip
## Save jupyter to requirements.txt
echo 'jupyter==1.0.0' > requirements.txt
## Install
pip install -r requirements.txt

# Exit
source deactivate
```

## Usages

```sh
# Launch notebook
jupyter notebook

# Launch without opening a browser
jupyter notebook --no-browser
```
