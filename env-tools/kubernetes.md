# Kubernetes

## K8s Concepts

### Build blocks

- Node: a physical or VM computer.
- Cluster: a poll of nodes.
- Persistent volume: contrary to the cluster, a persistent volume offers a persistent file system.

### Elements

- Ingress or load balancer: pass public traffic to a cluster or a node.
- Deployment: a deployment contains one pod or replication of pods.
- Pod: a pod consists of multiple containers.
- Container: e.g. a docker container.

### Terms

- Context: a cluster that is available for K8s to use.

## Installation

### Mac

<!-- As of June 2019, install [virtual box](https://www.virtualbox.org/wiki/Downloads). -->

### Installing minikube

[hyperkit-driver](https://github.com/kubernetes/minikube/blob/master/docs/drivers.md#hyperkit-driver)

```sh
# Test if hyperkit is already installed by docker for mac:
which hyperkit
# -> /usr/local/bin/hyperkit
ls -l /usr/local/bin/hyperkit
# -> lrwxr-xr-x 1 drewfle 67 May 31  2018 /usr/local/bin/hyperkit -> /Applications/Docker.app/Contents/Resources/bin/com.docker.hyperkit

# Only run below command when hyperkit is not installed
brew install hyperkit

# Install minikube hyperkit driver
curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-hyperkit \
&& sudo install -o root -g wheel -m 4755 docker-machine-driver-hyperkit /usr/local/bin/

# Set default to hyperkit
minikube config set vm-driver hyperkit
# or, to use hyperkit temporarily
minikube start --vm-driver hyperkit
```

### Installing kubectl

```sh
# Install k8s
brew install kubernetes-cli
# Install minikube
brew cask install minikube
```

## Minikube Usages

```sh
minikube version
minikube start # Start a single VM node
minikube dashboard # fix 503 error https://stackoverflow.com/a/52920051
```

## Kubectl Usages

```sh
kubectl version

cat ~/.kube/config # Show the config file
kubectl config view # Show config

kubectl config get-contexts # List contexts
kubectl config current-context # Show the current context; shows empty string when no cluster is running
kubectl config use-context <cluster-name> #  Set the default context

kubectl cluster-info # List kube master and services
kubectl get nodes

kubectl create <options> # Creates resources
kubectl run <options> # Creates resources
kubectl apply <options> # Modify resources

kubectl create deployment hello-world --image=hello-world # Create deployment with docker image
kubectl run hello-world --image=hello-world # Create deployment with docker image (will be deprecated so that `run` can only create more complicate resources)

kubectl get pods
kubectl get pod hello-world-<hash>
kubectl get deployments
kubectl get deployment hello-world

kubectl delete pod hello-world
kubectl delete pods --all
kubectl delete deployment hello-world # Delete deployment also deletes pod
kubectl delete deployments --all # Delete all deployments
```

## Troubleshooting

### Minikube

#### `Temporary Error: unexpected response code: 503`

```sh
minikube stop
rm -rf  ~/.minikube
minikube start
```

## References

- [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet)
