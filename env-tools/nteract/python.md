# python

## Installation

### Global

```sh
# Install for python3
python3 -m pip install ipykernel
python3 -m ipykernel install

# Install for python
python -m pip install ipykernel
python -m ipykernel install   
```

### Pyenv + Virtualenv

```sh
## Create venv
pyenv virtualenv <pyhthon-version> <venv-name>
## Use venv
pyenv activate <venv-name>
## Upgrade pip
pip install --upgrade pip
## Save ipykernel to requirements.txt
echo 'ipykernel==5.1.0' > requirements.txt
## Install
pip install -r requirements.txt
```
