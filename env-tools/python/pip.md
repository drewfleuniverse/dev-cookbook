# pip

## Usages

```sh
# get version
pip -V
# or
pip --version

# upgrade self
pip install --upgrade pip

# save all packages in current env
pip freeze > requirements.txt

# install from requirements.txt
pip install -r requirements.txt
```
