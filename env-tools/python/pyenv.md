# pyenv

[pyenv/pyenv](https://github.com/pyenv/pyenv)

## Installation

### macOS

```sh
brew update
brew install pyenv
# if macOS recently upgraded
brew reinstall pyenv
```

## Plugins

### macOS

```sh
brew install pyenv-virtualenv;
cp ~/.zshrc ~/.zshrc.bak;
echo '
# pyenv-virtualenv
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/dev/py-venv
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
source /usr/local/bin/virtualenvwrapper.sh
' >> ~/.zshrc;
source ~/.zshrc;
pip3 install virtualenvwrapper;
```

## Usages

### pyenv

```sh
# Current python version
pyenv version

# List
pyenv versions

# Install

## Prerequisites - macOS Mojave
CFLAGS="-I$(xcrun --show-sdk-path)/usr/include" \
  pyenv install -v <python-version>;

## Installation
pyenv install -v <python-version>;

# Set global python version
pyenv global <python-version>
# Set local python version
pyenv local <python-version>

# Execute
pyenv exec python <args>
pyenv exec pip <args>

# Remove a python version
rm -r ~/.pyenv/versions/<python-version>
```

### pyenv-virtualenv

```sh
# list python versions and virtual envs
pyenv versions
# list virtual envs
pyenv virtualenvs

# create virtual env
pyenv virtualenv <python-version> <name>

# use virtual env
pyenv activate <name>

# exit
source deactivate

# remove virtual env
rm -r ~/.pyenv/versions/<python-version>/env/<name>
```
