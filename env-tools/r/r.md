# R

## Installation

Download [R studio](http://cran.cnr.berkeley.edu)

### Usages

```sh
# Run in terminal
R < test.r --no-save
# Or
R < test.r --save
```

### R language server

[R language server](https://cran.r-project.org/web/packages/languageserver/index.html)


#### Prerequisite - macOS

Install [XQuartz](https://www.xquartz.org/)

#### Installation

In R from terminal:

```r
install.packages("languageserver")
source("https://install-github.me/REditorSupport/languageserver")
```
