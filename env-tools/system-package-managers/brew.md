# Homebrew

```sh
# Upgrade all packages
brew upgrade

# Uninstall
brew uninstall <pkg>
# or
brew delete <pkg>
```
