let bodypix;
let img;

function preload() {
  img = loadImage("assets/arm.jpeg");
  bodypix = ml5.bodyPix();
}

function setup() {
  createCanvas(480, 560);
  bodypix.segment(img, gotResults);
}

function gotResults(err, segmentation) {
  if (err) {
    console.log(err);
    return;
  }

  background(0);
  image(segmentation.backgroundMask, 0, 0, width, height);
}
