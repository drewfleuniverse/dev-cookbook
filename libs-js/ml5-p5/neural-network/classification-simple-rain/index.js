const options = {
  inputs: ["avg_temperature", "humidity"],
  outputs: ["rained"],
  task: "classification",
  debug: true
};
let nn;

function setup() {
  createCanvas(400, 400);
  nn = ml5.neuralNetwork(options);
  createTrainingData();
  nn.normalizeData();
  nn.train(finishedTraining);
}

function finishedTraining() {
  nn.classify(
    {
      avg_temperature: 70,
      humidity: 0.7
    },
    (err, results) => {
      console.log(results[0]);
    }
  );
}

// Helpers --------------------------------------------------------------------

function createTrainingData() {
  for (let i = 0; i < 50; i++) {
    nn.addData(
      {
        avg_temperature: getRandomRangeInt(0, 100),
        humidity: getRandomRange(0.6, 1)
      },
      { rained: "yes" }
    );
  }
  for (let i = 0; i < 50; i++) {
    nn.addData(
      {
        avg_temperature: getRandomRangeInt(0, 100),
        humidity: getRandomRange(0, 0.5)
      },
      { rained: "no" }
    );
  }
}

// Utilities ------------------------------------------------------------------

function getRandomRange(min, max) {
  return Math.random() * (max - min) + min;
}
function getRandomRangeInt(min, max) {
  return Math.floor(getRandomRange(min, max));
}
