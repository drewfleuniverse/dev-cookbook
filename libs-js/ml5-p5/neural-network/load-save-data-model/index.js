const options = {
  inputs: 1,
  outputs: 1,
  debug: true
};
const modelPaths = {
  model: "assets/model.json",
  metadata: "assets/model_meta.json",
  weights: "assets/model.weights.bin"
};
let nn;
let isDataLoaded = false;
let isModelLoaded = false;
function setup() {
  nn = ml5.neuralNetwork(options);
  nn.loadData("assets/data.json", () => {
    isDataLoaded = true;
  });
  nn.load(modelPaths, () => {
    isModelLoaded = true;
  });
  console.log(nn);
  // Saves data from browser as a json file. Sample data:
  //   {"data":[{"xs":{"input0":0},"ys":{"output0":0}}]}
  // nn.saveData();
  const timer = setInterval(() => {
    if (isDataLoaded && isModelLoaded) {
      nn.data.normalize();
      // Saves data as three files:
      //   model.json, model.weights.bin, model_meta.json
      // nn.train(finishedTraining);
      // nn.save();
      finishedTraining();
      clearInterval(timer);
    }
  }, 1000);
}

function finishedTraining() {
  nn.predict([50], (err, results) => {
    console.log(results);
  });
}
