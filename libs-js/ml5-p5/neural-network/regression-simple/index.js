const options = {
  inputs: 1,
  outputs: 1,
  debug: true
};
let nn;

function setup() {
  nn = ml5.neuralNetwork(options);
  createTrainingData();
  console.log(nn);
  nn.data.normalize();
  nn.train(finishedTraining);
}

function finishedTraining() {
  nn.predict([50], (err, results) => {
    console.log(results);
  });
}

function createTrainingData() {
  for (let i = 0; i < 100; i++) {
    const x = i;
    const y = i * 2;
    nn.data.addData([x], [y]);
  }
}
