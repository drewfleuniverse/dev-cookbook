// https://github.com/tensorflow/tfjs-models/tree/master/body-pix

const img = document.getElementById("arm");

async function loadAndPredict() {
  const net = await bodyPix.load(/** optional arguments, see below **/);

  /**
   * One of (see documentation below):
   *   - net.segmentPerson
   *   - net.segmentPersonParts
   *   - net.segmentMultiPerson
   *   - net.segmentMultiPersonParts
   * See documentation below for details on each method.
   */
  const segmentation = await net.segmentPerson(img);
  console.log(segmentation);
}
loadAndPredict();
