import { calcMean } from "./calc-mean";

export const calcMeanAbsoluteDeviation = (values: number[]) => {
  const mean = calcMean(values);
  const sumOfAbsDeviation = values.reduce((a, c) => a + Math.abs(c - mean), 0);
  return sumOfAbsDeviation / mean;
};
