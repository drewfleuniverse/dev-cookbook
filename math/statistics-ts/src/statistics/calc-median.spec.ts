import { calcMedian } from "./calc-median";

describe.only("calcMedian", () => {
  test("should return correct value when values has odd length", () => {
    expect(calcMedian([1, 2, 3])).toBe(2);
  });
  test("should return correct value when values has even length", () => {
    expect(calcMedian([1, 2, 3, 4])).toBe(2.5);
  });
});
