import { checkIsArrayEmpty, checkIsNumbersSafe } from "../utils";

export const calcMedian = (values: number[]) => {
  const { length } = values;
  const mid = Math.floor(length / 2);
  values.sort();

  return length % 2 === 0 ? (values[mid - 1] + values[mid]) / 2 : values[mid];
};

export default calcMedian;
