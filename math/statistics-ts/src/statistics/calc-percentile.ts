import { sort } from "../utils";

export const calcPercentileNearest = (values: number[], nth: number) => {
  if (!Number.isInteger(nth)) {
    throw new Error(`nth value should be integer; received ${nth}`);
  }
  if (nth <= 0 || nth > 100) {
    throw new Error(
      `nth value should be larger than 0 or smaller than or equal to 100; received ${nth}`
    );
  }

  const { length } = values;
  sort(values);
  const num = (nth / 100) * length;
  const int = Math.ceil(num);

  return values[int - 1];
};
