import { calcVariance } from "./calc-calcVariance";

export const calcStandardDeviation = (values: number[]) => {
  return Math.sqrt(calcVariance(values));
};
