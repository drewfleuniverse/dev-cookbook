import { calcWeightedMean } from "./calc-weighted-mean";

let values: number[];

describe("calcWeightedMean", () => {
  beforeAll(() => {
    values = [1, 2, 3];
  });
  describe("when values and weights are provided", () => {
    test("should return correct value when values and weights have same length", () => {
      expect(calcWeightedMean(values, [1, 2, 3])).toBeCloseTo(4.67);
    });
    test("should return correct value when weights has less length", () => {
      expect(calcWeightedMean(values, [1, 2])).toBeCloseTo(2.67);
    });
    test("should return correct value when values has less length", () => {
      expect(calcWeightedMean(values, [1, 2, 3, 4])).toBeCloseTo(4.67);
    });
  });
  test("should return unweighted mean when weights is empty", () => {
    expect(calcWeightedMean(values, [])).toBe(2);
  });
});
