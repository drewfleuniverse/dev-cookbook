import { checkIsArrayEmpty, checkIsNumberSafe } from "../utils";

export const calcWeightedMean = (
  values: number[],
  weights: Array<number | undefined>
) => {
  const { length } = values;
  let sum = 0;
  for (let i = 0; i < length; i++) {
    const weight = weights[i];
    sum += weight !== undefined ? values[i] * weight : values[i];
  }

  return sum / length;
};

export default calcWeightedMean;
