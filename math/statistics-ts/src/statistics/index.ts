export { default as calcMean } from "./calc-mean";
export { default as calcWeightedMean } from "./calc-weighted-mean";
