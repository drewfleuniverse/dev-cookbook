export const checkIsArrayEmpty = (arr: any[]) => {
  if (arr.length === 0) {
    throw new Error("Array has length 0.");
  }
};

export default checkIsArrayEmpty;
