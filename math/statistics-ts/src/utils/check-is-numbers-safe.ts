import { getUnsafeNumberIndex } from "./get-unsafe-number-index";

export const checkIsNumbersSafe = (values: number[]) => {
  const unsafe = getUnsafeNumberIndex(values);
  if (unsafe !== -1) {
    throw new Error(`Values array contains unsafe number at index ${unsafe}`);
  }
};

export default checkIsNumbersSafe;
