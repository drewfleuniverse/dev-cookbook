import { getDecimal } from "./get-decemal";

describe("getDecimal", () => {
  test("should return the first decimal digit", () => {
    expect(getDecimal(1.23)).toBe(0.2);
  });
  test("should return the decimal digits with length specified by param digits", () => {
    expect(getDecimal(1.23, 1)).toBe(0.2);
    expect(getDecimal(1.23, 2)).toBe(0.23);
    expect(getDecimal(1.23, 3)).toBe(0.23);
  });
  test("should round up", () => {
    expect(getDecimal(1.25)).toBe(0.3);
  });
});
