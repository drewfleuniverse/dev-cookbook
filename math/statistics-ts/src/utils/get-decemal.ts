export const getDecimal = (num: number, digits: number = 1) => {
  return Number.parseFloat((num % 1).toFixed(digits));
};

export default getDecimal;
