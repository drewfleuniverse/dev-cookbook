const { MAX_SAFE_INTEGER, MIN_SAFE_INTEGER } = Number;

export const isSafeNumber = (num: number) => {
  if (num >= MIN_SAFE_INTEGER && num <= MAX_SAFE_INTEGER) {
    return true;
  }
  return false;
};

export default isSafeNumber;