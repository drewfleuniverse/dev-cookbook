export const sort = (values: number[], order = "asc") => {
  let fn = (a: number, b: number) => a - b;
  if (order === "desc") {
    fn = (a: number, b: number) => b - a;
  }
  return values.sort(fn);
};

export default sort;
