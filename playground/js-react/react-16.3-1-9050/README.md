## Links

- [facebook/create-react-app](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md)
- [React](https://reactjs.org/)
- [Jest](https://facebook.github.io/jest/)
- [Flow](https://flow.org/en/docs/)
- [Flow Cheat Sheet](https://www.saltycrane.com/flow-type-cheat-sheet/latest/)
