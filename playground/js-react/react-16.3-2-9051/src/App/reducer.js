// @flow
import type { Action } from './actions';


type State = {
  +formData: {
    +username: string,
    +password: string,
  },
  +formErrors: {
    +username: string,
    +password: string,
  },
  +isFormValid: boolean,
  +errorResponse: string,
}

const initialState: State = {
  formData: {
    username: '',
    password: '',
  },
  formErrors: {
    username: '',
    password: '',
  },
  isFormValid: false,
  errorResponse: '',
};


const reducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case 'UPDATE_FORM_DATA':
      return {
        ...state,
        formData: {
          ...state.formData,
          ...action.payload,
        },
      };
    case 'UPDATE_FORM_ERRORS':
      return {
        ...state,
        formErrors: {
          ...state.formErrors,
          ...action.payload,
        },
      };
    case 'SET_IS_FORM_VALID':
      return {
        ...state,
        ...action.payload,
      };
    case 'SET_ERROR_RESPONSE':
      return {
        ...state,
        ...action.payload,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};


export default reducer;
