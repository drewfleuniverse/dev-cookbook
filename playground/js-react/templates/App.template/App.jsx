// @flow
import * as React from 'react';
import './App.css';


const App = (): React.Element<'div'> => (
  <div className="App" />
);


export default App;
