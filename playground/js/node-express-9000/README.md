
## Development

**Lunch Docker MySQL**

```sh
docker run -p 3306:3306 --name docker-mysql -e MYSQL_ROOT_PASSWORD=password -d mysql:8
```

**Access**

```sh
# In terminal
docker exec -it docker-mysql bash
# In docker container
mysql -uroot -ppassword
# In mysql
CREATE USER 'app'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON * . * TO 'app'@'%';
```

**Create database**

```sh
sequelize db:drop app_dev
sequelize db:create app_dev
```

**Notes on model**
<!-- link.belongsTo(models.user); -->
```sh
sequelize db:drop app_dev
sequelize db:create app_dev
# User
sequelize model:generate --name user \
--underscored true --attributes \
first_name:string,\
last_name:string,\
username:string,\
city:string,\
state:string,\
country:string,\
company:string,\
occupation:string,\
sections:json;
# Field
sequelize model:generate --name field \
--underscored true --attributes \
field_name:string;
# UserField
sequelize model:generate --name user_field \
--underscored true --attributes \
user_id:string,\
field_name:string;
# Link
sequelize model:generate --name link \
--underscored true --attributes \
url:string,\
title:string;
# UserLink
sequelize model:generate --name user_link \
--underscored true --attributes \
user_id:string,\
field_name:string;
# Project
sequelize model:generate --name project \
--underscored true --attributes \
name:string,\
field_name:string,\
published_on:date;
```
