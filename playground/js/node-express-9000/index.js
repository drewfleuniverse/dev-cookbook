const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser'); // eslint-disable-line
const logger = require('morgan');
const fs = require('fs');


/*
Set up
 */

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(logger('dev'));



/*
Helpers
 */

const promisifyReadJSON = path => new Promise((resolve, reject) => {
  try {
    const rawData = fs.readFileSync(path);
    const json = JSON.parse(rawData);
    resolve(json)
  } catch (err) {
    reject(err);
  }
});


/*
Serve images
 */

app.use(express.static('images'));


/*
User router
 */

const usersRouter = express.Router();
usersRouter.get('/',
async (req, res, next) => {
  try {
    const users = await promisifyReadJSON('json/users.json');
    return res.json(users);
  } catch (err) {
    return next(err);
  }
});
app.use('/users', usersRouter);


/*
Error handling
 */

app.use((error, req, res, next) => { // eslint-disable-line
  res.status(error.status || 500);
  res.send(error.message);
});


/*
Finish up
 */

app.get('/', (req, res) => res.json({
  message: 'It works'
}));
app.listen('9000');
