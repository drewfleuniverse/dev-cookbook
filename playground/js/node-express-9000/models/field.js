'use strict';
module.exports = (sequelize, DataTypes) => {
  var field = sequelize.define('field', {
    field_name: DataTypes.STRING
  }, {
    underscored: true,
  });
  field.associate = function(models) {
    // associations can be defined here
  };
  return field;
};