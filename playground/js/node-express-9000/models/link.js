'use strict';
module.exports = (sequelize, DataTypes) => {
  var link = sequelize.define('link', {
    url: DataTypes.STRING,
    title: DataTypes.STRING
  }, {
    underscored: true,
  });
  link.associate = function(models) {
    // associations can be defined here
    link.belongsTo(models.user);
  };
  return link;
};
