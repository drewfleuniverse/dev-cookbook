'use strict';
module.exports = (sequelize, DataTypes) => {
  var project = sequelize.define('project', {
    name: DataTypes.STRING,
    field_name: DataTypes.STRING,
    published_on: DataTypes.DATE
  }, {
    underscored: true,
  });
  project.associate = function(models) {
    // associations can be defined here
  };
  return project;
};