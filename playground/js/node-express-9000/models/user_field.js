'use strict';
module.exports = (sequelize, DataTypes) => {
  var user_field = sequelize.define('user_field', {
    user_id: DataTypes.STRING,
    field_name: DataTypes.STRING
  }, {
    underscored: true,
  });
  user_field.associate = function(models) {
    // associations can be defined here
  };
  return user_field;
};