'use strict';
module.exports = (sequelize, DataTypes) => {
  var user_link = sequelize.define('user_link', {
    user_id: DataTypes.STRING,
    field_name: DataTypes.STRING
  }, {
    underscored: true,
  });
  user_link.associate = function(models) {
    // associations can be defined here
  };
  return user_link;
};