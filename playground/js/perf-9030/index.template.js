const suite = new Benchmark.Suite;


/*
Chrome:
 */


const prep = {
  'setup': () => {
  },
  'teardown': () => {
  }
}

suite
.add('test1', () => {
  // do something
}, prep)
.on('cycle', event => {
  console.log(String(event.target));
})
.on('complete', function () {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run({ 'async': true });
