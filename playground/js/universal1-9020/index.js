const cl = s => console.log(s);


const power = function (base, exp) {
 if (exp === 0) {
   return 1;
 }
 if (exp === 1) {
   return base;
 }

 let half = Math.floor(exp/2);

 if (exp%2 === 0) {
   return power(base, half) * power(base, half);
 } else {
   return power(base, half+1) * power(base, half);
 }
}


cl(power(2, 0) === Math.pow(2, 0))
cl(power(2, 1) === Math.pow(2, 1))
cl(power(2, 10) === Math.pow(2, 10))
cl(power(2, 19) === Math.pow(2, 19))
cl(power(2.1, 19))
cl(Math.pow(2.1, 19))
cl(power(2.1, -2))
cl(Math.pow(2.1, -2)) // failed



/*
Utils
 */

function describe(s,f) {console.log(`# ${s} #`);f();};
function it(s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert(c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
