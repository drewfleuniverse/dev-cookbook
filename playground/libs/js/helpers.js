const isBrowser = new Function('return typeof window === "object"')
const isNode = new Function('return typeof global === "object"')


const cl = i => console.log(i)
const js = i => JSON.stringify(i)
const jsf = i => JSON.stringify(i, null, '  ')
const jsa = o => JSON.stringify(Array.from(o))

const ogpo = o => Object.getPrototypeOf(o)
const ok = i => Object.keys(i)
const ogopd = (o, p) => Object.getOwnPropertyDescriptor(o, p)
const ogopds = o => Object.getOwnPropertyDescriptors(o)
const ogopns = o => Object.getOwnPropertyNames(o)


const hop = (o, p) => Object.prototype
  .hasOwnProperty.call(o, p)
const ipo = (o, p) => Object.prototype
  .isPrototypeOf.call(o, p)
const ts = o => Object.prototype
  .toString.call(o)
const pie = (o, p) => Object.prototype
  .propertyIsEnumerable.call(o, p)

const prs = x => Promise.resolve(x)
const prj = x => Promise.reject(new Error(x))
const pcond = (x, success=true) => (
  new Promise((resolve, reject) => {
    window.setTimeout(() => {
      success
        ? resolve(x)
        : reject(new Error('wrong'))
    }, 1)
  })
)


const swap = (a, m, n) => {
  const t = a[m]
  a[m] = a[n]
  a[n] = t
}

const describe = function (str, func) {
  console.log('%c' + str, 'font-weight: bold;');
  func();
};
const it = function (str, func) {
  console.log('%c' + str, 'color:grey;');
  func();
};
const assert = function (cond, log=false, msg='') {
  log && console.log(`Assertion log: ${cond}`);
  if (!cond) {
    throw new Error(msg);
  }
};


function describe (s,f) {console.log(`# ${s} #`);f();};
function it (s,f,r=true) {console.log(`${r?'':'skip -> '}${s}`);r&&f();};
function assert (c,l=false,m='') {l&&console.log(`> ${c}`);if(!c){throw new Error(m);}};
function fakeFetch(url, config, success=true) {
  return new Promise((resolve, reject) => (
    success
      ? resolve(config.body)
      : reject(new Error('Duh'))
  ));
};

var js = JSON.stringify;
