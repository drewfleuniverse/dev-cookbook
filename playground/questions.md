[](https://www.glassdoor.com/Interview/sorting-algorithm-interview-questions-SRCH_KT0,17.htm)

1. Find common elements between two arrays of integers.
2. Find cycles in a graph.
3. Efficiently find duplicate elements in an array of numbers with bounded entries (for example, elements are between 0 and 99).
4. Reverse word sequence in a string inplace.
5. Efficiently find all Pythogorean triplets in a given array of integers.
6. Find all anagrams in a list of words.
7. Set operations.

If you have a file containing millions of integers, how would you sort the data in the file using extremely limited resources, such a s 1GB of memory?
[](https://www.toptal.com/algorithms/interview-questions)
