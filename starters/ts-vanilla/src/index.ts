const n: number = 42;
const f = () => n;

export default f;
